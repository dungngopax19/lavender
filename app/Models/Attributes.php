<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class Attributes extends Base
{
    protected $table = 'attributes';

    protected $fillable = ["parent_id", "name"];

    public function childrens(){
        return $this->hasMany('App\Models\Attributes', 'parent_id', 'id');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Attributes', 'parent_id');
    }

    public function attributes_products(){
        return $this->hasMany('App\Models\AttributesProducts', 'attributes_id', 'id');
    }

    public function setNameAttribute($value){
        $this->attributes['name'] = $this->cleanData($value);
    }

}
