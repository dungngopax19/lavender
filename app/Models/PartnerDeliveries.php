<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\PartnerDeliveriesSearch;

class PartnerDeliveries extends Base
{
    use partnerDeliveriesSearch;

    protected $table = 'partner_delivery';

    protected $fillable = ["type", "code","name", "phone", "province_id", "address", "email", "note", "activity_status"];

}
