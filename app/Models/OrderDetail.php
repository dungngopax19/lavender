<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Base
{
    protected $table = 'order_detail';

    protected $fillable = ["orders_id", "products_id", "price", "qty", "discount", "discount_unit", "activity_status"];

    public function product(){
        return $this->HasOne('App\Models\Products', 'id', 'products_id');
    }

    public function order(){
        return $this->belongsTo('App\Models\Orders', 'orders_id');
    }

    public static function getProducts($startDate, $endDate) {
        $products = self::whereBetween('created_at', [$startDate, $endDate])
			->selectRaw('products_id')
			->groupBy('products_id')
            ->get();
        return $products;
    }

    public static function getOrderDate($startDate, $endDate) {
        $dates = self::whereBetween('created_at', [$startDate, $endDate])
			->selectRaw('DATE_FORMAT(created_at, "%Y-%m-%d") AS order_date')
			->groupBy('order_date')
            ->get();
        return $dates;
    }


    //Top san pham ban chay theo so luong / doanh thu
    public static function getOrderDetailReport01($startDate, $endDate, $reportType, $items_per_page=20, &$return=array()) { 
        $data = [
            'type' => intval($reportType)
        ];

        $query = self::join('orders', function ($join) use ($startDate, $endDate) {
            $join->on('orders.id', '=', 'order_detail.orders_id')
                ->whereBetween('orders.created_at', [date('Y-m-d 00:00:00', strtotime($startDate)), date('Y-m-d 23:59:59', strtotime($endDate))])
                ->whereNull('orders.deleted_at');
        });
        
        if ($reportType == Orders::REPORT_TYPE_1) {
            $query->where('order_detail.qty', '>', 0);
            $query->selectRaw('sum(order_detail.qty) AS total_order, order_detail.products_id');
            $data['label'] = 'Số lượng';
        } else {
            $query->where('order_detail.price', '>', 0);
            $query->selectRaw('sum(order_detail.price) AS total_order, order_detail.products_id');
            $data['label'] = 'Doanh thu';
        }
        
        $query->groupBy('order_detail.products_id');
        $query->orderBy('total_order', 'DESC')->orderBy('products_id', 'DESC');

        $orders = $query->paginate($items_per_page); 
        
        $return = $orders;
        if (count($orders)) {
            $total = 0;
            $tempName = array();
            $tempTotal = array();

            foreach ($orders as $item) {
                $tempName[] = $item->product['code'];
                $tempTotal[] = intval($item->total_order);
            }

            $data['name'] = $tempName;
            $data['value'] = $tempTotal;
            $data['total'] = max($orders[0]->total_order, 100);
        } else {
            $data['name'] = array();
            $data['value'] = array();
            $data['total'] = 100;
        } 

        return $data;
    }
 

}
