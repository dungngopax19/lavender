<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ReceiptsSearch;

class Receipts extends Base
{
    use ReceiptsSearch;
    
    protected $table = 'receipts';

    protected $fillable = ["code", "orders_id", "users_id", "customers_id", "price", "payment_method", "create_user", "update_user"];

    const PAYMENT_METHOD = [ 
        1 => 'Tiền mặt',
        2 => 'Thẻ',
        3 => 'Chuyển khoản'
    ];

    public function customer(){
        return $this->belongsTo('App\Models\Customers', 'customers_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'users_id', 'id');
    }

    public function order(){
        return $this->belongsTo('App\Models\Orders', 'orders_id', 'id');
    }

    public function setPriceAttribute($value){
        $this->attributes['price'] = intval(preg_replace("/[^0-9]/", "", trim($value) ));
    }				
}
