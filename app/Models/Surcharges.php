<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\SurchargesSearch;

class Surcharges extends Base
{
    use SurchargesSearch;

    protected $table = 'surcharges';

    protected $fillable = ["code", "type", "value_type", "value", "display_order", "is_put_bill", "is_refund", "activity_status"];

    public function branches_surcharges(){
        return $this->hasMany('App\Models\BranchesSurcharges', 'surcharges_id', 'id');
    }

}
