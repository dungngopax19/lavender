<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class Memberships extends Base
{
    protected $table = 'membership';

    protected $fillable = ["code", "name", "activity_status"];

}
