<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class ProductsComponents extends Base
{   
    protected $table = 'products_components';
    protected $fillable = ["products_id_parent", "products_id", "qty"];

    public function products(){
        return $this->belongsTo('App\Models\Products', 'products_id', 'id');
    }
}