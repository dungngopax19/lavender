<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class Provinces extends Base
{
    protected $table = 'provinces';

    protected $fillable = ["parent_id", "name"];

    public function childrens(){
        return $this->hasMany('App\Models\Provinces', 'parent_id', 'id');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Provinces', 'parent_id');
    }
}
