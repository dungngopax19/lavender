<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\Common;
use App\Helpers\DateHelper;
use App\Models\Traits\TmpClothSearch;

class TmpCloth extends Base
{
    use TmpClothSearch;

    protected $table = 'tmp_cloths';

    protected $fillable = ["ma_sp", "kho_vai","hinh_anh","sl_met_vai","vo_chan","mem_chan","vo_goi_nam", "vo_goi_om", "drap","custom","ngay_cap_nhat", "create_user", "update_user"];
    
    public function resizeImage($filename, $w = 100)
    {
        return Common::resizeImage($filename, $w);
    }
}
