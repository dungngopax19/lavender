<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\CustomerSearch;
use Illuminate\Support\Facades\Storage;
use App\Helpers\DateHelper;

class Customers extends Base
{
    use CustomerSearch;

    const roles = [ 'Cá nhân'=> 1, 'Công ty'=>2];
    const roles_2 = [ '1'=> 'Cá nhân', '2'=> 'Công ty'];
    const genders = [ 'Nam'=> 1, 'Nữ'=>2];
    const genders_2 = ['1'=> 'Nam', '2'=> 'Nữ'];

    protected $table = 'customers';

    protected $fillable = [ "role", "membership_id", "total_price", "buys", "code", "name", "company", "tax_code", "phone", "gender", "birthday", "email", "province_id", "address","customers_types_id","text", 'image', "activity_status"];

    public function customers_types(){
        return $this->belongsToMany('App\Models\CustomerTypes', 'customers_types_id', 'id');
    }

    public function province(){
        return $this->belongsTo('App\Models\Provinces', 'province_id', 'id');
    }

    public function orders(){
        return $this->hasMany('App\Models\Orders', 'customers_id', 'id');
    }

    public function getThumbnailDisplayAttribute()
    {
        $parsed = parse_url($this->thumbnail);
        if (empty($parsed['scheme'])) {
            return $this->thumbnail ? Storage::url($this->thumbnail) : 'images/noimage.jpg';
        }
        return $this->thumbnail;
    }

    public function getBirthdayDisplayAttribute(){
        return DateHelper::formatDate($this->birthday, 'd-m-Y');
    }

    public function getRoleDisplayAttribute(){
        $role_name = (array_key_exists($this->role, self::roles_2)) ? self::roles_2["$this->role"] : null;
        return $role_name;
    }

    public function getGenderDisplayAttribute(){
        $gender_name = (array_key_exists($this->gender, self::genders_2)) ? self::genders_2["$this->gender"] : null;
        return $gender_name;
    }

}
