<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class BranchesSurcharges extends Base
{
    protected $table = 'branches_surcharges';
    protected $fillable = ["branches_id", "surcharges_id"];
}
