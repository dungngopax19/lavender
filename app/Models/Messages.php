<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
// use App\Models\Traits\MessagesSearch;

class Messages extends Base
{
    // use MessagesSearch;

    protected $table = 'messages';

    protected $fillable = ["user_send_id", "user_receive_id", "content", "status"];

    const UNREAD = 1;
    const READ = 2;
    const IS_READ = [
        1 => 'Chưa xem',
        2 => 'Đã xem'
    ];

    public function userSend(){
        return $this->belongsTo('App\User', 'user_send_id', 'id');
    }

    public function userReceipt(){
        return $this->belongsTo('App\User', 'user_receipt_id', 'id');
    }

    public function saveMessage ($user_send_id, $user_receive_id, $content){

        $data = [
            'user_send_id'=> $user_send_id,
            'user_receive_id'=> $user_receive_id,
            'content'=> $content,
        ];

        $message =  new Messages();
        $message->fill($data);
        $message->save();
    }

}
