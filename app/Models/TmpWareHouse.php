<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\Common;
use App\Helpers\DateHelper;
use App\Models\Traits\TmpWareHouseSearch;

class TmpWareHouse extends Base
{
    use TmpWareHouseSearch;

    protected $table = 'tmp_ware_houses';

    protected $fillable = ["code", "chi_nhanh","ma_hoa_don","phi_giao_hang_tra_dt","thoi_gian","thoi_gian_tao","ma_dat_hang","ma_khach_hang","ten_khach_hang","so_dien_thoai","dia_chi_khach_hang","khu_vuc_khach_hang","phuongxa_khach_hang","nguoi_ban","kenh_ban","nguoi_tao","doi_tac_giao_hang","nguoi_nhan","dien_thoai","dia_chi_nguoi_nhan","khu_vuc_nguoi_nhan","phuongxa_nguoi_nhan","dich_vu","trong_luong","dai","rong","cao","ghi_chu","tong_tien_hang","giam_gia_hoa_don","thu_khac","khach_can_tra","khach_da_tra","tien_mat","the","chuyen_khoan","diem","voucher","ma_voucher","thoi_gian_giao_hang","trang_thai","trang_thai_giao_hang", "tt_giao_hang","ma_hang","ten_hang","dvt","ghi_chu_hang_hoa","so_luong","don_gia","giam_gia","gia_ban","thanh_tien", "create_user", "update_user"];
    
    public function getThoiGianTaoAttribute($value)
    {
        return DateHelper::printDate($value, 'd-m-Y');
    }

    public function getThoiGianGiaoHangAttribute($value)
    {
        return DateHelper::printDate($value);
    }

    public function setThoiGianAttribute($value){
        $this->attributes['thoi_gian'] = Common::convertDateExcelToDateDb($value);
    }

    public function setThoiGianTaoAttribute($value){
        $this->attributes['thoi_gian_tao'] = Common::convertDateExcelToDateDb($value);
    }

    public function setThoiGianGiaoHangAttribute($value){
        $this->attributes['thoi_gian_giao_hang'] = Common::convertDateExcelToDateDb($value);
    }
}
