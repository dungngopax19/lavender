<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CustomerTypes extends Base
{
    protected $table = 'customers_types';

    protected $fillable = ["name","activity_status"];

}
