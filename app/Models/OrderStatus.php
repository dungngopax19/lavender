<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Base
{
    protected $table = 'orders_status';

    protected $fillable = ["name", "type"];

    public static function getIdByName($name, $type = 'invoices'){
        $order_status = OrderStatus::where('name', $name)->where('type', $type)->first();
        if($order_status){
            return $order_status->id;
        }
        return null;
    }

}
