<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\MembershipGiftSearch;
use App\Helpers\DateHelper;

class MembershipGifts extends Base
{
    use MembershipGiftSearch;

    protected $table = 'membership_gift';

    protected $fillable = ["membership_id", "type", "value", "unit", "activity_status", "start_date", "end_date", "is_expires"];

    public function membership(){
        return $this->belongsTo('App\Models\Memberships', 'membership_id', 'id');
    }

    public function product(){
        return $this->hasOne('App\Models\Products', 'id', 'value');
    }

    public function setStartDateAttribute($value){
        $this->attributes['start_date'] = \Carbon\Carbon::parse($value);
    }

    public function setEndDateAttribute($value){
        $this->attributes['end_date'] = \Carbon\Carbon::parse($value);
    }

    public function setValueAttribute($value){
        $this->attributes['value'] = intval(preg_replace("/[^0-9]/", "", trim($value) ));
    }

}
