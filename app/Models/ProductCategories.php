<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ProductCategoriesSearch;

class ProductCategories extends Base
{
    use ProductCategoriesSearch;
    
    protected $table = 'product_categories';

    protected $fillable = ["name", "parent_id", "activity_status", "create_user", "update_user"];

    public function childrens(){
        return $this->hasMany('App\Models\ProductCategories', 'parent_id', 'id');
    }

    public function parent(){
        return $this->belongsTo('App\Models\ProductCategories', 'parent_id');
    }

    public function products(){
        return $this->hasMany('App\Models\Products', 'product_categories_id', 'id');
    }
}
