<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\DateHelper;

class ShipmentDetails extends Base
{
    protected $table = 'shipment_details';

    protected $fillable = ["orders_id", "name", "phone", "provinces_id", "address", "partner_delivery_id", "type_services_id", "delivery_charges", "lading_code", "delivery_time"];

    public function partnerDelivery(){
        return $this->belongsTo('App\Models\PartnerDeliveries', 'partner_delivery_id', 'id');
    }

    public function shipmentStatus(){
        return $this->hasOne('App\Models\ShipmentStatus', 'id', 'shipment_status_id');
    }

    public function getDeliveryTimeDisplayAttribute(){
        return DateHelper::formatDate($this->delivery_time, 'd/m/Y');
    }

    public function setDeliveryTimeAttribute($value){
        if(!empty($value)){
            return $this->attributes['delivery_time'] = \Carbon\Carbon::parse($value);
        }

        return $this->attributes['delivery_time'] = null;
    }
}
