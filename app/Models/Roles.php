<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\RolesSearch;

class Roles extends Base
{
    use RolesSearch;
    
    protected $table = 'roles';

    protected $fillable = ["name", "activity_status", "create_user", "update_user"];
}
