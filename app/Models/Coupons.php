<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\CouponsSearch;
use App\Helpers\DateHelper;

class Coupons extends Base
{
    use CouponsSearch;

    protected $table = 'coupons';

    protected $fillable = ["membership_ids", "name", "price", "from_date", "to_date"];

    protected $dates = ['from_date', 'to_date'];

    public function setStartDateAttribute($value){
        $this->attributes['start_date'] = \Carbon\Carbon::parse($value);
    }

    public function setEndDateAttribute($value){
        $this->attributes['end_date'] = \Carbon\Carbon::parse($value);
    }
}
