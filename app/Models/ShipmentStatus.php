<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class ShipmentStatus extends Base
{
    protected $table = 'shipment_status';

    protected $fillable = ["name", "activity_status"];

    public static function getIdByName($name){
        $shipment_status = ShipmentStatus::where('name', $name)->first();
        if($shipment_status){
            return $shipment_status->id;
        }
        return null;
    }

}
