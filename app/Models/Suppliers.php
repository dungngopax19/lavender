<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\SuppliersSearch;

class Suppliers extends Base
{
    use SuppliersSearch;
    
    protected $table = 'suppliers';

    protected $fillable = ["name", "code", "phone", "province_id", "address", "email", "company", "tax_code", "note", "activity_status"];

}
