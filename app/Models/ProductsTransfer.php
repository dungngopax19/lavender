<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ProductsTransferSearch;

class ProductsTransfer extends Base
{
    use ProductsTransferSearch;
    
    protected $table = 'products_transfer';

    protected $fillable = ["products_id", "qty", "branch_from", "branch_to", "is_confirmed", "date_confirmed", "uid_confirmed", "create_user", "update_user"];

    public function product(){
        return $this->belongsTo('App\Models\Products', 'products_id');
    }

    public function branchFrom(){
        return $this->belongsTo('App\Models\Branches', 'branch_from');
    }

    public function branchTo(){
        return $this->belongsTo('App\Models\Branches', 'branch_to');
    }

    public function userConfirmed(){
        return $this->belongsTo('App\User', 'uid_confirmed');
    }

}