<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\User;
use App\Helpers\DateHelper;

trait UsersSearch{
    public function scopeSearch($query, Request $request)
    {
        if(strlen($request->id) > 0){
            $query = $query->where('id', $request->id);
        }

        if(strlen($request->name) > 0){
            $name = $request->name;
            $query = $query->where(function($query) use ($name){
                $query->where('name', 'like', '%'.$name.'%')
                    ->orWhere('username', 'like', '%'.$name.'%');
            });
        }

        if(strlen($request->activity_status) > 0 && $request->activity_status > 0){
            $query = $query->where('status', $request->activity_status);
        }

        if(!empty($request->role_id)){
            $roles_id = $request->role_id;

            $query = $query->whereHas('modelHasRoles', function ($query) use ($roles_id) {
                $query->where('model_type', 'App\User')->whereIn('role_id', $roles_id);
            });
        }

        $query->orderBy('created_at', 'desc');

        return $query;
    }
}
