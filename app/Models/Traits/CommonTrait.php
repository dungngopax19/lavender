<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Helpers\Common;
use App\Helpers\Permissions;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

trait CommonTrait{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdUser()
    {
        return $this->belongsTo('App\User', 'create_user', 'email');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedUser()
    {
        return $this->belongsTo('App\User', 'update_user', 'email');
    }

    public function getCreatedAtDisplayAttribute(){
        $date = $this->created_at;
        return empty($date) ? '' : Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($date){
        return empty($date) ? '' : Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i:s');
    }

    public function getCreatedAtHidmyAttribute(){
        return date('H:i d-m-Y', strtotime($this->created_at));
    }

    public function getCreatedAtDmyAttribute(){
        return date('d/m/Y', strtotime($this->created_at));
    }

    public function getIdDisplayAttribute() {
        return str_pad($this->id, 6, "0", STR_PAD_LEFT);
    }

    public function cleanData($txt){
        return Common::cleanData($txt);
    }

    public function resizeImage($urlImage, $w = 300){
        return Common::resizeImage($urlImage, $w);
    }

    public function hasAllPermission($permissions, $permission){
        return Permissions::hasAllPermission($permissions, $permission, $this);
    }
}
