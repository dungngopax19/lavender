<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\Coupons;
use App\Helpers\DateHelper;

trait CouponsSearch{
    public function scopeSearch($query, Request $request){

        if(strlen($request->keyword) > 0){
            $keyword = trim($request->keyword);
            $query = $query->where(function($query) use ($keyword){
                $query->where('name', 'like', '%'.$keyword.'%');
            });
        }

        $query->orderBy('created_at', 'desc');

        return $query;
    }
}
