<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\Receipts;
use App\Helpers\DateHelper;

trait ReceiptsSearch{
    public function scopeSearch($query, Request $request)
    {
        if(strlen($request->id) > 0){
            $query = $query->where('id', $request->id);
        }

        if(strlen($request->filter_receipt) > 0){
            $filter_receipt = trim($request->filter_receipt);
            $query = $query->where(function($query) use ($filter_receipt){
                $query->where('code', 'like', '%'.$filter_receipt.'%')
                    ->orWhere('id', $filter_receipt);
            });
        } 

        if(strlen($request->filter_customer) > 0){
            $filter_customer = trim($request->filter_customer);
            $query = $query->whereHas('customer', function ($query) use($filter_customer) {
                $query->where('code', 'like', '%'.$filter_customer.'%')
                    ->orWhere('name', 'like', '%'.$filter_customer.'%')
                    ->orWhere('id', $filter_customer);
            });
        }

        if(strlen($request->filter_user) > 0){
            $filter_user = trim($request->filter_user);
            $query = $query->whereHas('user', function ($query) use($filter_user) {
                $query->where('code', 'like', '%'.$filter_user.'%')
                    ->orWhere('name', 'like', '%'.$filter_user.'%')
                    ->orWhere('id', $filter_user);
            });
        } 

        if(strlen($request->created_range) > 0){
            $created_range = trim($request->created_range);
            $created_range = explode(' -> ', $created_range);
            if(count($created_range) == 2){
                $query->whereBetween('created_at', [date('Y-m-d 00:00:01', strtotime($created_range[0])), date('Y-m-d 23:59:59', strtotime($created_range[1]))]);
            }
        } 

        $query->orderBy('created_at', 'desc');

        return $query;
    }
}
