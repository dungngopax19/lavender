<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\Branches;
use App\Helpers\DateHelper;

trait BranchesSearch{
    public function scopeSearch($query, Request $request)
    {
        if(strlen($request->id) > 0){
            $query = $query->where('id', $request->id);
        }

        if(strlen($request->name) > 0){
            $query = $query->where('name', 'like', '%'.$request->name.'%');
        }

        if(strlen($request->activity_status) > 0 && $request->activity_status > 0){
            $query = $query->where('activity_status', $request->activity_status);
        }

        $query->orderBy('created_at', 'desc');
        
        return $query;
    }
}
