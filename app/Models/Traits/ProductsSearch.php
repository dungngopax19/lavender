<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\Products;
use App\Helpers\DateHelper;
use Illuminate\Support\Str;

trait ProductsSearch{
    public function scopeSearch($query, Request $request)
    {
        //Tìm kiếm
        if(strlen($request->id) > 0){
            $query = $query->where('id', $request->id);
        }

        if(strlen($request->product_name_code) > 0){
            $nameCode = Str::slug(trim($request->product_name_code));
            $query = $query->where(function($query) use ($nameCode){
                $query->where('code', 'like', '%'.$nameCode.'%')
                    ->orWhere('name', 'like', '%'.$nameCode.'%')
                    ->orWhere('id', $nameCode);
            });
        }

        if(strlen($request->product_note) > 0){
            $note = trim($request->product_note);
            $query = $query->where('sample_notes', 'like', '%'.$note.'%');
        }

        // Loại hàng
        $types = (array)$request->product_types;
        if(count($types) > 0){
            $query = $query->whereIn('product_types_id', $types);
        }

        // Nhóm hàng
        if(strlen($request->product_categories) > 0){
            $categories = explode(',', $request->product_categories);
            if(count($categories) > 0){
                $query = $query->whereIn('product_categories_id', $categories);
            }
        }

        // Trạng thái
        $activity = intval($request->activity_status);
        if($activity > 0){
            $query = $query->where('activity_status', $activity);
        }

        //Chi nhánh + Tồn kho
        $branchActive = json_decode($_COOKIE['branch_active']);
        $branch_active_id = $branchActive->id;

        $inventory = intval($request->inventory_status);
        /*$query = $query->whereHas('branches_products', function ($query) use($inventory, $branch_active_id) {
            $query->where('branches_id', $branch_active_id)->where('in_branch', 1);

            if($inventory == 1){
                $query->whereRaw('inventory < min_inventory');
            }else if($inventory == 2){
                $query->where('inventory', '>', 0);
            }else if($inventory == 3){
                $query->where('inventory', 0);
            }
        });*/

        //Bán trực tiếp
        $directSelling = intval($request->direct_selling);
        if($directSelling > 0){
            $query = $query->where('direct_selling', $directSelling);
        }

        //Thuộc tính
        if($request->product_attributes){
            $attributes = explode(',', $request->product_attributes);
            if(count($attributes) > 0){
                $query = $query->whereHas('attributes_products', function ($query) use($attributes) {
                    $query->whereIn('attributes_id', $attributes);
                });
            }
        }

        //Ngày tạo
        $createdRange = trim($request->created_range);
        if(!empty($createdRange)){
            $createdRange = explode(' -> ', $createdRange);
            if(count($createdRange) == 2){
                //dd([$createdRange[0] . ' 00:00:00', $createdRange[1] . ' 23:59:59']);

                $createdFrom = DateHelper::convertDateViToDateDb($createdRange[0]) . ' 00:00:00';
                $createdTo = DateHelper::convertDateViToDateDb($createdRange[1]) . ' 23:59:59';

                $query = $query->whereBetween('created_at', [$createdFrom, $createdTo]);
            }
        }

        $query->orderBy('id', 'desc');
        
        return $query;
    }
}
