<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\ProductCategories;
use App\Helpers\DateHelper;

trait ProductCategoriesSearch{
    public function scopeSearch($query, Request $request)
    {
        if(strlen($request->id) > 0){
            $query = $query->where('id', $request->id);
        }

        if(strlen($request->name) > 0){
            $name = $request->name;
            $query = $query->where(function($query) use ($name){
                $query->where('name', 'like', '%'.$name.'%')
                ->orWhereHas('childrens', function ($query) use ($name) {
                    $query->where('name', 'like', '%'.$name.'%');
                });
            });
        }

        $query->orderBy('created_at', 'desc');
        
        return $query;
    }
}
