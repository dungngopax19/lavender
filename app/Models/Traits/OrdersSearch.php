<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\Orders;
use App\Helpers\DateHelper;
use App\Helpers\Common as CommonHelper;
use App\Helpers\Permissions;

trait OrdersSearch{
    public function scopeSearch($query, Request $request)
    {
        $hasJoinShipmentDetails = CommonHelper::isJoined($query, 'shipment_details');
        $hasJoinCustomer = CommonHelper::isJoined($query, 'customers');

        /*if(strlen($request->id) > 0){
            $query = $query->where('orders.id', $request->id);
        }*/

        $highlight = intval($request->highlight);
        if($highlight > 0){
            $query = $query->where('orders.highlight', $request->highlight);
        }

        if(strlen($request->filter_invoice) > 0){
            $filter_invoice = trim($request->filter_invoice);
            $query = $query->where(function($query) use ($filter_invoice){
                $query->where('orders.code', 'like', '%'.$filter_invoice.'%');
            });
        }

        if(strlen($request->filter_customer) > 0){
            $filter_customer = trim($request->filter_customer);

            if($hasJoinCustomer){

                $query = $query->where(function($query) use ($filter_customer){
                    $query->where('customers.name', 'like', '%'.$filter_customer.'%')
                        ->orWhere('customers.phone', 'like', '%'.$filter_customer.'%')
                        ->orWhere('customers.code', 'like', '%'.$filter_customer.'%')
                        ->orWhere('customers.id', $filter_customer);
                });
                
            }else{
                $query = $query->join('customers', function($join) use($filter_customer){
                    $join->on('orders.customers_id', '=', 'customers.id')
                        ->where(function($query) use ($filter_customer){
                            $query->where('customers.name', 'like', '%'.$filter_customer.'%')
                                ->orWhere('customers.phone', 'like', '%'.$filter_customer.'%')
                                ->orWhere('customers.code', 'like', '%'.$filter_customer.'%')
                                ->orWhere('customers.id', $filter_customer);
                        });

                        
                });
            }
        }

        //Search by branches
        $currentUser = \Auth::user();
        $branches_id = $currentUser->branchesUsers->pluck('branches_id')->toArray();
        $hasPermissionViewAllInvoices = Permissions::hasAllPermission(['giao-dich-xem-tat-ca-cac-giao-dich']);

        if($hasPermissionViewAllInvoices){
            if($request->branches_id){
                $query = $query->whereIn('orders.branches_id', array_values($request->branches_id));
            }
        }else{
            if($request->branches_id){
                $branches_id_request = array_values($request->branches_id);
                $branches_id = array_intersect($branches_id_request, $branches_id);
            }

            $query = $query->whereIn('orders.branches_id', $branches_id);
        }

        if($request->customers_id){
            $query = $query->whereIn('orders.customers_id', array_values($request->customers_id));
        }

        if($request->user_id){
            $query = $query->whereIn('orders.users_id', array_values($request->user_id));
        }

        if($request->status_confirm_order){
            $query = $query->whereIn('orders.verified', array_values($request->status_confirm_order));
        }

        if($request->invoices_status){
            $query = $query->whereIn('orders.orders_status_id', array_values($request->invoices_status));
        }

        if($request->channel){
            $query = $query->where('channel', $request->channel);
        }

        
        if($request->shipment_status_id){
            $shipment_status_id = is_array($request->shipment_status_id) ? array_values($request->shipment_status_id) : [$request->shipment_status_id];
            $query = $query->whereIn('shipment_status_id', $shipment_status_id);
        }

        //Check "Đơn hàng sắp tới ngày giao" và "Đơn hàng quá ngày giao"

        if($request->orders_upcomming_delivery || $request->orders_exceeding_delivery){
            $query = $query->where('shipment_status_id', 1);
        }

        if(strlen($request->created_range) > 0){
            $created_range = trim($request->created_range);
            $created_range = explode(' -> ', $created_range);
            if(count($created_range) == 2){
                $query->whereBetween('orders.created_at', [date('Y-m-d 00:00:01', strtotime($created_range[0])), date('Y-m-d 23:59:59', strtotime($created_range[1]))]);
            }
        }

        if(strlen($request->delivery_time) > 0){
            $delivery_time = trim($request->delivery_time);
            $delivery_time = explode(' -> ', $delivery_time);

            if(count($delivery_time) == 2){
                if($hasJoinShipmentDetails){
                    $query = $query->whereBetween('shipment_details.delivery_time', [date('Y-m-d', strtotime($delivery_time[0])), date('Y-m-d', strtotime($delivery_time[1]))]);
                }else{
                    $query = $query->join('shipment_details', function($join) use($delivery_time){
                        $join->on('shipment_details.orders_id', '=', 'orders.id')
                            ->whereBetween('shipment_details.delivery_time', [date('Y-m-d', strtotime($delivery_time[0])), date('Y-m-d', strtotime($delivery_time[1]))]);
                    });
                }
                
            }
        }

        if($request->province_id){
            $province_id = $request->province_id;
            
            if($hasJoinShipmentDetails){
                $query = $query->where('provinces_id', $province_id);
            }else{
                $query = $query->join('shipment_details', function($join) use($delivery_time){
                    $join->on('shipment_details.orders_id', '=', 'orders.id')
                        ->where('provinces_id', $province_id);
                });
            }

        }
        // if(strlen($request->activity_status) > 0 && $request->activity_status > 0){
        //     $query = $query->where('activity_status', $request->activity_status);
        // }

        return $query;
    }
}
