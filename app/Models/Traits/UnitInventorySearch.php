<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\UnitInventory;
use App\Helpers\DateHelper;

trait UnitInventorySearch{
    public function scopeSearch($query, Request $request)
    {
        if(strlen($request->id) > 0){
            $query = $query->where('id', $request->id);
        }

        if(strlen($request->name) > 0){
            $query = $query->where('code', 'like', '%'.$request->name.'%');
        }

        $query->orderBy('created_at', 'desc');
        
        return $query;
    }
}
