<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\Surcharges;
use App\Helpers\DateHelper;

trait SurchargesSearch{
    public function scopeSearch($query, Request $request){

        if(strlen($request->keyword) > 0){
            $keyword = trim($request->keyword);
            $query = $query->where(function($query) use ($keyword){
                $query->where('code', 'like', '%'.$keyword.'%')
                    ->orWhere('type', 'like', '%'.$keyword.'%');
            });
        }

        //Chi nhánh
        $branches = (array)$request->branches_id;
        if(count($branches) > 0){
            $query = $query->whereHas('branches_surcharges', function ($query) use($branches) {
                $query->whereIn('branches_id', $branches);
            });
        }

        if(strlen($request->activity_status) > 0 && $request->activity_status > 0){
            $query = $query->where('activity_status', $request->activity_status);
        }

        $query->orderBy('created_at', 'desc');

        return $query;
    }
}
