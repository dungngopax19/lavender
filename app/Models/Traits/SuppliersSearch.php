<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\Suppliers;
use App\Helpers\DateHelper;

trait SuppliersSearch{
    public function scopeSearch($query, Request $request){

        if(strlen($request->keyword) > 0){
            $keyword = trim($request->keyword);
            $query = $query->where(function($query) use ($keyword){
                $query->where('code', 'like', '%'.$keyword.'%')
                    ->orWhere('name', 'like', '%'.$keyword.'%')
                    ->orWhere('id', $keyword);
            });
        }

        if(strlen($request->activity_status) > 0 && $request->activity_status > 0){
            $query = $query->where('activity_status', $request->activity_status);
        }

        $query->orderBy('created_at', 'desc');
        
        return $query;
    }
}
