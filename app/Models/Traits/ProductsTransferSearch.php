<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\ProductsTransfer;
use App\Helpers\DateHelper;

trait ProductsTransferSearch{
    public function scopeSearch($query, Request $request)
    {
        if(strlen($request->products_id) > 0){
            $query = $query->where('products_id', $request->products_id);
        }

        if(strlen($request->uid_confirmed) > 0){
            $query = $query->where('uid_confirmed', $request->uid_confirmed);
        }

        $query->orderBy('created_at', 'desc');
        
        return $query;
    }
}
