<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\Customers;
use App\Helpers\DateHelper;

trait CustomerSearch{
    public function scopeSearch($query, Request $request)
    {
        if(strlen($request->id) > 0){
            $query = $query->where('id', $request->id);
        }

        if(strlen($request->id_code_name) > 0){
            $id_code_name = $request->id_code_name;
            $query = $query->where(function($query) use ($id_code_name){
                $query->where('name', 'like', '%'.$id_code_name.'%')
                    ->orWhere('code', 'like', '%'.$id_code_name.'%')
                    ->orWhere('id', 'like', '%'.$id_code_name.'%');
            });
        }

        // if($request->customers_types_id){
        //     $query = $query->where('customers_types_id', $request->customers_types_id);
        // }

        if(strlen($request->phone) > 0){
            $query = $query->where('phone',  'like', '%'.$request->phone.'%');
        }
        if(strlen($request->tax_code) > 0){
            $query = $query->where('tax_code',  'like', '%'.$request->tax_code.'%');
        }
        if(strlen($request->email) > 0){
            $query = $query->where('email',  'like', '%'.$request->email.'%');
        }
        if(strlen($request->text) > 0){
            $query = $query->where('text',  'like', '%'.$request->text.'%');
        }

        if($request->role){
            $query = $query->where('role', $request->role);
        }
        if($request->gender){
            $query = $query->where('gender', $request->gender);
        }

        $createdRange = trim($request->created_range);
        if(!empty($createdRange)){
            $createdRange = explode(' -> ', $createdRange);
            if(count($createdRange) == 2){

                $createdFrom = DateHelper::convertDateViToDateDb($createdRange[0]) . ' 00:00:00';
                $createdTo = DateHelper::convertDateViToDateDb($createdRange[1]) . ' 23:59:59';

                $query = $query->whereBetween('created_at', [$createdFrom, $createdTo]);
            }
        }

        if(strlen($request->activity_status) > 0 && $request->activity_status > 0){
            $query = $query->where('activity_status', $request->activity_status);
        }

        $query->orderByRaw('CHAR_LENGTH(code) desc');
        $query->orderByRaw('code desc');

        return $query;
    }
}
