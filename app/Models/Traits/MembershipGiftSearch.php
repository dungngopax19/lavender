<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\MemberShipGift;
use App\Helpers\DateHelper;

trait MemberShipGiftSearch{
    public function scopeSearch($query, Request $request){

        if(strlen($request->keyword) > 0){
            $keyword = trim($request->keyword);
            $query = $query->where(function($query) use ($keyword){
                $query->where('code', 'like', '%'.$keyword.'%')
                    ->orWhere('type', 'like', '%'.$keyword.'%');
            });
        }

        if(strlen($request->activity_status) > 0 && $request->activity_status > 0){
            $query = $query->where('activity_status', $request->activity_status);
        }

        $query->orderBy('created_at', 'desc');

        return $query;
    }
}
