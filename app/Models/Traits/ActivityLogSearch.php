<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Helpers\DateHelper;

trait ActivityLogSearch{
    public function scopeSearch($query, Request $request)
    {
        if(strlen($request->id) > 0){
            $query = $query->where('id', $request->id);
        }

        if(strlen($request->keyword) > 0){
            $keyword = $request->keyword;
            $query = $query->where(function($query) use ($keyword){
                $query->where('description', 'like', '%'.$keyword.'%')
                    ->orWhere('properties', 'like', '%'.$keyword.'%');
            });
        }

        if($request->user_id){
            $query = $query->whereIn('causer_id', array_values($request->user_id));
        }

        if($request->action){
            $query = $query->whereIn('description', array_values($request->action));
        }

        if(strlen($request->created_range) > 0){
            $created_range = trim($request->created_range);
            $created_range = explode(' -> ', $created_range);
            if(count($created_range) == 2){
                $query->whereBetween('created_at', [date('Y-m-d 00:00:01', strtotime($created_range[0])), date('Y-m-d 23:59:59', strtotime($created_range[1]))]);
            }
        }

        $query->orderBy('id', 'desc');

        return $query;
    }
}
