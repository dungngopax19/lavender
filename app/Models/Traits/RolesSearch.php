<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\Roles;
use App\Helpers\DateHelper;

trait RolesSearch{
    public function scopeSearch($query, Request $request)
    {
        if(strlen($request->id) > 0){
            $query = $query->where('id', $request->id);
        }

        if(strlen($request->name) > 0){
            $query = $query->where('name', 'like', '%'.$request->name.'%');
        }


        if($request->branches_id){
            $query = $query->whereIn('branches_id', $request->branches_id);
        }

        $query->orderBy('id', 'desc');
        
        return $query;
    }
}
