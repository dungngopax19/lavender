<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\OrdersSearch;
use Carbon\Carbon;

class Orders extends Base
{
    use OrdersSearch;

    protected $table = 'orders';

    protected $fillable = ['code', 'is_invoices', 'customers_id', 'users_id', 'branches_id', 'price', 'price_final', 'interval', 'channel', 'discount', 'discount_unit', 'discount_gift', 'discount_gift_unit', 'discount_coupon', 'coupon', 'deposit', 'ship', 'type', 'orders_status_id', 'interval', 'verified', 'note_verified', 'date_verified', 'orders_status_id', 'comment', 'comment_transfer', 'highlight', 'create_end_invoices', 'shipment_status_id'];

    const REPORT_TYPE_1 = 1;
    const REPORT_TYPE_2 = 2;
    const ORDER_TYPE = 'orders';
    const INVOICE_TYPE = 'invoices';

    const CHANNEL = [
        1 => 'Bán trực tiếp',
        2 => 'Facebook',
        3 => 'Khác'
    ];
    const VERIFIED = [
        -1 => 'Không duyệt',
        0 => 'Đang chờ',
        1 => 'Đã duyệt'
    ];
    const IS_INVOICES = [
        0 => 'Chỉ là đơn hàng',
        1 => 'Chuyền từ đơn hàng sang Đơn hàng',
        2 => 'Tạo Đơn hàng mà ko tạo đơn hàng'
    ];

    public function customer(){
        return $this->belongsTo('App\Models\Customers', 'customers_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'users_id', 'id');
    }

    public function branch(){
        return $this->belongsTo('App\Models\Branches', 'branches_id', 'id');
    }

    public function orderDetail(){
        return $this->HasMany('App\Models\OrderDetail', 'orders_id', 'id');
    }

    public function status(){
        return $this->belongsTo('App\Models\OrderStatus', 'orders_status_id', 'id');
    }

    public function shipmentStatus(){
        return $this->belongsTo('App\Models\ShipmentStatus', 'shipment_status_id', 'id');
    }

    public function shipmentDetail(){
        return $this->HasOne('App\Models\ShipmentDetails', 'orders_id', 'id');
    }

    public function setPriceAttribute($value){
        $this->attributes['price'] = intval(preg_replace("/[^0-9]/", "", trim($value) ));
    }

    public function setDiscountAttribute($value){
        $this->attributes['discount'] = intval(preg_replace("/[^0-9]/", "", trim($value) ));
    }

    public function setDepositAttribute($value){
        $this->attributes['deposit'] = intval(preg_replace("/[^0-9]/", "", trim($value) ));
    }

    public function setShipAttribute($value){
        $this->attributes['ship'] = intval(preg_replace("/[^0-9]/", "", trim($value) ));
    }

    public function setPriceFinalAttribute($value){
        $this->attributes['price_final'] = intval(preg_replace("/[^0-9]/", "", trim($value) ));
    }

    public function getDateVerifiedDisplayAttribute(){
        return empty($this->date_verified) ? '' : date('d-m-Y', strtotime($this->date_verified) );
    }

    public static function getBranches($startDate, $endDate) {
        $branches = self::whereBetween('created_at', [$startDate, $endDate])
			->selectRaw('branches_id')
			->groupBy('branches_id')
            ->get();
        return $branches;
    }

    public static function getOrderDate($startDate, $endDate) {
        $dates = self::whereBetween('created_at', [$startDate, $endDate])
			->selectRaw('DATE_FORMAT(created_at, "%Y-%m-%d") AS order_date')
			->groupBy('order_date')
            ->get();
        return $dates;
    }

    public static function getDataReport01($startDate, $endDate, $reportType) {

        $branches = self::getBranches($startDate, $endDate);
        $orderDates = self::getOrderDate($startDate, $endDate);
        $genres = array();
        $branchIDs = array();
        $genres[] = 'Genre';
        foreach ($branches as $item) {
            $branchIDs[] = $item->branches_id;
            $genres[] = $item->branch->name;
        }
        $genres[] = (object) array('role' => 'annotation');
        $query = self::whereBetween('created_at', [$startDate, $endDate]);
        if ($reportType == self::REPORT_TYPE_1) {
            $query->selectRaw('count(id) AS total_order, branches_id, DATE_FORMAT(created_at, "%Y-%m-%d") AS order_date');
        } else {
            $query->selectRaw('sum(price) AS total_order, branches_id, DATE_FORMAT(created_at, "%Y-%m-%d") AS order_date');
        }
        $query->groupBy('order_date')->groupBy('branches_id');
        $orders = $query->get();

        $data = array();
        $data[] = $genres;
        foreach ($orderDates as $item) {
            $data[] = self::prepareDataReport01($orders, $branchIDs, $item->order_date);
        }
        return $data;
    }

    public static function prepareDataReport01($orders, $branchIDs, $orderDate) {
        $data = array();
        $i = 0;
        $data[$i] = $orderDate;
        $i++;
        foreach ($branchIDs as $branch) {
            $data[$i] = 0;
            foreach ($orders as $item) {
                if ($item->order_date == $orderDate && $item->branches_id == $branch) {
                    $data[$i] = intval($item->total_order);
                }
            }
            $i++;
        }
        $data[$i++] = '';
        return $data;
    }

    public static function getOrdersReport01( $reportType, $year = null ) {
        $data = array();
        $dataTemp = array();
        $dateData = array();
        $tempTotal = array();

        $year = empty($year) ? date('Y') : $year;

        $data['type'] = intval($reportType);
        if ($reportType == Orders::REPORT_TYPE_1) {
            $data['label'] = 'Số lượng';
        } else {
            $data['label'] = 'Doanh thu';
        }

        $query = self::whereBetween('created_at', [$year.'-01-01', $year.'-12-31']);
        if ($reportType == self::REPORT_TYPE_1) {
            $query->selectRaw('count(customers_id) AS total_order, DATE_FORMAT(created_at, "%m-%Y") AS order_date');
        } else {
            $query->selectRaw('sum(price) AS total_order, DATE_FORMAT(created_at, "%m-%Y") AS order_date');
        }
        $query->groupBy('order_date');
        $orders = $query->get();
        for ($i = 1; $i < 13; $i++) {
            $dateData[] = $i<10 ? '0'.$i.'-'.$year : $i.'-'.$year;
        }
        foreach ($orders as $item) {
            $dataTemp[] = array($item->order_date, intval($item->total_order));
        }
        foreach ($dateData as $date) {
            $finder = 0;
            foreach ($dataTemp as $item) {
                if ($date == $item[0]) {
                    $finder = $item[1];
                }
            }
            $tempTotal[] = $finder;
        }
        $data['name'] = $dateData;
        $data['value'] = $tempTotal;
        $data['total'] = 0;
        
        return $data;
    }
}
