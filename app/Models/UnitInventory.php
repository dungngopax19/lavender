<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\UnitInventorySearch;

class UnitInventory extends Base
{
    use UnitInventorySearch;
    
    protected $table = 'unit_inventory';

    protected $fillable = ["code", "unit", "create_user", "update_user"];
}