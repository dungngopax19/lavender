<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ProductsSearch;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Product as ProductHelper;

class Products extends Base
{
    use ProductsSearch;

    protected $table = 'products';

    protected $fillable = [ "code", "name", "product_types_id", "product_categories_id", "thumbnail", "direct_selling", "price_sell", "price", "cloth_template", "number_of_sets", "meter_number", "meter_number_register", "warning_out_of_stock", "inventory_number", "description", "sample_notes", "is_gift", "activity_status"];

    public function attributes(){
        return $this->belongsToMany('App\Models\Attributes', 'attributes_products', 'products_id', 'attributes_id');
    }

    public function productTypes(){
        return $this->belongsTo('App\Models\ProductTypes');
    }

    public function product_categories(){
        return $this->belongsTo('App\Models\ProductCategories');
    }

    public function branches_products(){
        return $this->hasMany('App\Models\BranchesProducts');
    }

    public function attributes_products(){
        return $this->hasMany('App\Models\AttributesProducts');
    }

    public function getThumbnailDisplayAttribute()
    {
        $parsed = parse_url($this->thumbnail);
        if (empty($parsed['scheme'])) {
            return $this->thumbnail ? Storage::url($this->thumbnail) : asset('images/noimage.jpg');
        }
        return $this->thumbnail;
    }

    public function setPriceSellAttribute($value){
        $this->attributes['price_sell'] = intval(preg_replace("/[^0-9]/", "", trim($value) ));
    }

    public function setPriceAttribute($value){
         $price = intval(preg_replace("/[^0-9]/", "", trim($value) ));

        if(isset($this->attributes['price_sell'])){
            $this->attributes['price'] = ($price <= 0) ? $this->attributes['price_sell'] : $price;
        }else{
            $this->attributes['price'] = $price;
        }
    }

    public function getInventoryByProduct(){
        return ProductHelper::getInventoryByProduct($this->id);
    }

    public static function getIdByCode($code){
        $product = Products::where('code', $code)->first();
        if($product){
            return $product->id;
        }
        return null;
    }
}
