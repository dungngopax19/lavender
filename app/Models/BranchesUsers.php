<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class BranchesUsers extends Base
{   
    protected $table = 'branches_users';
    protected $fillable = ["branches_id", "users_id"];
}