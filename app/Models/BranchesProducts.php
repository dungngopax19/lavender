<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class BranchesProducts extends Model
{   
    protected $table = 'branches_products';
    protected $fillable = ["branches_id", "products_id", "inventory", "min_inventory"];

    public $timestamps = false;
    protected $softDelete = false;
}