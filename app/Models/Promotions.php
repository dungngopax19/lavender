<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\PromotionsSearch;
use App\Helpers\DateHelper;

class Promotions extends Base
{
    use PromotionsSearch;

    protected $table = 'promotions';

    protected $fillable = ["product_categories_ids", "name", "price", "from_date", "to_date"];

    protected $dates = ['from_date', 'to_date'];

    public function setStartDateAttribute($value){
        $this->attributes['start_date'] = \Carbon\Carbon::parse($value);
    }

    public function setEndDateAttribute($value){
        $this->attributes['end_date'] = \Carbon\Carbon::parse($value);
    }
}
