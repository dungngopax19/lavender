<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class Files extends Base
{
    protected $table = 'files';

    protected $fillable = ["model_name", "model_id", "path", "uniqid"];
}
