<?php

namespace App\Models;

use App\Models\Base;
use Spatie\Activitylog\Models\Activity;
use Carbon\Carbon;
use App\Models\Traits\ActivityLogSearch;

class ActivityLog extends Activity
{
    use ActivityLogSearch;

    public function modelCauser(){
        return $this->belongsTo($this->causer_type, 'causer_id');
    }

    public function modelSubject(){
        return $this->belongsTo($this->subject_type, 'subject_id');
    }

    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i:s');
    }
}
