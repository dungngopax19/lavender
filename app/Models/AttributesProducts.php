<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class AttributesProducts extends Base
{
    protected $table = 'attributes_products';
    public $timestamps = false;
    protected $fillable = ["attributes_id", "products_id"];
}
