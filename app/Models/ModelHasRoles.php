<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class ModelHasRoles extends Base
{   
    protected $table = 'model_has_roles';

    protected $fillable = ["role_id", "model_type", "model_id"];
}
