<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BranchesSearch;

class Branches extends Base
{
    use BranchesSearch;
    
    protected $table = 'branches';

    protected $fillable = ["name", "code", "address", "phone", "activity_status", "create_user", "update_user"];

    public function users(){
        return $this->belongsToMany('App\Models\Users', 'branches_users', 'branches_id', 'users_id');
    }

    public function products(){
        return $this->belongsToMany('App\Models\Products', 'branches_products', 'branches_id', 'products_id');
    }

    public function branchesProducts(){
        return $this->hasMany('App\Models\BranchesProducts', 'branches_id');
    }
}