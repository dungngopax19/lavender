<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class ProductTypes extends Base
{   
    protected $table = 'product_types';

    protected $fillable = ["name"];

    public function setNameAttribute($value){
        $this->attributes['name'] = trim($value);
    }
}
