<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;

class TypeServices extends Base
{
    protected $table = 'type_services';

    protected $fillable = ["name", "activity_status"];

}
