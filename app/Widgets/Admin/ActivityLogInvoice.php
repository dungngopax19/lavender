<?php

namespace App\Widgets\Admin;

use Arrilot\Widgets\AbstractWidget;
use Spatie\Activitylog\Models\Activity;

class ActivityLogInvoice extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $actions = config('activitylog.actions');
        $fields = config('activitylog.fields');

        $records = Activity::where('subject_type', 'App\Models\Orders')
            ->where('subject_id', $this->config['id'])
            ->orderBy('created_at', 'desc')
            ->get();

        return view('widgets.admin.activity-log.invoice', [
            'records' => $records,
            'actions' => $actions,
            'fields' => $fields
        ]);
    }
}
