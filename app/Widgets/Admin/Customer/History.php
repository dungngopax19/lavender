<?php

namespace App\Widgets\Admin\Customer;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Orders;
use Illuminate\Http\Request;

class History extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(Request $request)
    {
        $customer_id = intval($this->config['customer_id']);
        $records = Orders::with(['createdUser', 'status'])->orderBy('created_at', 'desc')->where('customers_id', $customer_id)->get();

        return view('widgets.admin.customer.history', [
            'records' => $records,
        ]);
    }
}
