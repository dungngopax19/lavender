<?php

namespace App\Widgets\Admin;

use Arrilot\Widgets\AbstractWidget;
use Melihovv\ShoppingCart\Facades\ShoppingCart as Cart;
use Auth;

class Carts extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $userId = Auth::user()->id;
        $branchActive = json_decode($_COOKIE['branch_active']);
        $type = $this->config['type'];
        $cartNumber = $this->config['cartNumber'];
        
        $records = null;
        $totalPrice = 0;
        
        if(Cart::restore($userId . '_' . $type . '_' . $branchActive->id . '_' . $cartNumber)){
            $records = Cart::content();
            $totalPrice = Cart::getTotal();
        }

        return view('admin.cart.list-hover', [
            'records' => $records,
            'totalPrice' => $totalPrice,
            'type' => $type,
            'cartNumber' => $cartNumber,
            'typeTxt' => ($type == 'orders') ? 'phiếu đặt hàng' : 'Đơn hàng'
        ]);
    }
}