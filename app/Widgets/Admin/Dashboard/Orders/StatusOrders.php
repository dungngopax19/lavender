<?php

namespace App\Widgets\Admin\Dashboard\Orders;

use Illuminate\Http\Request;
use Arrilot\Widgets\AbstractWidget;
use App\Models\Orders;
use App\Helpers\Order\Status;
use App\Helpers\Customer as CustomerHelper;
use Carbon\Carbon;

class StatusOrders extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(Request $request)
    {
        $now = Carbon::now();
        $month = ($now->month > 9) ? $now->month : '0' . $now->month;
        
        $start_date = $now->year . '-' . $month . '-01 00:00:00';
        $end_date = date('Y-m-d') . ' 23:59:59';

        switch($this->config['type']){
            case (1):
                $records = Status::prepareDelivery($request);
                break;
            case (2):
                $records = Status::beingTransport($start_date, $end_date);
                break;
            case (3):
                $records = Status::exceedingDelivery($request);
                break;
            case (4):
                $records = Status::canceled($start_date, $end_date);
                break;
            case (5):
                $records = CustomerHelper::highestRevenue($start_date, $end_date);
                break;
            case (6):
                $records = CustomerHelper::highestPurchases($start_date, $end_date);
                break;
            default:
                $records = false;
        }

        return view('widgets.admin.dashboard.orders.status', [
            'config' => $this->config,
            'records' => $records,
            'start_date' => '01-' . $month . '-' . $now->year,
            'end_date' => date('d-m-Y')
        ]);
    }
}
