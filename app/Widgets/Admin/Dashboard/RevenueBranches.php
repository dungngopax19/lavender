<?php

namespace App\Widgets\Admin\Dashboard;

use Arrilot\Widgets\AbstractWidget;

use App\Models\Orders;
use App\Models\OrderDetail;
use App\Helpers\Order\Report;
use Carbon\Carbon;

class RevenueBranches extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
		$start = new Carbon('first day of now');
        $end = new Carbon('last day of now');

        $data_revenue_branches = Report::revenueOfBranches($start->format('Y-m-d'), $end->format('Y-m-d'));
        
        return view('widgets.admin.dashboard.revenue-branches', [
            'config' => $this->config,
            'data_revenue_branches' => json_encode($data_revenue_branches),
            'startDate' => $start->format('d/m/Y'),
            'endDate' => $end->format('d/m/Y'),
        ]);
    }
}
