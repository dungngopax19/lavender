<?php

namespace App\Widgets\Admin\Dashboard;

use Arrilot\Widgets\AbstractWidget;

use App\Models\Orders;
use App\Models\OrderDetail;
use App\Models\Branches;

class Customers extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $day = date('w');
		$startDate = date('Y-m-d', strtotime('-'.$day.' days'));
        $endDate = date('Y-m-d', strtotime('+'.(6-$day).' days')); 
        $startDate = date('Y-m-01'); 
        $endDate  = date('Y-m-t');
        
        $report = Orders::getOrdersReport01(Orders::REPORT_TYPE_1);
        return view('widgets.admin.dashboard.customers', [
            'config' => $this->config,
            'report02' => $report,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }
}
