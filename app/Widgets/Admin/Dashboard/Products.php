<?php

namespace App\Widgets\Admin\Dashboard;

use Arrilot\Widgets\AbstractWidget;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Orders;
use App\Models\OrderDetail;
use App\Models\Branches;

class Products extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(Request $request)
    {
        $day = date('w');
		$startDate = date('Y-m-d', strtotime('-'.$day.' days'));
        $endDate = date('Y-m-d', strtotime('+'.(6-$day).' days')); 
        $startDate = date('Y-m-01'); 
        $endDate  = date('Y-m-t');
        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;
        
        $records = array();

        $report_type = isset($request->report_type) ? $request->report_type : Orders::REPORT_TYPE_1;
        $report01 = OrderDetail::getOrderDetailReport01($startDate, $endDate, $report_type, $items_per_page, $records);
        
        foreach (Input::except('page') as $input => $value){
            $records->appends($input, $value);
        }
        
        return view('widgets.admin.dashboard.products', [
            'config' => $this->config,
            'report01' => $report01,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'records' => $records,
            'request' => $request
        ]);
    }
}
