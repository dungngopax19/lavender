<?php

namespace App\Widgets\Admin;

use Arrilot\Widgets\AbstractWidget;
use App\User;
use App\Models\Messages;
use Illuminate\Http\Request;

class Message extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(Request $request)
    {
        $user_receive_id = intval($this->config['user_receive_id']);

        $records = [];
        $records['data'] =  Messages::with('userSend')->where('user_receive_id', $user_receive_id)->orderBy('id', 'desc')->skip(0)->take(10)->get();
        $records['unread'] = Messages::with('userSend')->where('user_receive_id', $user_receive_id)->where('status', Messages::UNREAD)->count();

        return view('widgets.admin.message', [
            'records' => $records,
        ]);
    }
}
