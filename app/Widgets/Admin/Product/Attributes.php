<?php

namespace App\Widgets\Admin\Product;

use Arrilot\Widgets\AbstractWidget;
use App\Models\AttributesProducts;
use App\Models\Attributes as ModelAttributes;

class Attributes extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $dataSoure = [];
        $attributes = ModelAttributes::where('parent_id', 0)->orderBy('name')->get();

        $attributesSearch = explode(',', request()->product_attributes);

        if($attributes){
            foreach ($attributes as $key => $item) {

                $data = [ 'id' => $item->id, 'text' => $item->name, 'checked' => in_array($item->id, $attributesSearch) ];
                $childs = $item->childrens;
                if($childs->count() > 0){
                    $items = [];
                    foreach($childs as $itemChild){
                        $items[] = (object)[ 'id' => $itemChild->id, 'text' => $itemChild->name, 'checked' => in_array($itemChild->id, $attributesSearch) ];
                    }
                    $data['items'] = $items;
                }

                $dataSoure[] = (object)$data;
            }
        }

        return view('widgets.admin.product.attributes', [
            'config' => $this->config,
            'dataSoure' => $dataSoure
        ]);
    }
}
