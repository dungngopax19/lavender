<?php

namespace App\Widgets\Admin\Product;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Http\Request;

use App\Models\Products;
use App\Models\ProductsComponents;
use App\Models\ProductCategories;

class Components extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(Request $request)
    {
        $componentsIds = [];
        $productsId = intval($this->config['products_id']);
        $ids = [$productsId];
        $components = ProductsComponents::where('products_id_parent', $productsId)->get();
        if($components){
            foreach($components as $item){
                $ids[] = $item->products_id;
                $componentsIds[] = (object)['id'=>$item->products_id, 'qty' => $item->qty];
            }
        }

        return view('widgets.admin.product.components', [
            'config' => $this->config,
            'productsId' => $productsId,
            'componentsIds' => $componentsIds,
            'ids' => $ids
        ]);
    }
}
