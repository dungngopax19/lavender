<?php

namespace App\Widgets\Admin\Product;

use Arrilot\Widgets\AbstractWidget;
use App\Models\ProductCategories;

class Categories extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $dataSoure = [];
        $productCategories = ProductCategories::where('parent_id', 0)->orderBy('name')->get();

        $categoriesSearch = explode(',', request()->product_categories);

        if($productCategories){
            foreach ($productCategories as $key => $item) {

                $data = [ 'id' => $item->id, 'text' => $item->name, 'checked' => in_array($item->id, $categoriesSearch) ];
                $childs = $item->childrens;
                if($childs->count() > 0){
                    $items = [];
                    foreach($childs as $itemChild){
                        $items[] = (object)[ 'id' => $itemChild->id, 'text' => $itemChild->name, 'checked' => in_array($itemChild->id, $categoriesSearch) ];
                    }
                    $data['items'] = $items;
                }

                $dataSoure[] = (object)$data;
            }
        }

        return view('widgets.admin.product.categories', [
            'config' => $this->config,
            'dataSoure' => $dataSoure
        ]);
    }
}
