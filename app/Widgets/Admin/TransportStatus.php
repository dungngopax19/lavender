<?php

namespace App\Widgets\Admin;

use Arrilot\Widgets\AbstractWidget;
use App\Models\ShipmentStatus;
use App\Helpers\Common as CommonHelper;
use App\Helpers\Invoice as InvoiceHelper;
use App\Helpers\Order\Status as StatusOrdersHelper;
use Illuminate\Http\Request;
use App\Models\Orders;

class TransportStatus extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(Request $request)
    {
        $records = ShipmentStatus::all();

        if($records){
            foreach ($records as $item) {
                $item->count = CommonHelper::getOrderByShipmentStatus($request, $item->id);
            }
        }

        $invoicesHighlight = InvoiceHelper::countOrdersHighlight($request);
        $invoicesHighlightSize = InvoiceHelper::countOrdersHighlightSize($request);
        $ordersUpcommingDelivery = StatusOrdersHelper::prepareDelivery($request, 'count');
        $ordersExceedingDelivery = StatusOrdersHelper::exceedingDelivery($request, 'count');

        return view('widgets.admin.transport-status', [
            'records' => $records,
            'request' => $request,
            'invoicesHighlight' => $invoicesHighlight,
            'invoicesHighlightSize' => $invoicesHighlightSize,
            'ordersUpcommingDelivery' => $ordersUpcommingDelivery,
            'ordersExceedingDelivery' => $ordersExceedingDelivery
        ]);
    }
}
