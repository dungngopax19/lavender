<?php

namespace App\Widgets\Admin;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Products;

class Inventory extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $records = Products::where('warning_out_of_stock', 1)
            ->where('inventory_number', '>', 0)
            ->orderBy('code', 'desc')->limit(100)->get();

        return view('widgets.admin.inventory', [
            'records' => $records,
        ]);
    }
}
