<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Provinces as ModelProvinces;

class Provinces extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $provinces = ModelProvinces::where('parent_id', 0)->get();
        $province_id = intval($this->config['value']);
        $provinceChild = $province_id ? ModelProvinces::where('id', $province_id)->first() : null;

        return view('widgets.provinces', [
            'config' => $this->config,
            'provinces' => $provinces,
            'provinceChild' => $provinceChild,
            'province_id' => $province_id
        ]);
    }
}
