<?php
namespace App\Helpers\Report;

use App\Models\Orders;
use App\Models\Customers;
use App\Helpers\DateHelper;

class Sell
{
    public static function getOrderSellByEmployee($arr_status, $request){
        $query = Customers::whereHas('orders', function($q) use($arr_status, $request){
            $q->search($request)->whereIn('is_invoices', $arr_status);
        });
        $customers = $query->get(['id', 'name']);
        foreach($customers as $key=>$customer){
            $query = Orders::search($request)->where('customers_id', $customer->id);
            $customers[$key]['total'] = $query->sum('price');
            $customers[$key]['expand'] = false;
            $dates = $query->pluck('created_at')->toArray();
            $customers[$key]['dates']= DateHelper::sortDateInArray($dates, 'Y-m-d H:00');

            $whereDateStart = 'Y-m-d H:00:00';
            $whereDateEnd = 'Y-m-d H:59:59';

            $orders = [];
            foreach($customers[$key ]['dates'] as $key1=>$date){
                $orders[$key1]['data'] = Orders::search($request)->where('customers_id', $customer->id)
                    ->whereBetween('created_at', [date($whereDateStart, strtotime($date)), date($whereDateEnd, strtotime($date))])
                    ->selectRaw('sum(price) as price')
                    ->first();
                $orders[$key1]['date'] = $date;
            }

            $customers[$key]['orders'] = $orders;
        }
        $report['total'] = Orders::search($request)->selectRaw('sum(price) as price')->first();
        $report['customers'] = $customers;
        return $report;
    }

}
