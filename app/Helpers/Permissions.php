<?php
namespace App\Helpers;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class Permissions{

    public static function hasAllPermission($permissions, $permission = null, $model = null){
        $permissionOwner = $permission.'owner';
        $user = auth()->user();

        //Check role Admin
        if($user->id == 1 || $user->hasRole('Admin')){
            return true;
        }

        //Check permisson owner
        if(!empty($permission) && !empty($model)){
            $chk_permission = Permission::where('name', $permissionOwner)->first();
            if(!$chk_permission){
                $chk_permission = Permission::create(['name' => $permissionOwner]);
            }

            if($chk_permission && $user->hasPermissionTo($permissionOwner)){
                if($user->email === $model->create_user){
                    return true;
                }
            }
        }

        //Check list permission
        foreach($permissions as $name){
            $chk_permission = Permission::where('name', $name)->first();
            if(!$chk_permission){
                $chk_permission = Permission::create(['name' => $name]);
            }
        }

        return $user->hasAllPermissions($permissions);
    }

    public static function hasPermission($model, $permission, $isRole = true) {
        if($isRole){
            return $model->hasPermissionTo($permission) ? 'true' : 'false';
        }

    }

    public static function renderPermissions($permissions, $model, $isRole = true){
        $res = [];
        if($permissions){
            foreach($permissions as $label => $item){
                $permission = self::renderRecursivePermissions(str_slug($label), $label, $item, $model, $isRole);
                $permission->expanded = true;
                $res[] = (object)$permission;
            }
        }
        return $res;
    }

    public static function renderRecursivePermissions($slugParent, $label, $items, $model, $isRole){
        if(empty($items) || !is_array($items)){
            $slug = $slugParent . '-' .str_slug($items);
            $permission = ['id' => $slug, 'text' => $items];

            if($isRole){
                $chk_permission = Permission::where('name', $slug)->first();
                if(!$chk_permission){
                    $chk_permission = Permission::create(['name' => $slug]);
                }

                // the multi-models
                foreach($model as $item){
                    

                    if($item->hasPermissionTo($slug)){
                        $className = get_class($item);

                        if($className == 'Spatie\Permission\Models\Role'){
                            $permission['checked'] = true;
    
                            // check the Role Model
                            if(empty($item->email)){
                                $permission['enabled'] = false;
                            }
                            break;
                        }else{
                            $permission['checked'] = true;
                        }
                    }
                    

                    
                }
            }

            return (object)$permission;
        }

        $slug = str_slug($label);
        $slugParent = ($slugParent != $slug) ? $slugParent . '-' .$slug : $slug;
        $permission = ['id' => $slugParent, 'text' => $label];
        foreach ($items as $key => $item) {
            $permission['items'][] = self::renderRecursivePermissions($slugParent, $key, $item, $model, $isRole);
        }

        return (object)$permission;
    }

}
