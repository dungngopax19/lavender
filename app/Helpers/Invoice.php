<?php
namespace App\Helpers;

use App\Models\Products;
use App\Models\Branches;
use App\Models\Orders;
use App\Models\OrderDetail;
use App\Models\Customers;
use App\User;
use App\Models\OrderStatus;
use App\Models\ShipmentDetails;
use App\Models\ShipmentStatus;
use App\Helpers\Customer as CustomerHelper;
use Carbon\Carbon;
use App\Helpers\DateHelper;
use Melihovv\ShoppingCart\Facades\ShoppingCart as Cart;
use App\Models\BranchesProducts;
use App\Helpers\Product as ProductHelper;

class Invoice{

    public static function backInventory($orders_id){
        $orderDetail = Orders::find($orders_id);
        if($orderDetail){
            $records = OrderDetail::where('orders_id', $orders_id)->get();
            if($records){
                foreach($records as $item){
                    $branchProduct = BranchesProducts::where('branches_id', $orderDetail->branches_id)
                        ->where('products_id', $item->products_id)
                        ->first();
                    if($branchProduct && $branchProduct->inventory > 0 && $item->qty > 0){
                        $branchProduct->inventory = $branchProduct->inventory + $item->qty;
                        $branchProduct->save();
                    }
                }
            }
        }
    }

    public static function updateInventoryPDH($recordOrder){
        $orderDetail = Orders::find($recordOrder->id);
        if($orderDetail){
            $records = OrderDetail::where('orders_id', $recordOrder->id)->get();
            if($records){
                foreach($records as $item){

                    $product = Products::find($item->products_id);

                    if($product){
                        $again = $item->qty;

                        $branchesProducts = BranchesProducts::where('branches_id', $recordOrder->branches_id)->where('products_id', $item->products_id)->first();

                        //Còn đủ hàng trong kho
                        if($branchesProducts && $branchesProducts->inventory > 0){
                            
                            if($branchesProducts->inventory > $item->qty){
                                $branchesProducts->inventory = $branchesProducts->inventory - $item->qty;
                                $again = 0;
                            }else{
                                $branchesProducts->inventory = 0;
                                $again = $item->qty - $branchesProducts->inventory;
                            }
                            
                            if($branchesProducts->save()){
                                ProductHelper::updateInventoryNumber( $product->product_types_id, $product->id );
                            }
                        }
                        
                        //Số lượng hàng có sẵn trong kho không đủ cung cấp
                        //thì ta lấy thêm từ số lương ước tính (hàng sắp về)
                        if($again > 0){
                            $unit = ProductHelper::getUnitInventory($product->code);
                            $product->meter_number_register += $unit * $again;
                            $product->save();
                            
                            ProductHelper::synsInventoryProductLikeCode($product->code);
                        }

                    }

                }
            }
        }
    }

    public static function countOrdersHighlight($request){
        return Orders::with('customer')
            ->where('is_invoices', '>', 0)
            ->where('verified', 1)
            ->search($request)
            ->where('highlight', 1)
            ->count();
    }

    public static function cloneInvoice($invoice_id){
        $invoiceDetail = Orders::find($invoice_id);
        if(!$invoiceDetail){
            return (object)[ 'status' => 500, 'msg' => 'Mã đơn hàng không hợp lệ' ];
        }

        //Process table orders
        $invoiceNew = $invoiceDetail->replicate();
        $invoiceNew->save();

        dd($invoiceNew);

        return (object)[ 'msg' => 'asd' ];
        
    }

    public static function countOrdersHighlightSize($request){
        return Orders::with('customer')
            ->where('is_invoices', '>', 0)
            ->where('verified', 1)
            ->search($request)
            ->where('highlight', 2)
            ->count();
    }

    public static function insertOrder($item){
        $invoice = new Orders();
        $invoice->code = trim($item->ma_hoa_don);
        $invoice->is_invoices = 1;
        
        $customer = Customers::where('code', trim($item->ma_khach_hang))->first();
        $invoice->customers_id = empty($customer) ? NULL : $customer->id;

        $user_id = User::getIdByName($item->nguoi_ban);
        $invoice->users_id = empty($user_id) ? 1 : $user_id;

        $branch = Branches::where('name', $item->chi_nhanh)->first();
        $invoice->branches_id = empty($branch) ? NULL : $branch->id;

        $invoice->discount = $item->giam_gia_hoa_don;
        $invoice->discount_unit = 'vnd';
        $invoice->discount_gift = null;
        $invoice->discount_gift_unit = null;
        $invoice->price = $item->khach_can_tra;//Price after discount
        $invoice->deposit = $item->khach_da_tra;
        $invoice->price_final = $item->khach_can_tra - $item->khach_da_tra;
        
        $invoice->channel = array_search($item->kenh_ban, Orders::CHANNEL);
        $invoice->interval = 0;
        $invoice->verified = 1;
        
        $invoice->comment = $item->ghi_chu;

        $datetime = DateHelper::convertDateTimeViToDateDb($item->thoi_gian_tao, '/');
        $carbon = new Carbon($datetime);
        if($carbon->year == 2018 && $carbon->month <= 12){
            $invoice->date_verified = date('Y-m-d H:i:s', strtotime($item->thoi_gian_tao));
            $invoice->orders_status_id = 10;
            $invoice->shipment_status_id = 4;
        }else{
            $invoice->date_verified = date('Y-m-d H:i:s', strtotime($item->thoi_gian_tao));
            $invoice->orders_status_id = OrderStatus::getIdByName($item->trang_thai);
            $invoice->shipment_status_id = ShipmentStatus::getIdByName($item->trang_thai_giao_hang);
        }

        //Update highlight = 0 if shipment_status_id in 2,4,7
        if( in_array($invoice->shipment_status_id, [2,4,7]) ){
            $invoice->highlight = 0;
        }else{
            $invoice->highlight = $item->highlight;
        }

        $invoice->note_verified = null;
        $invoice->create_end_invoices = 0;
        $invoice->is_full_product = 0;
        
        $invoice->create_user = CustomerHelper::getUserByFullName($item->nguoi_tao);
        $invoice->created_at = DateHelper::convertDateTimeViToDateDb($item->thoi_gian_tao, '/');
        $invoice->update_user = CustomerHelper::getUserByFullName($item->nguoi_tao);
        $invoice->save();
        return $invoice;
    }

    public static function insertShipmentDetail ($item){
        $shipmentDetail = new ShipmentDetails();
        $shipmentDetail->orders_id = $item->orders_id;
        $shipmentDetail->name = $item->nguoi_nhan;
        $shipmentDetail->phone = $item->dien_thoai;
        $shipmentDetail->provinces_id = null;
        $shipmentDetail->address = $item->dia_chi_nguoi_nhan;
        $shipmentDetail->partner_delivery_id = null;
        $shipmentDetail->type_services_id = null;
        $shipmentDetail->delivery_charges = null;
        $shipmentDetail->lading_code = null;

        $item->thoi_gian_giao_hang = str_replace(': ', ':', $item->thoi_gian_giao_hang);
        $shipmentDetail->delivery_time = DateHelper::convertDateTimeViToDateDb($item->thoi_gian_giao_hang, '/');

        //dd($item->thoi_gian_giao_hang);

        $shipmentDetail->save();

        return $shipmentDetail;
    }

    public static function insertOrderDetail ($item){

        $orderDetail = new OrderDetail();
        $orderDetail->orders_id = $item->orders_id;
        $orderDetail->products_id = Products::getIdByCode($item->ma_hang);
        $orderDetail->price = $item->gia_ban;
        $orderDetail->qty = $item->so_luong;
        $orderDetail->discount = $item->giam_gia;
        $orderDetail->discount_unit = 'vnd';
        $orderDetail->discount_note = $item->ghi_chu_hang_hoa;
        $orderDetail->save();
        
        return $orderDetail;
    }

    public static function insertShoppingCart($item){

        $product = Products::where('id', $item->products_id)->first();

        if(empty($product)){
            return false;
        }

        Cart::restore($item->ma_hoa_don);

        $priceDiscountItem = intval($item->price) - intval($item->discount);
        $qty = intval($item->qty);

        $cartItem = Cart::add($product->id, $product->name, $priceDiscountItem, $qty, [
            'code' => $product->code,
            'thumbnail' => $product->thumbnail_display,
            'price_sell' => intval($item->price),
            'discount' => $item->discount,
            'discountUnit' => 'vnd',
            'discount_note' => $item->discount_note,
        ]);

        Cart::store($item->ma_hoa_don);

        return $item;
    }

    public static function productCodeYetDelivered(){

        $query = OrderDetail::join('orders', 'order_detail.orders_id', '=', 'orders.id')
            ->join('products', 'order_detail.products_id', '=', 'products.id')
            ->where('orders.shipment_status_id', 1)
            ->whereNotNull('order_detail.products_id')
            
            ->when(strlen(request()->created_range) > 0, function ($query) {
                $created_range = trim(request()->created_range);
                $created_range = explode(' -> ', $created_range);
                if(count($created_range) == 2){
                    return $query->whereBetween('orders.created_at', [date('Y-m-d 00:00:01', strtotime($created_range[0])), date('Y-m-d 23:59:59', strtotime($created_range[1]))]);
                }
            })

            ->when(strlen(request()->exclude_code_product_delivered) > 0, function ($query) {
                $exclude_code_product_delivered = trim(request()->exclude_code_product_delivered);

                return $query->where('products.code', '<>', $exclude_code_product_delivered);
            })

            ->where('orders.highlight', 0)
            ->whereNull('orders.deleted_at')

            ->select('order_detail.products_id', \DB::raw('sum(order_detail.qty) as totalQty'), \DB::raw('count(*) as totalOrders'))
            ->groupBy('order_detail.products_id');

        $code_product_delivered = trim(request()->code_product_delivered);    

        if($code_product_delivered != ''){
            $query = $query->where('products.code', 'like', '%' . $code_product_delivered . '%');
        }

        $createdRange = trim(request()->created_range);
        
        if(!empty($createdRange)){
            $createdRange = explode(' -> ', $createdRange);
            if(count($createdRange) == 2){
                $createdFrom = DateHelper::convertDateViToDateDb($createdRange[0]) . ' 00:00:00';
                $createdTo = DateHelper::convertDateViToDateDb($createdRange[1]) . ' 23:59:59';

                $query = $query->whereBetween('orders.created_at', [$createdFrom, $createdTo]);
            }
        }

        $records = $query->with('product')->get();

        return $records;
    }

    public static function productCodeOrderHighlight(){
        $query = OrderDetail::join('orders', 'order_detail.orders_id', '=', 'orders.id')
            ->join('products', 'order_detail.products_id', '=', 'products.id')
            ->where('orders.highlight', 1)
            ->whereNotNull('order_detail.products_id')
            ->select('order_detail.products_id', 'orders.code as orders_code', \DB::raw('sum(order_detail.qty) as totalQty'), \DB::raw('count(*) as totalOrders'))
            ->groupBy('order_detail.products_id');

        $code_product_order_highlight = trim(request()->code_product_order_highlight);    

        if($code_product_order_highlight != ''){
            $query = $query->where('products.code', 'like', '%' . $code_product_order_highlight . '%');
        }

        $records = $query->with('product')->get();

        return $records;
    }
}
