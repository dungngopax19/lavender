<?php
namespace App\Helpers;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductTypes;
use App\Models\ProductCategories;
use App\Models\Products;
use App\Models\Branches;
use App\Models\Attributes;
use App\Models\ShipmentStatus;
use App\Models\OrderStatus;

class ActivityLog{

    public static function highlightAction($text){
        $actions = config('constants.action_activity_log');
        if($actions){
            foreach ($actions as $item) {
                $text = str_replace($item,"<b style='color:#3390dc'>$item</b>", $text);
            }
        }
        return $text;
    }

    public static function writeLog($model, $msg, $properties = []){
        $user = Auth::user();

        $log = activity()
            ->causedBy($user)
            ->performedOn($model)
            ->log($msg);

        if(!empty($properties)){
            $log->withProperties($properties);
        }
    }

    public static function showText($field, $record, $txt){
        if($field == 'product_types_id'){
            $record = ProductTypes::find($txt);
            return $record ? $record->name : $txt;

        }else if($field == 'product_categories_id'){
            $record = ProductCategories::find($txt);
            return $record ? $record->name : $txt;

        }else if($field == 'products_id'){
            $record = Products::find($txt);
            return $record ? $record->name : $txt;

        }else if($field == 'branches_id'){
            $record = Branches::find($txt);
            return $record ? $record->name : $txt;

        }else if($field == 'attributes_id'){
            $record = Attributes::find($txt);
            return $record ? $record->name : $txt;

        }else if( in_array($field, ['is_gift', 'warning_out_of_stock', 'direct_selling']) ){
            return $txt ? 'Có' : 'Không';
        }else if($field == 'verified'){
            return empty($txt) ? 'Không duyệt' : 'Đã duyệt';
        }else if($field == 'shipment_status_id'){
            $record = ShipmentStatus::find($txt);
            return $record ? $record->name : $txt;
        }else if($field == 'orders_status_id'){
            $record = OrderStatus::find($txt);
            return $record ? $record->name : $txt;
        }

        return $txt;
    }

    public static function changeOrdersStatus($statusFrom, $statusTo){
        $from = OrderStatus::find($statusFrom);
        $to = OrderStatus::find($statusTo);

        if($from && $to){
            return "( $from->name -> $to->name )";
        }

        return '';
    }

    public static function changeShipmentStatus($statusFrom, $statusTo){
        $from = ShipmentStatus::find($statusFrom);
        $to = ShipmentStatus::find($statusTo);

        if($from && $to){
            return "( $from->name -> $to->name )";
        }

        return '';
    }

}
