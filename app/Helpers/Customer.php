<?php
namespace App\Helpers;

use App\Models\Orders;
use App\Models\Provinces;
use App\Models\Customers;
use App\User;
use Carbon\Carbon;

class Customer{

    public static function checkExistCustomerByPhone($customer){
        $user = Customers::where('phone', $customer->dien_thoai)
            ->where('activity_status', 1)
            ->first();
        if($user){
            return $user;
        }
        return false;
    }

    public static function getProvinceIdByName($name){
        $province = Provinces::where('name', '=', $name)->first();
        if($province){
            return $province->id;
        }
        return null;
    }

    public static function getUserByFullName($full_name){
        $user = User::where('name', '=', $full_name)->orWhere('username', '=', $full_name)->first();
        if($user){
            return $user->username;
        }
        return 'admin';
    }

    public static function updateLevel($customers_id){
        $customer = Customers::find($customers_id);
        if($customer){
            $level = self::checkLevel($customers_id);
            if($level != false){
    
                $level_id = str_replace("level-","",$level);
    
                $customer->update([
                    'membership_id' => $level_id
                ]);
            }
        }
    }

    public static function synsdataCustomersInvoices($customers_id){
        $customer = Customers::find($customers_id);
        if($customer){
            self::updateLevel($customers_id);
            
            //Update total_price, buys in table customer
            $query = Orders::where('customers_id', $customers_id)->select('price');
            
            $customer->update([
                'buys' => $query->count(),
                'total_price' => $query->sum('price')
            ]);
        }
    }

    public static function checkLevel($customers_id){
        $check_level = self::checkLevel6($customers_id);
        if(!$check_level){
            $check_level = self::checkOtherLevel($customers_id);
        }
        return $check_level;
    }

    //Cấp 1: KH đã mua nhiều lần với tổng Đơn hàng > 20tr (các khoảng thời gian mua gần nhau dưới 6 tháng)
    public static function checkLevel6($customers_id){
        $query = Orders::where('customers_id', $customers_id)->where('interval', '<=', 6);
        $count = $query->count();
        $total = $query->sum('price');
        if($count > 2 && $total > 20000000){
            return 'level-6';
        }
        return false;
    }

    public static function checkOtherLevel($customers_id){
        $query = Orders::where('customers_id', $customers_id);
        $count = $query->count();
        $total = $query->sum('price');

        switch(true){
            case ($count >= 2 and $total > 10000000):
                $level = 'level-5';
                break;
            case ($count > 2 and $total > 2000000):
                $level = 'level-4';
                break;
            case ($total > 10000000):
                $level = 'level-3';
                break;
            case ($total > 5000000):
                $level = 'level-2';
                break;
            case ($count > 0):
                $level = 'level-1';
                break;
            default:
                $level = false;
        }
        return $level;
    }

    //Khách hàng có doanh thu cao nhất
    public static function highestRevenue($start_date = '', $end_date = ''){
        $query = Orders::leftJoin('customers', 'customers_id', '=', 'customers.id');
        if($start_date && $end_date){
            $query->whereBetween('orders.created_at', [date('Y-m-d 00:00:01', strtotime($start_date)), date('Y-m-d 23:59:59', strtotime($end_date))]);
        }
        $query->selectRaw('
            count("customers_id") as countOrders,
            sum(price) as price,
            customers.id,
            customers.name,
            customers.code
        ');
        $records = $query->groupBy('customers_id')->orderBy('price', 'desc')->paginate(50);
        return $records;
    }

    //Khách hàng mua nhiều lần nhất
    public static function highestPurchases($start_date = '', $end_date = ''){
        $query = Orders::leftJoin('customers', 'customers_id', '=', 'customers.id');

        $query = Orders::join('customers', function ($join) {
            $join->on('orders.customers_id', '=', 'customers.id')
                ->whereNotNull('orders.customers_id');
        });

        if($start_date && $end_date){
            $query->whereBetween('orders.created_at', [date('Y-m-d 00:00:01', strtotime($start_date)), date('Y-m-d 23:59:59', strtotime($end_date))]);
        }

        $query->selectRaw('
            count("customers_id") as countOrders,
            sum(price) as price,
            customers.id,
            customers.name,
            customers.code
        ');

        $records = $query->groupBy('customers.id')
                ->orderBy('countOrders', 'desc')
                ->orderBy('customers.code', 'desc')
                ->paginate(50);

        return $records;
    }

}
