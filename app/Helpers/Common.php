<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Request;
use App\Helpers\DateHelper;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\TmpWareHouse;
use Spatie\Permission\Models\Permission;
use App\Models\Orders;

class Common{

    public static function resizeImage($urlImage, $w = 300){
        try {
            if(empty($urlImage) || strpos($urlImage, 'noimage') != false){
                $urlImage = 'images/noimage.jpg';
            }else{
                $parsed = parse_url($urlImage);
                
                if (empty($parsed['scheme'])) {
                    if(strpos($urlImage, 'storage') != false){
                        $urlImage = str_replace('/storage/', '', $urlImage);
                    }
            
                    $exists = Storage::exists($urlImage);
                    if(!$exists){
                        $urlImage = 'images/noimage.jpg';
                    }else{
                        $urlImage = trim(Storage::url($urlImage), '/');
                    }
                }
            }

            $image = Image::cache(function($image) use ($urlImage, $w) {
                $image->make($urlImage)->resize($w, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }, 60*24*30, true);//30 days

        } catch (\Exception $e) {
            $urlImage = 'images/noimage.jpg';
            $image = Image::cache(function($image) use ($urlImage, $w) {
                $image->make($urlImage)->resize($w, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }, 60*24*30, true);//30 days
        }

        return $image->encode('data-url');
    }

    public static function cleanData($txt){
        return strip_tags(trim($txt));
    }

    public static function convertArrayToString($array){
        if(empty($array)){
            return "[]";
        }

        $txt = "[";
        foreach ($array as $item) {
            $txt .= '"' . $item . '",';
        }

        return trim($txt, ",") . "]";
    }

    public static function convertDateExcelToDateDb($date, $format = 'Y-m-d H:i:s'){
        if(empty($date)){
            return null;
        }

        $date = str_replace('/', '-', $date);
        $date = str_replace(': ', ':', $date);
        return date($format, strtotime($date));
    }

    // Trạng thái giao hàng
    public static function countStatusOrder($status){
        return number_format(TmpWareHouse::where('tt_giao_hang', $status)->count());
    }

    // Mã hàng (Chưa giao hàng)
    public static function countCodeOrder($code){
        return number_format(TmpWareHouse::where('ma_hang', $code)->where('tt_giao_hang', 0)->count());
    }

    public static function isArrayMulti($array) {
        $rv = array_filter($array,'is_array');
        if(count($rv)>0) return true;
        return false;
    }

    public static function hasPermission($model, $permission, $isRole = true) {
        if($isRole){
            return $model->hasPermissionTo($permission) ? 'true' : 'false';
        }

    }

    public static function renderPermissions($permissions, $model, $isRole = true){
        $res = [];
        if($permissions){
            foreach($permissions as $label => $item){
                $permission = Common::renderRecursivePermissions(str_slug($label), $label, $item, $model, $isRole);
                $permission->expanded = true;
                $res[] = (object)$permission;
            }
        }
        return $res;
    }

    public static function renderRecursivePermissions($slugParent, $label, $items, $model, $isRole){
        if(empty($items) || !is_array($items)){
            $slug = $slugParent . '-' .str_slug($items);
            $permission = ['id' => $slug, 'text' => $items];

            if($isRole){
                $chk_permission = Permission::where('name', $slug)->first();
                if(!$chk_permission){
                    $chk_permission = Permission::create(['name' => $slug]);
                }

                $permission['checked'] = $model ? $model->hasPermissionTo($slug) : '';
            }

            return (object)$permission;
        }

        $slug = str_slug($label);
        $slugParent = ($slugParent != $slug) ? $slugParent . '-' .$slug : $slug;
        $permission = ['id' => $slugParent, 'text' => $label];
        foreach ($items as $key => $item) {
            $permission['items'][] = self::renderRecursivePermissions($slugParent, $key, $item, $model, $isRole);
        }

        return (object)$permission;
    }

    public static function slugFilename($filename){
        $info = pathinfo(storage_path() . $filename);
        $ext = $info['extension'];

        $arr = explode('.', trim($filename));
        if(empty($arr) || count($arr) < 2){
            return '';
        }

        return str_slug($arr[0]) . '.' . $arr[1];
    }

    public static function getOrderByShipmentStatus($request, $shipment_status_id, $count = true){
        $idProductDelivered = trim($request->id_product_delivered);

        $query = Orders::where('is_invoices', '>', 0);

        if($idProductDelivered){
            $query = $query->join('order_detail', function ($join) use ($idProductDelivered) {
                    $join->on('orders.id', '=', 'order_detail.orders_id')
                        ->where('order_detail.products_id', $idProductDelivered)
                        ->where('orders.shipment_status_id', 1);
                    });
        }

        $query = $query->search($request)
            ->where('shipment_status_id', $shipment_status_id)
            ->where('is_invoices', '>', 0)
            ->select('orders.*');

        //Nếu trường hợp đã chọn "Ưu tiên" thì mình sẽ bỏ chọn trong "CHưa giao hàng"
        if($shipment_status_id == 1){
            $query = $query->where('highlight', 0);
        }

        return ($count) ? $query->count() : $query->get();
    }

    public static function isJoined($query, $table)
    {
        $joins = $query->getQuery()->joins;
        if($joins == null) {
            return false;
        }
        foreach ($joins as $join) {
            if ($join->table == $table) {
                return true;
            }
        }
        return false;
    }
}
