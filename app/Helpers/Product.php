<?php
namespace App\Helpers;

use App\Models\UnitInventory;
use App\Models\Products;
use App\Models\ProductsComponents;
use App\Models\ProductCategories;
use App\Models\BranchesProducts;
use App\Models\Promotions;
use App\Models\Coupons;
use App\Helpers\DateHelper;

class Product{

    public static function getUnitInventory($codeProduct){
        $product = Products::where('code', $codeProduct)->first();

        if($product){
            list($code) = explode('-', $product->code);
            if($code == 'TB'){
                return ($product->cloth_template == '2.5') ? 11 : 14;
            }

            $unitInventory = UnitInventory::where('code', $code)->first();

            return $unitInventory ? $unitInventory->unit : false;
        }

        return false;
    }

    public static function synsInventoryProductLikeCode($codeProduct){
        $product = Products::where('code', $codeProduct)->first();
        if($product){
            $code = self::getCoreCode($codeProduct);

            $likeCodes = Products::where('code', 'like', '%' . $code . '%')->where('id', '<>', $product->id)->get();

            if($likeCodes){
                foreach ($likeCodes as $item) {
                    //Update table products
                    Products::where('id', $item->id)->update([
                        'cloth_template' => $product->cloth_template,
                        'meter_number' => $product->meter_number,
                        'number_of_sets' => $product->number_of_sets,
                        'meter_number_register' => $product->meter_number_register,
                        'warning_out_of_stock' => $product->warning_out_of_stock,
                        'inventory_number' => $product->inventory_number,
                    ]);

                    //Update table branches_products
                    BranchesProducts::where('products_id', $item->id)->forceDelete();

                    $branchesProducts = BranchesProducts::where('products_id', $product->id)->get();
                    if($branchesProducts){
                        foreach ($branchesProducts as $itemBranchesProducts) {
                            $itemNew = $itemBranchesProducts->replicate();
                            $itemNew->products_id = $item->id;
                            $itemNew->save();
                        }
                    }
                }
            }

        }
    }

    public static function getCoreCode($codeProduct){
        $arr = explode('-', $codeProduct);
        $collection = collect($arr);
        $filtered = $collection->filter(function ($value, $key) use ($arr) {
            return $key > 0 && $key < (count($arr) - 1);
        });

        return implode('-', $filtered->toArray());
    }

    public static function checkInventoryCart($type = 'orders', $itemsCart, $branches_id = false){
        $dataError = [];

        //Tam thời sẽ không kiểm tra số lượng đặt hàng
        if($type == 'orders'){
            return $dataError;
        }

        if($itemsCart){
            foreach($itemsCart as $item){

                //Lấy số lượng tồn kho của sp tại toàn bô kho hàng

                if($branches_id){
                    $inventory = self::getInventoryByBranch($item->id, $branches_id);
                }else{
                    $inventory = self::getInventoryByProduct($item->id);
                }

                if($type == 'orders'){
                    //Lấy số bộ(sp) còn có thể đặt hàng
                    $setsInventory = self::getSetsInventory($item->id);
                    $inventory = max($inventory, $setsInventory);
                }
                
                if($inventory < $item->quantity){
                    $item->inventory = $inventory;
                    $dataError[] = $item;
                }
            }
        }

        return $dataError;
    }

    public static function getItemCartByProductId($products_id, $itemsCart){
        if($itemsCart){
            foreach($itemsCart as $item){
                if($item->id == $products_id){
                    return $item;
                }
            }
        }

        return null;
    }

    //Nếu sản phẩm trong các kho đều hết hàng và số lượng m vải trong kho < 30(Phieu dat hang) => Không cho đặt hàng
    public static function checkAddToCart($product, $qty, $type = 'orders'){
        $inventory = self::getInventoryByProduct($product->id);
    
        if( $inventory >= $qty ){
            return true;
        }else if($type == 'invoices'){
            return false;
        }

        //Tạm thời sẽ cho đặt hàng, không kiểm tra điều kiện đặt hàng
        return true;

        //Nếu là phiếu đặt hàng và hàng trong kho không đủ giao
        $sets = self::getSetsInventory($product->id);
        return ( $inventory + $sets >= $qty ) ? true : false;
    }

    public static function getInventoryByBranch($products_id, $branches_id = 0){
        $product = Products::find($products_id);

        if(!$product){
            return 0;
        }

        if(!$branches_id){
            $branchActive = json_decode($_COOKIE['branch_active']);
            $branches_id = $branchActive->id;
        }

        $branchProduct = BranchesProducts::where('branches_id', $branches_id)
                        ->where('in_branch', 1)
                        ->where('products_id', $products_id)
                        ->first();

        return $branchProduct ? $branchProduct->inventory : 0;
    }

    public static function getInventoryByProduct($products_id){
        $product = Products::find($products_id);

        if(!$product){
            return 0;
        }

        return BranchesProducts::where('in_branch', 1)
                        ->where('products_id', $products_id)
                        ->select('inventory')
                        ->sum('inventory');
    }

    //Lấy ra số bộ sp còn có thể đặt hàng
    public static function getSetsInventory($products_id){
        //30: dưới 30m thi không cho đặt hàng
        $product = Products::find($products_id);
        $mets = $product ? ($product->meter_number - ($product->meter_number_register + 30)) : 0;
        if($mets < 1){
            return 0;
        }

        //Chuyển số m vải thành số bộ
        $unit = self::getUnitInventory($product->code);

        return ($unit > 0) ? intval($mets/$unit) : 0;
    }

    public static function convertMetToSet($product){

    }

    //Update Inventory number
    public static function updateInventoryNumber($product_types_id, $products_id){
        $product = Products::find($products_id);

        if($product){

            if($product_types_id == 1){//Combo - Dong goi

                $components = ProductsComponents::with('products')->where('products_id_parent', $products_id)->get();

                if(count($components) > 0){
                    $inventory_number = 999999;
                    $components = $components->map(function ($item) use(&$inventory_number) {
                        $item->inventory_number = $item->products->inventory_number;
    
                        $number_sale = 0;
                        if($item->qty > 0){
                            $number_sale = max(intval(floor($item->products->inventory_number / $item->qty)), 0);
                        }
                        
                        $inventory_number = min($number_sale, $inventory_number);
                        return $item;
                    });

                    $product->inventory_number = $inventory_number;
                }else{
                    $product->inventory_number = 0;
                }
                
            }else{//Check single product
                $product->inventory_number = BranchesProducts::where('products_id', $products_id)->sum('inventory');
            }

            if($product->save()){
                self::updateWarningOutOfStock($products_id);
                return $product->inventory_number;
            }
        }

        return false;
    }

    //Warning product is out of stock
    public static function updateWarningOutOfStock($products_id){
        $product = Products::find($products_id);

        if(!$product){
            return false;
        }

        $record = BranchesProducts::where('products_id', $products_id)
            ->whereColumn('inventory', '<=', 'min_inventory')
            ->where('in_branch', 1)
            ->first();
            
        $product->warning_out_of_stock = $record ? 1 : 0;

        return $product->save();
    }

    //Tu dong giam gia theo chuong trinh khuyen mai(theo nhom san pham)
    public static function discountByProductCats($product_id){
        $discount = 0;
        $now = DateHelper::currentDate('Y-m-d');

        $product = Products::find($product_id);
        if($product){
            $cat = ProductCategories::where('id', $product->product_categories_id)->first();
            
            if($cat){
                if($cat->parent_id > 0){
                    $cat = ProductCategories::where('id', $cat->parent_id)->first();
                }

                if($cat){
                    $promotion = Promotions::where('product_categories_ids', 'like', '"' . $cat->id . '"')
                        ->selectRaw('sum(price) as discount')
                        ->where('from_date', '<=', $now)
                        ->where('to_date', '>=', $now)
                        ->value('discount');

                    return $promotion ? $promotion : 0;
                }
            }
        }

        return $discount;
    }

    //Giam gia theo coupon
    public static function discountByCoupon($coupon, $membership_id){
        $now = DateHelper::currentDate('Y-m-d');

        $promotion = Coupons::where('membership_ids', 'like', '"' . $membership_id . '"')
            ->where('name', $coupon)
            ->where('from_date', '<=', $now)
            ->where('to_date', '>=', $now)
            ->selectRaw('sum(price) as discount')
            ->value('discount');

        return $promotion ? $promotion : 0;
    }
}
