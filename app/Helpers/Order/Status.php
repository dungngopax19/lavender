<?php
namespace App\Helpers\Order;

use App\Models\Orders;

class Status{

    public static function status(){
        $order_status= Orders::selectRaw('
            sum(if(orders_status_id = 2, 1, 0)) as status_2,
            sum(if(orders_status_id = 6, 1, 0)) as status_6'
        )
        ->first();
        $order_status->prepare_delivery = Orders::where('orders_status_id', 1)
            ->whereHas('shipmentDetail', function ($query) {
                $query->whereBetween('delivery_time', [date('Y-m-d'), date('Y-m-d', strtotime("+2 days"))]);
            })
            ->count();
        $order_status->over_delivery = Orders::where('orders_status_id', 1)
            ->whereHas('shipmentDetail', function ($query) {
                $query->whereDate('delivery_time', '<', date('Y-m-d'));
            })
            ->count();
        return $order_status;
    }

    public static function prepareDelivery($request, $typeReturn = 'paginate', $orderByDirection = 'asc'){
        $query = Orders::where('shipment_status_id', 1)
            ->join('shipment_details', 'shipment_details.orders_id', '=', 'orders.id')
            ->whereBetween('shipment_details.delivery_time', [date('Y-m-d'), date('Y-m-d', strtotime("+1 days"))])
            ->search($request)
            ->orderby('orders.created_at', $orderByDirection)
            ->select('orders.*');

        if($typeReturn == 'paginate'){
            return $query->paginate(50);
        }else if( $typeReturn == 'count' ){
            return number_format($query->count());
        }

        return $query->get();
    }

    public static function exceedingDelivery($request, $typeReturn = 'paginate', $orderByDirection = 'asc'){
        $query = Orders::where('shipment_status_id', 1)
            ->join('shipment_details', 'shipment_details.orders_id', '=', 'orders.id')
            ->whereNotNull('shipment_details.delivery_time')
            ->where('shipment_details.delivery_time', '<', date('Y-m-d 00:00:00') )
            ->search($request)
            ->orderby('orders.created_at', $orderByDirection)
            ->select('orders.*');

        if($typeReturn == 'paginate'){
            return $query->paginate(50);
        }else if( $typeReturn == 'count' ){
            return number_format($query->count());
        }

        return $query->get();
    }

    public static function beingTransport($start_date = '', $end_date = ''){
        $query = Orders::join('shipment_details', function ($join) use ($start_date, $end_date) {
            $join->on('orders.id', '=', 'shipment_details.orders_id')
                ->whereBetween('shipment_details.delivery_time', [date('Y-m-d 00:00:01', strtotime($start_date)), date('Y-m-d 23:59:59', strtotime($end_date))])
                ->where('orders.shipment_status_id', 2);
        });
        $records = $query->orderBy('orders.created_at', 'desc')
                        ->orderBy('orders.code', 'desc')
                        ->paginate(10);
        return $records;
    }

    public static function overDelivery($start_date = '', $end_date = ''){
        $query = Orders::where('orders_status_id', 1)
            ->whereHas('shipmentDetail', function ($query) {
                $query->whereDate('delivery_time', '<', date('Y-m-d'));
            });
        if($start_date && $end_date){
            $query->whereBetween('created_at', [date('Y-m-d 00:00:01', strtotime($start_date)), date('Y-m-d 23:59:59', strtotime($end_date))]);
        }
        $records = $query->orderby('created_at', 'asc')->paginate(10);
        return $records;
    }

    public static function canceled($start_date = '', $end_date = ''){
        $query = Orders::where('shipment_status_id', 7);
        if($start_date && $end_date){
            $query->whereBetween('created_at', [date('Y-m-d 00:00:01', strtotime($start_date)), date('Y-m-d 23:59:59', strtotime($end_date))]);
        }
        $records = $query->orderBy('orders.created_at', 'desc')
            ->orderBy('orders.code', 'desc')
            ->paginate(10);

        return $records;
    }

    

}
