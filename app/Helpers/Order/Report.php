<?php
namespace App\Helpers\Order;

use App\Models\Orders;
use App\Models\OrderDetail;
use App\Models\Branches;
use Carbon\Carbon;
use App\Helpers\DateHelper;
use DB;

class Report{

    public static function revenue($start_date, $end_date){
        $data = [];
        $data['dates'] = [];
        $branches = Branches::get(['id', 'name']);
        if($start_date == $end_date){
            $data = self::revenueByOneDay($data, $branches, $start_date, $end_date);
        }else{
            $data = self::revenueByMultiDay($data, $branches, $start_date, $end_date);
        }

        return $data;
    }

    public static function revenueOfBranches($start_date, $end_date){
        $query = Orders::whereBetween('orders.created_at', [date('Y-m-d 00:00:00', strtotime($start_date)), date('Y-m-d 23:59:59', strtotime($end_date))])
                        ->whereNotNull('orders.branches_id');

        $data['total'] = $query->sum('price');

        $data['branches'] = $query->leftJoin('branches', function($join) {
                $join->on('orders.branches_id', '=', 'branches.id');
            })
            ->selectRaw('sum(price) AS value, branches.name as category')
            ->groupBy('category')
            ->orderBy('category', 'DESC')
            ->get();
        return $data;
    }

    //
    public static function revenueByOneDay($data, $branches, $start_date, $end_date){
        $query = Orders::whereDate('created_at', $start_date)->whereNotNull('orders.branches_id');
        $data['total'] = $query->sum('price');
        $dates = $query->orderBy('created_at', 'ASC')->pluck('created_at')->toArray();

        $format_dates = [];
        foreach($dates as $date){
            array_push($format_dates, date('Y-m-d H:00', strtotime($date)));
        }

        $format_dates = array_values(array_unique($format_dates));

        foreach( $branches as $key1=>$branch){
            $data[$key1]['name'] = $branch->name;
            if(count($format_dates)){
                foreach($format_dates as $key2=>$date){
                    $data[$key1]['data'][$key2] = (int)Orders::whereBetween('created_at', [date('Y-m-d H:00:00', strtotime($date)), date('Y-m-d H:59:59', strtotime($date))])
                    ->where('branches_id', $branch->id)
                    ->sum('price');

                }
            }else{
                $data[$key1]['data'] = [0];
            }
        }

        if(count($format_dates)){
            foreach($format_dates as $key2=>$date){
                array_push($data['dates'], date('H:00', strtotime($date)));
            }

        }


        return $data;
    }

    public static function revenueByMultiDay($data, $branches, $start_date, $end_date){
        $query = Orders::whereBetween('orders.created_at', [date('Y-m-d 00:00:00', strtotime($start_date)), date('Y-m-d 23:59:59', strtotime($end_date))])
                    ->whereNotNull('orders.branches_id');
                    
        $data['total'] = $query->sum('price');
        $dates = $query->selectRaw('DATE(created_at) as date')
                ->groupBy('date')
                ->orderBy('date', 'ASC')
                ->pluck('date')->toArray();
        foreach($dates as $date){
            array_push($data['dates'], date('d', strtotime($date)));
        }
        foreach( $branches as $key1=>$branch){
            $data[$key1]['name'] = $branch->name;
            if(count($dates)){
                foreach($dates as $key2=>$date){
                    $data[$key1]['data'][$key2] = (int)Orders::whereDate('created_at', $date)
                    ->where('branches_id', $branch->id)
                    ->sum('price');
                }
            }else{
                $data[$key1]['data'] = [0];
            }
        }

        return $data;
    }

    public static function getOrderWithStatus($arr_status, $request){
        $orders = Orders::with('orderDetail')->search($request)->selectRaw('
            count(id) as count,
            sum(price) as total_price,
            sum(deposit) as total_deposit'
        )->whereIn('is_invoices', $arr_status)->first();
        if($orders->count){
            $orders->sub = Orders::with('orderDetail')->search($request)->whereIn('is_invoices', $arr_status)->get();
        }else{
            $orders->sub = [];
        }
        return $orders;
    }

    public static function getOrderSellByTime($arr_status, $request){
        $created_range = trim($request->created_range);
        $created_range = explode(' -> ', $created_range);
        $whereDateStart = '';
        $whereDateEnd = '';

        $query = Orders::search($request);
        $data['total'] = $query->sum('price');
        $dates = $query->pluck('created_at')->toArray();

        if($request->whereDate == 'hour'){
            $dates = DateHelper::sortDateInArray($dates, 'Y-m-d H:00');
            $whereDateStart = 'Y-m-d H:00:00';
            $whereDateEnd = 'Y-m-d H:59:59';
        }elseif($request->whereDate == 'day'){
            $dates = DateHelper::sortDateInArray($dates, 'Y-m-d');
            $whereDateStart = 'Y-m-d 00:00:01';
            $whereDateEnd = 'Y-m-d 23:59:59';
        }elseif($request->whereDate == 'month'){
            $dates = DateHelper::sortDateInArray($dates, 'Y-m-01');
            $whereDateStart = 'Y-m-01 00:00:01';
            $whereDateEnd = 'Y-m-31 23:59:59';
        }

        $data = self::getOrderChildByTime($data, $dates, $request, $whereDateStart, $whereDateEnd);

        return $data;

    }

    public static function getOrderChildByTime($data, $dates, $request, $whereDateStart, $whereDateEnd){
        if($dates){
            foreach($dates as $key=>$date){
                $data['orders'][$key]['total'] = Orders::search($request)
                    ->whereBetween('created_at', [date($whereDateStart, strtotime($date)), date($whereDateEnd, strtotime($date))])
                    ->selectRaw('sum(price) as price')
                    ->first();
                $data['orders'][$key]['hour'] = date("H", strtotime($date));
                $data['orders'][$key]['ymd'] = date("Y-m-d", strtotime($date));
                $data['orders'][$key]['date'] = $date;
                $data['orders'][$key]['expand'] = true;
                $data['orders'][$key]['orders'] = Orders::orderBy('created_at', 'ASC')->with('customer')->search($request)
                    ->whereBetween('created_at', [date($whereDateStart, strtotime($date)), date($whereDateEnd, strtotime($date))])
                    ->get();
            }
        }else{
            $data['orders'] = [];
        }

        return $data;
    }

    // public static function getOrderChildByEmployee($data, $dates, $request, $whereDateStart, $whereDateEnd){
    //     if($dates){
    //         foreach($dates as $key=>$date){
    //             $data['report'][$key]['total'] = Orders::search($request)
    //                 ->whereBetween('created_at', [date($whereDateStart, strtotime($date)), date($whereDateEnd, strtotime($date))])
    //                 ->selectRaw('sum(price) as price')
    //                 ->first();
    //             $data['report'][$key]['hour'] = date("H", strtotime($date));
    //             $data['report'][$key]['ymd'] = date("Y-m-d", strtotime($date));
    //             $data['report'][$key]['date'] = $date;
    //             $data['report'][$key]['expand'] = true;
    //             $data['report'][$key]['orders'] = Orders::orderBy('created_at', 'ASC')->with('customer')->search($request)
    //                 ->whereBetween('created_at', [date($whereDateStart, strtotime($date)), date($whereDateEnd, strtotime($date))])
    //                 ->get();
    //         }
    //     }else{
    //         $data['orders'] = [];
    //     }

    //     return $data;
    // }

}
