<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use App\Models\Branches;
use App\Models\Customers;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Get + Set Branch Default
        if(!isset($_COOKIE['branch_active'])) {
            $branches = Branches::orderBy('is_default', 'DESC')->get();
            if(count($branches) > 0){
                setcookie('branch_active', json_encode($branches[0]), time() + (86400 * 30), "/");
            }else{
                dd('Không có chi nhánh trong hệ thống');
            }
        }

        //Validation
        Validator::extend('check_percent', function($attribute, $value, $parameters, $validator) {
            // 2 is percent
            if($parameters['0'] == 2){
                if($value > 100){
                    return false;
                }
            }
            return true;
        });

        Validator::extend('unique_membership_id', function($attribute, $value, $parameters, $validator) {
            if(!$parameters['0']){
                $count = DB::table('membership_gift')->where('membership_id', $value)->count();
                if($count){
                    return false;
                }
            }
            return true;
        });

        Validator::extend('check_exist_customer', function($attribute, $value, $parameters, $validator) {
            $check_sample_field = 0;
            
            list($id, $name, $phone, $provinceId) = $parameters;

            $checkName = Customers::where('name', $name)->where('id', '<>', $id)->first();

            if(!$checkName){
                return true;
            }

            $checkPhoneName = Customers::where('name', $name)
                ->where('phone', $phone)
                ->where('id', '<>', $id)
                ->first();

            if(!$checkPhoneName){
                return true;
            }

            $checkPhoneNameAddress = Customers::where('name', $name)
                ->where('phone', $phone)
                ->where('province_id', $provinceId)
                ->where('id', '<>', $id)
                ->first();

            return $checkPhoneNameAddress ? false : true;
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
