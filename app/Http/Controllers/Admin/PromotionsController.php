<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Promotions;
use App\Http\Requests\StorePromotions;
use App\Models\ProductCategories;
use App\Helpers\Permissions;

class PromotionsController extends Controller
{
    public $title = 'Chương trình khuyến mãi';
    public $mod = 'promotions';
    public $permission = 'chuong-trinh-khuyen-mai-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = Promotions::search($request)->paginate($items_per_page);

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);
        $is_permission_import = Permissions::hasAllPermission([$this->permission. 'import']);

        return view('admin.promotions.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create,
            'is_permission_import' => $is_permission_import
        ]);
    }

    public function show($id){
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);

            $record = new Promotions();
            $title = 'Thêm mới';
        }else{
            $record = Promotions::find($id);
            $title = "Cập nhật";
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $productCategories = ProductCategories::where('parent_id', 0)->orderBy('name')->select('id', 'name')->get()->toArray();
        
        $html = (string)view('admin.promotions.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'productCategories' => $productCategories,
            'permission' => $this->permission,
            'cat_ids' => json_decode($record->product_categories_ids),
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StorePromotions $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Promotions();
        }else{
            $record = Promotions::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $params = $request->all();

        $params['from_date'] = ($params['from_date']) ? date("Y-m-d", strtotime($params['from_date'])) : null;
        $params['to_date'] = ($params['to_date']) ? date("Y-m-d", strtotime($params['to_date'])) : null;
        $params['product_categories_ids'] = json_encode($params['cat_ids']);

        $record->fill($params);

        if($record->save()){
            return response()->json(['st' => 200, 'data' => $record]);
        }
        return response()->json(['status' => 500]);
    }

    public function destroy($id){
        $st = Promotions::find($id)->delete();
        return response()->json(['html' => $st]);
    }

}
