<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Branches;

use App\Http\Requests\StoreBranch;
use App\Helpers\Permissions;

class BranchController extends Controller
{
    public $permission = 'he-thong-chi-nhanh-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = Branches::search($request)->paginate($items_per_page);
        $permission = $this->permission;
        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.branch.index', compact('records', 'permission', 'is_permission_create'));
    }

    public function show($id){

        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Branches();
            $title = 'Thêm mới';
        }else{
            $record = Branches::find($id);
            $title = "Cập nhật";
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $html = (string)view('admin.branch.edit', compact('title', 'record'));

        return response()->json(['html' => $html]);
    }

    public function update(StoreBranch $request){

        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Branches();
        }else{
            $record = Branches::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $params = $request->all();
        $params['code'] = str_slug($params['name']);
        
        $record->fill($params)->save();

        return response()->json(['st' => $params]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, Branches::find($id))) return response()->json(['st' => 403]);

        $st = Branches::find($id)->delete();
        return response()->json(['html' => $st]);
    }

    public function changeBranch(Request $request){
        $branch = Branches::find($request->id);
        if($branch){
            setcookie('branch_active', $branch, time() + (86400 * 30), "/");
            return response()->json(['st' => 200, 'msg' => $branch->name]);
        }

        return response()->json(['st' => 500, 'msg' => 'Có lỗi sảy ra khi chuyển đổi chi nhánh']);
    }
}
