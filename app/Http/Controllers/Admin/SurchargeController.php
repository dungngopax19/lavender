<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Surcharges;
use App\Models\Branches;
use App\Models\BranchesSurcharges;
use App\Http\Requests\StoreSurcharge;
use App\Helpers\Permissions;

class SurchargeController extends Controller
{
    public $title = 'thu khác';
    public $mod = 'surcharge';
    public $permission = 'he-thong-thu-khac-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = Surcharges::search($request)->paginate($items_per_page);

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.surcharge.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create
        ]);
    }

    public function show($id){
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Surcharges();
            $branches_ides = [];
            $title = 'Thêm mới';
        }else{
            $record = Surcharges::find($id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
            $branches_ides = BranchesSurcharges::where('surcharges_id', $id)->pluck('branches_id')->toArray();
            $title = "Cập nhật";
        }

        $branches = Branches::orderBy('name')->select('id', 'name')->get()->toArray();
        $activity_status = (array)config('constants.activity_status');
        unset( $activity_status[0] );

        $html = (string)view('admin.surcharge.edit', [
            'record' => $record,
            'title' => $this->title,
            'mod' => $this->mod,
            'activity_status' => $activity_status,
            'branches' => $branches,
            'branches_ides' => $branches_ides,
        ]);
        return response()->json(['html' => $html]);
    }

    public function update(StoreSurcharge $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Surcharges();
        }else{
            $record = Surcharges::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $params = $request->all();
        $params['value_type'] = ($request->value_type) ? $request->value_type : 1;
        $params['is_put_bill'] = ($request->is_put_bill) ? $request->is_put_bill : null;
        $params['is_refund'] = ($request->is_refund) ? $request->is_refund : null;
        $record->fill($params);

        if($record->save()){
            $this->saveBranchesSurcharges($record->id);
            return response()->json(['st' => 200, 'data' => $record]);
        }
        return response()->json(['status' => 500]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, Surcharges::find($id))) return response()->json(['st' => 403]);

        $st = Surcharges::find($id)->delete();
        return response()->json(['html' => $st]);
    }

    public function saveBranchesSurcharges($surcharges_id){
        BranchesSurcharges::where( 'surcharges_id', $surcharges_id )->forcedelete();

        foreach(request()->branches_ides as $i => $branches_id){
            $record = new BranchesSurcharges();
            $record->branches_id = $branches_id;
            $record->surcharges_id = $surcharges_id;
            $record->save();
        }
    }
}
