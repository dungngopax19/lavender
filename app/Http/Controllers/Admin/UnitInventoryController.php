<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UnitInventory;
use App\Helpers\Common;
use App\Helpers\Permissions;
use App\Http\Requests\StoreUnitInventory;

class UnitInventoryController extends Controller
{
    public $title = 'Chuyển đổi vải thành phẩm';
    public $mod = 'unit-inventory';
    public $permission = 'hang-hoa-unit-inventory-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = UnitInventory::search($request)->paginate($items_per_page);

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.unit-inventory.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create
        ]);
    }

    public function show($id){

        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new UnitInventory();
        }else{
            $record = UnitInventory::find($id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $html = (string)view('admin.unit-inventory.edit', [
            'title' => lcfirst($this->title),
            'mod' => $this->mod,
            'record' => $record
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreUnitInventory $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new UnitInventory();
        }else{
            $record = UnitInventory::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $record->fill($request->all())->save();

        return response()->json(['st' => 200]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, UnitInventory::find($id))) return response()->json(['st' => 403]);

        $st = UnitInventory::find($id)->delete();
        return response()->json(['html' => $st]);
    }
}
