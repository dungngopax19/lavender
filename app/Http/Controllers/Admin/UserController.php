<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\StoreUser;
use App\Models\Provinces;
use Auth;
use App\Models\Roles;
use App\Helpers\Permissions;
use Illuminate\Support\Facades\Hash;
use App\Models\Branches;
use App\Models\BranchesUsers;

class UserController extends Controller
{
    public $title = 'Người dùng';
    public $mod = 'user';
    public $permission = 'he-thong-nguoi-dung-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = User::search($request)->paginate($items_per_page);

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.user.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create
        ]);
    }

    public function show($id){
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);

            $record = new User();
            $title = 'Thêm mới';
            $user_roles = [];
            $models = [$record];
        }
        else{
            $userCurrent = Auth::user();
            $record = User::find($id);

            if($userCurrent->id != $id){
                if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
            }

            $title = "Cập nhật";

            $user_roles = array_values($record->getRoleNames()->toArray());
            $models = [];

            foreach($user_roles as $role_name){
                array_push($models, Role::findByName($role_name));
            }
            array_push($models, $record);
        }

        $provinces = Provinces::with('childrens')->where('parent_id', 0)->orderBy('name')->select('id', 'name')->get()->toArray();

        $activity_status = (array)config('constants.activity_status');
        $roles = Roles::orderBy('name')->select('id', 'name')->get()->toArray();
        $branches = Branches::orderBy('name')->select('id', 'name')->get()->toArray();

        $user_branches = BranchesUsers::where('users_id', $record->id)->pluck('branches_id')->toArray();

        $html = (string)view('admin.user.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'provinces' => $provinces,
            'roles' => $roles,
            'user_roles' => $user_roles,
            'branches' => $branches,
            'user_branches' => $user_branches,
            'activity_status' => $activity_status,
            'permission' => $this->permission,
            'dataSourceSystem' => Permissions::renderPermissions( config('permissions.systems'), $models ),
            'dataSourceGoods' => Permissions::renderPermissions( config('permissions.goods'), $models ),
            'dataSourcePartners' => Permissions::renderPermissions( config('permissions.partners'), $models ),
            'dataSourceDeals' => Permissions::renderPermissions( config('permissions.deals'), $models )
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreUser $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new User();
        }else{
            $record = User::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $params = $request->all();
        $params['birthday'] = ($params['birthday']) ? date("Y-m-d", strtotime($params['birthday'])) : null;

        if($params['thumbnail']){
            $this->updateFileRecord($params['thumbnail'], $record->image, 'users');
        }

        $params['image'] = $params['thumbnail'];
        $record->fill($params);

        if( strlen($request->password) < 60 ){
            $record->password = Hash::make($request->password);
        }

        if($record->save()){
            $record->syncRoles($request->roles);

            //Update data table branches_users
            BranchesUsers::where('users_id', $record->id)->forceDelete();
            if($request->branches){
                foreach ($request->branches as $item) {
                    BranchesUsers::create([
                        'users_id' => $record->id,
                        'branches_id' => $item
                    ]);
                }
            }

            $permissions = '';
            if(!empty($request->permissions)){
                self::updatePermission($record, $request->permissions);
            }
        }

        return response()->json(['st' => 200, 'data' => $request->permissions]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, User::find($id))) return response()->json(['st' => 403]);

        $st = User::find($id)->delete();
        return response()->json(['html' => $st]);
    }

    //

    public function updatePermission ($record, $str_permissions) {
        $permissions = explode(',', $str_permissions);

        foreach($permissions as $name_permission){
            $permission = Permission::where('name', $name_permission)->first();
            if(!$permission){
                $permission = Permission::create(['name' => $name_permission]);
            }
        }

        $record->syncPermissions($permissions);
    }
}
