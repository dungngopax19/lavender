<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;

use App\User;
use App\Helpers\Product as ProductHelper;
use App\Models\Products;
use App\Models\ProductTypes;
use App\Models\ProductCategories;
use App\Http\Requests\StoreProduct;
use App\Models\Branches;
use App\Models\BranchesProducts;
use App\Models\Attributes;
use App\Models\AttributesProducts;
use App\Models\ProductsComponents;
use App\Helpers\Permissions;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public $title = 'sản phẩm';
    public $mod = 'product';
    public $permission = 'hang-hoa-san-pham-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;
        $records = Products::search($request)->paginate($items_per_page);

        foreach (Input::except('page') as $input => $value){
            $records->appends($input, $value);
        }

        $branchActive = json_decode($_COOKIE['branch_active']);
        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);
        $is_permission_import = Permissions::hasAllPermission([$this->permission. 'xuat-file']);

        return view('admin.product.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'branchActive' => $branchActive,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create,
            'is_permission_import' => $is_permission_import
        ]);
    }

    public function show(Request $request, $id){
        $branches = Branches::orderBy('name')->get();
        $branchActive = json_decode($_COOKIE['branch_active']);

        if(count($branches) == 0){
            return response()->json(['html' => '']);
        }

        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Products();
        }else{
            $record = Products::find($id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $productTypes = ProductTypes::orderBy('name')->select('id', 'name')->get()->toArray();
        $productCategories = ProductCategories::where('parent_id', 0)
            ->with('childrens')
            ->select('id', 'parent_id', 'name')
            ->get()->toArray();

        //Attributes
        $attributes = Attributes::where('parent_id', 0)->orderBy('name')->get();
        $attributesSelected = [];
        if($record->attributes()){
            $attributesSelected = $record->attributes()->pluck('attributes.id')->toArray();
        }

        //Check product components
        $components = null;
        if($record->id > 0){
            $components = ProductsComponents::with('products')->where('products_id_parent', $record->id)->get();
            if(count($components) > 0){
                $components = $components->map(function ($item) {
                    $item->product = $item->products;

                    $item->canBeSold = 0;
                    if($item->qty > 0){
                        $item->canBeSold = max(intval(floor($item->products->inventory_number / $item->qty)), 0);
                    }
                    return $item;
                });
            }
        }

        $html = (string)view('admin.product.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'productTypes' => $productTypes,
            'productCategories' => $productCategories,
            'branches' => $branches,
            'attributes' => $attributes,
            'attributesSelected' => $attributesSelected,
            'productsComponents' => $components,
            'branchActive' => $branchActive,
            'permission' => $this->permission,
            'type_action' => empty($request->type_action) ? '' : $request->type_action
        ]);
        return response()->json(['html' => $html]);
    }

    public function update(StoreProduct $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Products();
        }else{
            $record = Products::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $params = $request->all();

        if( empty($params['link_thumbnail']) ){
            $this->updateFileRecord($params['thumbnail'], $record->thumbnail, 'products');
        }else{
            $params['thumbnail'] = $params['link_thumbnail'];
        }

        $record->fill($params);

        if($record->save()){
            $this->saveBranchesProducts($record->id);
            $this->saveAttributesProducts($record->id);
            $this->saveComponentsProducts($record->product_types_id, $record->id);
            ProductHelper::updateInventoryNumber($record->product_types_id, $record->id);
            ProductHelper::synsInventoryProductLikeCode($record->code);
            return response()->json(['status' => 200]);
        }

        return response()->json(['status' => 500]);
    }

    public function saveBranchesProducts($productId){
        BranchesProducts::where( 'products_id', $productId )->forcedelete();

        $inBranches = (array)request()->in_branch;
        $inventories = (array)request()->inventory;
        $minInventories = (array)request()->min_inventory;

        foreach(request()->branches_id as $i => $branches_id){
            if(isset($inventories[$i]) && isset($minInventories[$i])){
                $record = new BranchesProducts();
                $record->branches_id = $branches_id;
                $record->products_id = $productId;
                $record->in_branch = $inBranches[$i];
                $record->inventory = $inventories[$i];
                $record->min_inventory = $minInventories[$i];
                $record->save();
            }
        }
    }

    public function saveAttributesProducts($productId){
        AttributesProducts::where( 'products_id', $productId )->forcedelete();

        //Save attributes child
        $attrsChildId = (array)request()->attrs_child_id;
        if($attrsChildId){
            foreach ($attrsChildId as $item) {
                $record = new AttributesProducts();
                $record->attributes_id = $item;
                $record->products_id = $productId;
                $record->save();
            }
        }

        //Add new attributes more
        $attrsMore = (array)request()->attrs_more;
        $attrsID = (array)request()->attrs_id;
        if($attrsMore){
            foreach ($attrsMore as $k => $item) {
                if(!empty($item) && isset($attrsID[$k])){
                    $data = explode(',', $item);
                    foreach ($data as $attr) {
                        $attr = trim($attr);
                        $chk = Attributes::where('name', $attr)->where('parent_id', $attrsID[$k])->first();
                        if(!$chk){
                            $chk = new Attributes();
                            $chk->parent_id = $attrsID[$k];
                            $chk->name = $attr;
                            $chk->save();
                        }

                        $record = new AttributesProducts();
                        $record->attributes_id = $chk->id;
                        $record->products_id = $productId;
                        $record->save();
                    }
                }
            }
        }
    }

    public function saveComponentsProducts($product_types_id, $productId){
        ProductsComponents::where( 'products_id_parent', $productId )->forcedelete();

        if($product_types_id == 1){//Combo - Đóng gói
            $ids = (array)request()->component_ids;
            $amounts = (array)request()->component_amount;

            if(count($ids) > 0 && count($amounts) > 0 ){
                foreach($ids as $i => $id){
                    if(isset($amounts[$i]) && ( $productId != $id ) ){
                        $record = new ProductsComponents();
                        $record->fill([
                            'products_id_parent' => $productId,
                            'products_id' => $id,
                            'qty' => $amounts[$i]
                        ]);
                        $record->save();
                    }
                }
            }
        }
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, User::find($id))) return response()->json(['st' => 403]);

        $record = Products::find($id);

        if($record->delete()){
            return response()->json(['status' => 200]);
        }

        return response()->json(['status' => 500]);
    }

    public function search(Request $request){
        $q = trim($request->q);

        if(is_array($request->id)){
            $ids = $request->id;
        }else{
            $ids = explode(',', $request->id);
        }

        $records = Products::whereNotIn('id', $ids )
                ->where(function($query) use ($q){
                    $query->where('code', 'like', '%'.$q.'%')->orWhere('name', 'like', '%'.$q.'%');
                })
                ->select('id', 'code', 'name as text')->limit(20)->get()->toArray();

        return response()->json(['records' => $records]);
    }

    public function searchInCart(Request $request){
        $q = Str::slug(trim($request->q));

        $records = Products::where(function($query) use ($q){
                $query->where('code', 'like', '%'.$q.'%')->orWhere('id', 'like', '%'.$q.'%')->orWhere('name', 'like', '%'.$q.'%');
            })
            ->select('id', 'code')->limit(500)
            ->get()->toArray();

        return response()->json(['records' => $records]);
    }

    public function listComponents(Request $request){
        $records = $request->data;

        if(!$records){
            return response()->json(['html' => '']);
        }

        foreach ($records as $i => $item) {
            $records[$i]['detail'] = Products::find($item['id']);
        }

        $html = (string)view('admin.product.list-components', compact('records'));

        return response()->json(['html' => $html]);
    }

    //Import products
    public function import(Request $request){
        $html = (string)view('admin.product.import');
        return response()->json(['html' => $html]);
    }

    public function submitImport(Request $request){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 18000);

        $limit = 100;
        $page = $request->page ?? 1;

        $begin = ($page - 1) * $limit;
        $end = $begin + $limit;

        if ($page > 1 || $request->hasFile('file')) {

            $msgError = [];

            if($page == 1){
                //Upload file
                $objFile = $request->file('file');
            
                $fileName = $objFile->getClientOriginalName();
                $newFileName = uniqid() . '.' . \File::extension($fileName);
    
                $path = $objFile->storeAs('tmp', $newFileName);
            }else{
                $path = $request->path;
            }

            //Import data
            $records = Excel::load(Storage::path($path), function($reader){})->limit(2000)->get()->toArray();
            $totalRecords = count($records);
            
            if($records){
                
                $stop = min($end, $totalRecords);

                for($i = $begin; $i < $stop; $i = $i + 1){
                    $item = (object)$records[$i];

                    if ( empty($item->ma_hang) ) {
                        continue;
                    }

                    $ma_hang = trim($item->ma_hang);
                    $ten_hang_hoa = trim($item->ten_hang_hoa);

                    $record = Products::where('code' , $ma_hang)->first();

                    // If there exists an product code
                    if(!empty($record->id)){
                        if($request->like_code == 'skip'){
                            continue;
                        }else{

                            //Delete table attributes_products
                            AttributesProducts::where('products_id', $record->id)->forceDelete();

                            //Delete table branches_products
                            BranchesProducts::where('products_id', $record->id)->forceDelete();

                            //Delete table products_components
                            ProductsComponents::where('products_id', $record->id)->forceDelete();

                            //Delete table Products
                            Products::where('code', $ma_hang)->forceDelete();

                            $record = new Products();
                        }
                    }else{
                        $record = new Products();   
                    }

                    $record->code = $ma_hang;
                    $record->name = $ten_hang_hoa;

                    //Loại sp
                    $loai_hang = trim($item->loai_hang);
                    if (empty($loai_hang)) {
                        $record->product_types_id = null;
                    }else{
                        $productTypes = ProductTypes::firstOrCreate(['name' => $loai_hang]);
                        $record->product_types_id = $productTypes->id;
                    }

                    //Nhóm sp
                    $nhom_hang2_cap = isset($item->nhom_hang2_cap) ? trim($item->nhom_hang2_cap) : trim($item->nhom_hang3_cap);
                    $nhomHang = explode('>>', $nhom_hang2_cap);
                    if(count($nhomHang) < 3 && !empty($nhom_hang2_cap) ){
                        if(count($nhomHang) == 2){
                            $cParent = ProductCategories::firstOrCreate(['name' => $nhomHang[0], 'parent_id' => 0]);
                            $cChild = ProductCategories::firstOrCreate(['name' => $nhomHang[1], 'parent_id' => $cParent->id]);
                        }else{
                            $cChild = ProductCategories::firstOrCreate(['name' => $nhomHang[0], 'parent_id' => 0]);
                        }
                        $record->product_categories_id = $cChild->id;
                    }else{
                        $record->product_categories_id = NULL;
                    }

                    //Giá bán
                    $record->price = $item->gia_von;
                    $record->price_sell = $item->gia_ban;

                    //Thumbnail
                    $record->thumbnail = isset($item->hinh_anh) ? 'https://drive.google.com/uc?export=view&id=' . trim($item->hinh_anh) : null;

                    $record->direct_selling = ($item->duoc_ban_truc_tiep == 'Được bán trực tiếp') ? 1 : 2;
                    $record->activity_status = (isset($item->trang_thai) && $item->trang_thai == 'Ngừng hoạt động') ? 1 : 2;
                    $record->sample_notes = isset($item->mau_ghi_chu) ? trim($item->mau_ghi_chu) : null;
                    $record->description = isset($item->mo_ta) ? trim($item->mo_ta) : null;

                    $record->save();

                    //Thuoc tinh
                    AttributesProducts::where( 'products_id', $record->id )->forcedelete();
                    if(!empty($item->thuoc_tinh)){
                        $this->importAttrProduct($record->id, $item->thuoc_tinh);
                    }

                    //Sản phẩm thành phần
                    ProductsComponents::where( 'products_id_parent', $record->id )->forcedelete();
                    if(!empty($item->ma_hang_thanh_phan)){
                        $this->importComponentProduct($record->id, $item->ma_hang_thanh_phan);
                    }

                    //Kho hàng
                    if(in_array( $record->cloth_template, ['1.6', '2.5']) ){
                        $record->cloth_template = $item->kho_vai;
                        $record->meter_number = intval($item->so_luong_met_vai);

                        $record->number_of_sets = 0;
                        if($record->meter_number >= 0){
                            if($record->cloth_template == '1.6'){
                                $record->number_of_sets = floor( $record->meter_number / 14 );
                            }else{
                                $record->number_of_sets = floor( $record->meter_number / 11 );
                            }
                        }
                    }else{
                        $record->cloth_template = '';
                        $record->meter_number = 0;
                        $record->number_of_sets = 0;
                    }

                    $this->importInventoryProduct($record->id, $item);
                    ProductHelper::updateInventoryNumber($record->product_types_id, $record->id);
                }
            }

            //Delete file upload excel
            if($end >= $totalRecords){
                Storage::delete($path);
                $stop = true;
            }else{
                $stop = false;
            }

            return response()->json([ 'status' => 200, 'stop' => $stop, 'path' => $path, 'msg' => $msgError, 'page' => $page + 1]);
        }

        return response()->json(['status' => 500]);
    }

    public function importAttrProduct($productId, $attrText){
        $data = [];

        if(!empty($attrText)){
            $atts1 = explode('|', $attrText);
            foreach($atts1 as $item1){
                $atts2 = explode(':', $item1);
                if(count($atts2) > 1){
                    $attParent = Attributes::firstOrCreate([
                        'parent_id' => 0,
                        'name' => $this->cleanData($atts2[0])
                    ]);

                    $attChild = Attributes::firstOrCreate([
                        'parent_id' => $attParent->id,
                        'name' => $this->cleanData($atts2[1])
                    ]);

                    $data[] = ['products_id' => $productId, 'attributes_id' => $attChild->id];
                }
            }
        }

        if(!empty($data)){
            AttributesProducts::insert($data);
        }

    }

    public function importComponentProduct($productId, $compText){
        $comps = explode('|', $compText);
        foreach ($comps as $item) {
            $item = str_replace(')', '', $item);
            $comps2 = explode('(', $item);

            $chk = Products::where('code', $comps2[0])->first();
            if($chk){
                $qty = isset($comps2[1]) ? max(intval($comps2[1]), 0) : 1;
                ProductsComponents::insert(['products_id_parent' => $productId, 'products_id' => $chk->id, 'qty' => $qty]);
            }
        }
    }

    public function importInventoryProduct($productId, $item){
        BranchesProducts::where('products_id', $productId)->forcedelete();
        $branches = Branches::orderBy('name')->get();
        if(count($branches) > 0){
            foreach ($branches as $branch) {
                $key = trim($branch->code);

                $key = str_replace("-", "_", $key);

                //$item->$key = 100;
                
                if(isset($item->$key)){
                    $inventory = intval($item->$key);
                    
                    if($inventory > 0){
                        $branchesProducts = new BranchesProducts;
                        $branchesProducts->fill([
                            'branches_id' => $branch->id,
                            'products_id' => $productId,
                            'inventory' => $inventory,
                            'min_inventory' => $branch->min_inventory,
                        ]);
                        $branchesProducts->save();
                    }
                }
                
            }
        }
    }

    //Export products
    public function export(Request $request){
        $records = Products::search($request)->get();
        $branches = Branches::orderBy('name')->get();
        $attributes = Attributes::where('parent_id', 0)->orderBy('name')->get();

        Excel::create('Product-export', function($excel) use ($records, $branches, $attributes) {
            $excel->sheet('Sheet1', function($sheet) use ($records, $branches, $attributes) {
                $sheet->freezeFirstRow();
                $sheet->loadView('admin.product.export', compact('records', 'branches', 'attributes'));
            });
        })->export('xlsx');
    }
}
