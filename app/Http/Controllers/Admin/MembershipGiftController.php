<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MembershipGifts;
use App\Models\Memberships;
use App\Http\Requests\StoreMembershipGift;
use App\Models\Products;
use App\Helpers\Permissions;

class MembershipGiftController extends Controller
{
    public $title = 'Khuyến mãi';
    public $mod = 'membership-gift';
    public $permission = 'he-thong-khuyen-mai-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = MembershipGifts::select('membership_id')->groupBy('membership_id')->paginate($items_per_page);
        foreach($records as $key=>$item){
            $membership_discount = MembershipGifts::where('membership_id', $item['membership_id'])->where('type', 1)->first();
            $records[$key]['value'] = ($membership_discount) ? $membership_discount->value : null;
            $records[$key]['unit'] = ($membership_discount) ? $membership_discount->unit : null;

            $records[$key]['start_date'] = ($membership_discount) ? $membership_discount->start_date : null;
            $records[$key]['end_date'] = ($membership_discount) ? $membership_discount->end_date : null;
            $records[$key]['is_expires'] = ($membership_discount) ? $membership_discount->is_expires : null;

            $records[$key]['activity_status'] = ($membership_discount) ? $membership_discount->activity_status : null;
            $records[$key]['products'] = MembershipGifts::where('membership_id', $item['membership_id'])->where('type', 2)->get();
        }

        return view('admin.membership-gift.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'permission' => $this->permission,
        ]);
    }

    public function show($id){
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new MembershipGifts();
            $title = 'Thêm mới';
            $product_ids = [];
        }else{
            $title = "Cập nhật";
            $record= MembershipGifts::where('membership_id', $id)->where('type', 1)->first();
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
            $product_ids = MembershipGifts::where('membership_id', $id)->where('type', 2)->pluck('value')->toArray();
        }

        $products_gift = Products::where('is_gift', 1)->orderBy('name')->select('id', 'name')->get()->toArray();
        $memberships = Memberships::orderBy('name')->select('id', 'name')->get()->toArray();
        $activity_status = (array)config('constants.activity_status');
        unset( $activity_status[0] );

        $html = (string)view('admin.membership-gift.edit', [
            'record' => $record,
            'title' => $this->title,
            'mod' => $this->mod,
            'activity_status' => $activity_status,
            'products_gift' => $products_gift,
            'memberships' => $memberships,
            'product_ids' => $product_ids,
        ]);
        return response()->json(['html' => $html]);
    }

    public function update(StoreMembershipGift $request){
        // if(empty($request->id)){
        //     if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
        //     $record = new Surcharges();
        // }else{
        //     $record = Surcharges::find($request->id);
        //     if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        // }
        MembershipGifts::where('membership_id', $request->membership_id )->forcedelete();
        $record = new MembershipGifts();

        $params = $request->all();

        $params['value'] = $params['value'];
        $record->is_expires = empty($params['is_expires']) ? 0 : intval($params['is_expires']);

        if( $record->is_expires ){
            $params['start_date'] = '2000-01-01';
            $params['end_date'] = '3000-12-30';
        }

        $record->fill($params);
        $record->save();

        if(isset($params['product_ids']) && count($params['product_ids'])){
            $this->saveProductMembership($record);
        }

        return response()->json(['st' => 200, 'data' => $record]);
    }

    public function destroy($id){
        // $st = MembershipGifts::find($id)->delete();
        // return response()->json(['html' => $st]);
    }

    public function saveProductMembership($request){
        foreach(request()->product_ids as $i => $product_id){
            $record = new MembershipGifts();
            $record->membership_id = $request->membership_id;
            $record->type = 2;
            $record->value = $product_id;
            $record->start_date = $request->start_date;
            $record->end_date = $request->end_date;
            $record->is_expires = $request->is_expires;
            $record->save();
        }
    }

}
