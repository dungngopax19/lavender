<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\StoreReceipts;
use App\Http\Controllers\Controller;
use App\Models\Orders;
use App\Models\Customers;
use App\Models\Receipts;
use App\User;
use Auth;
use App\Helpers\Permissions;

class ReceiptsController extends Controller
{
    public $title = 'Phiếu thu';
    public $mod = 'receipt';
    public $permission = 'giao-dich-phieu-thu-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;
        $records = Receipts::search($request)->paginate($items_per_page);

        foreach (Input::except('page') as $input => $value){
            $records->appends($input, $value);
        }

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.receipt.index', [
            'title' => $this->title,
            'mod' => $this->mod,
            'records' => $records,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create
        ]);
    }

    public function show(Request $request, $id){
        $ordersData = array();
        $customersData = array();
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);

            $record = new Receipts();
            $title = 'Thêm mới';
        }else{
            $record = Receipts::find($id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);

            $title = "Cập nhật";
            $ordersData[] = array('id' => $record->order->id, 'name' => $record->order->code);
            $customersData[] = array('id' => $record->customer->id, 'name' => $record->customer->name);
        }

        $html = (string)view('admin.receipt.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'ordersData' => $ordersData,
            'customersData' => $customersData,
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreReceipts $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Receipts();
        }else{
            $record = Receipts::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }
        $params = $request->all();
        if (isset($params['price'])) {
            $params['price'] = str_replace(',', '', $params['price']);
        }
        $record->fill($params);
        $record->users_id = Auth::user()->id;

        if($record->save()){

            $count = Receipts::where('orders_id', $record->orders_id)->count();
            $record->code = 'PT-' . $record->orders_id . '-' . $count;

            $record->save();
            return response()->json(['status' => 200]);
        }

        return response()->json(['status' => 500]);
    }


}
