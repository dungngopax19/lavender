<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Common;
use App\Helpers\Product as ProductHelper;
use App\Helpers\Invoice as InvoiceHelper;
use Illuminate\Support\Facades\Storage;
use Auth;
use Melihovv\ShoppingCart\Facades\ShoppingCart as Cart;
use App\User;
use App\Models\Orders;
use App\Models\Products;
use App\Models\Branches;
use App\Models\Provinces;
use App\Models\Suppliers;
use App\Models\PartnerDeliveries;
use App\Models\TypeServices;
use App\Models\ShipmentStatus;
use App\Models\OrderStatus;
use App\Models\Receipts;
use App\Models\OrderDetail;
use App\Helpers\Permissions;

class CartController extends Controller
{
    public $title = 'Giỏ hàng';
    public $mod = 'cart';

    public function addToCart(Request $request){
        //Get branch active
        $branchActive = json_decode($_COOKIE['branch_active']);

        $userId = Auth::user()->id;
        $type = $request->type;
        $typeTxt = ($type == 'orders') ? 'phiếu đặt hàng' : 'Đơn hàng';

        $qty = intval($request->qty);
        $id = intval($request->id);
        $customPrice = intval($request->customPrice);
        $cartNumber = intval($request->cartNumber);

        $nameCart = $userId . '_' . $type . '_' . $branchActive->id . '_' . $cartNumber;

        $discount = empty($request->discount) ? 0 : intval($request->discount);
        $discountUnit = in_array($request->discountUnit, ['vnd', 'percent', '%']) ? $request->discountUnit : 'vnd';

        //Check product exists
        $product = Products::find($id);
        if(!$product){
            return response()->json([
                'type' => 'danger',
                'title' => 'Thêm sản phẩm vào ' . $typeTxt,
                'msg' => 'Sản phẩm này không khả dụng'
            ]);
        }

        //Compare customPrice with priceDiscount
        if($qty > 0 && $request->uniqueId != ''){
            if($customPrice < 0){
                return response()->json(['type' => 'danger','title' => 'Đơn giá không hợp lệ','msg' => '']);

            }elseif($discount > 100 && $discountUnit != 'vnd'){
                return response()->json(['type' => 'danger','title' => 'Giảm giá không hợp lệ','msg' => '']);

            }else if($customPrice <= $discount){
                return response()->json(['type' => 'danger','title' => 'Đơn giá phải lớn hơn giảm giá','msg' => '']);
            }
        }

        //Calculate discount price
        if( $discountUnit == 'vnd' ){
            $priceDiscountItem = $customPrice - $discount;
        }else{
            $priceDiscountItem = $customPrice - $customPrice * $discount / 100;
        }

        $priceDiscountItem = max($priceDiscountItem, 0);

        //Get content cart
        Cart::restore($nameCart);
        $itemsCart = Cart::content();

        //Remove item cart
        if($qty == 0){
            if($itemsCart){
                Cart::remove($request->uniqueId);
                $msg = 'Đã xoá khỏi '. $typeTxt .'';
            }else{
                return response()->json([
                    'type' => 'danger',
                    'title' => $typeTxt . ' chưa có dữ liệu',
                    'msg' => ''
                ]);
            }
        }else{

            //Check can add/update to cart
            if($request->updateQty){
                $checkAddToCart = ProductHelper::checkAddToCart( $product, $qty, $type );

                if($checkAddToCart){//Update item cart

                    $itemCart = Cart::get($request->uniqueId);

                    if( $itemCart ){

                        $itemCart->price = $priceDiscountItem;
                        $itemCart->quantity = $qty;
                        $itemCart->options['discount'] = $discount;
                        $itemCart->options['discountUnit'] = $discountUnit;
                        $itemCart->options['price_sell'] = $product->price_sell;

                        $itemsCart->put($request->uniqueId, $itemCart);

                    }else{//Not yet product in cart

                        $cartItem = Cart::add($product->id, $product->name, $priceDiscountItem, $qty, [
                            'code' => $product->code,
                            'thumbnail' => $product->thumbnail_display,
                            'price_sell' => $product->price_sell,
                            'discount' => $discount,
                            'discountUnit' => $discountUnit
                        ]);
                    }

                    $msg = 'Đã cập nhật '. $typeTxt .' thành công';
                }else{
                    return response()->json([
                        'type' => 'danger',
                        'title' => 'Hiện tại sản phẩm này không thể thêm vào ' . $typeTxt,
                        'msg' => ''
                    ]);
                }
            }else{//Add to cart
                $itemCart = ProductHelper::getItemCartByProductId($product->id, $itemsCart);
                $addQty = $itemCart ? ($qty + $itemCart->quantity) : $qty;
                $checkAddToCart = ProductHelper::checkAddToCart( $product, $addQty, $type );

                if($checkAddToCart){
                    $cartItem = Cart::add($product->id, $product->name, $product->price_sell, $qty, [
                        'code' => $product->code,
                        'thumbnail' => $product->thumbnail_display,
                        'price_sell' => $product->price_sell,
                        'discount' => ProductHelper::discountByProductCats($product->id),
                        'discountUnit' => 'vnd'
                    ]);
                    $msg = 'Đã thêm vào '. $typeTxt .' thành công';
                }else{
                    return response()->json([
                        'type' => 'danger',
                        'title' => 'Hiện tại sản phẩm này không thể thêm vào ' . $typeTxt,
                        'msg' => ''
                    ]);
                }
            }

        }

        //Store items cart
        Cart::store($nameCart);

        $products = Products::select('id', 'code')->limit(500)->get();

        //Html cart hover
        $htmlCart = (string)view('admin.cart.list-hover', [
            'records' => Cart::content(),
            'totalPrice' => Cart::getTotal(),
            'cartNumber' => $cartNumber,
            'type' => $type,
            'typeTxt' => $typeTxt
        ]);

        //Html cart modal edit quanlity
        $htmlCartModal = (string)view('admin.components.cart.template-2', [
            'itemsCart' => Cart::content(),
            'type' => $type,
            'disabledEditCart' => '',
            'products' => $products,
        ]);

        $htmlCartPrint = (string)view('admin.components.cart.template-print', [
            'itemsCart' => Cart::content(),
            'type' => $type,
            'disabledEditCart' => ''
        ]);

        return response()->json([
            'type' => 'success',
            'title' => $product->name . '(' . $product->code . ')',
            'msg' => $msg,
            'items' => Cart::content(),
            'htmlCart' => $htmlCart,
            'htmlCartModal' => $htmlCartModal,
            'htmlCartPrint' => $htmlCartPrint,
            'totalPrice' => number_format(Cart::getTotal()),
        ]);
    }

    public function detailCart(Request $request){
        $userId = Auth::user()->id;

        $detailOrders = new Orders();
        $giftProducts = null;
        $isCopyCart = false;

        $cartNumber = intval($request->cartNumber);

        if(empty($request->orders_code)){
            $type = $request->type;
            $branchActive = json_decode($_COOKIE['branch_active']);
            Cart::restore($userId . '_' . $type . '_' . $branchActive->id . '_' . $cartNumber);
            $receipts = null;
        }else{

            //Check copy data cart
            if(strpos($request->orders_code, "copy_") !== false){
                $request->orders_code = str_replace('copy_', '', $request->orders_code);
                $isCopyCart = true;
            }

            $detailOrders = Orders::where('code', $request->orders_code)->first();
            if(empty($detailOrders)){
                return response()->json(['html' => 'Mã đơn hàng không hợp lệ']);
            }

            $type = ($detailOrders->is_invoices > 0) ? 'invoices' : 'orders';

            //Get info shopping cart
            Cart::restore($request->orders_code);
            $itemsCart = Cart::content();

            //Insert to shopping if not exists
            if($itemsCart->count() < 1){
                $ordersDetail = OrderDetail::where('orders_id', $detailOrders->id)->get();
                if(count($ordersDetail) > 0){
                    foreach ($ordersDetail as $itemOrderDetail) {
                        $itemOrderDetail->orders_id = $detailOrders->id;
                        $itemOrderDetail->ma_hoa_don = $detailOrders->code;
                        InvoiceHelper::insertShoppingCart($itemOrderDetail);
                    }
                    Cart::restore($request->orders_code);
                }
            }

            $branchActive = Branches::find($detailOrders->branches_id);
            $giftProducts = $detailOrders->orderDetail()->where('price', 0)->get();

            $receipts = Receipts::where('orders_id', $detailOrders->id)->get();
        }

        $typeTxt = ($type == 'orders') ? 'phiếu đặt hàng' : 'Đơn hàng';
        $users = User::get();

        $provinces = Provinces::with('childrens')->where('parent_id', 0)->orderBy('name')->select('id', 'name')->get()->toArray();
        $type_services = TypeServices::orderBy('name')->select('id', 'name')->get()->toArray();
        $partners = PartnerDeliveries::orderBy('name')->select('id', 'name')->get()->toArray();


        $permissionViewAllBranches = 'giao-dich-xem-tat-ca-cac-giao-dich';
        if(!Permissions::hasAllPermission([$permissionViewAllBranches])){
            $orderStatus = OrderStatus::where('type', $type)->whereNotIn('id', [10, 11])->select('id', 'name')->get();
            $shipment_status = ShipmentStatus::orderBy('name')->whereNotIn('id', [4])->select('id', 'name')->get();
        }else{
            $orderStatus = OrderStatus::where('type', $type)->select('id', 'name')->get();
            $shipment_status = ShipmentStatus::orderBy('name')->select('id', 'name')->get();
        }

        $products = Products::select('id', 'code')->limit(500)->get();

        //Check lavender online
        if(empty($branchActive)){
            $branchActive = (object)[
                'id' => null,
                'name' => 'Lavender Online',
                'address' => 'Lavender Online'
            ];
        }

        $html = (string)view('admin.cart.detail', [
            'itemsCart' => Cart::content(),
            'totalPrice' => Cart::getTotal(),
            'currentUser' => Auth::user(),
            'users' => $users,
            'products' => $products,
            'type' => $type,
            'typeTxt' => $typeTxt,
            'provinces' => $provinces,
            'type_services' => $type_services,
            'partners' => $partners,
            'shipment_status' => $shipment_status,
            'branchActive' => $branchActive,
            'request' => $request,
            'disabledEditCart' => (empty($request->orders_code) || $isCopyCart) ? '' : 'disabled',
            'detailOrders' => $detailOrders,
            'giftProducts' => $giftProducts,
            'orderStatus' => $orderStatus,
            'receipts' => $receipts,
            'isCopyCart' => $isCopyCart,
            'cartNumber' => $cartNumber
        ]);

        return response()->json(['html' => $html]);
    }

    public function clearCart(Request $request){
        $userId = Auth::user()->id;
        $type = $request->type;

        $branchActive = json_decode($_COOKIE['branch_active']);
        $cartNumber = intval($request->cartNumber);
        $nameCart = $userId . '_' . $type . '_' . $branchActive->id . '_' . $cartNumber;
        Cart::destroy($nameCart);

        $html = (string)view('admin.cart.list-hover', [
            'records' => null,
            'totalPrice' => 0,
            'cartNumber' => $cartNumber,
            'typeTxt' => ($type == 'orders') ? 'phiếu đặt hàng' : 'Đơn hàng'
        ]);

        return response()->json(['htmlCart' => $html]);
    }

    public function updateDiscountNote(Request $request){
        $orderDetail = OrderDetail::where('orders_id', $request->orders_id)->where('products_id', $request->products_id)->first();
        
        if($orderDetail){
            $orderDetail->discount_note = $request->discountNote;
            if($orderDetail->save()){
                
                $orderRecord = Orders::find($request->orders_id);

                if($orderRecord){
                    Cart::restore($orderRecord->code);
                    $itemsCart = Cart::content();
        
                    $itemCart = Cart::get($request->uniqueId);
                    $itemCart->options['discount_note'] = $request->discountNote;
                    $itemsCart->put($request->uniqueId, $itemCart);
        
                    Cart::store($orderRecord->code);
                }
            }
        }else{
            $branchActive = json_decode($_COOKIE['branch_active']);
            $userId = Auth::user()->id;
            $type = $request->type;

            $cartNumber = intval($request->cartNumber);
            Cart::restore($userId . '_' . $type . '_' . $branchActive->id . '_' . $cartNumber);
            $itemsCart = Cart::content();

            $itemCart = Cart::get($request->uniqueId);
            $itemCart->options['discount_note'] = $request->discountNote;
            $itemsCart->put($request->uniqueId, $itemCart);

            Cart::store($userId . '_' . $type . '_' . $branchActive->id . '_' . $cartNumber);
        }

        return response()->json(['status' => 200, 'msg' => 'Đã cập nhật ghi chú sản phẩm thành công', 'type' => 'success']);
    }
}
