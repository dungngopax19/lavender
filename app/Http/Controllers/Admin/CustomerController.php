<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customers;
use App\Models\CustomerTypes;
use App\Http\Requests\StoreCustomer;
use App\Helpers\Common;
use App\Helpers\Customer as CustomerHelper;
use App\Helpers\Product as ProductHelper;
use App\Models\Provinces;
use App\Models\Orders;
use App\Models\MembershipGifts;
use App\Models\Memberships;
use App\Helpers\DateHelper;
use App\Helpers\Permissions;

use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;

class CustomerController extends Controller
{
    public $title = 'Khách hàng';
    public $mod = 'customer';
    public $permission = 'doi-tac-khach-hang-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = Customers::search($request)->paginate($items_per_page);

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);
        $is_permission_import = Permissions::hasAllPermission([$this->permission. 'import']);

        return view('admin.customer.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create,
            'is_permission_import' => $is_permission_import
        ]);
    }

    public function show(Request $request, $id){
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);

            $record = new Customers();
            $title = 'Thêm mới';
        }else{
            $record = Customers::find($id);
            $title = "Cập nhật";
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);

        }

        $provinces = Provinces::with('childrens')->where('parent_id', 0)->orderBy('name')->select('id', 'name')->get()->toArray();
        $memberships = Memberships::orderBy('name')->select('id', 'name')->get()->toArray();
        $activity_status = (array)config('constants.activity_status');
        unset( $activity_status[0] );

        $html = (string)view('admin.customer.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'provinces' => $provinces,
            'memberships' => $memberships,
            'activity_status' => $activity_status,
            'classBtnSubmit' => $request->classBtnSubmit,
            'permission' => $this->permission,
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreCustomer $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Customers();
        }else{
            $record = Customers::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $params = $request->all();

        if($params['thumbnail']){
            $this->updateFileRecord($params['thumbnail'], $record->image, 'customers');
        }

        $params['image'] = $params['thumbnail'];
        $params['birthday'] = ($params['birthday']) ? date("Y-m-d", strtotime($params['birthday'])) : null;
        $params['company'] = ($params['role'] == '2') ? $params['company'] : null;

        $record->fill($params);

        if($record->save()){
            $record->code = 'KH' . $record->id_display;
            $record->save();

            return response()->json(['st' => 200, 'data' => $record]);
        }
        return response()->json(['status' => 500]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, Customers::find($id))) return response()->json(['st' => 403]);

        $st = Customers::find($id)->delete();
        return response()->json(['html' => $st]);
    }

    public function search(Request $request){
        $q = trim($request->q);

        $records = Customers::where(function($query) use ($q){
                    $query->where('code', 'like', '%'.$q.'%')
                    ->orWhere('name', 'like', '%'.$q.'%')
                    ->orWhere('phone', 'like', '%'.$q.'%')
                    ->orWhere('address', 'like', '%'.$q.'%');
                })
                ->select('id', 'code', 'phone', 'name as text')
                ->orderBy('id', 'desc')->limit(100)->get()->toArray();

        return response()->json(['records' => $records]);
    }

    public function getInfo(Request $request){

        $customer = Customers::find($request->id);

        if(!$customer){
            return response()->json(['status' => '500', 'msg' => 'ID Khách hàng không hợp lệ']);
        }

        $customer->membership = CustomerHelper::checkLevel($customer->id);
        $customer->membership_id = str_replace('level-', '', $customer->membership);
        $customer->order_last = Orders::where('customers_id', $customer->id)->where('is_invoices', '>', 0)->orderby('id', 'DESC')->first();
        $customer->totalOrders = Orders::where('customers_id', $customer->id)->where('is_invoices', '>', 0)->count();

        $now = DateHelper::currentDate('Y-m-d');

        $sql = MembershipGifts::with('product')
            ->whereHas('membership', function ($query) use ($customer) {
                $query->where('code', $customer->membership);
            })
            ->where('start_date', '<=', $now)->where('end_date', '>=', $now);

        $customer->discountPrice = MembershipGifts::where('membership_id', $customer->membership_id)->where('type', 1)
                                        ->where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $customer->giftsProducts = MembershipGifts::where('membership_id', $customer->membership_id)->where('type', 2)
                                        ->where('start_date', '<=', $now)->where('end_date', '>=', $now)->get();

        $ordersDetail = Orders::where('id', $request->orders_id)->first();
        if($ordersDetail){
            $customer->coupon = $ordersDetail->coupon;
            $customer->discount_coupon = $ordersDetail->discount_coupon;
        }else{
            $customer->coupon = '';
            $customer->discount_coupon = 0;
        }

        $info_extra_customer = (string)view('admin.cart.info_extra_customer', ['customer'=> $customer, 'ordersDetail' => $ordersDetail]);
        $history_order_customer = (string)view('admin.cart.order-histories', ['customer'=> $customer]);

        return response()->json([
            'status' => 200,
            'customer' => $customer,
            'info_extra_customer' => $info_extra_customer,
            'history_order_customer' => $history_order_customer,
            'ordersDetail' => $ordersDetail
        ]);
    }

    public function getCustomer(Request $request){
        $order = !empty($request->id) ? Orders::find($request->id) : new Orders;
        $customer = Customers::find($order->customers_id);
        return response()->json([
            'status' => 200,
            'customer' => $customer,
        ]);
    }

    //Import customers
    public function import(Request $request){
        $html = (string)view('admin.customer.import');
        return response()->json(['html' => $html]);
    }

    public function submitImport(Request $request){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 18000);

        if ($request->hasFile('file')) {

            //Upload file
            $objFile = $request->file('file');

            $fileName = $objFile->getClientOriginalName();
            $newFileName = uniqid() . '.' . \File::extension($fileName);

            $path = $objFile->storeAs('tmp', $newFileName);

            //Import data
            $records = Excel::load(Storage::path($path), function($reader){})->limitRows(50000)->formatDates(true, 'Y-m-d H:i:s')->get()->toArray();

            if($records){
                foreach($records as $item){
                    $item = (object)$item;

                    if(isset($item->ma_khach_hang)){
                        $ma_khach_hang = trim($item->ma_khach_hang);
                        $ten_khach_hang = trim($item->ten_khach_hang);

                        if ( empty($ma_khach_hang) || empty($ten_khach_hang) ) {
                            continue;
                        }

                        $record = Customers::where('code' , $ma_khach_hang)->first();

                        if(empty($record->id)){
                            $record = new Customers();

                            $record->code = $ma_khach_hang;
                            $record->name = $ten_khach_hang;
                        }

                        $record->role = (array_key_exists($item->loai_khach, Customers::roles)) ? Customers::roles["$item->loai_khach"] : 1;
                        $record->company = $item->cong_ty;
                        $record->tax_code = $item->ma_so_thue;
                        $record->phone = (strlen($item->dien_thoai) > 16) ? null : $item->dien_thoai;
                        $record->gender = (array_key_exists($item->gioi_tinh, Customers::genders)) ? Customers::genders["$item->gioi_tinh"] : null;
                        $record->birthday = ($item->ngay_sinh) ? date('Y-m-d', strtotime($item->ngay_sinh)) : null;
                        $record->email = $item->email;
                        $record->province_id = CustomerHelper::getProvinceIdByName($item->khu_vuc);
                        $record->address = $item->dia_chi;
                        $record->text = $item->ghi_chu;
                        $record->activity_status = $item->trang_thai;
                        $record->create_user = CustomerHelper::getUserByFullName($item->nguoi_tao);
                        $record->created_at = $item->ngay_tao;
                        $record->update_user = CustomerHelper::getUserByFullName($item->nguoi_tao);

                        $record->save();

                        CustomerHelper::synsdataCustomersInvoices($record->customers_id);
                    }

                }
            }

            //Delete file upload excel
            Storage::delete($path);

            return response()->json([ 'status' => 200]);
        }

        return response()->json(['status' => 500]);
    }

    //Export customers
    public function export(Request $request){
        $records = Customers::search($request)->get();

        Excel::create('Customer-export', function($excel) use ($records) {
            $excel->sheet('Sheet1', function($sheet) use ($records) {
                $sheet->freezeFirstRow();
                $sheet->loadView('admin.customer.export', compact('records'));
            });
        })->export('xlsx');
    }

    public function updateLevelCustomer(Request $request){
        $customers = Customers::offset(9000)->limit(1000)->get();

        foreach ($customers as $customer) {
            CustomerHelper::updateLevel($customer->id);
        }
        
    }
}
