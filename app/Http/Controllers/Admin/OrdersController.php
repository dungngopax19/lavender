<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

use App\Helpers\Invoice as InvoiceHelper;
use App\Helpers\Product as ProductHelper;
use App\Http\Requests\StoreOrders;
use App\Http\Requests\StoreShipOrders;
use App\Http\Controllers\Controller;
use App\Models\OrderStatus;
use App\Models\Orders;
use App\Models\OrderDetail;
use App\Models\ShipmentDetails;
use App\Models\Products;
use App\Models\ProductTypes;
use App\Models\ProductCategories;
use App\Models\Branches;
use App\Models\Receipts;
use App\Models\BranchesUsers;
use App\Models\BranchesProducts;
use App\Models\Attributes;
use App\Models\AttributesProducts;
use App\Models\TypeServices;
use App\Models\PartnerDeliveries;
use App\User;
use App\Helpers\Order\Status;
use App\Helpers\DateHelper;
use App\Helpers\ActivityLog as LogHelper;
use Auth;
use Melihovv\ShoppingCart\Facades\ShoppingCart as Cart;
use App\Helpers\Permissions;
use App\Helpers\Customer as CustomerHelper;

class OrdersController extends Controller
{
    public $title = 'Phiếu đặt hàng';
    public $mod = 'order';
    public $permission = 'giao-dich-hoa-don-';

    //Tao moi / update don hang hoac hoa don
    public function create(StoreOrders $request){
        $type = ($request->is_invoices == 0) ? 'orders' : 'invoices';
        $title = ($type == 'orders') ? ' đặt hàng' : ' tạo đơn hàng';
        $titleReturn = ($type == 'orders') ? ' Phiếu đặt hàng' : ' Đơn hàng';
        $cartNumber = intval($request->cartNumber);

        //Check update or create orders/invoice
        if(!$request->isCopyCart && $request->orders_id){
            $logs = [];
            $record = Orders::find($request->orders_id);

            $record->highlight = $request->highlight;

            //Cập nhật trang thái đơn hàng + Trang thái vận chuyển
            if(!empty($request->orders_status_id)){

                if($record->orders_status_id != $request->orders_status_id){
                    //Activity log
                    $logs[] = 'Đơn hàng đã cập nhật tình trạng ' . LogHelper::changeOrdersStatus($record->orders_status_id, $request->orders_status_id) . ' bởi';

                    $record->orders_status_id = $request->orders_status_id;
                }

                if($record->shipment_status_id != $request->shipment_status_id){
                    //Activity log
                    $logs[] = 'Đơn hàng đã cập nhật trạng thái giao hàng ' . LogHelper::changeShipmentStatus($record->shipment_status_id, $request->shipment_status_id) . ' bởi';

                    $record->shipment_status_id = $request->shipment_status_id;

                    //Back lại số lượng sản phẩm trong kho
                    if($record->verified == 1 && $record->orders_status_id == 9){//Đã huỷ đơn hàng
                        InvoiceHelper::backInventory($record->id);
                    }
                }
            }

            //Cập nhật coupon
            if($record->coupon != $request->coupon){
                //Activity log
                $logs[] = $titleReturn . " đã cập nhật coupon($record->coupon -> $request->coupon ) bởi";

                $record->coupon = $request->coupon;
                $record->discount_coupon = $request->discount_coupon;
                $record->price_final = $request->againPay;
            }

            //Xác nhận đơn hàng / Phiếu đặt hàng
            if(!empty($request->verified) && ($record->verified != $request->verified)){
                $record->verified = empty($request->verified) ? $record->verified : $request->verified;
                $record->note_verified = $request->note_verified;
                $record->date_verified = DateHelper::currentDate();

                //Activity log
                $logs[] = $titleReturn . ' đã được xác thực bởi';

                if($type == 'orders'){

                    if($record->verified == 1){//Cap nhật số lượng sp trong kho hàng

                        //Kiểm tra xem số lương sản phẩm đã đươc điều chuyển về kho hàng hay chưa
                        //Tránh tình trạng duyệt phiếu đặt hàng mà hàng trong kho không đủ số lượng giao
                        Cart::restore($record->code);
                        $itemsCart = Cart::content();
                        $dataError = ProductHelper::checkInventoryCart($type, $itemsCart, $record->branches_id);
                        if($dataError){
                
                            $msg = '<ul>';
                            foreach($dataError as $item){
                                $msg .= '<li>' . $item->name . ' (' . $item->options['code'] . ')</li>';
                            }
                            $msg .= '</ul>';
                
                            return response()->json([
                                'status' => 500,
                                'type' => 'danger',
                                'title' => 'Không thể xác thực phiếu đặt hàng này',
                                'msg' => 'Bạn vui lòng kiểm tra số lượng của các sản phẩm: ' . $msg
                            ]);
                        }


                        InvoiceHelper::updateInventoryPDH($record);
                    }

                }else{//Đơn hàng (Back lại số lượng sản phẩm trong kho)
                    if($record->verified == -1){
                        InvoiceHelper::backInventory($record->id);
                    }
                }
            }

            //Tao don hang tu phieu dat hang
            if($request->is_invoices == 1){
                $record->is_invoices = 1;

                //Activity log
                $logs[] = 'Đơn hàng đã được tạo từ phiếu đặt hàng bởi';
            }

            //Cập nhật ghi chú đơn hàng
            if($record->comment != $request->comment){
                //Activity log
                $logs[] = $titleReturn . " đã cập nhật ghi chú giao hàng bởi";
                $record->comment = $request->comment;
            }

            //Cập nhật ghi chú điều chuyển sản phẩm
            if($record->comment_transfer != $request->comment_transfer){
                //Activity log
                $logs[] = $titleReturn . " đã cập nhật nội dung điều chuyển sản phẩm bởi";
                $record->comment_transfer = $request->comment_transfer;
            }

            if($record->save()){
                $logs[] = $titleReturn . ' đã được cập nhật bởi';
                foreach($logs as $log){
                    activity()->performedOn($record)->causedBy(Auth::user())->log($log);
                }
            }

            //Shipment info
            $this->updateShipmentInfo($request, $record->id);

            return response()->json(['data' => 'Bạn đã cập nhật thành công' . $titleReturn]);
        }

        //Tao mới đơn đặt hàng / hoá đơn
        $userId = Auth::user()->id;

        $branchActive = json_decode($_COOKIE['branch_active']);

        if($request->isCopyCart){
            Cart::restore($request->orders_code_copy);
        }else{
            Cart::restore($userId . '_' . $type . '_' . $branchActive->id . '_' . $cartNumber);
        }

        $itemsCart = Cart::content();

        //Kiểm tra số lương sản phẩm đặt mua so với số lượng sp trong kho
        $dataError = ProductHelper::checkInventoryCart($type, $itemsCart);
        if($dataError){

            $msg = '<ul>';
            foreach($dataError as $item){
                $msg .= '<li>' . $item->name . ' (' . $item->options['code'] . ')</li>';
            }
            $msg .= '</ul>';

            return response()->json([
                'status' => 500,
                'type' => 'danger',
                'title' => 'Không thể thực hiện việc ' . $title,
                'msg' => 'Bạn vui lòng kiểm tra số lượng của các sản phẩm: ' . $msg
            ]);
        }

        //Create orders
        $orders = new Orders();
        $params = $request->all();

        $params['price'] = Cart::getTotal();
        $params['price_final'] = $params['againPay'];

        if($type == 'orders'){
            $nameLog = 'Phiếu đặt hàng';
            $params['orders_status_id'] = 1;//Phiếu tạm(orders)
            $prefixCode = 'PDH';
        }else{
            $nameLog = 'Đơn hàng';
            $params['orders_status_id'] = 7;//Đang xử lý(invoices)
            $prefixCode = 'HD';
        }

        if($request->optionGiftDiscount == 1){
            $params['discount_gift'] = $request->discountGiftPrice;
            $params['discount_gift_unit'] = $request->discountGiftUnit;
        }else{
            $params['discount_gift'] = 0;
            $params['discount_gift_unit'] = 'vnd';
        }

        $orders->fill($params);
        if($orders->save()){

            //Activity log
            activity()
                ->performedOn($orders)
                ->causedBy(Auth::user())
                ->log($nameLog . ' đã được tạo bởi');

            if($request->isCopyCart){
                $count = Orders::where('code', 'like', '%'. $request->orders_code_copy .'%')->count();
                $orders->code = $request->orders_code_copy . '.' . $count;
            }else{
                $orders->code = $prefixCode . $orders->id_display;
            }

            //Kiểm tra trường hợp click button "Tạo và kết thúc đơn hàng"
            if($type == 'invoices' && $request->create_end_invoices == 1 ){
                $orders->verified = 1;
                $orders->is_full_product = 1;
            }

            $orders->discount_gift_product = intval($request->optionGiftDiscount);

            $orders->save();

            $this->setIntervalMonths( $params['customers_id'] );

            //Create Receipt
            if( $params['deposit'] > 0 ){
                $this->createReceipts($orders, $params['deposit']);
            }

            //Orders Detail
            $productGiftIds = trim($request->productGiftIds);
            if($request->optionGiftDiscount == 2 && !empty($productGiftIds)){
                $productGiftIds = explode(',', $productGiftIds);
                foreach ($productGiftIds as $id) {
                    $productGift = Products::find($id);
                    if($productGift){
                        Cart::add($productGift->id, $productGift->name, 0, 1, [
                            'code' => $productGift->code,
                            'thumbnail' => $productGift->thumbnail_display,
                            'price_sell' => 0,
                            'discount' => 0,
                            'discountUnit' => 'vnd',
                            'discount_note' => 'Sản phẩm khuyến mãi'
                        ]);
                    }
                }
            }

            $this->insertOrdersDetail($params['branches_id'], $itemsCart, $orders, $params['is_invoices']);

            //Shipment info
            $this->insertShipmentInfo($request, $orders->id);

            Cart::store($orders->code);

            Cart::destroy($userId . '_' . $type . '_' . $branchActive->id . '_' . $cartNumber);
        }

        $htmlPrint = (string)view('admin.components.prints.order', ['itemsCart'=> $itemsCart, 'detailOrder' => $orders]);

        return response()->json(['htmlPrint' => $htmlPrint, 'data' => 'Bạn đã tạo thành công' . $titleReturn]);
    }

    public function createReceipts($detailOrders, $deposit){
        $record = new Receipts;
        $record->fill([
            'orders_id' => $detailOrders->id,
            'users_id' => $detailOrders->users_id,
            'customers_id' => $detailOrders->customers_id,
            'price' => $deposit
        ]);
        if($record->save()){
            $count = Receipts::where('orders_id', $detailOrders->id)->count();
            $record->code = 'PT-' . $detailOrders->code . '-' . $count;
            $record->save();
        }
    }

    public function setIntervalMonths($customers_id){
        $orderLastest = Orders::where('customers_id', $customers_id)->orderBy('id', 'desc')->first();
        if(!$orderLastest){
            return 0;
        }

        $days = DateHelper::subtractDate($orderLastest->created_at, 'months');
        $orderLastest->interval = $days;
        $orderLastest->save();

        return $days;
    }

    public function insertOrdersDetail($branches_id, $itemsCart, $orders, $is_invoices){
        if(count($itemsCart) > 0){
            foreach($itemsCart as $item){
                $product = Products::find($item->id);
                if($product){
                    $orderDetail = new OrderDetail();
                    $orderDetail->fill([
                        'orders_id' => $orders->id,
                        'products_id' => $item->id,
                        'qty' => $item->quantity,
                        'price' => $item->price,
                        'discount' => isset($item->options['discount']) ? $item->options['discount'] : '',
                        'discount_unit' => isset($item->options['discountUnit']) ? $item->options['discountUnit'] : '',
                        'discount_note' => isset($item->options['discount_note']) ? $item->options['discount_note'] : '',
                    ]);

                    if($orderDetail->save()){
                        $branchesProducts = BranchesProducts::where('branches_id', $branches_id)->where('products_id', $item->id)->first();

                        if($branchesProducts){

                            if( $is_invoices == 1 ){//invoices

                                $branchesProducts->inventory = max($branchesProducts->inventory - $item->quantity, 0);
                                if($branchesProducts->save()){
                                    ProductHelper::updateInventoryNumber( $product->product_types_id, $product->id );
                                }

                            }
                            
                            //Sẽ cập nhật số lượng tồn kho cho phiếu đặt hàng khi PDH đã được kiểm duyệt

                        }
                    }
                }
            }
        }
    }

    public function insertShipmentInfo($request, $orders_id){
        $shipment = new ShipmentDetails();
        $params = $request->all();
        $params['orders_id'] = $orders_id;
        $shipment->fill($params);
        $shipment->save();
    }

    public function updateShipmentInfo($request, $orders_id){
        $shipment = ShipmentDetails::where('orders_id', $orders_id)->first();
        if($shipment){
            $shipment->fill($request->all());
            $shipment->save();
        }
    }

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;
        
        $records = Orders::search($request)
            ->where('is_invoices', '=', 0)
            ->orderBy('orders.created_at', 'desc')
            ->orderBy('orders.code', 'desc')
            ->paginate($items_per_page);

        $total = 0;
        $totalPaid = 0;
        foreach (Input::except('page') as $input => $value){
            $records->appends($input, $value);
        }
        foreach ($records as $item){
            $total += $item->price_final;
            $totalPaid += $item->deposit;
        }

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.order.index', [
            'title' => $this->title,
            'mod' => $this->mod,
            'records' => $records,
            'total' => $total,
            'totalPaid' => $totalPaid,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create
        ]);
    }

    public function show(Request $request, $id){
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Orders();
            $title = 'Thêm mới';
        }else{
            $record = Orders::find($id);
            $title = "Cập nhật";
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $shipDetail = ShipmentDetails::find($record->id);
        $html = (string)view('admin.order.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'shipDetail' => $shipDetail,
        ]);
        return response()->json(['html' => $html]);
    }

    public function update(StoreShipOrders $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Orders();
        }else{
            $record = Orders::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $params = $request->all();
        $record->fill($params);

        if($record->save()){
            $shipDetail = ShipmentDetails::find($record->id);
            if (empty($shipDetail)) $shipDetail = new ShipmentDetails();
            if (isset($params['shipment_details']['delivery_charges'])) {
                $params['shipment_details']['delivery_charges'] = str_replace(',', '', $params['shipment_details']['delivery_charges']);
            }
            $shipDetail->fill($params['shipment_details']);
            $shipDetail->save();
            return response()->json(['status' => 200]);
        }

        return response()->json(['status' => 500]);
    }

    // Export orders
    public function export(Request $request){
        $records = Orders::search($request)->get();
        $branches = Branches::orderBy('name')->get();
        $attributes = array();
        Excel::create('Order-export', function($excel) use ($records, $branches, $attributes) {
            $excel->sheet('Sheet1', function($sheet) use ($records, $branches, $attributes) {
                $sheet->freezeFirstRow();
                $sheet->loadView('admin.order.export', compact('records', 'branches', 'attributes'));
            });
        })->export('xlsx');
    }

    public function exportDetail(Request $request){
        $records = Orders::with(['orderDetail.product', 'branch', 'customer', 'shipmentDetail.partnerDelivery', 'shipmentDetail.shipmentStatus'])->search($request)->get();
        Excel::create('Order-export-detail', function($excel) use ($records) {
            $excel->sheet('Sheet1', function($sheet) use ($records) {
                $sheet->freezeFirstRow();
                $sheet->loadView('admin.order.export-detail', compact('records'));
            });
        })->export('xlsx');
    }

    public function search(Request $request){
        $q = trim($request->q);

        $records = Orders::where(function($query) use ($q){
                    $query->where('code', 'like', '%'.$q.'%')->orWhere('id', 'like', '%'.$q.'%');
                })
                ->select('id', 'code', 'code as text')
                ->selectRaw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") AS order_date')
                ->orderBy('id', 'desc')->limit(100)->get()->toArray();

        return response()->json(['records' => $records]);
    }

    public function getOrders(Request $request){
        $type = $request->type;
        switch($type){
            case (1):
                $records = Status::prepareDelivery($request);
                break;
            case (2):
                $records = Status::beingTransport($request->start_date, $request->end_date);
                break;
            case (3):
                $records = Status::overDelivery($request->start_date, $request->end_date);
                break;
            case (4):
                $records = Status::canceled($request->start_date, $request->end_date);
                break;
            case (5):
                $records = CustomerHelper::highestRevenue($request->start_date, $request->end_date);
                break;
            case (6):
                $records = CustomerHelper::highestPurchases($request->start_date, $request->end_date);
                break;
            default:
                $records = false;
        }

        $data = [
            'records' => $records,
            'type'=> $type,
        ];
        if( (int)$request->type < 5){
            $html = (string)view('admin.components.orders.item-list', $data);
        }else{
            $html = (string)view('admin.components.customers.customer-list', $data);
        }
        return response()->json(['data' => $html, 'total_page'=> $records->lastPage()]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, Orders::find($id))) return response()->json(['st' => 403]);
        
        $record = Orders::find($id);
        if($record){
            $st = $record->delete();
            $log = 'Record này đã bị xoá bởi';
            activity()->performedOn($record)->causedBy(Auth::user())->log($log);
        }
        return response()->json(['html' => $st]);
    }
    
}
