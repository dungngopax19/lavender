<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;

use App\Models\Products;
use App\Models\ProductTypes;
use App\Models\ProductCategories;
use App\Http\Requests\StoreProduct;
use App\Models\Branches;
use App\Models\BranchesProducts;
use App\Models\Attributes;
use App\Models\AttributesProducts;
use App\Models\ProductsComponents;
use App\Models\Orders;
use Auth;
use Melihovv\ShoppingCart\Facades\ShoppingCart as Cart;
use App\Helpers\Order\Report;
use App\Helpers\DateHelper;
use App\Helpers\Report\Sell as SellReport;

class ReportController extends Controller
{
    public $title = 'Thống kê';
    public $mod = 'report';

    public function index(Request $request){

        $userId = Auth::user()->id;

        Cart::restore($userId);

        $records = Cart::content();
        $totalPrice = Cart::getTotal();

        // var_dump($totalPrice);
        //exit;

        return view('admin.report.index', [
            'records' => $records,
            'totalPrice' => $totalPrice,
            'title' => $this->title,
            'mod' => $this->mod
        ]);
    }

    public function report(Request $request, $type, $concerns){
        $request->created_range = ($request->created_range) ? $request->created_range : date('d-m-Y').' -> '.date('d-m-Y');
        switch(true){
            case ($type == 'end-day'):
                $records = $this->getRecordEndDaySell($request);
                break;
            case ($type == 'sell'):
                $records = $this->getRecordSell($request, $concerns);
                break;
            default:
                break;
        }
        // dd($records);
        return view('admin.report.'.$type.'.'.$concerns.'.index', [
            'records' => $records,
            'title' => $this->title,
            'type' => $type,
            'concerns' => $concerns,
            'mod' => $this->mod
        ]);
    }

    public function ajaxGetReport(Request $request){
        $request->created_range = ($request->created_range) ? $request->created_range : date('d-m-Y').' -> '.date('d-m-Y');
        $type = $request->type;
        $concerns = $request->concerns;

        $request->whereDate = DateHelper::getTypeWhereDate($request->created_range);

        switch(true){
            case ($type == 'end-day'):
                $records = $this->getRecordEndDaySell($request);
                break;
            case ($type == 'sell'):
                $records = $this->getRecordSell($request, $concerns);
                break;
            default:
                break;
        }
        $html = (string)view('admin.report.'.$type.'.'.$concerns.'.list', [
            'created_range'=> $request->created_range,
            'records' => $records,
            'title' => $this->title,
            'whereDate' => $request->whereDate,
            'type' => $type,
            'concerns' => $concerns,
            'mod' => $this->mod
        ]);

        return response()->json(['st'=> 200, 'html'=>$html, 'records'=>$records, 'whereDate' => $request->whereDate]);
    }

    // Export orders
    public function export(Request $request, $type){
        $records = $this->getRecordEndDaySell();

        Excel::create('Report', function($excel) use ($records, $type) {
            $excel->sheet('Sheet1', function($sheet) use ($records, $type) {
                $sheet->loadView('admin.report.'.$type.'.index-excel', compact('records', 'type'));
            });
        })->export('xlsx');
    }

    public function show(Request $request, $id){
        //
    }

    public function update(StoreProduct $request){
        //
    }

    public function destroy($id){
        //
    }

    // common function get report
    public function getRecordEndDaySell($request){
        $orders = Report::getOrderWithStatus([0], $request);
        $orders->name = 'Đặt hàng';
        $invoices = Report::getOrderWithStatus([1,2], $request);
        $invoices->name = 'Hóa đơn';

        $records = [$orders, $invoices];
        return $records;
    }

    public function getRecordSell($request, $concerns){
        switch(true){
            case ($concerns == 'time'):
                $records = Report::getOrderSellByTime([0,1,2], $request);
                break;
            case ($concerns == 'profit'):
                $records = Report::getOrderSellByTime([0,1,2], $request);
                break;
            case ($concerns == 'employee'):
                $records = SellReport::getOrderSellByEmployee([0,1,2], $request);
                break;
            default:
                break;
        }
        return $records;
    }


}
