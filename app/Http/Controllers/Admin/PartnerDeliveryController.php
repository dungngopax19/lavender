<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PartnerDeliveries;
use App\Models\Provinces;
use App\Helpers\ActivityLog;
use App\Helpers\Permissions;

use App\Http\Requests\StorePartnerDelivery;

class PartnerDeliveryController extends Controller
{
    public $title = 'Đối tác giao hàng';
    public $mod = 'partner-delivery';
    public $permission = 'doi-tac-doi-tac-giao-hang-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = PartnerDeliveries::search($request)->paginate($items_per_page);
        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.partner-delivery.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create
        ]);
    }

    public function show($id){
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);

            $record = new PartnerDeliveries();
            $title = 'Thêm mới';
        }else{
            $record = PartnerDeliveries::find($id);
            $title = "Cập nhật";
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);

        }

        $provinces = Provinces::with('childrens')->where('parent_id', 0)->orderBy('name')->select('id', 'name')->get()->toArray();

        $activity_status = (array)config('constants.activity_status');
        unset( $activity_status[0] );

        $html = (string)view('admin.partner-delivery.edit', [
            'title' => lcfirst($this->title),
            'mod' => $this->mod,
            'record' => $record,
            'provinces' => $provinces,
            'activity_status' => $activity_status,
            'permission' => $this->permission,
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StorePartnerDelivery $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new PartnerDeliveries();
        }else{
            $record = PartnerDeliveries::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }
        $record->fill($request->all());

        if($record->save()){
            return response()->json(['st' => 200, 'data' => $record]);
        }
        return response()->json(['status' => 500]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, User::find($id))) return response()->json(['st' => 403]);

        $st = PartnerDeliveries::find($id)->delete();
        return response()->json(['html' => $st]);
    }

}
