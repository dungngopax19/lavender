<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Messages;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Common;

class MessageController extends Controller
{
    public $title = 'Tin nhắn';
    public $mod = 'message';

    public function index(Request $request){
        $number = ($request->page - 1)*10;
        $records = Messages::with('userSend')->where('user_receive_id', Auth::user()->id)->orderBy('id', 'desc')->skip($number)->take(10)->get();

        $html = (string)view('components.message.item', [
            'records' => $records,
        ]);

        return response()->json(['st' => 200, 'data' => $html]);
    }

    public function markRead(){
        Messages::where('user_receive_id', Auth::user()->id)->update(['status'=> 2]);
        return response()->json(['st' => 200]);
    }

    public function show($id){
        //
    }

    public function update(StoreSurcharge $request){
        //
    }

    public function destroy($id){
        // $st = Surcharges::find($id)->delete();
        return response()->json(['html' => $st]);
    }
}
