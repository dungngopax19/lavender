<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Common;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function uploadTmp(Request $request){

        if ($request->hasFile('file')) {
            $objFile = $request->file('file');

            $fileName = $objFile->getClientOriginalName();
            $newFileName = uniqid() . '.' . \File::extension($fileName);

            $path = $objFile->storeAs('tmp', $newFileName);

            return response()->json([
                'status' => 200, 
                'pathTmpFile' => $path,
                'url' => asset(Storage::url($path))
            ]);
        }
        
        return response()->json(['status' => 500]);
    }
}
