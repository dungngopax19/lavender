<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

use App\Helpers\Customer as CustomerHelper;
use App\Models\Provinces;
use App\Models\CustomerTypes;
use App\Helpers\DateHelper;
use App\Models\Products;
use App\Models\ProductTypes;
use App\Models\ProductCategories;
use App\Http\Requests\StoreInvoice;
use App\Models\Branches;
use App\Models\BranchesProducts;
use App\Models\Attributes;
use App\Models\AttributesProducts;
use App\Models\ProductsComponents;
use App\Models\Orders;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\ShipmentStatus;
use App\Models\ShipmentDetails;
use Auth;
use App\User;
use App\Helpers\Permissions;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use App\Helpers\Invoice as InvoiceHelper;
use App\Models\Customers;
use DB;

class InvoiceController extends Controller
{
    public $title = 'Đơn hàng';
    public $mod = 'invoice';
    public $permission = 'giao-dich-hoa-don-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = Orders::with('customer')
            ->where('is_invoices', '>', 0)
            ->search($request)
            ->orderBy('orders.created_at', 'desc')
            ->orderBy('orders.code', 'desc')
            ->select('orders.*')
            ->paginate($items_per_page);

        $records->m_total = Orders::where('is_invoices', '>', 0)->search($request)
            ->selectRaw('sum(price) as price, sum(discount) as discount, sum(deposit) as deposit, sum(price_final) as price_final')
            ->first();


        $totalRecords = Orders::with('customer')->where('is_invoices', '>', 0)->search($request)->count();

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        foreach (Input::except('page') as $input => $value){
            $records->appends($input, $value);
        }
        
        return view('admin.invoice.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create,
            'totalRecords' => $totalRecords
        ]);
    }

    public function getConfirmOrder(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = Orders::with('customer')
            ->where('is_invoices', '>', 0)
            ->search($request)
            ->orderBy('orders.created_at', 'desc')
            ->orderBy('orders.code', 'desc')
            ->select('orders.*')
            ->paginate($items_per_page);
        
        $records->m_total = Orders::where('is_invoices', '>', 0)->search($request)
            ->selectRaw('sum(price) as price, sum(discount) as discount, sum(deposit) as deposit, sum(price_final) as price_final')
            ->first();

        $totalRecords = Orders::with('customer')->where('is_invoices', '>', 0)->search($request)->count();

        foreach (Input::except('page') as $input => $value){
            $records->appends($input, $value);
        }

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.invoice.confirm-order.index', [
            'records' => $records,
            'title' => 'Xác nhận đơn hàng',
            'mod' => 'invoice/confirm-order',
            'totalRecords' => $totalRecords,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create
        ]);
    }

    public function getTransportOrder(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        //Đơn hàng chưa giao
        $idProductDelivered = trim($request->id_product_delivered);
        if($idProductDelivered){
            $query = Orders::join('order_detail', function ($join) use ($idProductDelivered) {
                    $join->on('orders.id', '=', 'order_detail.orders_id')
                        ->where('order_detail.products_id', $idProductDelivered)
                        ->where('orders.shipment_status_id', 1);
                });
        }

        //Đơn hàng sắp tới ngày giao
        if($request->orders_upcomming_delivery){
            $query = Orders::join('shipment_details', function ($join) use ($idProductDelivered) {
                $join->on('orders.id', '=', 'shipment_details.orders_id')
                    ->whereBetween('shipment_details.delivery_time', [date('Y-m-d'), date('Y-m-d', strtotime("+1 days"))])
                    ->where('orders.shipment_status_id', 1);
            });
        }

        //Đơn hàng quá ngày giao
        if($request->orders_exceeding_delivery){
            $query = Orders::join('shipment_details', function ($join) use ($idProductDelivered) {
                $join->on('orders.id', '=', 'shipment_details.orders_id')
                    ->where('shipment_details.delivery_time', '<', date('Y-m-d 00:00:00') )
                    ->where('orders.shipment_status_id', 1);
            });
        }

        //Đơn hàng ưu tiên
        $id_product_order_highlight = trim(request()->id_product_order_highlight);    
        if($id_product_order_highlight){
            $query = Orders::join('order_detail', function ($join) use ($id_product_order_highlight) {
                $join->on('orders.id', '=', 'order_detail.orders_id')
                    ->where('order_detail.products_id', $id_product_order_highlight)
                    ->where('orders.highlight', 1);
            });
        }

        if(empty($query)){
            $query = Orders::select('orders.*');
        }

        //Nếu trường hợp đã chọn "Ưu tiên" thì mình sẽ bỏ chọn trong "CHưa giao hàng"
        if($request->shipment_status_id == 1){
            $query = $query->where('highlight', 0);
        }

        $records = $query->with('customer')
            ->where('is_invoices', '>', 0)
            ->where('verified', 1)
            ->orderBy('orders.created_at', 'desc')
            ->orderBy('orders.code', 'desc')
            ->search($request)
            ->select('orders.*')
            ->paginate($items_per_page);

        $records->m_total = Orders::where('is_invoices', '>', 0)
            ->where('verified', 1)
            ->search($request)
            ->selectRaw('sum(price) as price, sum(discount) as discount, sum(deposit) as deposit, sum(price_final) as price_final')
            ->first();

        $totalRecords = Orders::with('customer')
            ->where('is_invoices', '>', 0)
            ->where('verified', 1)
            ->search($request)->count();

        foreach (Input::except('page') as $input => $value){
            $records->appends($input, $value);
        }

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.invoice.transport-order.index', [
            'records' => $records,
            'title' => 'Vận chuyển đơn hàng',
            'mod' => 'invoice/transport-order',
            'totalRecords' => $totalRecords,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create
        ]);
    }

    public function show(Request $request, $id){
        $record = Orders::with(['customer', 'createdUser', 'branch', 'orderDetail.product'])->whereId($id)->first();
        $title = 'Đơn hàng';

        $invoices_status = OrderStatus::where('type', 'invoices')->pluck('name', 'id')->toArray();
        $shipment_status = ShipmentStatus::pluck('name', 'id')->toArray();

        $html = (string)view('admin.invoice.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'users' => User::get()->toArray(),
            'invoices_status' => $invoices_status,
            'shipment_status' => $shipment_status,
            'channels' => config('constants.channels'),
            'permission' => $this->permission,

        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreInvoice $request){
        $params = $request->all();

        $invoice = Orders::find($params['id']);
        $shipment_detail = ShipmentDetails::where('orders_id', $params['id'])->first();

        $invoice->fill($params);
        $shipment_detail->fill(['shipment_status_id'=> $params['shipment_status_id']]);

        $invoice->save();
        $shipment_detail->save();

        return response()->json(['record' => $params]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, Orders::find($id))) return response()->json(['st' => 403]);
        
        $record = Orders::find($id);
        if($record){
            $st = $record->delete();
            $log = 'Record này đã bị xoá bởi';
            activity()->performedOn($record)->causedBy(Auth::user())->log($log);
        }
        return response()->json(['html' => $st]);
    }

    // Import invoice
    public function import(Request $request){
        $html = (string)view('admin.invoice.import');
        return response()->json(['html' => $html]);
    }

    public function submitImport(Request $request){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 18000);
        
        $limit = 100;
        $page = $request->page ?? 1;

        $begin = ($page - 1) * $limit;
        $end = $begin + $limit;

        if ($page > 1 || $request->hasFile('file')) {

            if($page == 1){
                //Upload file
                $objFile = $request->file('file');
            
                $fileName = $objFile->getClientOriginalName();
                $newFileName = uniqid() . '.' . \File::extension($fileName);
    
                $path = $objFile->storeAs('tmp', $newFileName);
            }else{
                $path = $request->path;
            }

            //Import data
            //$records = Excel::load(Storage::path($path), function($reader){})->limitRows(1000)->formatDates(true, 'Y-m-d H:i:s')->get()->toArray();

            $recordsAll = Excel::load(Storage::path($path), function($reader){})->get()->toArray();
            $totalRecords = count($recordsAll);

            if($recordsAll){
                $stop = min($end, $totalRecords);

                $records = [];
                for($i = $begin; $i < $stop; $i = $i + 1){
                    $records[] = $recordsAll[$i];
                }

                //Cập nhật lại trạng thái đơn hàng và trạng thái giao hàng
                if($request->like_code == 'update_status'){
                    if($records){
                        foreach ($records as $key => $item) {
                            

                            if(isset($item['ma_hoa_don'])){
                                $invoice = Orders::where('code' , $item['ma_hoa_don'])->first();
                                if($invoice){
                                    $invoice->orders_status_id = OrderStatus::getIdByName($item['trang_thai']);

                                    $shipment_status = ($item['trang_thai_giao_hang'] == 'Hoàn thành') ? 'Giao thành công' : $item['trang_thai_giao_hang'];
                                    $invoice->shipment_status_id = ShipmentStatus::getIdByName($shipment_status);
                                    $invoice->update();
                                }
                            }
                        }
                    }
                    return response()->json([ 'status' => 200]);
                }

                //Bỏ qua đơn hàng đó || Cập nhật lại đơn hàng đó
                $collections = collect(array_reverse($records))->groupBy('ma_hoa_don')->toArray();

                foreach($collections as $invoices){
                    if($invoices){

                        $keepHighlight = false;
                        $orders_id = null;
                        $ma_hoa_don = $invoices[0]['ma_hoa_don'];
                        $invoice = Orders::where('code' , $ma_hoa_don)->first();

                        // If there exists an invoice code
                        if(!empty($invoice->id)){
                            if($request->like_code == 'skip'){
                                continue;
                            }else{
                                $keepHighlight = $invoice->highlight;

                                //Delete table order_detail
                                OrderDetail::where('orders_id', $invoice->id)->forceDelete();

                                //Delete table shipment_details
                                ShipmentDetails::where('orders_id', $invoice->id)->forceDelete();

                                //Delete table orders
                                Orders::where('code', $ma_hoa_don)->forceDelete();
                            }
                        }

                        foreach($invoices as $key=>$item){
                            $item = (object)$item;
                            $item->highlight = $keepHighlight;

                            if(isset($item->ma_hoa_don)){
                                if($key === 0){
                                    // insert Order
                                    $invoice = InvoiceHelper::insertOrder($item);

                                    if(!$invoice){
                                        return response()->json([ 'status' => 500, 'item' => $item]);
                                    }

                                    //Activity log
                                    activity()
                                        ->performedOn($invoice)
                                        ->causedBy(Auth::user())
                                        ->log('Hoá đơn đã được tạo bởi');
                                    
                                    $orders_id = $invoice->id;
                                    $item->orders_id = $orders_id;

                                    // insert Shipment Detail
                                    InvoiceHelper::insertShipmentDetail($item);

                                    //Update interval order of customer
                                    $this->setIntervalMonths( $invoice->customers_id );

                                    CustomerHelper::synsdataCustomersInvoices($invoice->customers_id);
                                }

                                // insert OderDetail table
                                $item->orders_id = $orders_id;
                                InvoiceHelper::insertOrderDetail($item);
                            }
                        }
                    }
                }
            }

            //Delete file upload excel
            if($end >= $totalRecords){
                Storage::delete($path);
                $stop = true;
            }else{
                $stop = false;
            }

            return response()->json([ 'status' => 200, 'stop' => $stop, 'path' => $path, 'page' => $page + 1]);
        }

        return response()->json(['status' => 500]);
    }

    public function setIntervalMonths($customers_id){

        $ordersCount = Orders::where('customers_id', $customers_id)->count();
        
        if($ordersCount > 1){
            $twoOrdersRecent = Orders::where('customers_id', $customers_id)->limit(2)->orderBy('created_at', 'desc')->get();
            
            $months = DateHelper::subtractDate($twoOrdersRecent[1]->created_at, 'months', $twoOrdersRecent[0]->created_at);
    
            $twoOrdersRecent[0]->update([
                'interval' => $months
            ]);
    
        }

        return 0;
    }

    public function export(Request $request){
        $limit = 1500;

        if($request->page == 'invoices'){
            $query = Orders::with('branch', 'customer', 'orderDetail');
        }else{
            //Đơn hàng chưa giao
            $idProductDelivered = trim($request->id_product_delivered);
            if($idProductDelivered){
                $query = Orders::join('order_detail', function ($join) use ($idProductDelivered) {
                        $join->on('orders.id', '=', 'order_detail.orders_id')
                            ->where('order_detail.products_id', $idProductDelivered)
                            ->where('orders.shipment_status_id', 1);
                    });
            }

            //Đơn hàng sắp tới ngày giao
            if($request->orders_upcomming_delivery){
                $query = Orders::join('shipment_details', function ($join) use ($idProductDelivered) {
                    $join->on('orders.id', '=', 'shipment_details.orders_id')
                        ->whereBetween('shipment_details.delivery_time', [date('Y-m-d'), date('Y-m-d', strtotime("+1 days"))])
                        ->where('orders.shipment_status_id', 1);
                });
            }

            //Đơn hàng quá ngày giao
            if($request->orders_exceeding_delivery){
                $query = Orders::join('shipment_details', function ($join) use ($idProductDelivered) {
                    $join->on('orders.id', '=', 'shipment_details.orders_id')
                        ->where('shipment_details.delivery_time', '<', date('Y-m-d 00:00:00') )
                        ->where('orders.shipment_status_id', 1);
                });
            }

            //Đơn hàng ưu tiên
            $id_product_order_highlight = trim(request()->id_product_order_highlight);    
            if($id_product_order_highlight){
                $query = Orders::join('order_detail', function ($join) use ($id_product_order_highlight) {
                    $join->on('orders.id', '=', 'order_detail.orders_id')
                        ->where('order_detail.products_id', $id_product_order_highlight)
                        ->where('orders.highlight', 1);
                });
            }

            if(empty($query)){
                $query = Orders::select('orders.*');
            }

            //Nếu trường hợp đã chọn "Ưu tiên" thì mình sẽ bỏ chọn trong "CHưa giao hàng"
            if($request->shipment_status_id == 1){
                $query = $query->where('highlight', 0);
            }

            $query = $query->with('branch', 'customer', 'orderDetail')->where('verified', 1);
        }

        $records = $query->limit($limit)
            ->where('orders.is_invoices', '>', 0)->search($request)
            ->orderBy('orders.created_at', 'desc')->orderBy('orders.code', 'desc')
            ->get();
        

        Excel::create('Invoice-export', function($excel) use ($records) {
            $excel->sheet('Sheet1', function($sheet) use ($records) {
                $sheet->freezeFirstRow();
                $sheet->loadView('admin.invoice.export', compact('records'));
            });
        })->export('xlsx');
    }
}
