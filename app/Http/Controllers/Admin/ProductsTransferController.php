<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductsTransfer;
use App\Models\Products;
use App\Models\Branches;
use App\Models\BranchesProducts;
use Auth;
use App\Http\Requests\StoreProductsTransfer;
use App\Helpers\Permissions;
use App\Helpers\Product as ProductHelper;

class ProductsTransferController extends Controller
{
    public $permission = 'dieu-chuyen-san-pham-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = ProductsTransfer::search($request)->paginate($items_per_page);
        $permission = $this->permission;
        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.products-transfer.index', compact('records', 'permission', 'is_permission_create'));
    }

    public function show($id){

        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new ProductsTransfer();
            $title = 'Tạo';
        }else{
            $record = ProductsTransfer::find($id);
            $title = "Cập nhật";
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        if($record->products_id){
            $products = Products::where('id', $record->products_id)->select('id', 'code as name')->get()->toArray();
        }else{
            $products = Products::select('id', 'code as name')->limit(500)->get()->toArray();
        }
        
        $branches = Branches::select('id', 'name')->orderBy('name', 'asc')->get()->toArray();

        $permission = $this->permission;

        $html = (string)view('admin.products-transfer.edit', compact('title', 'permission', 'record', 'products', 'branches'));

        return response()->json(['html' => $html]);
    }

    public function update(StoreProductsTransfer $request){

        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new ProductsTransfer();
        }else{
            $record = ProductsTransfer::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $params = $request->all();

        $from = BranchesProducts::where('branches_id', $params['branch_from'])
                ->where('products_id', $params['products_id'])
                ->first();

        $branch = Branches::find($params['branch_from']);

        //Validate qty
        if(!$branch){
            return response()->json(['st' => 500, 'msg' => 'Dữ liệu cần điều chuyển khôn hợp lệ']);
        }

        if(!$from){
            return response()->json(['st' => 500, 'msg' => 'Hiện tại ' . $branch->name . ' không có sản phẩm này']);
        }
        
        if($from->inventory < $params['qty']){
            return response()->json(['st' => 500, 'msg' => 'Số lượng sản phẩm cần điều chuyển không hợp lệ']);
        }

        if($params['is_confirmed'] == 2 && empty($record->date_confirmed) ){
            $params['date_confirmed'] = date('Y-m-d');
            $params['uid_confirmed'] = Auth::user()->id;
        }

        $transfer = false;
        if( $params['is_confirmed'] == 2 && $record->is_confirmed == 0 ){
            $transfer = true;
        }
        
        $product = Products::find($params['products_id']);

        if($product){
            if($record->fill($params)->save()){
                if($transfer){
                    //To Branch
                    $to = BranchesProducts::where('branches_id', $params['branch_to'])
                        ->where('products_id', $params['products_id'])
                        ->first();
                    
                    if($to){
                        $to->inventory = $to->inventory + $params['qty'];
                    }else{
                        $to = new BranchesProducts();
    
                        $to->fill([
                            'branches_id' => $params['branch_to'],
                            'products_id' => $params['products_id'],
                            'in_branch' => 1,
                            'inventory' => $params['qty'],
                            'min_inventory' => 1
                        ]);
                    }
    
                    $to->save();
    
                    //From branch
                    $from->inventory = $from->inventory - $params['qty'];
                    $from->save();
    
                    //Update warning inventory of this product
                    ProductHelper::updateInventoryNumber( $product->product_types_id, $product->id );
                }
            }
        }
        

        return response()->json(['st' => $params]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, ProductsTransfer::find($id))) return response()->json(['st' => 403]);

        $st = ProductsTransfer::find($id)->delete();
        return response()->json(['html' => $st]);
    }
}
