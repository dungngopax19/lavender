<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Coupons;
use App\Http\Requests\StoreCoupons;
use App\Models\Memberships;
use App\Helpers\Permissions;
use App\Helpers\Product as ProductHelper;

class CouponsController extends Controller
{
    public $title = 'Coupon';
    public $mod = 'coupons';
    public $permission = 'coupons-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = Coupons::search($request)->paginate($items_per_page);

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);
        $is_permission_import = Permissions::hasAllPermission([$this->permission. 'import']);

        return view('admin.coupons.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create,
            'is_permission_import' => $is_permission_import
        ]);
    }

    public function show($id){
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);

            $record = new Coupons();
            $title = 'Thêm mới';
        }else{
            $record = Coupons::find($id);
            $title = "Cập nhật";
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $memberships = Memberships::orderBy('name')->select('id', 'name')->get()->toArray();
        
        $html = (string)view('admin.coupons.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'memberships' => $memberships,
            'cat_ids' => json_decode($record->membership_ids),
            'permission' => $this->permission,
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreCoupons $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Coupons();
        }else{
            $record = Coupons::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $params = $request->all();

        $params['from_date'] = ($params['from_date']) ? date("Y-m-d", strtotime($params['from_date'])) : null;
        $params['to_date'] = ($params['to_date']) ? date("Y-m-d", strtotime($params['to_date'])) : null;
        $params['membership_ids'] = json_encode($params['cat_ids']);

        $record->fill($params);

        if($record->save()){
            return response()->json(['st' => 200, 'data' => $record]);
        }
        return response()->json(['status' => 500]);
    }

    public function destroy($id){
        $st = Coupons::find($id)->delete();
        return response()->json(['html' => $st]);
    }

    public function validateCoupon(Request $request){
        if(empty($request->coupon)){
            return response()->json(['msg' => '', 'discount_coupon' => 0]);
        }

        $discount = ProductHelper::discountByCoupon($request->coupon, $request->membership_id);
        if($discount > 0){
            $msg = '<span style="color:#3390dc">Bạn được giảm '. number_format($discount) .'đ</span>';
        }else{
            $msg = '<span style="color:red">Coupon không hợp lệ</span>';
        }
        return response()->json(['msg' => $msg, 'discount_coupon' => $discount]);
    }

}
