<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductCategories;

use App\Http\Requests\StoreProductCategory;
use App\Helpers\Permissions;

class ProductCategoryController extends Controller
{
    public $permission = 'hang-hoa-danh-muc-san-pham-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = ProductCategories::search($request)->where('parent_id', 0)->paginate($items_per_page);
        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);
        $permission = $this->permission;


        return view('admin.product-category.index', compact('records', 'is_permission_create', 'permission'));
    }

    public function show($id){
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);

            $record = new ProductCategories();
            $title = 'Thêm mới';
        }else{
            $record = ProductCategories::find($id);
            $title = "Cập nhật";
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);

        }

        $recordsParent = ProductCategories::where('parent_id', 0)->select('id', 'name')->orderBy('name', 'asc')->get()->toArray();
        $permission = $this->permission;

        $html = (string)view('admin.product-category.edit', compact('title', 'record', 'recordsParent', 'permission'));
        return response()->json(['html' => $html]);
    }

    public function update(StoreProductCategory $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new ProductCategories();
        }else{
            $record = ProductCategories::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }
        $record->fill($request->all())->save();

        return response()->json(['st' => 200]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, User::find($id))) return response()->json(['st' => 403]);

        $st = ProductCategories::find($id)->delete();
        return response()->json(['html' => $st]);
    }
}
