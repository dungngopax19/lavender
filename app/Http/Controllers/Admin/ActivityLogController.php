<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ActivityLog;
use App\Helpers\Permissions;

class ActivityLogController extends Controller
{
    public $title = 'Lịch sử thao tác';
    public $mod = 'activity-log';
    public $permission = 'he-thong-lich-su-thao-tac';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = ActivityLog::search($request)->paginate($items_per_page);

        $actions = config('activitylog.actions');
        $nameModels = config('activitylog.models');

        return view('admin.activity-log.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'actions' => $actions,
            'nameModels' => $nameModels,
            'permission' => $this->permission,
        ]);
    }

    public function show($id){
        $record = ActivityLog::find($id);

        $dataChange = $record->changes->toArray();
        $actions = config('activitylog.actions');
        $nameModels = config('activitylog.models');
        $fields = config('activitylog.fields');

        $html = (string)view('admin.activity-log.edit', [
            'record' => $record,
            'actions' => $actions,
            'fields' => $fields,
            'nameModels' => $nameModels,
            'dataNew' => $dataChange['attributes'],
            'dataOld' => isset($dataChange['old']) ? $dataChange['old'] : null
        ]);

        return response()->json(['html' => $html]);
    }
}
