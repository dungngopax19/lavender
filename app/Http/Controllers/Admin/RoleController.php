<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Roles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\StoreRole;
use App\Helpers\Common;
use App\Helpers\Permissions;
use App\Models\Branches;

class RoleController extends Controller
{
    public $title = 'Nhóm người dùng';
    public $mod = 'role';
    public $permission = 'he-thong-nhom-nguoi-dung-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = Roles::search($request)->paginate($items_per_page);

        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.role.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create
        ]);
    }

    public function show($id){
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Roles();

            $role = null;
        }else{
            $record = Roles::find($id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);

            $role = Role::findByName($record->name);
        }

        $html = (string)view('admin.role.edit', [
            'title' => lcfirst($this->title),
            'mod' => $this->mod,
            'record' => $record,
            'branches' => Branches::orderBy('name')->get()->toArray(),
            'role' => $role,
            'dataSourceSystem' => Common::renderPermissions( config('permissions.systems'), $role ),
            'dataSourceGoods' => Common::renderPermissions( config('permissions.goods'), $role ),
            'dataSourcePartners' => Common::renderPermissions( config('permissions.partners'), $role ),
            'dataSourceDeals' => Common::renderPermissions( config('permissions.deals'), $role )
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreRole $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Roles();
        }else{
            $record = Roles::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }

        $params = $request->all();
        $params['activity_status'] = empty($params['activity_status']) ? 1 : $params['activity_status'];

        $record->fill($params)->save();

        $permissions = '';
        if(!empty($request->permissions)){
            $permissions = explode(',', $request->permissions);

            foreach($permissions as $name_permission){
                $permission = Permission::where('name', $name_permission)->first();
                if(!$permission){
                    $permission = Permission::create(['name' => $name_permission]);
                }
            }

            $role = Role::findByName($record->name);
            $role->syncPermissions($permissions);
        }

        return response()->json(['st' => 200]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, Roles::find($id))) return response()->json(['st' => 403]);

        $st = Roles::find($id)->delete();
        return response()->json(['html' => $st]);
    }
}
