<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Suppliers;
use App\Models\Provinces;
use App\Helpers\Permissions;

use App\Http\Requests\StoreSuppliers;

class SuppliersController extends Controller
{
    public $title = 'Nhà cung cấp';
    public $mod = 'suppliers';
    public $permission = 'doi-tac-nha-cung-cap-';

    public function index(Request $request){
        if(!Permissions::hasAllPermission([$this->permission. 'xem-ds'], $this->permission)) return redirect('/');

        $items_per_page = intval($request->items_per_page);
        $items_per_page = $items_per_page > 0 ? $items_per_page : 20;

        $records = Suppliers::search($request)->paginate($items_per_page);
        $is_permission_create = Permissions::hasAllPermission([$this->permission. 'them-moi']);

        return view('admin.suppliers.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod,
            'permission' => $this->permission,
            'is_permission_create' => $is_permission_create
        ]);
    }

    public function show($id){
        if(empty($id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);

            $record = new Suppliers();
            $title = 'Thêm mới';
        }else{
            $record = Suppliers::find($id);
            $title = "Cập nhật";
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);

        }

        $provinces = Provinces::where('parent_id', 0)->orderBy('name')->select('id', 'name')->get()->toArray();
        foreach($provinces as $key => $item) {
            $provinces[$key]['childrens'] = Provinces::where('parent_id', $item['id'])->orderBy('name')->select('id', 'name')->get()->toArray();
        }

        $activity_status = (array)config('constants.activity_status');
        unset( $activity_status[0] );

        $html = (string)view('admin.suppliers.edit', [
            'title' => lcfirst($this->title),
            'mod' => $this->mod,
            'record' => $record,
            'provinces' => $provinces,
            'activity_status' => $activity_status,
            'permission' => $this->permission,
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreSuppliers $request){
        if(empty($request->id)){
            if(!Permissions::hasAllPermission([$this->permission. 'them-moi'], $this->permission)) return response()->json(['st' => 403]);
            $record = new Suppliers();
        }else{
            $record = Suppliers::find($request->id);
            if(!Permissions::hasAllPermission([$this->permission. 'cap-nhat'], $this->permission, $record)) return response()->json(['st' => 403]);
        }
        $record->fill($request->all())->save();

        return response()->json(['st' => 200]);
    }

    public function destroy($id){
        if(!Permissions::hasAllPermission([$this->permission. 'xoa'], $this->permission, User::find($id))) return response()->json(['st' => 403]);

        $st = Suppliers::find($id)->delete();
        return response()->json(['html' => $st]);
    }
}
