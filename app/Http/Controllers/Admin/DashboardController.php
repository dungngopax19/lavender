<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use App\Models\Orders;
use App\Models\OrderDetail;
use App\Models\Branches;
use App\Helpers\Order\Report;
use App\Helpers\Order\Status;


class DashboardController extends Controller{

	public function index(Request $request) {
        $order_status= Status::status();
		return view('admin.dashboard.index', [
            'title' => 'Tổng quan',
            'order_status' => $order_status,
        ]);
	}

	public function report01(Request $request) {
		$params = $request->all();
		$reportType = isset($params['report_type']) ? $params['report_type'] : 1;
		$startDate = isset($params['start_date']) ? $params['start_date'] : date('Y-m-d');
		$endDate = isset($params['end_date']) ? $params['end_date'] : date('Y-m-d');
        $items_per_page = min(intval($request->items_per_page), 20);
		

		$records = array();
		$data = OrderDetail::getOrderDetailReport01($startDate, $endDate, $reportType, $items_per_page, $records);
		
        foreach (Input::except('page') as $input => $value){
            $records->appends($input, $value);
		}

		$htmlPagination = (String)view('admin.components.render-pagination', compact('records'));
		
		return response()->json(['st' => 200, 'data' => $data, 'htmlPagination' => $htmlPagination]);
	}

	public function report02(Request $request) {
		$params = $request->all();
		$reportType = isset($params['report_type']) ? $params['report_type'] : 1;
		$data = Orders::getOrdersReport01($reportType, $request->year_report_customer);
		return response()->json(['st' => 200, 'data' => $data]);
    }

    public function revenue(Request $request) {
		$data = Report::revenue($request->start_date, $request->end_date);

		return response()->json(['st' => 200, 'data' => $data]);
	}

    public function revenueOfBranches(Request $request) {
		$data = Report::revenueOfBranches($request->start_date, $request->end_date);

		return response()->json(['st' => 200, 'data' => $data]);
	}


}
