<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request()->all();
        if(isset($request['id'])){
            $rule_password = 'nullable';
        }else{
            $rule_password = 'required|max:16';
        }

        return [
            'name' => 'bail|required|max:255',
            'username' => 'bail|required|max:255|unique:users,username,'. request()->id.',id',
            'password' => 'bail|'.$rule_password,
            're_password' => 'bail|'.$rule_password.'|same:password',
            'roles'=> 'bail|required',
            'email' => 'bail|nullable|email|max:255|unique:users,email,'. request()->id.',id',
            'birthday' => 'bail|nullable|date',
            'phone' => 'bail|required|max:100',
            'province_id' => 'bail|nullable',
            'address' => 'bail|nullable',
            'text' => 'bail|nullable',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Tên người dùng là bắt buộc',
            'username.required' => 'Tên đăng nhập là bắt buộc',
            'username.unique' => 'Tên đăng nhập đã tồn tại',
            'password.required' => 'Mật khẩu là bắt buộc',
            're_password.required' => 'Xác nhận mật khẩu là bắt buộc',
            're_password.same' => 'Xác nhận mật khẩu không khớp với mật khẩu',
            'roles.required' => 'Nhóm người dùng là bắt buộc',
            'email.required' => 'Email là bắt buộc',
            'email.unique' => 'Email đã tồn tại',
        ];

    }
}
