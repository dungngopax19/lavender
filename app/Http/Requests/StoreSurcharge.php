<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSurcharge extends FormRequest
{
    /**
     * Determine if the Surcharge is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request()->all();
        return [
            'activity_status' => 'bail|required',
            'code' => 'bail|required|unique:surcharges,code,'. $request['id'].',id|max:255',
            'type' => 'bail|required|max:255',
            'value' => 'bail|required|numeric|min:0|check_percent:'.$request['value_type'].'|max:999999999999',
            'branches_ides' => 'bail|required',
        ];
    }

    public function messages(){
        return [
            'activity_status.*'      => 'Trạng thái là bắt buộc',
            'code.required'          => 'Mã thu khác là bắt buộc',
            'code.unique'            => 'Mã thu khác đã tồn tại',
            'type.required'          => 'Loại thu khác là bắt buộc',
            'value.*'                => 'Giá trị không hợp lệ',
            'branches_ides.required' => 'Chi nhánh là bắt buộc',
        ];

    }
}
