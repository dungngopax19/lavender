<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePartnerDelivery extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request()->all();
        return [
            'role' => 'bail|required',
            'code' => 'bail|required|unique:partner_delivery,code,'. $request['id'].',id|max:255',
            'name' => 'bail|required|max:255',
            'phone' => 'bail|nullable|max:16',
            'email' => 'bail|nullable|email|max:256',
            'province_id' => 'bail|nullable|exists:provinces,id',
            'address' => 'bail|nullable|max:1024',
            'note' => 'bail|nullable|max:1024',
            'activity_status' => 'bail|required',
        ];
    }

    public function messages(){
        return [
            'role.required'     => 'Loại đối tác là bắt buộc',
            'code.required'     => 'Mã đối tác là bắt buộc',
            'code.unique'       => 'Mã đối tác đã tồn tại',
            'name.required'     => 'Tên đối tác là bắt buộc',
            'phone.required'    => 'Điện thoại là bắt buộc',
            'province_id'       => 'Khu vực không hợp lệ',
            'address.*'         => 'Địa chỉ không hợp lệ',
            'email.*'           => 'Email không hợp lệ',
            'note.*'            => 'Ghi chú không hợp lệ',
            'activity_status.*' => 'Trạng thái là bắt buộc',
        ];

    }
}
