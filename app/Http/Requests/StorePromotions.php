<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePromotions extends FormRequest
{
    /**
     * Determine if the customer is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request()->all();
        return [
            'name' => 'bail|required',
            'price' => 'bail|required',
            'from_date' => 'bail|nullable|required|date',
            'to_date' => 'bail|nullable|required|date',
        ];
    }

    public function messages(){
        return [
            'name.required'      => 'Tên chương trình là bắt buộc',
            'price.required'     => 'Giá tiền là bắt buộc',
            'from_date.required' => 'Ngày bắt đầu là bắt buộc',
            'to_date.required'   => 'Ngày kết thúc là bắt buộc',
        ];

    }
}
