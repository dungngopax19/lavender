<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => "bail|required|unique:products,code,". $this->id .",id|max:255",
            'name' => 'bail|required|max:255',
            'product_types_id' => 'bail|exists:product_types,id',
            'product_categories_id' => 'bail|exists:product_categories,id'
        ];
    }

    public function messages(){
        return [
            'code.required' => 'Mã sản phẩm là bắt buộc',
            'code.unique' => 'Mã sản phẩm đã được sử dụng',
            'name.required' => 'Tên sản phẩm là bắt buộc',
            'product_types_id.exists' => 'Loại sản phẩm là bắt buộc',
            'product_categories_id.exists' => 'Nhóm sản phẩm là bắt buộc'
        ];

    }
}