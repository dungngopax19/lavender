<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUnitInventory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'bail|required|max:255',
            'unit' => 'bail|required'
        ];
    }

    public function messages(){
        return [
            'code.required' => 'Code là bắt buộc',
            'unit.required' => 'Số mét vải là bắt buộc',
        ];

    }
}