<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreOrders extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if($request->orders_id){
            return [];
        }

        return [
            'customers_id' => 'bail|required|exists:customers,id',
            'users_id' => 'bail|required|exists:users,id',
            'channel' => 'bail|required',
            // 'orders_status_id' => 'bail|required',

            'name' => 'bail|required',
            'provinces_id' => 'required',

            // 'lading_code' => 'bail|required',
            // 'type_services_id' => 'bail|required',
            // 'partner_delivery_id' => 'bail|required',
            // 'delivery_charges' => 'bail|required',
            // 'delivery_time'  => 'bail|required',
        ];
    }

    public function messages(){
        return [
            'customers_id.exists' => 'Khách hàng là bắt buộc',
            'users_id.exists' => 'Người nhận đặt là bắt buộc',
            'users_id.required' => 'Người nhận đặt là bắt buộc',
            'channel.required' => 'Kênh bán là bắt buộc',
            // 'orders_status_id.required' => 'Trạng thái là bắt buộc',
            
            'name.required' => 'Tên người nhận hàng là bắt buộc',
            'phone.required' => 'Số điện thoại là bắt buộc',
            'address.required' => 'Địa chỉ nhận hàng là bắt buộc',
            'provinces_id.required' => 'Khu vực là bắt buộc',

            // 'shipment_details.lading_code.required' => 'bail|required', 
            // 'shipment_details.type_services_id.required' => 'bail|required',
            // 'shipment_details.partner_delivery_id.required' => 'bail|required',
            // 'shipment_details.delivery_charges.required' => 'bail|required',
            // 'shipment_details.delivery_time.required'  => 'bail|required',
        ];

    }
}