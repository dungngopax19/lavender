<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInvoice extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'users_id' => 'bail|required|exists:users,id',
            'orders_status_id' => 'bail|required',
            'shipment_status_id' => 'bail|required',
            'channel' => 'bail|required',
        ];
    }

    public function messages(){
        return [
            'users_id.required' => 'Người bán là bắt buộc',
            'orders_status_id.required' => 'Trạng thái đơn hàng là bắt buộc',
            'shipment_status_id.required' => 'Trạng thái giao hàng là bắt buộc',
            'channel.required' => 'Kênh bán là bắt buộc',
        ];

    }
}
