<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMembershipGift extends FormRequest
{
    /**
     * Determine if the Surcharge is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request()->all();
        

        $rule_compare_date = '';
        if(isset($request['is_expires'])){
            $rule_date = 'nullable';
        }else{
            $rule_compare_date = ($request['start_date'] === $request['end_date']) ? '' : '|before:end_date';
            $rule_date = 'required';
        }
        $arrRule = [
            'membership_id' => 'bail|required|unique_membership_id:'.$request['id'],
            'unit' => 'bail|nullable|max:255',
            'start_date' => 'bail|'. $rule_date .'|date'. $rule_compare_date,
            'end_date' => 'bail|' .$rule_date. '|date',
        ];

        if($request['unit'] == '%'){
            $arrRule['value'] = "bail|nullable|lte:100";
        }

        return $arrRule;

    }

    public function messages(){
        return [
            'membership_id.required'   => 'Thành viên là bắt buộc',
            'membership_id.unique_membership_id'   => 'Thành viên đã tồn tại',
            'value.*'           => 'Giảm giá không hợp lệ',
            'start_date.required' => 'Thời gian bắt đầu chưa được chọn',
            'start_date.before' => 'Thời gian bắt đầu phải trước ngày kết thúc',
            'end_date.required' => 'Thời gian kết thúc chưa được chọn',
        ];

    }
}
