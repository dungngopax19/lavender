<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:255',
            'username' => 'bail|required|max:255',
            'email' => 'bail|email|max:255',
            'birthday' => 'nullable|date',
            'phone' => 'max:100'
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Họ tên là bắt buộc'
        ];

    }
}
