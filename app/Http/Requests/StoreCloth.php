<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCloth extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ma_sp' => 'bail|required|max:255',
            'kho_vai' => 'bail|required|max:255'
        ];
    }

    public function messages(){
        return [
            'ma_sp.required' => 'Mã sản phẩm là bắt buộc',
            'kho_vai.required' => 'Khổ vải là bắt buộc',
        ];

    }
}