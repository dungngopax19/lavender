<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreReceipts extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customers_id' => 'bail|required|exists:customers,id',
            // 'users_id' => 'bail|required|exists:users,id',
            'orders_id' => 'bail|required',
            'payment_method' => 'bail|required',
            'price' => 'bail|required',
        ];
    }

    public function messages(){
        return [
            'customers_id.required' => 'Khách hàng là bắt buộc',
            // 'users_id.required' => 'Người nhận đặt là bắt buộc',
            'orders_id.required' => 'Đơn hàng là bắt buộc',
            'payment_method.required' => 'Phương thức là bắt buộc', 
            'price.required' => 'Kênh bán là bắt buộc', 
        ];

    }
}