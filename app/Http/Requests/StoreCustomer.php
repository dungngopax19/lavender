<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCustomer extends FormRequest
{
    /**
     * Determine if the customer is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request()->all();
        return [
            'role' => 'bail|required',
            'company' => 'bail|nullable|required_if:role,2|max:255',
            'name' => 'bail|required|max:255|check_exist_customer:'.$this->id.','.$this->name.','.$this->phone. ',' . $this->province_id,
            'tax_code' => 'bail|nullable|max:16',
            'phone' => 'bail|required',
            'gender' => 'bail|nullable',
            'birthday' => 'bail|nullable|date',
            'email' => 'bail|nullable|email|max:256',
            'province_id' => 'bail|required|exists:provinces,id',
            'address' => 'bail|required|max:1024',
            'customers_types_id' => 'bail|nullable',
            'text' => 'bail|nullable|max:1024',
            'activity_status' => 'required',
        ];
    }

    public function messages(){
        return [
            'role.required'     => 'Loại khách hàng là bắt buộc',
            'company.*'         => 'Tên công ty là bắt buộc',
            'code.required'     => 'Mã khách hàng là bắt buộc',
            'code.unique'       => 'Mã khách hàng đã tồn tại',
            'name.required'     => 'Tên khách hàng là bắt buộc',
            'name.check_exist_customer' => 'Khách hàng đã tồn tại',
            'phone.required'    => 'Điện thoại là bắt buộc',
            'phone.digits_between' => 'Điện thoại phải có từ 10 đến 12 chữ số',
            'phone.unique' => 'Số điện thoại ngày đã được sử dụng',
            'province_id.*'     => 'Khu vực là bắt buộc',
            'address.required'  => 'Địa chỉ là bắt buộc',
            'tax_code.*'        => 'Mã số thuế không hợp lệ',
            'gender.*'          => 'Giới tính không hợp lệ',
            'birthday.*'        => 'Ngày sinh không hợp lệ',
            'email.*'           => 'Email không hợp lệ',
            'text.*'            => 'Ghi chú không hợp lệ',
            'activity_status.*' => 'Trạng thái là bắt buộc',
            'gender.required'     => 'Giới tính là bắt buộc',
        ];

    }
}
