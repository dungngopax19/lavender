<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Traits\CommonTrait;
use App\Models\Traits\UsersSearch;
use App\Helpers\DateHelper;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Common;
use App\Helpers\Permissions;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use UsersSearch;
    use CommonTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username', 'email', 'password', 'address', 'birthday', 'phone', 'province_id', 'address', 'text', 'status', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function branches(){
        return $this->hasManyThrough(
            'App\Models\Branches',
            'App\Models\BranchesUsers',
            'users_id', // Foreign key on users table...
            'id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'branches_id' // Local key on users table...
        );

        return $this->hasManyThrough('App\Models\BranchesUsers', 'users_id', 'id');
    }

    public function getBirthdayDisplayAttribute(){
        return DateHelper::formatDate($this->birthday, 'd/m/Y');
    }

    public function setBirthdayAttribute($value){
        $this->attributes['birthday'] = \Carbon\Carbon::parse($value);
    }

    public function getThumbnailDisplayAttribute()
    {
        $parsed = parse_url($this->thumbnail);
        if (empty($parsed['scheme'])) {
            return $this->thumbnail ? Storage::url($this->thumbnail) : 'images/noimage.jpg';
        }
        return $this->thumbnail;
    }

    public function modelHasRoles(){
        return $this->hasMany('App\Models\ModelHasRoles', 'model_id');
    }

    public static function getIdByName($name){
        $user = User::where('name', $name)->first();
        if($user){
            return $user->id;
        }
        return null;
    }

    public function branchesUsers(){
        return $this->hasMany('App\Models\BranchesUsers', 'users_id', 'id');
    }
}
