<?php
    use App\Helpers\Common;
    use App\Models\TmpWareHouse;

    $codes = TmpWareHouse::distinct()->pluck('code');
    $branchs = TmpWareHouse::distinct()->pluck('chi_nhanh');
?>

<div class="box box-code">
    <div class="header row">
        <div class="col-md-12">Tìm kiếm</div>
    </div>

    <div class="content">
        <input type="text" name="ma_sp" value="{{$request->ma_sp}}" placeholder="Tìm theo mã sản phẩm" class="form-control mb-10">    

        <div class="mb-10">
            <select class="select2 form-control" name="kho_vai">
                <option value="">Tìm theo khổ vải</option>    
                <option value="1.6" {{$request->kho_vai == '1.6' ? 'selected' : ''}}>1.6 m</option>
                <option value="2.5" {{$request->kho_vai == '2.5' ? 'selected' : ''}}>2.5 m</option>
            </select>
        </div>
        
        <input type="submit" class="btn btn-success" value="Tìm kiếm" style="width:100%">
    </div>
</div>