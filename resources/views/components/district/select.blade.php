<select class="select2 form-control" name="district_id">
    <option value="0">---</option>
    @foreach($districts as $item)
    <option value="{{ $item['id']}}" @if($item['id'] == $district_id) selected @endif>{{ $item['name']}}</option>
    @endforeach  
</select>