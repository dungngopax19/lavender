<?php
    use App\Helpers\Common;
?>

<div class="box box-order-close">
    <div class="header row">
        <div class="col-md-10">Đơn hàng sắp giao</div>
        <div class="col-md-1"><i class="fas fa-chevron-circle-up"></i></div>
    </div>

    <div class="content">
        <input type="hidden" name="orderClose" value="{{$request->orderClose}}">

        @php
            $codes = (strlen($request->orderClose) == 0) ? [] : explode('_', $request->orderClose);
        @endphp

        @foreach ($orderClose as $k => $item)
            <div class="checkbox">
                <label>
                    <input type="checkbox" {{ in_array( $item, $codes )? 'checked' : '' }} value="{{$item}}">{{$item}}
                </label>
            </div>
        @endforeach
    </div>
</div>