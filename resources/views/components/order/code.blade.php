<?php
    use App\Helpers\Common;
?>

<div class="box box-code">
    <div class="header row">
        <div class="col-md-10">Mã hàng chưa giao</div>
        <div class="col-md-1"><i class="fas fa-chevron-circle-up"></i></div>
    </div>

    <div class="content">
        <input type="hidden" name="codes" value="{{$request->codes}}">

        @php
            $codes = (strlen($request->codes) == 0) ? [] : explode('_', $request->codes);
        @endphp

        @foreach ($productCodes as $k => $item)
            <div class="checkbox">
                <label>
                    <input type="checkbox" {{ in_array( $item, $codes )? 'checked' : '' }} value="{{$item}}">{{$item}}
                    <strong class="color-green">({{ Common::countCodeOrder($item) }})</strong class="color-red">
                </label>
            </div>
        @endforeach
    </div>
</div>