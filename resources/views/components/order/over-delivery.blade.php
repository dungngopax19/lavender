<?php
    use App\Helpers\Common;
?>

<div class="box box-order-over">
    <div class="header row">
        <div class="col-md-10">Đơn hàng quá ngày giao</div>
        <div class="col-md-1"><i class="fas fa-chevron-circle-up"></i></div>
    </div>

    <div class="content">
        <input type="hidden" name="orderOver" value="{{$request->orderOver}}">

        @php
            $codes = (strlen($request->orderOver) == 0) ? [] : explode('_', $request->orderOver);
        @endphp

        @foreach ($orderOver as $k => $item)
            <div class="checkbox">
                <label>
                    <input type="checkbox" {{ in_array( $item, $codes )? 'checked' : '' }} value="{{$item}}">{{$item}}
                </label>
            </div>
        @endforeach
    </div>
</div>