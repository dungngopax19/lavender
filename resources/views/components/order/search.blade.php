<?php
    use App\Helpers\Common;
    use App\Models\TmpWareHouse;

    $codes = TmpWareHouse::distinct()->pluck('code');
    $branchs = TmpWareHouse::distinct()->pluck('chi_nhanh');
?>

<div class="box box-code">
    <div class="header row">
        <div class="col-md-12">Tìm kiếm</div>
    </div>

    <div class="content">
        <div class="mb-10">
            <select class="select2 form-control" name="code">
                <option data-tokens="" value="">Tìm theo tên file upload</option>    
                @foreach ($codes as $item)
                    <option value="{{$item}}" {{$request->code == $item ? 'selected' : ''}}>{{$item}}</option>    
                @endforeach
            </select>
        </div>

        <div class="mb-10">
            <select class="select2 form-control" name="branch">
                <option data-tokens="" value="">Tìm theo chi nhánh</option>    
                @foreach ($branchs as $item)
                    <option value="{{$item}}" {{$request->branch == $item ? 'selected' : ''}}>{{$item}}</option>    
                @endforeach
            </select>
        </div>

        <div class="mb-10">
            @if (empty($selectDonHang))
                <input type="text" name="ma_hoa_don" value="{{$request->ma_hoa_don}}" placeholder="Tìm theo mã Đơn hàng" class="form-control">    
            @else
                <select class="select2 form-control" name="ma_hoa_don">
                    <option data-tokens="" value="">Tìm theo mã Đơn hàng</option>    
                    @foreach ($selectDonHang as $item)
                        <option value="{{$item}}" {{$request->ma_hoa_don == $item ? 'selected' : ''}}>{{$item}}</option>    
                    @endforeach
                </select>
            @endif
        </div>

        <div class="mb-10">
            @if (empty($selectMaHang))
                <input type="text" name="ma_hang" value="{{$request->ma_hang}}" placeholder="Tìm theo mã hàng" class="form-control">    
            @else
                <select class="select2 form-control" name="ma_hang">
                    <option data-tokens="" value="">Tìm theo mã hàng</option>    
                    @foreach ($selectMaHang as $item)
                        <option value="{{$item}}" {{$request->ma_hang == $item ? 'selected' : ''}}>{{$item}} ({{Common::countCodeOrder($item)}})</option>    
                    @endforeach
                </select>
            @endif
        </div>
        
        <input type="submit" class="btn btn-success" value="Tìm kiếm" style="width:100%">
    </div>
</div>