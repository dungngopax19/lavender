<?php
    use App\Helpers\Common;
?>

<div class="box box-status">
    <div class="header row">
        <div class="col-md-12">Trạng thái giao hàng</div>
    </div>

    <div class="content">
        <input type="hidden" name="tt_giao_hang" value="{{$request->tt_giao_hang}}">
        @php
            $status = config('constants.order_status');
            $chks = (strlen($request->tt_giao_hang) == 0) ? [] : explode('_', $request->tt_giao_hang);
        @endphp
        @foreach ($status as $k => $item)
            <div class="checkbox">
                <label>
                    <input type="checkbox" {{ in_array( $k, $chks )? 'checked' : '' }} value="{{$k}}">{{$item}}
                    <strong class="color-green">({{ Common::countStatusOrder($k) }})</strong>
                </label>
            </div>
        @endforeach

        <div class="checkbox">
            <label>
                <input type="checkbox" {{ in_array( -1, $chks )? 'checked' : '' }} value="{{-1}}">Không xác định
                <strong class="color-green">({{ Common::countStatusOrder(-1) }})</strong>
            </label>
        </div>
    </div>
</div>