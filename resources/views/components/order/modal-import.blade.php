<div class="modal" id="import-excel">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            
            <form class="form-horizontal" id="main-form" method="post" action="/import-data" enctype="multipart/form-data">
                {{csrf_field()}}
        
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">Chọn file Execel</div>
                        <div class="col-md-6">
                            <input type="file" name="fileUpload">
                        </div>
                    </div>
                </div>
        
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-success">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="clear-data">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form-horizontal" id="main-form" method="post" action="/clear-data" enctype="multipart/form-data">
                {{csrf_field()}}
        
                <!-- Modal body -->
                <div class="modal-body">
                    <h6>Bạn có thật sự muốn xoá tất cả dữ liệu trong hệ thống không?</h6>
                </div>
        
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-success">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>