@foreach ($records as $k => $item)
    <div class="media">
        <div class="media-left mr-2">
            <img src="{{$item->resizeImage($item->userSend['image'], 60)}}" class="media-object" style="width:60px">
        </div>
        <div class="media-body">
            <div class="media-heading">
                <div class="row">
                    <div class="col-md-6"><strong>{{$item->userSend['name']}}</strong></div>
                    <div class="col-md-6 text-right">{{$item->created_at_hidmy}}</div>
                </div>
            </div>
            <div>{{$item->content}}</div>
        </div>
    </div>
@endforeach
