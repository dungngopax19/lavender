<header>
    <div class="top">
        <div class="container">
            <nav class="navbar">
                <a class="navbar-brand" href="#">
                    <img src="{{asset('images/kiotvietLogo.png')}}" alt="">
                </a>
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="fas fa-paint-brush"></i> Chủ đề
                        </a>
                    </li>
                    <li class="nav-item dropdown hover">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Chi nhánh 1</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Chi nhánh 1</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Chi nhánh 2</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Chi nhánh 3</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown hover">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-clipboard-check"></i> Thiết lập
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Menu item 1</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Menu item 2</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown hover">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user"></i> User
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-unlock-alt"></i> Tài khoản
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-external-link-alt"></i> Đăng xuất
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="main-menu">
        <div class="container">
            <ul class="nav nav-pills">
                <li class="nav-item dropdown hover">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-cube"></i> Hàng hóa
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item active" href="#">
                            <i class="fas fa-th"></i> Danh mục
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-tags"></i> Thiết lập giá
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-check-square-o"></i> Kiểm kho
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown hover">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-exchange"></i> Giao dịch
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-share-square-o"></i> Nhập hàng
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-share-square"></i> Trả hàng nhập
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-truck"></i> Chuyển hàng
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown hover">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-male"></i> Đối tác
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-user"></i> Khách hàng
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-undo"></i> Nhà cung cấp
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</header>