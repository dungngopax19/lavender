
<div class="row form-group {{$className ?? ''}} fg-{{$name}}">
    <div class="col-md-4">
        <label>
            {{$label}}
            
            @isset ($required)
                <span class="text-danger"> ※</span>
            @endisset
        </label>
    </div>
    <div class="col-md-8">
        @foreach ($data as $i => $item)
            <div class="pretty p-default p-round d-flex mt-3">
                <input type="radio" value="{{$i}}" id="{{$name.$i}}" {{$value == $i ? 'checked' : ''}} name="{{$name}}">
                <div class="state">
                    <label for="{{$name.$i}}">{{$item}}</label>
                </div>
            </div>    
        @endforeach

        <div class="error-msg"></div>
    </div>
</div>