@php
    $colLeft = empty($colLeft) ? 4 : $colLeft;
    $typeTxt = (isset($type) && in_array($type, ['text', 'number'])) ? $type : 'text';
@endphp
<div class="row form-group {{$className ?? ''}} fg-{{$name}}">
    <div class="col-md-{{$colLeft}} {{$labelAlign??'text-left'}}">
        <label>
            {{$label}}
            
            @isset ($required)
                <span class="text-danger"> ※</span>
            @endisset
        </label>
    </div>
    <div class="col-md-{{$colRight ?? 12 - $colLeft}}">
        <input maxlength="{{$maxlength ?? 1000}}" type="{{$typeTxt}}" class="form-control {{$classObj ?? ''}}" {{$readonly ?? ''}} name="{{$name??''}}" value="{{$value??''}}" placeholder="{{$placeholder??''}}">
        <div class="error-msg"></div>
    </div>
</div>