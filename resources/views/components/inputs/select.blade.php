@php
    use App\Helpers\Common;
    $colLeft = empty($colLeft) ? 4 : $colLeft;

    $isArrayMulti = Common::isArrayMulti($data);
    $value = $value ?? 0;
@endphp
<div class="row form-group {{$className ?? ''}} fg-{{ (empty($multiple)) ? $name : str_replace('[]', '', $name) }}">
    <div class="col-md-{{$colLeft}} {{$labelAlign??'text-left'}}">
        <label>
            {{$label}}

            @isset ($required)
                <span class="text-danger"> ※</span>
            @endisset
        </label>
    </div>
    <div class="col-md-{{$colRight ?? 12 - $colLeft}}">

        @if (!empty($disabled))
            <input type="hidden" name="{{$name}}" value="{{$record->membership_id}}">
        @endif

        <select class="{{empty($disabled) ? 'select2' : 'select2-disabled'}} form-control {{$classObj ?? ''}}" 
                name="{{empty($disabled) ? $name : ''}}" 
                {{empty($multiple) ? '' : 'multiple="multiple"'}}>
            <option value="">---</option>
            @foreach ($data as $i => $item)
                @if ($isArrayMulti)

                    @if (empty($item['childrens']))
                        @if(isset($multiple))
                            @if(isset($select))
                            <option {{ (in_array($item[$select], $value))? 'selected' : ''}} value="{{$item[$select]}}">{{$item['name']}}</option>
                            @else
                            <option {{ (in_array($item['id'], $value))? 'selected' : ''}} value="{{$item['id']}}">{{$item['name']}}</option>
                            @endif
                        @else
                            <option {{ ($item['id'] == $value)? 'selected' : ''}} value="{{$item['id']}}">{{$item['name']}}</option>
                        @endif
                    @else
                        <option {{ ($item['id'] == $value)? 'selected' : ''}} value="{{$item['id']}}">{{$item['name']}}</option>
                        
                        @foreach ($item['childrens'] as $itemChild)
                            <option {{ in_array($itemChild['id'], (array)($value))? 'selected' : ''}} value="{{$itemChild['id']}}">&nbsp&nbsp&nbsp{{$itemChild['name']}}</option>
                        @endforeach
                    @endif

                @else
                    <option {{ ($i == $value)? 'selected' : ''}} value="{{$i}}">{{$item}}</option>
                @endif
            @endforeach
        </select>

        <div class="error-msg"></div>
    </div>
</div>
