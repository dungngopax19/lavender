@php
    $colLeft = empty($colLeft) ? 4 : $colLeft;
@endphp
<div class="row form-group {{$className ?? ''}}">
    <div class="col-md-{{$colLeft}} {{$labelAlign??'text-left'}}">
        <label>{{$label}}<label>    
    </div>
    <div class="col-md-{{$colRight ?? 12 - $colLeft}} mt-7">{{$value}}</div>
</div>