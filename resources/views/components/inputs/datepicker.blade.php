@php
    use App\Helpers\DateHelper;
    $colLeft = empty($colLeft) ? 4 : $colLeft;

    $value = $value ? DateHelper::formatDate($value, 'd-m-Y') : '';
@endphp

<div class="row form-group {{$className ?? ''}} fg-{{$name}}">
    <div class="col-md-{{$colLeft}} {{$labelAlign??'text-left'}}">
        <label>
            {{$label}}

            @isset ($required)
                <span class="text-danger"> ※</span>
            @endisset
        </label>
    </div>
    <div class="col-md-{{$colRight ?? 12 - $colLeft}}">
        <input type="text" class="form-control singleDatePicker" {{$disabled ?? ''}} {{$readonly ?? ''}} name="{{$name??''}}" value="{{$value??''}}" placeholder="{{$placeholder??''}}">
        <div class="error-msg"></div>
    </div>
</div>
