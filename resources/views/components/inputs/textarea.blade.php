@php
    $colLeft = empty($colLeft) ? 4 : $colLeft;
@endphp
<div class="row form-group {{$className ?? ''}} fg-{{$name}}">
    <div class="col-md-{{$colLeft}} {{$labelAlign??'text-left'}}">
        <label>
            {{$label}}
            
            @isset ($required)
                <span class="text-danger"> ※</span>
            @endisset
        </label>
    </div>
    <div class="col-md-{{$colRight ?? 12 - $colLeft}}">
        <textarea class="form-control" {{$readonly ?? ''}} name="{{$name}}" rows="{{$rows ?? 4}}">{{$value}}</textarea>
        <div class="error-msg"></div>
    </div>
</div>