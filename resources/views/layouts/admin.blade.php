<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- META SECTION -->
    <title>@yield('title', 'Tổng quan') - CRM Lavender</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @section('css')
        <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="{{asset('css/pretty-checkbox.min.css')}}">
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.3.911/styles/kendo.common-material.min.css" />
        <link rel="stylesheet" href="{{asset('css/kendo.material.min.css')}}" />
        <link rel="stylesheet" href="{{asset('css/jquery.fancybox.min.css')}}" />

        <link rel="stylesheet" type="text/css" href="{{asset('css/daterangepicker.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-tagsinput.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/theme-blue.css')}}" media="all">
        <link rel="stylesheet" type="text/css" href="{{asset('css/cart.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/report.css')}}">
    @show

    <script>
        var site_url = "<?php echo url('admin')?>/";
        var CSRF_TOKEN = "<?php echo csrf_token()?>";
        var urlNoImage = "{{asset('images/noimage.jpg')}}";
    </script>

</head>

<body>
    <div id="app">
        @include('admin.components.header')
        <main>
            <div class="container-fluid">
                @yield('content')
            </div>
        </main>
    </div>

    <div id="preloader" class="hide" style="background: url({{asset('images/loading.gif')}}) no-repeat center center"></div>

    {{-- Modal Global --}}
    <div class="modal fade" id="modal-edit-product">
        <div class="modal-dialog max-width-70 modal-dialog-centered">
            <div class="modal-content form-tabs"></div>
        </div>
    </div>

    <div class="modal fade" id="modal-cart">
        <div class="modal-dialog max-width-100 modal-dialog-centered">
            <div class="modal-content form-tabs"></div>
        </div>
    </div>

    <div class="modal fade" id="modal-add-customer-in-cart">
        <div class="modal-dialog max-width-80 modal-dialog-centered">
            <div class="modal-content form-tabs"></div>
        </div>
    </div>

    <div class="modal fade" id="modal-edit-user">
        <div class="modal-dialog max-width-95 modal-dialog-centered">
            <div class="modal-content form-tabs"></div>
        </div>
    </div>

    <div class="modal fade" id="modal-activity-log">
        <div class="modal-dialog max-width-60 modal-dialog-centered">
            <div class="modal-content"></div>
        </div>
    </div>
    @stack('modals')

    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/kendo.all.min.js')}}"></script>
    <script src="{{asset('js/dropzone.js')}}"></script>

    <script src="{{asset('js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-tagsinput.js')}}"></script>

    <script src="{{asset('js/jquery.form.min.js')}}"></script>
    <script src="{{asset('js/jquery.fancybox.min.js')}}"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/daterangepicker.min.js')}}"></script>
    <script src="{{asset('js/numeral.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-notify.min.js')}}"></script>
    <script src="{{asset('js/admin/app.js')}}"></script>

    @stack('scripts')


</body>
</html>
