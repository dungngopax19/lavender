<div id="report01" class="row item-block products mt-4">
    <div class="col-md-12">
        <div class="row header mb-3">
            <form method="GET" style="display: contents;">
                {{ csrf_field() }}
                {{ method_field('GET') }}
                <div class="col-md-9 mt-2">
                    <strong>TOP HÀNG HÓA BÁN CHẠY THEO</strong>
                    <!-- 
                        1. Thống kê tần suất mua hàng của khách hàng theo ngày
                        2. Thống kê theo số lượng sản phẩm bán chạy -->
                    <select name="report_type">
                        <option value="1" {{isset($request['report_type'])&&$request['report_type']==1 ? 'selected' : ''}}>SỐ LƯỢNG</option>
                        <option value="2" {{isset($request['report_type'])&&$request['report_type']==2 ? 'selected' : ''}}>DOANH THU</option>
                    </select>
                </div>

                <div class="col-md-3 tex-right" style="margin-top: 5px">
                    <input type="text" id="date_range" name="date_range" class="form-control daterange-picker" value="{{date('d/m/Y', strtotime($startDate)) .' - '. date('d/m/Y', strtotime($endDate))}}">
                    <input type="hidden" name="start_date" value="{{$startDate}}">	
                    <input type="hidden" name="end_date" value="{{$endDate}}">
                </div>
            </form>
        </div> 
    </div>

    <div class="col-md-12">
        <div class="demo-section k-content wide">
            <div id="chartReport01" style="height:700px"></div>
        </div>
        <div class="mt-15" id="htmlPagination">{{$records->links()}}</div>
    </div>

</div>


@push('scripts')

<script>
    $(document).ready(function() {

        var report01 = $('#report01');
        var input01 = <?php echo json_encode(!empty($report01) ? $report01 : array()) ?>; 

        report01.find('#date_range').on('apply.daterangepicker', function(ev, picker) {
            report01.find('input[name=start_date]').val(picker.startDate.format('YYYY/MM/DD'));
            report01.find('input[name=end_date]').val(picker.endDate.format('YYYY/MM/DD'));
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            
            getDataAndDrawChart();
        });

        function getDataAndDrawChart(page = 1){
            var params = report01.find('form').serializeArray();
            var url = '/admin/dashboard-report01?page=' + page;
            
            $.ajax({
                type: "GET",
                url: url,
                data: params,
                error: function(xhr, status, error) {},
                success: function( response ) {
                    if (response.st == 200) {
                        if (response.data) {
                            drawReport01(response.data);
                            var chart = $("#chartReport01").data("kendoChart");
                            if (response.data.type == 1) {
                                chart.options.valueAxis = { labels: {  } };
                            } else {
                                chart.options.valueAxis = { labels: { template: "#= generateLabel(value) # tr" } };
                            }

                            chart.options.valueAxis.max = response.data.total;

                            chart.redraw();

                            report01.find('#htmlPagination').html(response.htmlPagination);
                        } 

                        clickEventPagination();
                    }  
                }
            }); 
        }

        report01.find('select[name=report_type]').on('change', function(e) {
            getDataAndDrawChart();
        });

        function getReportData(url) {
            var params = report01.serializeArray();
            $.ajax({
                type: "GET",
                url: url,
                data: params,
                error: function(xhr, status, error) {},
                success: function( response ) {
                    if (response.st == 200) {
                        if (response.data) {
                            drawReport01(response.data);
                            var chart = $("#chartReport01").data("kendoChart");
                            if (response.data.type == 1) {
                                chart.options.valueAxis = { labels: {  } };
                            } else {
                                chart.options.valueAxis = { labels: { template: "#= generateLabel(value) # tr" } };
                            }
                            chart.redraw();
                        } 
                    }  
                }
            }); 
        }
        
        function drawReport01(data) {
            $("#chartReport01").kendoChart({
                title: {
                    text: " "
                },
                legend: {
                    visible: false
                },
                seriesDefaults: {
                    type: "bar"
                },
                series: [{
                    name: data.label,
                    data: data.value
                }],
                valueAxis: {
                    max: data.total,
                    line: {
                        visible: false
                    },
                    minorGridLines: {
                        visible: true
                    }, 
                },
                categoryAxis: {
                    categories: data.name,
                    majorGridLines: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: true,
                    template: "#= series.name #: #= generateTooltip(value) #"
                }
            }); 
            if ($('select[name=report_type]').val() == '2') {
                var chart = $("#chartReport01").data("kendoChart");
                chart.options.valueAxis = { labels: { template: "#= generateLabel(value) # tr" } };
                chart.redraw();
            } 

            clickEventPagination();
        } 

        function getUrlParameter(sPageURL, sParam) {
            var sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            sURLVariables = sURLVariables.length > 1 ? sURLVariables : sPageURL.split('?');

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };

        function clickEventPagination(){
            report01.find('a.page-link').on('click', function(e) {
                e.preventDefault();
                var page = getUrlParameter($(this).attr('href'), 'page');
                getDataAndDrawChart(page);
            });
        }
        
        drawReport01(input01); 
        
    });
</script> 
@endpush