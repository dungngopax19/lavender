<div class="row item-block">
    <div class="col-md-12">
        <div class="row header mb-3">
            <div class="col-md-9">
                <strong>DOANH THU:
                    <strong class="change-total-revenue text-danger"></strong>
                </strong>
            </div>

            <div class="col-md-3">
                <input type="text" name="date-filter-revenue" value="" class="form-control"/>
            </div>
        </div>
    </div>

    <div class="col-md-12" id="js-data-revenue">
        <div class="demo-section k-content wide">
            <div id="chart-revenue"></div>
        </div>
    </div>

</div>

@push('scripts')
<script>
    // select date filter
    $('input[name="date-filter-revenue"]').daterangepicker({
        autoUpdateInput: true,
        startDate: '{{ $startDate}}',
        endDate: '{{ $endDate}}',
        locale: {
            cancelLabel: 'Clear',
            format: 'DD/MM/YYYY'
        }
    });

    $('input[name="date-filter-revenue"]').on('apply.daterangepicker', function(ev, picker) {
        var start_date = picker.startDate.format('MM-DD-YYYY');
        var end_date = picker.endDate.format('MM-DD-YYYY');
        var day = ((new Date(end_date)).getTime() - (new Date(start_date)).getTime())/(24*60*60*1000) +1;

        var url = 'dashboard-revenue?start_date='+picker.startDate.format('YYYY-MM-DD')+'&end_date='+picker.endDate.format('YYYY-MM-DD');
        getDataRevenue(url);

        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

    $('input[name="date-filter-revenue"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    // end select date filter
    function formatMoney(n, c, d, t) {
        var c = isNaN(c = Math.abs(c)) ? 2 : c,
          d = d == undefined ? "." : d,
          t = t == undefined ? "," : t,
          s = n < 0 ? "-" : "",
          i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
          j = (j = i.length) > 3 ? j % 3 : 0;

        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    function generateLabel(value){
        if(value > 10){
            return formatMoney(value/1000000);
        }
        return value;
    }

    function generateTooltip(value){
        return formatMoney(value, 0);
    }

    function changeTotalRevenue(value){
        value = formatMoney(value, 0);
        $('.change-total-revenue').html(value);
    }

    // init data
    var _data_revenue = JSON.parse('{!! $data_revenue!!}');
    
    changeTotalRevenue(_data_revenue.total);

    function getDataRevenue(url) {
        $.ajax({
            type: "GET",
            url: url,
            error: function(xhr, status, error) {},
            success: function( response ) {
                if (response.st == 200) {
                    createChartRevenue(response.data);
                    changeTotalRevenue(response.data.total);
                }
            }
        });
    }

    function createChartRevenue(data) {
        $("#chart-revenue").kendoChart({
            legend: {
                position: "bottom"
            },
            seriesDefaults: {
                type: "column",
                stack: true
            },
            series: data,
            valueAxis: {
                labels: {
                    template: "#= generateLabel(value) # tr"
                },
                line: {
                    visible: false
                }
            },
            categoryAxis: {
                categories: data.dates,
                majorGridLines: {
                    visible: false
                }
            },
            tooltip: {
                visible: true,
                template: "#= series.name #: #= generateTooltip(value) #"
            }
        });
    }

    $(document).ready(function() {
        createChartRevenue(_data_revenue);
    });

</script>
@endpush
