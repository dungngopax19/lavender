<div class="panel panel-warning widget-dashboard" id="orders-status-{{ $config['type']}}">
    <div class="panel-heading">
        <div class="panel-title-box">
            <strong>
                <i class="fas {{ $config['icon']}}"></i> {{ $config['name']}} ({{number_format($records->total())}})
            </strong>
        </div>
    </div>

    <div class="container-body">
        <div class="panel-body text-center h-60">
            <button class="btn btn-default float-left previous-button" id="previous-button-type-{{ $config['type']}}">
                <i class="fas fa-angle-double-left"></i>
            </button>

            <button class="btn btn-default float-left d-none icon-spinner" id="spinner-type-{{ $config['type']}}">
                <i class="fa fa-spinner fa-spin"></i>
            </button>

            <span>
                <input class="{{ ( in_array($config['type'], [1,3]) ) ? 'hide' : '' }} date-range-picker" type="text" id="picker-type-{{ $config['type']}}" value=""/>
            </span>

            <button class="btn btn-default float-right next-button" id="next-button-type-{{ $config['type']}}">
                <i class="fas fa-angle-double-right"></i>
            </button>
        </div>

        <div class="panel-body pt-1 pb-0">
            <div class="list-content list-content-type-{{ $config['type']}}">
                @if( (int)$config['type'] < 5)
                    @include('admin.components.orders.item-list', ['records'=>$records])
                @else
                    @include('admin.components.customers.customer-list', ['records'=>$records, 'type' => $config['type']])
                @endif
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var page_type_1 = 1;
        var total_page = parseInt('{{ $records->lastPage()}}');
        var start_date_type_1 = '{{ $start_date}}';
        var end_date_type_1 = '{{ $end_date}}';

        var objWidget = $('#orders-status-{{ $config['type']}}');

        //Date range picker
        objWidget.find('#picker-type-{{ $config['type']}}').daterangepicker({
            autoUpdateInput: true,
            startDate: '{{ $start_date}}',
            endDate: '{{ $end_date}}',
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY'
            },
        }).on('apply.daterangepicker', function(ev, picker) {
            var start_date = picker.startDate.format('MM-DD-YYYY');
            var end_date = picker.endDate.format('MM-DD-YYYY');

            start_date_type_1 = picker.startDate.format('DD-MM-YYYY');
            end_date_type_1 = picker.endDate.format('DD-MM-YYYY');

            getOrders(1);
        });

        function hideSpinner(){
            $('#spinner-type-{{ $config['type']}}').addClass('d-none');
        }
        function showSpinner(){
            $('#spinner-type-{{ $config['type']}}').removeClass('d-none');
        }

        function getOrders(page){
            showSpinner();
            $(this).submitDataAjax({
                'url' : site_url + 'get-orders?type='+{{ $config['type'] }}+'&start_date='+start_date_type_1+'&end_date='+end_date_type_1+'&page=' + page,
                'method': 'GET',
                'success': function(res){
                    if(res.data){
                        var html = res.data;
                        $('.list-content-type-{{ $config['type']}}').html(html);
                        hideSpinner();
                        total_page = res.total_page;
                    }
                }
            });
        }

        $('#next-button-type-{{ $config['type']}}').on('click', function(){
            if(page_type_1 < total_page){
                page_type_1++;
                getOrders(page_type_1);
            }
        })

        $('#previous-button-type-{{ $config['type']}}').on('click', function(){
            if(page_type_1 > 1 ){
                page_type_1--;
                getOrders(page_type_1);
            }
        })

    });
</script>
