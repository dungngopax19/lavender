<div class="row item-block products mt-4">
    <div class="col-md-12">
        <div class="row header mb-3">
            <form id="report02" method="GET" style="display: contents;">
                <div class="col-md-9">
                    <strong>THỐNG KÊ SUẤT KHÁCH HÀNG MUA</strong>
                    <select name="report_type">
                        <option value="1">THEO SỐ LƯỢNG</option>
                        <option value="2">THEO DOANH THU</option>
                    </select>

                    <strong style="margin-left: 20px">TRONG NĂM</strong>

                    <select name="year_report_customer">
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                    </select>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-12">
        <div class="demo-section k-content wide">
            <div id="chartReport02"></div>
        </div>
    </div>
</div>


@push('scripts')

<script>
    $(document).ready(function() {

        var report02 = $('#report02');
        var input02 = <?php echo json_encode(!empty($report02) ? $report02 : array()) ?>; 

        report02.find('select[name=report_type]').on('change', function(e) {
            getDataAndDrawChart();
        });

        report02.find('select[name=year_report_customer]').on('change', function(e) {
            getDataAndDrawChart();
        });

        function getDataAndDrawChart(){
            var params = report02.serializeArray();
            var url = '/admin/dashboard-report02';
            $.ajax({
                type: "GET",
                url: url,
                data: params,
                error: function(xhr, status, error) {},
                success: function( response ) {
                    if (response.st == 200) {
                        if (response.data) {
                            drawReport02(response.data);
                            var chart = $("#chartReport02").data("kendoChart");
                            if (response.data.type == 1) {
                                chart.options.valueAxis = { labels: {  } };
                            } else {
                                chart.options.valueAxis = { labels: { template: "#= generateLabel(value) # tr" } };
                            }
                            chart.redraw();
                        } 
                    }  
                }
            });
        }

        function drawReport02(data) {
            $("#chartReport02").kendoChart({
                title: {
                    text: " "
                },
                legend: {
                    position: "bottom"
                },
                chartArea: {
                    background: ""
                },
                seriesDefaults: {
                    type: "line",
                    style: "smooth"
                },
                series: [{
                    name: data.label,
                    data: data.value
                }],
                valueAxis: {
                    labels: {
                        format: "{0}"
                    },
                    line: {
                        visible: false
                    },
                    axisCrossingValue: -10
                },
                categoryAxis: {
                    categories: data.name,
                    majorGridLines: {
                        visible: false
                    },
                    labels: {
                        rotation: "auto"
                    }
                },
                tooltip: {
                    visible: true,
                    format: "{0}",
                    template: "#= series.name #: #= generateTooltip(value) #"
                }
            }); 
        } 

        drawReport02(input02); 

    });
</script> 
@endpush