<div class="row item-block mt-4">
    <div class="col-md-12">
        <div class="row header mb-3">
            <div class="col-md-9">
                <strong>DOANH THU THEO CHI NHÁNH:
                    <strong class="change-total-revenue-branches text-danger"></strong>
                </strong>
            </div>

            <div class="col-md-3">
                <input type="text" name="date-filter-revenue-branches" value="" class="form-control"/>
            </div>
        </div>

    </div>

    <div class="col-md-12" id="js-data-revenue-branches">
        <div class="demo-section k-content wide">
            <div id="chart-revenue-branches"></div>
        </div>
    </div>

</div>

@push('scripts')
<script>
    // select date filter
    $('input[name="date-filter-revenue-branches"]').daterangepicker({
        autoUpdateInput: true,
        startDate: '{{ $startDate}}',
        endDate: '{{ $endDate}}',
        locale: {
            cancelLabel: 'Clear',
            format: 'DD/MM/YYYY'
        }
    });

    $('input[name="date-filter-revenue-branches"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        var url = 'dashboard-revenue-branches?start_date='+picker.startDate.format('YYYY-MM-DD')+'&end_date='+picker.endDate.format('YYYY-MM-DD');
        getData(url);
    });

    $('input[name="date-filter-revenue-branches"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    // end select date filter

    function changeTotalRevenueBranches(value){
        value = formatMoney(value, 0);
        $('.change-total-revenue-branches').html(value);
    }

    // init data
    var _data_revenue_branches = JSON.parse('{!! $data_revenue_branches!!}');
    changeTotalRevenueBranches(_data_revenue_branches.total);

    {{--  showHideChart(_data_revenue_branches);  --}}
    {{--  function showHideChart(data) {
        if(data.total == 0){
            $('#js-data-revenue-branches').addClass('d-none');
            $('#js-no-data-revenue-branches').removeClass('d-none');
        }else{
            $('#js-data-revenue-branches').removeClass('d-none');
            $('#js-no-data-revenue-branches').addClass('d-none');
        }
    }  --}}

    // get data when change the filter date
    function getData(url) {
        $.ajax({
            type: "GET",
            url: url,
            error: function(xhr, status, error) {},
            success: function( response ) {
                if (response.st == 200) {
                    createChartRevenueBranhes(response.data.branches);
                    changeTotalRevenueBranches(response.data.total);
                    {{--  showHideChart(response.data);  --}}
                }
            }
        });
    }

    function createChartRevenueBranhes(data) {
        $("#chart-revenue-branches").kendoChart({
            legend: {
                position: "bottom"
            },
            seriesDefaults: {
                labels: {
                    template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                    position: "outsideEnd",
                    visible: true,
                    background: "transparent"
                }
            },
            series: [{
                type: "pie",
                data: data
            }],
            tooltip: {
                visible: true,
                template: "#= generateTooltip(value) #"
            }
        });
    }

    $(document).ready(function() {
        createChartRevenueBranhes(_data_revenue_branches.branches);
    });

</script>
@endpush
