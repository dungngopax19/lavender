<div class="row form-group">
    <div class="col-md-3 text-right"><label>Chọn sản phẩm thành phần</label></div>
    <div class="col-md-8">
        <select class="search-product-component"></select>
    </div>
</div>

<div class="list-product-component"></div>

<script>
    $(document).ready(function(){
        var data = <?php echo json_encode($componentsIds)?>;
        var ids = <?php echo json_encode($ids)?>;

        if(data.length > 0){
            getProductComponent(data);
        }

        function getProductComponent(data){
            $(this).submitDataAjax({
                'url' : site_url + 'list-product-component',
                'method': 'POST',
                'data' : {
                    _token : CSRF_TOKEN,
                    data : data
                },
                'success': function(res){
                    $('.list-product-component').html(res.html);
                    var inventoryCombo = $('input[name=inventory_list_comps]').val();
                    $('#inventoryCombo').html(inventoryCombo);
                }
            });
        }

        $(".search-product-component").select2({
            ajax: {
                url: 'search-products',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        q: params.term,
                        id : ids,
                    }
                    return query;
                },
                processResults: function (res) {
                    return {
                        results: res.records
                    };
                }
            },
            templateResult: function($item){
                var $item = $(
                    '<div>'+$item.text+'</div>'+'<div>'+$item.code+'</div>'
                );
                return $item;
            }
        }).on("select2:select", function (e) {
            if(data.indexOf(e.params.data.id) == -1){
                var obj = { id : e.params.data.id, qty : 1 };
                data.push(obj);
                ids += ',' + obj.id;
            }

            $(".search-product-component").val(null).trigger('change');
            getProductComponent(data);
        });

        $(document).on("click", ".td-trash i",function(e){
            let id = $(this).data('id');
            data = jQuery.grep(data, function( item ) {
                return item.id != id;
            });

            if(!Array.isArray(ids)){
                ids = ids.split(',');
            }
            
            ids = jQuery.grep(ids, function( item ) {
                return item != id;
            });
            ids = ids.join(',');

            getProductComponent(data);
        });

        $(document).on("change", ".amount-of",function(e){
            let id = $(this).data('id');
            var qty = parseInt( $(this).val() );

            if(isNaN(qty) || qty < 1){
                qty = 1;
            }

            data = jQuery.grep(data, function( item ) {
                if(item.id == id){
                    item.qty = qty;
                }
                return item;
            });

            getProductComponent(data);
        });

    });
</script>