@php
    $product_categories = Request::get('product_categories');
@endphp
<div class="panel panel-default pb-3">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-product-cats">
            <a href="javascript:;">Nhóm hàng</a>
            <a href="javascript:;" class="float-right">
                @if($product_categories)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-product-cats" class="panel-collapse collapse @if($product_categories) show @endif">
        <div class="panel-body">
            <input type="hidden" name="product_categories" id="product_categories" value="{{request()->product_categories}}">
            <div id="treeviewProductCategories" class="mt-2"></div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $(document).ready(function(){

        var tree = $("#treeviewProductCategories").kendoTreeView({
            checkboxes: {checkChildren: true},
            check: onCheck,
            dataSource: {!! json_encode($dataSoure) !!}
        });

        // function that gathers IDs of checked nodes
        function checkedNodeIds(nodes, checkedNodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].checked) {
                    checkedNodes.push(nodes[i].id);
                }

                if (nodes[i].hasChildren) {
                    checkedNodeIds(nodes[i].children.view(), checkedNodes);
                }
            }
        }

        // show checked node IDs on datasource change
        function onCheck() {
            var checkedNodes = [],
            treeviewProductCategories = $("#treeviewProductCategories").data("kendoTreeView")
            ids = '';

            checkedNodeIds(treeviewProductCategories.dataSource.view(), checkedNodes);

            if (checkedNodes.length > 0) {
                ids = checkedNodes.join(",");
            }

            $('#product_categories').val(ids);
            $("form.auto-submit").submit();
        }
    });
</script>
@endpush
