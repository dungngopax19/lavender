@php
    $product_attributes = Request::get('product_attributes');
@endphp
<div class="panel panel-default pb-3">
        <div class="panel-heading">
            <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                <a href="javascript:;">Thuộc tính</a>
                <a href="javascript:;" class="float-right">
                    @if($product_attributes)
                    <i class="fa fa-chevron-circle-up"></i>
                    @else
                    <i class="fa fa-chevron-circle-down"></i>
                    @endif
                </a>
            </h6>
        </div>

        <div id="collapse1" class="panel-collapse collapse @if($product_attributes) show @endif">
            <div class="panel-body">
                <input type="hidden" name="product_attributes" id="product_attributes" value="{{request()->product_attributes}}">
                <div id="treeviewProductAttributes" class="mt-2"></div>
            </div>
        </div>
    </div>

    @push('scripts')
    <script>
        $(document).ready(function(){

            var tree = $("#treeviewProductAttributes").kendoTreeView({
                checkboxes: {checkChildren: true},
                check: onCheck,
                dataSource: {!! json_encode($dataSoure) !!}
            });

            // function that gathers IDs of checked nodes
            function checkedNodeIds(nodes, checkedNodes) {
                for (var i = 0; i < nodes.length; i++) {
                    if (nodes[i].checked) {
                        checkedNodes.push(nodes[i].id);
                    }

                    if (nodes[i].hasChildren) {
                        checkedNodeIds(nodes[i].children.view(), checkedNodes);
                    }
                }
            }

            // show checked node IDs on datasource change
            function onCheck() {
                var checkedNodes = [],
                treeviewProductAttributes = $("#treeviewProductAttributes").data("kendoTreeView")
                ids = '';

                checkedNodeIds(treeviewProductAttributes.dataSource.view(), checkedNodes);

                if (checkedNodes.length > 0) {
                    ids = checkedNodes.join(",");
                }

                $('#product_attributes').val(ids);
                $("form.auto-submit").submit();
            }
        });
    </script>
    @endpush
