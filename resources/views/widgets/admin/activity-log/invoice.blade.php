@php
    use App\Helpers\ActivityLog as ActivityLogHelper;
@endphp

@if (count($records) > 0)
    <div class="border-dashed-top mt-5 pt-3">
        <h5>Lịch sử thao tác</h5>
        <ul>
            @foreach ($records as $item)
                <li>
                    <div class="row mb-3">
                        <div class="col-md-10">{!! ActivityLogHelper::highlightAction($item->description) !!} {{$item->causer['name']}}</div>
                        <div class="col-md-2 text-right">{{$item->created_at->format('d-m-Y H:i:s')}}</div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endif