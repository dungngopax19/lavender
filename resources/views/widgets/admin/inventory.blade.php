@php
    use App\Helpers\Common;
@endphp

@if (count($records) > 0)
    <li class="nav-item dropdown active dropdown-inventory">
        <a class="nav-link" href="javascript:void(0)">
            Top 100 sản phẩm sắp hết hàng<span class="badge badge-danger">{{number_format(count($records))}}</span>
        </a>

        <div class="dropdown-content dropdown-content-cart menu-right pl-3 pr-3 pt-1 pb-1">
            <div class="item-cart">
                @foreach ($records as $k => $item)
                    <div class="media mt-2 pb-2 ajax-modal" data-action="products/{{$item->id}}" data-target="#modal-edit-product">
                        <div class="media-left">
                            <img src="{{Common::resizeImage($item->thumbnail, 100)}}" alt="">
                        </div>
                        <div class="media-body ml-2">
                            <div class="code">{{$item->id_display}}</div>
                            <div class="name">{{$item->name}}</div>
                            <div class="code">{{$item->code}}</div>
                            <div class="price">Tồn kho: {{number_format($item->inventory_number)}}</div>
                        </div>
                    </div>
                @endforeach
            </div>  
        </div>
    </li>
@endif