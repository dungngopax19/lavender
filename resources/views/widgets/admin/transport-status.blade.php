<div class="row mt-4 mb-4">
    <div class="{{ (request()->highlight == 2) ? 'active' : ''}} item-transport-status col-md-3">
        <a href="{{url('admin/transport-order?highlight=2')}}">
            <div class="title">Đơn hàng size riêng</div>
            <div class="number">{{number_format($invoicesHighlightSize)}}</div>
        </a>
    </div>

    <div class="{{ (request()->highlight == 1) ? 'active' : ''}} item-transport-status col-md-3">
        <a href="{{url('admin/transport-order?highlight=1')}}">
            <div class="title">Đơn hàng ưu tiên</div>
            <div class="number">{{number_format($invoicesHighlight)}}</div>
        </a>
    </div>

    <div class="{{ (request()->orders_upcomming_delivery == 1) ? 'active' : ''}} item-transport-status col-md-3">
        <a href="{{url('admin/transport-order?orders_upcomming_delivery=1')}}">
            <div class="title">Đơn hàng sắp tới ngày giao</div>
            <div class="number">{{$ordersUpcommingDelivery}}</div>
        </a>
    </div>

    <div class="{{ (request()->orders_exceeding_delivery == 1) ? 'active' : ''}} item-transport-status col-md-3">
        <a href="{{url('admin/transport-order?orders_exceeding_delivery=1')}}">
            <div class="title">Đơn hàng quá ngày giao</div>
            <div class="number">{{$ordersExceedingDelivery}}</div>
        </a>
    </div>

    @foreach ($records as $i => $item)
        @php
            $active = '';
            $url = url('admin/transport-order');

            if(($request->shipment_status_id == $item->id)){
                $active = 'active';
            }
            $url .= '?shipment_status_id=' . $item->id;
        @endphp
        <div class="{{$active}} item-transport-status col-md-3 mb-4">
            <a href="{{$url}}">
                <div class="title">{{$item->name}}</div>
                <div class="number">{{number_format($item->count)}}</div>
            </a>
        </div>
    @endforeach
</div>