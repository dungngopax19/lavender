@if(count($records['data']))
<li class="nav-item dropdown ml-3 dropdown-message">
    <a class="nav-link">Tin nhắn
        @if($records['unread'])
        <span class="badge badge-danger js-unread">{{number_format($records['unread'])}}</span>
        @endif
    </a>
    <div class="dropdown-content dropdown-content-message menu-right">

        <div class="messages">
            @foreach ($records['data'] as $k => $item)
                <div class="media">
                    <div class="media-left mr-2">
                        <img src="{{$item->resizeImage($item->userSend['image'], 60)}}" class="media-object" style="width:60px">
                    </div>
                    <div class="media-body">
                        <div class="media-heading">
                            <div class="row">
                                <div class="col-md-6"><strong>{{$item->userSend['name']}}</strong></div>
                                <div class="col-md-6 text-right">{{$item->created_at_hidmy}}</div>
                            </div>
                        </div>
                        <div>{{$item->content}}</div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row js-message-spiner hide">
            <div class="col-md-12 text-center bt">
                <i class="fa fa-spinner fa-spin float-center" style="font-size:24px"></i>
            </div>
        </div>

        <div class="row p-2 bt">
            <div class="col-md-6 text-center">
                <span class="view-more-message">Hiển thị tin nhắn cũ hơn</span>
            </div>
            <div class="col-md-6 text-center">
                <span class="read-all-message">Đánh dấu tất cả là đã đọc</span>
            </div>
        </div>
    </div>
</li>
@endif

<script>
    $(document).ready(function(){
        var page_message = 2;
        var is_show_more = true;
        function showMoreMessage(){
            if(is_show_more){
                $('.js-message-spiner').removeClass('hide');
                $(this).submitDataAjax({
                    'url' : site_url + 'messages?page=' + page_message,
                    'method': 'GET',
                    'success': function(res){
                        if(res.data){
                            var html = res.data;
                            $('.js-message-spiner').addClass('hide');
                            $('.messages').append(html);
                            page_message++;
                        }else{
                            is_show_more = false;
                        }
                    }
                });
            }
        }

        function markReadMessage(){
            $(this).submitDataAjax({
                'url' : site_url + 'messages-mark-read',
                'method': 'GET',
                'success': function(res){
                    $('.js-unread').html(0);
                }
            });
        }
        //
        $('.view-more-message').on('click', function(){
            showMoreMessage();
        });

        $('.read-all-message').on('click', function(){
            markReadMessage();
        });

    });
</script>
