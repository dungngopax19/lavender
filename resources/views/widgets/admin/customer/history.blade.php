<div class="row">
        <div class="col-md-12">
            <table class="table-hover table mt-10 mb-0">
                <thead style="background-color: #DCF5FC;">
                    <tr>
                        <th>Mã Đơn hàng</th>
                        <th>Thời gian</th>
                        <th>Người bán</th>
                        <th>Tổng cộng</th>
                        <th>Trạng thái</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($records as $i => $item)
                    <tr data-params="{{json_encode(['type'=>'invoices', 'orders_code' => $item->code])}}" data-target="#modal-cart" data-action="detail-cart" class="ajax-modal">
                        <td>{{ $item->code}}</td>
                        <td>{{$item->created_at}}</td>
                        <td>{{$item->createdUser['name']}}</td>
                        <td>{{number_format($item->price)}}</td>
                        <td>{{$item->status['name']}}</td>
                    @empty
                    <tr>
                        <td colspan="100" class="text-center">Không có dữ liệu</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
