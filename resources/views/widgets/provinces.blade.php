<div class="row form-group {{$config['className'] ?? ''}} fg-{{$config['name']}}">
    <div class="col-md-4 text-right">
        <label>
            {{$config['label']}}
            
            @isset ($config['required'])
                <span class="text-danger"> ※</span>
            @endisset
        </label>
    </div>

    <div class="col-md-4">
        <select class="select2 form-control" name="province_parent">
            <option value="">Chọn tỉnh / TP</option>
            @foreach ($provinces as $item)
                <option {{ ($provinceChild && $item->id == $provinceChild->parent_id)? 'selected' : ''}} value="{{$item->id}}">{{$item->name}}</option>    
            @endforeach
        </select>
        <div class="error-msg"></div>
    </div>

    <div class="col-md-4">
        <select class="select2 form-control" name="province_child">
            <option value="">Chọn quận / huyện</option>
            @if ($provinces->count() > 0 && $provinceChild->parent)
                @foreach ($provinceChild->parent->children as $item)
                    <option {{ ($item->id == $province_id)? 'selected' : ''}} value="{{$item->id}}">{{$item->name}}</option>    
                @endforeach
            @endif
        </select>
        <div class="error-msg"></div>
    </div>
</div>