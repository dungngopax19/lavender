<div class="row">
    <div class="col-md-6 title-main"><h4>Chi nhánh</h4></div>
    @if($is_permission_create)
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-success ajax-modal"
            data-target="#modal-edit-branch" data-action="branches/0">
            <i class="fa fa-plus"></i> Chi nhánh
        </button>
    </div>
    @endif
</div>

<table class="list-item table-hover table mt-10 mb-0">
    <thead>
        <tr>
            <th>#</th>
            <th>Tên chi nhánh</th>
            <th>Địa chỉ</th>
            <th>Điện thoại</th>
            <th>SL người dùng</th>
            <th>Trạng thái</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->name}}</td>
                <td>{{Str::words($item->address, 10)}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->id}}</td>
                <td>{{$item->activity_status == 1 ? 'Đang' : 'Ngừng'}} kinh doanh</td>
                <td>
                    @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                        <button data-action="branches/{{$item->id}}" data-target="#modal-edit-branch" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                    @endif
                    @if($item->hasAllPermission([$permission.'xoa'], $permission))
                        <button data-action="branches/{{$item->id}}" type="button" class="delete-item btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
