<form action="{{url('admin/branches/update')}}" method="post" id="form-edit-branch">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <input type="hidden" name="id" value="{{$record->id}}">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">{{$title}} chi nhánh</h4>
        <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.text', ['label'=>'Tên chi nhánh', 'name'=>'name', 'value'=>$record->name, 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.text', ['label'=>'Điện thoại', 'name'=>'phone', 'value'=>$record->phone])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.radio', [
                    'label'=>'Trạng thái', 'name'=>'activity_status', 'value'=>$record->activity_status ?? 1,
                    'data' => [ '1' => 'Đang hoạt động', '2' => 'Ngừng hoạt động' ]
                ])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.textarea', ['label'=>'Địa chỉ', 'name'=>'address', 'value'=>$record->address])
            </div>
        </div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
        <button data-form-target="#form-edit-branch" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
    </div>
</form>