@extends('layouts.admin')

@section('title', $title)

@section('content')

    <div class="dashboard mb-4">
        <div class="row mt-4">
            <div class="col-md-12 col-left">

                {{ Widget::run('Admin.Dashboard.Revenue') }}

                {{ Widget::run('Admin.Dashboard.RevenueBranches') }}

                {{ Widget::run('Admin.Dashboard.Products') }}

                {{ Widget::run('Admin.Dashboard.Customers') }}

            </div>

        </div>

        <div class="row mt-2">
            <div class="col-md-4">
                @asyncWidget('Admin\Dashboard\Orders\StatusOrders', ['type'=> 1, 'name'=>'Đơn hàng sắp giao', 'icon'=>'fa-home'])
            </div>

            <div class="col-md-4">
                @asyncWidget('Admin\Dashboard\Orders\StatusOrders', ['type'=> 2, 'name'=>'Đơn hàng đang giao', 'icon'=>'fa-home'])
            </div>

            <div class="col-md-4">
                @asyncWidget('Admin\Dashboard\Orders\StatusOrders', ['type'=> 3, 'name'=>'Đơn hàng quá ngày giao', 'icon'=>'fa-home'])
            </div>

        </div>

        <div class="row mt-4">
            <div class="col-md-4">
                @asyncWidget('Admin\Dashboard\Orders\StatusOrders', ['type'=> 4, 'name'=>'Đơn hàng bị hủy', 'icon'=>'fa-home'])
            </div>

            <div class="col-md-4">
                @asyncWidget('Admin\Dashboard\Orders\StatusOrders', ['type'=> 5, 'name'=>'Khách hàng có doanh thu cao nhất', 'icon'=>'fa-home'])
            </div>

            <div class="col-md-4">
                @asyncWidget('Admin\Dashboard\Orders\StatusOrders', ['type'=> 6, 'name'=>'Khách hàng mua nhiều lần nhất', 'icon'=>'fa-home'])
            </div>
        </div>
        
    </div>

@endsection
