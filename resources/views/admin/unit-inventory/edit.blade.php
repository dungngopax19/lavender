<form action="{{url('admin/unit-inventory/update')}}" method="post" id="form-edit-unit-inventory">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <input type="hidden" name="id" value="{{$record->id}}">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">Chuyển đổi vải thành phẩm</h4>
        <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.text', ['label'=>'Mã hàng', 'name'=>'code', 'value'=>$record->code, 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.text', ['label'=>'Số mét vải', 'name'=>'unit', 'value'=>$record->unit, 'required'=>true])
            </div>
        </div>

    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
        <button data-form-target="#form-edit-unit-inventory" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
    </div>
</form>