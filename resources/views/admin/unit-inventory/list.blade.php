<div class="row">
    <div class="col-md-6 title-main"><h4>{{$title}}</h4></div>
    @if($is_permission_create)
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-success ajax-modal"
            data-target="#modal-edit-unit-inventory" data-action="unit-inventory/0">
            <i class="fa fa-plus"></i> {{$title}}
        </button>
    </div>
    @endif
</div>

<table class="list-item table-hover table mt-10 mb-0">
    <thead>
        <tr>
            <th>#</th>
            <th class="text-center">Mã hàng</th>
            <th class="text-center">Số mét vải</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td class="text-center">{{$item->code}}</td>
                <td class="text-center">{{$item->unit}}</td>
                <td>
                    @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                        <button data-action="unit-inventory/{{$item->id}}" data-target="#modal-edit-unit-inventory" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                    @endif
                    @if($item->hasAllPermission([$permission.'xoa'], $permission))
                        <button data-action="unit-inventory/{{$item->id}}" type="button" class="delete-item btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
