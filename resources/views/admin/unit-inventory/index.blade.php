@extends('layouts.admin')

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-2 main-left">
            <form action="" method="get" class="auto-submit">
                @include('admin.unit-inventory.filter')
            </form>
        </div>

        <div class="col-md-10 main-right">
            @include('admin.unit-inventory.list')
        </div>
    </div>
@endsection

@push('modals')
<div class="modal fade" id="modal-edit-unit-inventory">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content"></div>
    </div>
</div>
@endpush