<div class="panel-group" id="accordion">
    @include('admin.components.searchs.surcharge.code-type')
    @include('admin.components.searchs.branches')
    @include('admin.components.searchs.activity-status')
    @include('admin.components.searchs.items-per-page')
</div>
