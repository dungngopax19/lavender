@php
    $record->activity_status = ($record->activity_status) ? $record->activity_status : 1;
@endphp
<form action="{{url('admin/surcharges/update')}}" method="post" id="form-edit-branch">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <input type="hidden" name="id" value="{{$record->id}}">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">{{($record->id)?'Cập nhật':'Thêm mới'}} {{$title}}</h4>
    <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.text', ['label'=>'Mã thu khác', 'name'=>'code', 'value'=>$record->code, 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.text', ['label'=>'Loại thu', 'name'=>'type', 'value'=>$record->type, 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row form-group fg-value">
                    <div class="col-md-4 text-left">
                        <label>
                            Giá trị
                            <span class="text-danger"> ※</span>
                        </label>
                    </div>
                    <div class="col-md-8">
                        <div class="input-group mb-2 mr-sm-2">
                            <input type="number" class="form-control" name="value" value="{{ $record->value}}" min="0">
                            <input type="number" class="form-control hide" name="value_type" value="{{ $record->value_type}}">
                            <button type="button" name="vnd" class="btn btn-sm {{ ( !$record->value_type||$record->value_type == 1) ? 'btn-primary' : 'btn-light'}} ">VND</button>
                            <button type="button" name="phantram" class="btn btn-sm {{ ($record->value_type == 2) ? 'btn-primary' : 'btn-light'}}">%</button>
                        </div>
                        <div class="error-msg"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.select', ['label'=>'Chi nhánh', 'name'=>'branches_ides[]', 'value'=>$branches_ides, 'multiple'=>true, 'data'=> $branches, 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.text', ['label'=>'Thứ tự hiển thị', 'name'=>'display_order', 'value'=>$record->display_order])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.select', ['label'=>'Trạng thái', 'name'=>'activity_status', 'value'=>$record->activity_status, 'data'=>$activity_status, 'required'=> true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 offset-md-4">
                @include('components.inputs.checkbox', ['label'=>'Tự động đưa vào Đơn hàng', 'name'=>'is_put_bill', 'value'=>$record->is_put_bill])
                @include('components.inputs.checkbox', ['label'=>'Hoàn lại khi trả hàng', 'name'=>'is_refund', 'value'=>$record->is_refund])
            </div>
        </div>


    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
        <button data-form-target="#form-edit-branch" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        // select value type
        $('button[name=vnd]').on("click", function(e){
            $('button[name=vnd]').removeClass('btn-light');
            $('button[name=vnd]').toggleClass('btn-primary');
            $('button[name=phantram]').removeClass('btn-primary');
            $('button[name=phantram]').addClass('btn-light');
            $('input[name=value_type]').val(1);
        });

        $('button[name=phantram]').on("click", function(e){
            $('button[name=phantram]').removeClass('btn-light');
            $('button[name=phantram]').addClass('btn-primary');
            $('button[name=vnd]').removeClass('btn-primary');
            $('button[name=vnd]').addClass('btn-light');
            $('input[name=value_type]').val(2)
            $('input[name=value]').val(10);
        });


    });
</script>
