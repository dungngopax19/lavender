<div class="panel-group" id="accordion">
    @include('admin.components.searchs.invoice.id-code')
    @include('admin.components.searchs.branches')
    @include('admin.components.searchs.created')
    @include('admin.components.searchs.invoice.order-status')
    @include('admin.components.searchs.invoice.delivery-status')
    @include('admin.components.searchs.invoice.channel')
    @include('admin.components.searchs.invoice.partner-delivery')
    @include('admin.components.searchs.invoice.delivery-time')
    @include('admin.components.searchs.province')
    @include('admin.components.searchs.items-per-page')
</div>
