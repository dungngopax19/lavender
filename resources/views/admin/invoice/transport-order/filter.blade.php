@php
    use App\Models\ShipmentStatus;
    $shipmentStatus = ShipmentStatus::all();
@endphp

<div class="panel-group" id="accordion">
    <input type="hidden" name="highlight" value="{{request()->highlight}}">
    <input type="hidden" name="orders_upcomming_delivery" value="{{request()->orders_upcomming_delivery}}">
    <input type="hidden" name="orders_exceeding_delivery" value="{{request()->orders_exceeding_delivery}}">

    @php
        
    @endphp

    @foreach ($shipmentStatus as $item)
        @if (request()->shipment_status_id == $item->id)
            <input type="hidden" name="shipment_status_id" value="{{request()->shipment_status_id}}">    
        @endif
    @endforeach

    @include('admin.components.searchs.invoice.id-code')
    @include('admin.components.searchs.invoice.product-code-yet-delivered')
    @include('admin.components.searchs.invoice.product-code-orders-highlight')
    @include('admin.components.searchs.branches')
    @include('admin.components.searchs.created')
    @include('admin.components.searchs.invoice.order-status')
    @include('admin.components.searchs.invoice.channel')
    @include('admin.components.searchs.invoice.partner-delivery')
    @include('admin.components.searchs.invoice.delivery-time')
    @include('admin.components.searchs.province')
    @include('admin.components.searchs.items-per-page')
</div>
