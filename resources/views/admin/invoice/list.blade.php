<div class="row">
    <div class="col-md-6 title-main"><h4>{{ucfirst($title)}} ({{$totalRecords}})</h4></div>

    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-success ajax-modal mr-2"
            data-target="#modal-import-invoice" data-action="invoice-import">
            <i class="fa fa-plus"></i> Import
        </button>

        <a href="{{url( 'admin/invoices-export/?page=invoices&' . http_build_query(request()->query()))}}" target="_blank">
            <button type="button" class="btn btn-primary"<i class="fas fa-file-export"></i> Export</button>
        </a>
    </div>
</div>

<div class="table-responsive">
    <table class="list-item table-hover table mt-10 mb-0">
        <thead>
            <tr>
                <th>Mã hoá đơn</th>
                <th>Khách hàng</th>
                <th>Số điện thoại</th>
                <th class="text-center">Phí ship</th>
                <th class="text-center">Tổng tiền hàng</th>
                <th class="text-center">Giảm giá</th>
                <th class="text-center">Khách đã trả</th>
                <th class="text-center">Số tiền còn lại phải thanh toán</th>
                <th>Trạng thái đơn hàng</th>
                <th>Ngày tạo</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @if(count($records))
                <tr>
                    <td colspan="4"></td>
                    <td class="text-center">{{number_format($records->m_total->price)}}đ</td>
                    <td class="text-center">{{number_format($records->m_total->discount)}}đ</td>
                    <td class="text-center">{{number_format($records->m_total->deposit)}}đ</td>
                    <td class="text-center text-danger">{{number_format($records->m_total->price_final)}}đ</td>
                    <td colspan="3"></td>
                </tr>
            @endif

            @forelse ($records as $item)
                <tr>
                    <td>{{$item->code}}</td>
                    <td>{{$item->customer['name']}}</td>
                    <td>{{$item->customer['phone']}}</td>
                    
                    <td class="text-center">{{number_format($item->ship)}}đ</td>
                    <td class="text-center">{{number_format($item->price)}}đ</td>
                    <td class="text-center">{{number_format($item->discount)}}đ</td>
                    <td class="text-center">{{number_format($item->deposit)}}đ</td>
                    <td class="text-center">{{number_format($item->price_final)}}đ</td>

                    <td>{{$item->status['name']}}</td>
                    <td class="text-center">{{$item->created_at->format('d/m/Y')}}</td>
                    <td>
                        @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                            <button data-params="{{json_encode(['type'=>'invoices', 'orders_code' => $item->code])}}" data-target="#modal-cart" data-action="detail-cart" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                        @endif

                        @if($item->hasAllPermission([$permission.'xoa'], $permission))
                            <button data-action="{{$mod}}s/{{$item->id}}" type="button" class="delete-item btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="100" class="text-center">Không có dữ liệu</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>

<div class="mt-15">
    {{$records->links()}}
</div>
