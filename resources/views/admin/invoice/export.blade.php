<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Document</title>
</head>
<body>

    <table>
        <tr>
            <th>Chi nhánh</th>
            <th>Mã hóa đơn</th>
            <th>Mã khách hàng</th>
            <th>Tên khách hàng</th>
            <th>Số điện thoại</th>
            <th>Địa chỉ (Khách hàng)</th>
            <th>Người bán</th>
            <th>Kênh bán</th>
            <th>Dịch vụ</th>
            <th>Ghi chú</th>
            <th>Tổng tiền hàng</th>
            <th>Giảm giá hóa đơn</th>
            <th>Thu khác</th>
            <th>Khách cần trả</th>
            <th>Khách đã trả</th>
            <th>Tiền mặt</th>
            <th>Trạng thái đơn hàng</th>
            <th>Trạng thái giao hàng</th>
            <th>Mã hàng</th>
            <th>Tên hàng</th>
            <th>Số lượng</th>
            <th>Đơn giá</th>
            <th>Thành tiền</th>
        </tr>

        @foreach ($records as $item)
            @php
                $discount = $item->discount;
                $orderDetails = $item->orderDetail;
            @endphp

            @foreach ($orderDetails as $itemDetail)
                <tr>
                    <th>{{$item->branch['name']?? 'Lavender - Online'}}</th>
                    <th>{{$item->code}}</th>
                    <th>{{$item->customer['code']}}</th>
                    <th>{{$item->customer['name']}}</th>
                    <th>{{$item->customer['phone']}}</th>
                    <th>{{$item->customer['address']}}</th>
                    <th>{{$item->user['name']}}</th>
                    <th>
                        @if ($item->channel == 1)
                            Bán trực tiếp
                        @elseif($item->channel == 2)
                            Facebook
                        @else
                            Khác
                        @endif
                    </th>
                    <th></th>
                    <th>{{$item->note}}</th>
                    <th>{{number_format($item->price + $discount)}}</th>
                    <th>{{number_format($discount)}}</th>
                    <th></th>
                    <th>{{number_format($item->price)}}</th>
                    <th>{{number_format($item->deposit)}}</th>
                    <th></th>
                    <th>{{$item->status['name']}}</th>
                    <th>{{$item->shipmentStatus['name']}}</th>
                    <th>{{$itemDetail->product['code']}}</th>
                    <th>{{$itemDetail->product['name']}}</th>
                    <th>{{number_format($itemDetail->qty)}}</th>
                    <th>{{number_format($itemDetail->price)}}</th>
                    <th>{{number_format($itemDetail->qty * $itemDetail->price)}}</th>
                </tr>
            @endforeach
        @endforeach
    </table>
</body>
</html>
