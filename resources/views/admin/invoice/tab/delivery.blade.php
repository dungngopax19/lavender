<div class="row">
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Người nhận', 'name'=>'address', 'value'=>1])
    </div>
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Mã vận đơn', 'name'=>'address', 'value'=>1])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Điện thoại', 'name'=>'role', 'value'=>1])
    </div>
    <div class="col-md-6">
        @include('components.inputs.select', ['label'=>'Trạng thái giao hàng:', 'name'=>'address', 'value'=>1, 'data' => [['id'=>1, 'name'=>'Cá nhân'], ['id'=>2, 'name'=>'Công ty']]])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Địa chỉ', 'name'=>'role', 'value'=>1])
    </div>
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Trọng lượng', 'name'=>'address', 'value'=>1])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.select', ['label'=>'Khu vực', 'name'=>'role', 'value'=>1, 'data' => [['id'=>1, 'name'=>'Cá nhân'], ['id'=>2, 'name'=>'Công ty']]])
    </div>
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Kích thước', 'name'=>'address', 'value'=>1])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.select', ['label'=>'Người giao:', 'name'=>'role', 'value'=>1, 'data' => [['id'=>1, 'name'=>'Cá nhân'], ['id'=>2, 'name'=>'Công ty']]])
    </div>
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Phí giao hàng:', 'name'=>'address', 'value'=>1])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Thời gian giao hàng:', 'name'=>'address', 'value'=>1])
    </div>
</div>
