@php
    use App\Helpers\Common;
@endphp

<form action="{{url('admin/'.$mod.'s/update')}}" method="post" id="form-edit-{{$mod}}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <input type="hidden" name="id" value="{{$record->id}}">

    <div class="row">
        <div class="col-md-6">
            @include('components.inputs.text', ['label'=>'Mã Đơn hàng', 'name' => 'code', 'readonly'=>'readonly', 'value'=>$record->code, 'labelAlign'=>'text-right'])
            @include('components.inputs.select', ['label'=>'Trạng thái đơn hàng', 'name'=>'orders_status_id', 'value'=>$record->orders_status_id, 'data'=>$invoices_status, 'labelAlign'=>'text-right'])
            @include('components.inputs.select', ['label'=>'Trạng thái giao hàng', 'name'=>'shipment_status_id', 'value'=>$record->shipmentDetail['shipment_status_id'], 'data'=>$shipment_status, 'labelAlign'=>'text-right'])
            @include('components.inputs.select', ['label'=>'Kênh bán', 'name'=>'channel', 'value'=>$record->channel, 'data'=>$channels, 'labelAlign'=>'text-right'])
            @include('components.inputs.text', ['label'=>'Ghi chú', 'name'=>'note', 'value'=>$record->note, 'labelAlign'=>'text-right'])
        </div>

        <div class="col-md-6">
            @include('components.inputs.label', ['label'=>'Chi nhánh', 'name'=>'branches_id', 'value'=>$record->branch['name'], 'labelAlign'=>'text-right' ])
            @include('components.inputs.text', ['label'=>'Thời gian', 'name'=>'created_at', 'readonly'=>'readonly', 'value'=>$record->created_at, 'labelAlign'=>'text-right'])
            @include('components.inputs.text', ['label'=>'Khách hàng', 'name'=>'customer', 'readonly'=>'readonly', 'value'=>$record->customer['name'], 'labelAlign'=>'text-right'])
            @include('components.inputs.select', ['label'=>'Người bán', 'name'=>'users_id', 'value'=>$record->users_id, 'data'=>$users, 'labelAlign'=>'text-right'])
            @include('components.inputs.label', ['label'=>'Người tạo', 'name'=>'user_created_id', 'value'=>$record->createdUser['name'], 'labelAlign'=>'text-right'])
        </div>

    </div>

    <hr>

    <div class="row">
        <div class="col-md-12">
            <table class="table mt-10 mb-0">
                <thead style="background-color: #DCF5FC;">
                    <tr>
                        <td>#</td>
                        <td>Hình ảnh</td>
                        <td>Mã sản phẩm</td>
                        <td>Tên sản phẩm</td>
                        <td class="text-center">Số lượng</td>
                        <td class="text-center">Đơn giá</td>
                        <td class="text-center">Thành tiền</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($record->orderDetail as $i => $orderDetail)
                        @php
                            $product = $orderDetail['product'];
                        @endphp

                        @if ($product)
                            <tr>
                                <td>{{ $product->id_display}}</td>
                                <td>
                                    <a data-fancybox="gallery" href="{{asset($product->thumbnailDisplay)}}">
                                        <img src="{{Common::resizeImage($product->thumbnail, 100)}}" alt="">
                                    </a>
                                </td>
                                <td><strong>{{$product->code}}</strong></td>
                                <td>{{$product->name}}</td>
                                <td class="text-center">{{number_format($orderDetail->qty)}}</td>
                                <td class="text-center">{{number_format($orderDetail->price)}}</td>
                                <td class="text-center">{{number_format( $orderDetail->qty * $orderDetail->price )}}</td>
                            </tr>
                        @endif
                    @endforeach

                    <tr>
                        <td colspan="6" class="text-right">
                            <p>Tổng tiền hàng:</p>
                            <p>Giảm giá Đơn hàng:</p>
                            <p>Khách đã trả:</p>
                            <p>Khách cần trả:</p>
                        </td>
                        <td class="text-right font-weight-bold">
                            <p>{{number_format($record->price)}}đ</p>
                            <p>{{$record->discount}}{{($record->discount_unit == 'percent') ? '%' : 'đ'}}</p>
                            <p>{{number_format($record->deposit)}}đ</p>
                            <p>{{number_format($record->price_final)}}đ</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer pb-0">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
        <button data-form-target="#form-edit-{{$mod}}" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">Cập nhật</button>
    </div>
</form>
