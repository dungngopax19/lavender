@php
    $record->thumbnail = $record->image;
    $record->role = ($record->role) ? $record->role : 1;
    $record->activity_status = ($record->activity_status) ? $record->activity_status : 1;
@endphp
<!-- Modal Header -->
<div class="modal-header">
    <h4 class="modal-title">Chi tiết {{$title}}</h4>
    <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<div class="modal-body">
    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" class="active show" href="#info">Thông tin</a></li>
        <li><a data-toggle="tab" href="#history">Lịch sử thanh toán</a></li>
    </ul>

    <div class="tab-content mt-3">
        <div id="info" class="tab-pane fade in active show">
            @include('admin.invoice.tab.info')
        </div>

        <div id="history" class="tab-pane fade">
            @include('admin.invoice.tab.history')
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $('input[name=birthday]').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            },
            autoUpdateInput: false,
        });

        $('input[name="birthday"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        // select role
        $('select[name=role]').on("change", function(e){
            if($(this).val() === '2'){
                $('.js-show-hide-company').removeClass('hide');
            }else{
                $('.js-show-hide-company').addClass('hide');
            }
        });


    });
</script>
