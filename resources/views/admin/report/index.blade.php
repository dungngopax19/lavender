@extends('layouts.admin')

@section('title', ucfirst($title))

@section('content')
    <div class="row">
        
        <div class="col-md-10 main-right">
        <div class="container">
        <div class="row">
            <div class="col-sm">
            One of three columns
            </div>
            <div class="col-sm">
            One of three columns
            </div>
            <div class="col-sm">
            One of three columns
            </div>
        </div>
        </div>

            <table class="table mt-10 mb-0">

                <tbody>
                    <tr>
                        <td colspan="100" class="text-center">Không có dữ liệu</td>
                    </tr>
                </tbody>
                
            </table> 

        </div>

        <div class="col-md-2 main-left">

        </div>

    </div>


@push('scripts') 

<script>

    $(document).ready(function(){
        var rootCart = $('.root-shopping-cart');
        
        $('.shopping-cart-item').on('click, change', '.quantity', function() {
            var quantity = parseInt($(this).val());
            $(this).val(quantity>1 ? quantity : 1); 
            calcItemPrice();
        });

    

        function currencyFormat(number, moneyCode) {
            var decimalplaces = 0; //2
            var decimalcharacter = ""; //"."
            var thousandseparater = ",";

            number = parseFloat(number);

            var sign = number < 0 ? "-" : "";
            var formatted = new String(number.toFixed(decimalplaces));
            if (decimalcharacter.length && decimalcharacter != ".") {
                formatted = formatted.replace(/\./, decimalcharacter);
            }
            var integer = "";
            var fraction = "";
            var strnumber = new String(formatted);
            var dotpos = decimalcharacter.length ? strnumber.indexOf(decimalcharacter) : -1;
            if (dotpos > -1) {
                if (dotpos) {
                    integer = strnumber.substr(0, dotpos);
                }
                fraction = strnumber.substr(dotpos + 1);
            } else {
                integer = strnumber;
            }
            if (integer) {
                integer = String(Math.abs(integer));
            }
            while (fraction.length < decimalplaces) {
                fraction += "0";
            }
            temparray = new Array();
            while (integer.length > 3) {
                temparray.unshift(integer.substr(-3));
                integer = integer.substr(0, integer.length - 3);
            }
            temparray.unshift(integer);
            integer = temparray.join(thousandseparater);

            var code = 'đ';
            if (moneyCode == false) code = "";

            return sign + integer + decimalcharacter + fraction + code;
        }        
    });
</script> 
@endpush


@endsection      


