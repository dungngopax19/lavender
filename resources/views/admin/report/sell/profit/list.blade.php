@php
    if($created_range){
        $created_range = explode(' -> ', $created_range);
    }
@endphp
<div class="col-md-12" style="background-color:#adabab;">
    <div style="margin: 30px 15px; background-color:white;" class="content-pdf">
        @include('admin.report.header', [
            'created_range'=> $created_range,
            'title'=> 'Báo cáo bán hàng theo lợi nhuận'
            ])

        <div class="col-md-12" style="min-height:25cm; margin-top:30px">
            <table class="table mt-10 mb-0">
                <thead>
                    <tr>
                        @if($whereDate == 'hour')
                        <th >Mã giao dịch</th>
                        @endif
                        <th>Thời gian</th>
                        <th class="text-right">Tổng tiền hàng</th>
                        <th class="text-right">Giảm giá HĐ</th>
                        <th class="text-right">Doanh thu</th>
                        <th class="text-right" >Tổng giá vốn</th></th>
                        <th class="text-right" >Lợi nhuận gộp</th>
                    </tr>
                </thead>
                <tbody>
                    @if($records['orders'])
                    <tr class="font-weight-bold">
                        @if($whereDate == 'hour')
                        <td ></td>
                        @endif
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="text-right"></td>
                        <td class="text-right"></td>
                        <td class="text-right">{{number_format($records['total'])}}</td>
                    </tr>

                    @if($whereDate == 'hour')
                        @foreach ($records['orders'][0]['orders'] as $key=>$item)
                        <tr >
                            <td >{{ $item['code']}}</td>
                            <td>{{ $item['created_at']}}</td>
                            <td class="text-right">{{number_format($item['price'])}}</td>
                            <td class="text-right">{{number_format(0)}}</td>
                            <td class="text-right">{{number_format($item['price_final'])}}</td>
                            <td class="text-right">0</td>
                            <td class="text-right">{{number_format($item['price'])}}</td>
                        </tr>
                        @endforeach
                    @else
                        @foreach ($records['orders'] as $key=>$item)
                        <tr >
                            <td>
                                <a href="javascript:;" data-date="{{$item['date']}}" data-whereDate="{{$whereDate}}" class="js-date">
                                    @if($whereDate == 'day')
                                    {{ date("d-m-Y", strtotime($item['date']))}}
                                    @else
                                    {{ date("m-Y", strtotime($item['date']))}}
                                    @endif
                                </a>
                            </td>
                            <td class="text-right">{{number_format($item['total']['price'])}}</td>
                            <td class="text-right">{{number_format(0)}}</td>
                            <td class="text-right">{{number_format($item['total']['price'])}}</td>
                            <td class="text-right">0</td>
                            <td class="text-right">{{number_format($item['total']['price'])}}</td>
                        </tr>
                        @endforeach
                    @endif
                    @else
                    <tr>
                        <td colspan="100" class="text-center">Không có dữ liệu</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

