@php
    if($created_range){
        $created_range = explode(' -> ', $created_range);
    }
@endphp
<div class="col-md-12" style="background-color:#adabab;">
    <div style="margin: 30px 15px; background-color:white;" class="content-pdf">
        @include('admin.report.header', [
            'created_range'=> $created_range,
            'title'=> 'Báo cáo bán hàng theo nhân viên'
            ])

        <div class="col-md-12" style="min-height:25cm; margin-top:30px">
            <table class="table mt-10 mb-0">
                <thead>
                    <tr>
                        <th >Người bán</th>
                        <th class="text-right">Doanh thu</th>
                        <th class="text-right" >Giá trị trả</th></th>
                        <th class="text-right" >Doanh thu thuần</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($records['customers']))
                    <tr class="font-weight-bold">
                        <td >SL người bán: {{ count($records)}}</td>
                        <td class="text-right">{{number_format($records['total']->price)}}</td>
                        <td class="text-right">{{number_format(0)}}</td>
                        <td class="text-right">{{number_format($records['total']->price)}}</td>
                    </tr>
                    @foreach ($records['customers'] as $key=>$item)
                    <tr class="font-weight-bold customer-{{$key}}">
                        <td >
                            <a href="javascript:;" data-customer="{{$item->id}}" data-index="{{$key}}" class="js-customer">{{$item->name}}</a>
                        </td>
                        <td class="text-right">{{number_format($item->total)}}</td>
                        <td class="text-right">0</td>
                        <td class="text-right">{{number_format($item->total)}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="100" class="text-center">Không có dữ liệu</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

