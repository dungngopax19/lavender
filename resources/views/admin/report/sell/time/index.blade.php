@extends('layouts.admin')

@section('title', ucfirst($title))

@section('content')
    {{--  @php
        $created_range = Request::get('created_range');
        if($created_range){
            $created_range = explode(' -> ', $created_range);
        }
    @endphp  --}}
    <div class="row">
        <div class="col-md-2 main-left">
            <form action="" method="get" class="auto-submit">
                @include('admin.'.$mod.'.'.$type.'.'.$concerns.'.filter')
            </form>
        </div>

        <div class="col-md-10 main-right">
            <div class="row">
                {{-- <div class="col-md-6 title-main"><h4>{{ucfirst($title)}}</h4></div> --}}

                <div class="col-md-12 text-right">
                    {{--  <a href="javascript:;">
                        <button type="button" class="btn btn-success btn-export-pdf"><i class="fas fa-file-export"></i> Xuất PDF</button>
                    </a>

                    <a href="{{url( 'admin/reports/'.$type.'/'.$concerns.'/xlsx?' . http_build_query(request()->query()))}}">
                        <button type="button" class="btn btn-primary"><i class="fas fa-file-export"></i> Xuất Excel</button>
                    </a>  --}}
                </div>
            </div>

            <div class="row content-report" >
            </div>

            <div class="mt-15">
                {{-- {{$records->links()}} --}}
            </div>
        </div>
    </div>


@endsection

@push('scripts')

<script>
    // Import DejaVu Sans font for embedding

    // NOTE: Only required if the Kendo UI stylesheets are loaded
    // from a different origin, e.g. cdn.kendostatic.com
    kendo.pdf.defineFont({
        "DejaVu Sans"             : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",
        "DejaVu Sans|Bold"        : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",
        "DejaVu Sans|Bold|Italic" : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
        "DejaVu Sans|Italic"      : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
        "WebComponentsIcons"      : "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"
    });
</script>

<!-- Load Pako ZLIB library to enable PDF compression -->
<script src="https://kendo.cdn.telerik.com/2017.3.913/js/pako_deflate.min.js"></script>

<script>
$(document).ready(function() {

    $(".btn-export-pdf").click(function() {
        // Convert the DOM element to a drawing using kendo.drawing.drawDOM
        kendo.drawing.drawDOM($(".content-pdf"), {
            paperSize: "A4",
            multiPage: "true",
            margin: { left: "1cm", top: "1cm", right: "1cm", bottom: "1cm" }
        })
        .then(function(group) {
            // Render the result as a PDF file
            return kendo.drawing.exportPDF(group);
        })
        .done(function(data) {
            // Save the PDF file
            kendo.saveAs({
                dataURI: data,
                fileName: "Report.pdf",
                proxyURL: "https://demos.telerik.com/kendo-ui/service/export"
            });
        });
    });

    var orders = [];
    function getDataReport(){
        $(this).submitDataAjax({
            'url' : site_url + 'get-reports',
            'method': 'POST',
            'data' : {
                '_token' : CSRF_TOKEN,
                'created_range': '{!!Request::get('created_range')!!}',
                'type' : 'sell',
                'concerns' : 'time',
            },
            'success': function(res){
                console.log(res);
                orders = res.records;
                $('.content-report').html(res.html);
            }
        });
    }

    getDataReport();

    $(document).on('click','.js-date', function(){
        let _index = $(this).data('index');
        if(orders.orders[_index].expand == true){
            let html = `
                <tr class="font-weight-bold child-hour-${_index}">
                    <td >Mã hóa đơn</td>
                    <td >Thời gian</td>
                    <td >Khách hàng</td>
                    <td class="text-right">Doanh thu</td>
                </tr>
            `;
            for(let i in orders.orders[_index].orders){
                html += `
                    <tr class="child-hour-${_index}">
                        <td >${orders.orders[_index].orders[i].code}</td>
                        <td >${orders.orders[_index].orders[i].created_at}</td>
                        <td >${orders.orders[_index].orders[i].customer.name}</td>
                        <td class="text-right">${orders.orders[_index].orders[i].price}</td>
                    </tr>
                `;
            }
            $('.hour-'+_index).after(html);
            orders.orders[_index].expand = false;
        }else{
            $(document).find('.child-hour-'+_index).remove();
            orders.orders[_index].expand = true;
        }
    })

});
</script>
@endpush
