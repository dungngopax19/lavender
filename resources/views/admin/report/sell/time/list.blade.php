@php
    if($created_range){
        $created_range = explode(' -> ', $created_range);
    }
@endphp
<div class="col-md-12" style="background-color:#adabab;">
    <div style="margin: 30px 15px; background-color:white;" class="content-pdf">
        @include('admin.report.header', [
            'created_range'=> $created_range,
            'title'=> 'Báo cáo bán hàng theo thời gian'
            ])

        <div class="col-md-12" style="min-height:25cm; margin-top:30px">
            <table class="table mt-10 mb-0">
                <thead>
                    <tr>
                        <th >Thời gian</th>
                        <th class="text-right">Doanh thu</th>
                        <th class="text-right" >Giá trị trả</th></th>
                        <th class="text-right" >Doanh thu thuần</th>
                    </tr>
                </thead>
                <tbody>
                    @if($records['orders'])
                    <tr class="font-weight-bold">
                        <td ></td>
                        <td class="text-right"></td>
                        <td class="text-right"></td>
                        <td class="text-right">{{number_format($records['total'])}}</td>
                    </tr>
                    @foreach ($records['orders'] as $key=>$item)
                    <tr class="font-weight-bold hour-{{$key}}">
                        <td >
                            @if($whereDate == 'hour')
                            <a href="javascript:;" data-index="{{$key}}" data-date="{{$item['date']}}" data-wherDate="{{$whereDate}}" data-hour="{{date("H", strtotime($item['date']))}}" class="js-date">{{ date("H:i", strtotime($item['date']))}}</a>
                            @elseif($whereDate == 'day')
                            <a href="javascript:;" data-index="{{$key}}" data-date="{{$item['date']}}" data-wherDate="{{$whereDate}}" class="js-date">{{ date("d-m-Y", strtotime($item['date']))}}</a>
                            @elseif($whereDate == 'month')
                            <a href="javascript:;" data-index="{{$key}}" data-date="{{$item['date']}}" data-wherDate="{{$whereDate}}" class="js-date">{{ date("m-Y", strtotime($item['date']))}}</a>
                            @endif
                        </td>
                        <td class="text-right">{{number_format($item['total']['price'])}}</td>
                        <td class="text-right">0</td>
                        <td class="text-right">{{number_format($item['total']['price'])}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="100" class="text-center">Không có dữ liệu</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

