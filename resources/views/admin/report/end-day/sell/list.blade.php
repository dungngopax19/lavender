
<div class="col-md-12" style="min-height:25cm; margin-top:30px">
    <table class="table mt-10 mb-0">
        <thead>
            <tr>
                <th style="width:16%">Mã chứng từ</th>
                <th style="width:13%">Thời gian</th>
                <th class="text-right" style="width:13%">Số lượng</th>
                <th class="text-right" style="width:15%">Doanh thu</th>
                <th class="text-right" style="width:13%">Thu khác</th>
                <th class="text-right" style="width:17%">Phí trả hàng</th>
                <th class="text-right" style="width:13%">Thực thu</th>
            </tr>
        </thead>
        <tbody>
            @php
                $check_data = false;
            @endphp
            @foreach ($records as $item)
                @php
                    if($item->count){
                        $check_data = true;
                    }
                @endphp
                @if(count($item->sub))
                <tr class="font-weight-bold">
                    <td >{{ $item->name}}: {{$item->count}}</td>
                    <td ></td>
                    <td ></td>
                    <td class="text-right">{{number_format($item->total_price)}}</td>
                    <td class="text-right"></td>
                    <td class="text-right"></td>
                    <td class="text-right">{{number_format($item->total_deposit)}}</td>
                </tr>
                @endif
                @foreach ($item->sub as $order)
                    <tr>
                        <td>{{$order->code}}</td>
                        <td>{{$order->created_at_dmy}}</td>
                        <td class="text-right">{{ count($order['orderDetail'])}}</td>
                        <td class="text-right">{{number_format($order->price)}}</td>
                        <td class="text-right">{{number_format(0)}}</td>
                        <td class="text-right">{{number_format(0)}}</td>
                        <td class="text-right">{{number_format($order->deposit)}}</td>
                    </tr>
                @endforeach
            @endforeach

            @if(!$check_data)
            <tr>
                <td colspan="100" class="text-center">Không có dữ liệu</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>

