@extends('layouts.admin')

@section('title', ucfirst($title))

@section('content')
    @php
        $created_range = Request::get('created_range');
        if($created_range){
            $created_range = explode(' -> ', $created_range);
        }
    @endphp
    <div class="row">
        <div class="col-md-2 main-left">
            <form action="" method="get" class="auto-submit">
                @include('admin.'.$mod.'.'.$type.'.'.$concerns.'.filter')
            </form>
        </div>

        <div class="col-md-10 main-right">
            <div class="row">
                {{-- <div class="col-md-6 title-main"><h4>{{ucfirst($title)}}</h4></div> --}}

                <div class="col-md-12 text-right">
                    <a href="javascript:;">
                        <button type="button" class="btn btn-success btn-export-pdf"><i class="fas fa-file-export"></i> Xuất PDF</button>
                    </a>

                    <a href="{{url( 'admin/reports/'.$type.'/'.$concerns.'/xlsx?' . http_build_query(request()->query()))}}">
                        <button type="button" class="btn btn-primary"><i class="fas fa-file-export"></i> Xuất Excel</button>
                    </a>
                </div>
            </div>

            <div class="row content-pdf" >
                {{-- <div class="col-md-12" style="background-color:#adabab;">
                    <div style="margin: 30px 15px; background-color:white;">
                        <div class="col-md-12 mt-10">
                            <p>Ngày lập: {{ date('d/m/Y H:i')}}</p>
                        </div>

                        <div class="col-md-12 text-center font-weight-bold">
                            <h4>Báo cáo cuối ngày về bán hàng</h4>
                        </div>

                        <div class="col-md-12 text-center">
                            @if($created_range[0] !== $created_range[1])
                            <p>Từ ngày {{ $created_range[0]}} đến ngày {{ $created_range[1]}}</p>
                            @else
                            <p>Ngày {{ $created_range[0]}}</p>
                            @endif
                        </div>

                        @include('admin.'.$mod.'.'.$type.'.list')

                    </div>
                </div> --}}
            </div>

            <div class="mt-15">
                {{-- {{$records->links()}} --}}
            </div>
        </div>
    </div>


@endsection

@push('scripts')

<script>
    // Import DejaVu Sans font for embedding

    // NOTE: Only required if the Kendo UI stylesheets are loaded
    // from a different origin, e.g. cdn.kendostatic.com
    kendo.pdf.defineFont({
        "DejaVu Sans"             : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",
        "DejaVu Sans|Bold"        : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",
        "DejaVu Sans|Bold|Italic" : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
        "DejaVu Sans|Italic"      : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
        "WebComponentsIcons"      : "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"
    });
</script>

<!-- Load Pako ZLIB library to enable PDF compression -->
<script src="https://kendo.cdn.telerik.com/2017.3.913/js/pako_deflate.min.js"></script>

<script>
$(document).ready(function() {

    $(".btn-export-pdf").click(function() {
        // Convert the DOM element to a drawing using kendo.drawing.drawDOM
        kendo.drawing.drawDOM($(".content-pdf"), {
            paperSize: "A4",
            multiPage: "true",
            margin: { left: "1cm", top: "1cm", right: "1cm", bottom: "1cm" }
        })
        .then(function(group) {
            // Render the result as a PDF file
            return kendo.drawing.exportPDF(group);
        })
        .done(function(data) {
            // Save the PDF file
            kendo.saveAs({
                dataURI: data,
                fileName: "Report.pdf",
                proxyURL: "https://demos.telerik.com/kendo-ui/service/export"
            });
        });
    });


});
</script>
@endpush
