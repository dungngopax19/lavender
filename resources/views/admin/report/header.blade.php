<div class="col-md-12 mt-10">
    <p>Ngày lập: {{ date('d/m/Y H:i')}}</p>
</div>

<div class="col-md-12 text-center font-weight-bold">
    <h4>{{ $title}}</h4>
</div>

<div class="col-md-12 text-center">
    @if($created_range[0] !== $created_range[1])
    <p>Từ ngày {{ $created_range[0]}} đến ngày {{ $created_range[1]}}</p>
    @else
    <p>Ngày {{ $created_range[0]}}</p>
    @endif
</div>
