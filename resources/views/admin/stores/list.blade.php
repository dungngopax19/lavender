<div class="row">
    <div class="col-md-6 title-main"><h4>{{$title}}</h4></div>
</div>

<div class="table-responsive">
    <table class="list-item table-hover table mt-10 mb-0">
        <thead>
            <tr>
                <th>Hình ảnh</th>
                <th class="text-center">Sản phẩm</th>

                @foreach ($branches as $item)
                    <th class="text-center">{{$item->name}}</th>
                @endforeach

                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($records as $item)
                <tr>
                    <td>
                        <a data-fancybox="gallery" href="{{asset($item->thumbnailDisplay)}}">
                            <img src="{{$item->resizeImage($item->thumbnail, 100)}}" alt="">
                        </a>
                    </td>

                    <td class="text-center">
                        {{$item->name}}
                        <div>({{$item->code}})</div>
                    </td>

                    @foreach ($branches as $branch)
                        @php
                            $inventory = 0;
                            $minInventory = 0;
                            $inBranch = 0;

                            $branchesProducts = $branch->branchesProducts()->where('products_id', $item->id)->first();
                            if($branchesProducts){
                                $inventory = $branchesProducts->inventory;
                                $minInventory = $branchesProducts->min_inventory;
                                $inBranch = $branchesProducts->in_branch;
                            }
                        @endphp
                        <th class="text-center">
                            <div>Tồn kho: {{number_format($inventory)}}</div>
                            <div>Tồn kho ít nhất: {{number_format($minInventory)}}</div>
                            <div>Thuộc chi nhánh này: {{($inBranch == 0) ? 'Ko' : 'Có'}}</div>
                            <div>
                                @if ($inBranch && ( $inventory <= $minInventory ) )
                                    <span class="out-of-stock">( {{ ($inventory > 0) ? 'Sắp hết hàng' : 'Đã hết hàng' }} )</span>
                                @endif
                            </div>
                        </th>
                    @endforeach

                    <td>
                        @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                        <button data-action="products/{{$item->id}}" data-target="#modal-edit-product"
                            data-params="{{json_encode(['type_action'=>'edit-store'])}}"
                            type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="mt-15">
    {{$records->links()}}
</div>
