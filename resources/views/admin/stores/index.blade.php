@extends('layouts.admin')

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-2 main-left">
            <form action="" method="get" class="auto-submit">
                @include('admin.'.$mod.'.filter')
            </form>
        </div>

        <div class="col-md-10 main-right">
            @include('admin.'.$mod.'.list')
        </div>
    </div>
@endsection

@push('modals')
<div class="modal fade" id="modal-edit-{{$mod}}">
    <div class="modal-dialog max-width-80 modal-dialog-centered">
        <div class="modal-content form-tabs"></div>
    </div>
</div>
@endpush