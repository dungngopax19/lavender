<div class="panel-group" id="accordion">
    @include('admin.components.searchs.product-adv')
    @include('admin.components.searchs.product-types')
    {{ Widget::run('Admin.Product.Categories') }}
    {{ Widget::run('Admin.Product.Attributes') }}
    @include('admin.components.searchs.created')
    @include('admin.components.searchs.inventory-status')
    @include('admin.components.searchs.direct-selling')
    @include('admin.components.searchs.activity-status')
    @include('admin.components.searchs.items-per-page')
</div>