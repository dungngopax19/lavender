@php
    use App\User;
@endphp

<div class="row">
    <div class="col-md-6 title-main"><h4>{{$title}}</h4></div>
    @if($is_permission_create)
        <div class="col-md-6 text-right">
            <button type="button" class="btn btn-success ajax-modal"
                data-target="#modal-edit-{{$mod}}" data-action="{{$mod}}s/0">
                <i class="fa fa-plus"></i> {{$title}}
            </button>
        </div>
    @endif
</div>

<table class="list-item table-hover table mt-10 mb-0">
    <thead>
        <tr>
            <th>#</th>
            <th>Tên {{$title}}</th>
            <th class="text-center">SL người dùng</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->name}}</td>
                <td class="text-center">
                    @php
                        $countUsers = User::role($item->name)->count();
                    @endphp

                    @if ($countUsers > 0)
                        <a href="{{url('admin/users/?role_id[]=' . $item->id)}}" target="_blank">
                            {{number_format($countUsers)}}
                        </a>    
                    @else
                        0
                    @endif
                </td>
                <td>
                    @if ($item->id > 2)
                        @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                            <button data-action="{{$mod}}s/{{$item->id}}" data-target="#modal-edit-{{$mod}}" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                        @endif

                        @if($item->hasAllPermission([$permission.'xoa'], $permission))
                            <button data-action="{{$mod}}s/{{$item->id}}" type="button" class="delete-item btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                        @endif
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
