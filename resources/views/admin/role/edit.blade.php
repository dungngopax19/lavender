<form action="{{url('admin/'.$mod.'s/update')}}" method="post" id="form-edit-{{$mod}}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <input type="hidden" name="id" value="{{$record->id}}">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">{{$record?'Thêm mới':'Cập nhật'}} {{$title}}</h4>
        <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                @include('components.inputs.text', ['label'=>'Tên nhóm người dùng', 'name'=>'name', 'value'=>$record->name, 'required'=>true])
            </div>
        </div>

        <input type="hidden" name="permissions" id="permissions">
        <div class="row">
            <div class="col-md-3">
                <div id="treeviewSystem"></div>
            </div>
            <div class="col-md-3">
                <div id="treeviewGoods"></div>
            </div>
            <div class="col-md-3">
                <div id="treeviewPartners"></div>
            </div>
            <div class="col-md-3">
                <div id="treeviewDeals"></div>
            </div>
        </div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
        <button data-form-target="#form-edit-{{$mod}}" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
    </div>
</form>

<script>

$("#treeviewSystem").kendoTreeView({
    checkboxes: {checkChildren: true},
    check: onCheck,
    dataSource: {!! json_encode($dataSourceSystem) !!}
});
$("#treeviewGoods").kendoTreeView({
    checkboxes: {checkChildren: true},
    check: onCheck,
    dataSource: {!! json_encode($dataSourceGoods) !!}
});
$("#treeviewDeals").kendoTreeView({
    checkboxes: {checkChildren: true},
    check: onCheck,
    dataSource: {!! json_encode($dataSourceDeals) !!}
});
$("#treeviewPartners").kendoTreeView({
    checkboxes: {checkChildren: true},
    check: onCheck,
    dataSource: {!! json_encode($dataSourcePartners) !!}
});

// function that gathers IDs of checked nodes
function checkedNodeIds(nodes, checkedNodes) {
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].checked) {
            checkedNodes.push(nodes[i].id);
        }

        if (nodes[i].hasChildren) {
            checkedNodeIds(nodes[i].children.view(), checkedNodes);
        }
    }
}

// show checked node IDs on datasource change
function onCheck() {
    var checkedNodes = [],
    treeviewSystem = $("#treeviewSystem").data("kendoTreeView"),
    treeviewGoods = $("#treeviewGoods").data("kendoTreeView"),
    treeviewDeals = $("#treeviewDeals").data("kendoTreeView"),
    treeviewPartners = $("#treeviewPartners").data("kendoTreeView"),
    ids = '';

    checkedNodeIds(treeviewSystem.dataSource.view(), checkedNodes);
    checkedNodeIds(treeviewGoods.dataSource.view(), checkedNodes);
    checkedNodeIds(treeviewDeals.dataSource.view(), checkedNodes);
    checkedNodeIds(treeviewPartners.dataSource.view(), checkedNodes);

    if (checkedNodes.length > 0) {
        ids = checkedNodes.join(",");
    }

    $('#permissions').val(ids);
}
</script>
