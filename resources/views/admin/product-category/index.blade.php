@extends('layouts.admin')

@section('title', 'Nhóm hàng')

@section('content')
    <div class="row">
        <div class="col-md-2 main-left">
            <form action="" method="get" class="auto-submit">
                @include('admin.product-category.filter')
            </form>
        </div>

        <div class="col-md-10 main-right">
            @include('admin.product-category.list')
        </div>
    </div>
@endsection

@push('modals')
<div class="modal fade" id="modal-edit-product-category">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content"></div>
    </div>
</div>
@endpush