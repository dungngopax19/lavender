<div class="row">
    <div class="col-md-6 title-main"><h4>Nhóm hàng</h4></div>
    @if($is_permission_create)
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-success ajax-modal"
            data-target="#modal-edit-product-category" data-action="product-categories/0">
            <i class="fa fa-plus"></i> Nhóm hàng
        </button>
    </div>
    @endif
</div>

<table class="table mt-10 mb-0 table-tree-view list-products-categories">
    <thead>
        <tr>
            <th>#</th>
            <th>Tên nhóm hàng cha</th>
            <th>Tên nhóm hàng</th>
            <th class="text-center">SL sản phẩm</th>
            <th>Ngày cập nhật</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $i => $item)
            <tr class="bg-gray">
                <td class="toogle" data-tr-id="{{$item->id}}"><i class="fas fa-plus"></i></td>
                <td>{{$item->name}}</td>
                <td></td>
                <td class="text-center num-products">{{number_format($item->products()->count())}}</td>
                <td>{{$item->updated_at}}</td>
                <td>
                    @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                        <button data-action="product-categories/{{$item->id}}" data-target="#modal-edit-product-category" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                    @endif

                    @if($item->hasAllPermission([$permission.'xoa'], $permission))
                        <button data-action="product-categories/{{$item->id}}" type="button" class="delete-item btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                    @endif
                </td>
            </tr>

            @php
                $query = $item->childrens();
                if(!empty(request()->name)){
                    $query = $query->where('name', 'like', '%' . request()->name . '%');
                }
                $itemChildrens = $query->orderBy('name')->get();
            @endphp
            @if ($itemChildrens->count() > 0)
                @foreach ($itemChildrens as $iChild => $itemChild)
                    @php
                        $numProducts = $itemChild->products()->count();
                    @endphp
                    <tr class="bg-white hide tr-child-{{$item->id}}">
                        <td></td>
                        <td>---</td>
                        <td>{{$itemChild->name}}</td>
                        <td class="text-center num-products" data-number="{{$numProducts}}">
                            {{number_format($numProducts)}}
                        </td>
                        <td>{{$itemChild->updated_at}}</td>
                        <td>
                            <button data-action="product-categories/{{$itemChild->id}}" data-target="#modal-edit-product-category" type="button" class="btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                            <button data-action="product-categories/{{$itemChild->id}}" type="button" class="delete-item btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
                @endforeach
            @endif
        @endforeach
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>

@push('scripts')
<script>
    $(document).ready(function(){
        $('table.list-products-categories .bg-gray .toogle').each(function(){
            var id = $(this).data('tr-id');
            var numProducts = 0;
            $('.tr-child-' + id + ' .num-products').each(function(){
                numProducts += $(this).data('number');
            });
            $(this).parent('.bg-gray').find('.num-products').html(numProducts.toLocaleString('vi'));
        });
    });
</script>
@endpush
