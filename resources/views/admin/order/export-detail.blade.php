<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Document</title>
</head>
<body>
    @php
        use App\Models\Orders;
    @endphp

    <table>
        <tr>
            <th>Chi nhánh</th>
            <th>Mã đặt hàng</th>
            <th>Mã vận đơn</th>
            <th>Mã Đơn hàng</th>
            <th>Thời gian</th>
            <th>Thời gian tạo</th>
            <th>Mã khách hàng</th>
            <th>Tên khách hàng</th>
            <th>Số điện thoại</th>
            <th>Địa chỉ (Khách hàng)</th>
            <th>Khu vực (Khách hàng)</th>
            <th>Người nhận đặt</th>
            <th>Kênh bán hàng</th>
            <th>Đối tác giao hàng</th>
            <th>Người nhận</th>
            <th>Điện thoại</th>
            <th>Địa chỉ (Người nhận)</th>
            <th>Khu vực (Người nhận)</th>
            <th>Dịch vụ</th>
            <th>Phí giao hàng (trả ĐT)</th>
            <th>Người tạo</th>
            <th>Ghi chú</th>
            <th>Tổng tiền hàng</th>
            <th>Giảm giá phiếu đặt</th>
            <th>Khách đã trả</th>
            <th>Thẻ</th>
            <th>Chuyển khoản</th>
            <th>ĐVT</th>
            <th>Điểm</th>
            <th>Thời gian giao hàng</th>
            <th>Trạng thái</th>
            <th>Mã hàng</th>
            <th>Tên hàng</th>
            <th>Ghi chú hàng hóa</th>
            <th>Số lượng</th>
            <th>Giá bán</th>
            <th>Thành tiền</th>
        </tr>

        @forelse ($records as $order)
            {{--  @if(isset($records->orderDetail))  --}}
                @foreach($order->orderDetail as $order_detail)
                <tr>
                    <td>{{$order->branch['name']}}</td>
                    <td>{{$order->code}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ $order->created_at}}</td>
                    <td>{{ $order->customer['code']}}</td>
                    <td>{{ $order->customer['name']}}</td>
                    <td>{{ $order->customer['phone']}}</td>
                    <td>{{ $order->customer['address']}}</td>
                    <td>{{ $order->customer['province_display']}}</td>

                    <td>{{$order->user['name']}}</td>
                    <td>{{Orders::CHANNEL[$order->channel]}}</td>

                    <td>{{$order->shipmentDetail['partnerDelivery']['name']}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td>{{ $order->shipmentDetail['type_services_id']}}</td>
                    <td>{{ $order->createdUser['name']}}</td>
                    <td>{{ $order->note}}</td>
                    <td>{{ number_format($order->price)}}</td>
                    <td>{{ number_format($order->price - $order->price_final)}}</td>
                    <td>{{ number_format($order->deposit)}}</td>

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td>{{ $order->shipmentDetail['delivery_time']}}</td>
                    <td>{{ $order->shipmentDetail['shipmentStatus']['name']}}</td>

                    <td>{{ $order_detail->product['code']}}</td>
                    <td>{{ $order_detail->product['name']}}</td>
                    <td>{{ $order_detail->product['sample_note']}}</td>
                    <td>{{ number_format($order_detail->qty)}}</td>
                    <td>{{ number_format($order_detail->price)}}</td>
                    <td>{{ number_format($order_detail->price * $order_detail->qty)}}</td>

                </tr>
                @endforeach
            {{--  @endif  --}}
        @empty
            <tr>
                <td colspan="100" class="text-center"></td>
            </tr>
        @endforelse
    </table>
</body>
</html>
