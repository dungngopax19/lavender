@php
    use App\Models\Orders;
    $flag = true;
@endphp
<div class="row">
    <div class="col-md-6 title-main"><h4>Phiếu đặt hàng</h4></div>
    <div class="col-md-6 text-right">

        <a href="{{url( 'admin/order-export/?' . http_build_query(request()->query()))}}" target="_blank">
            <button type="button" class="btn btn-success"><i class="fas fa-file-export"></i> Xuất file</button>
        </a>

        <a href="{{url( 'admin/order-export-detail/?' . http_build_query(request()->query()))}}" target="_blank">
            <button type="button" class="btn btn-success"><i class="fas fa-file-export"></i> Xuất file chi tiết</button>
        </a>
    </div>
</div>

<table class="list-item table-hover table mt-10 mb-0">
    <thead>
        <tr>
            <th>Mã đặt hàng</th>
            <th>Thời gian</th>
            <th>Khách hàng</th>
            <th class="text-center">Khách cần trả</th>
            <th class="text-center">Khách đã trả</th>
            <th>Trạng thái</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @forelse ($records as $item)
            @if ($flag)
                <tr style="background: #fefced;">
                    <td colspan="3"></td>
                    <td class="text-center"><strong>{{number_format($total)}}đ</strong></td>
                    <td class="text-center"><strong>{{number_format($totalPaid)}}đ</strong></td>
                    <td colspan="2"></td>
                </tr>
            @php $flag = false; @endphp
            @endif
            <tr>
                <td>{{$item->code}}</td>
                <td>{{date('d-m-Y H:i', strtotime($item->created_at))}}</td>
                <td>{{!empty($item->customer) ? $item->customer->name : ''}}</td>
                <td class="text-center">
                    @if($item->price_final)
                        {{number_format($item->price_final)}}đ
                    @else
                        {{number_format($item->price)}}đ
                    @endif
                </td>
                <td class="text-center">{{number_format($item->deposit)}}đ</td>
                <td>{{!empty($item->orders_status_id) ? $item->status->name : ''}}</td>
                <td>
                    @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                        <button data-params="{{json_encode(['type'=>'orders', 'orders_code' => $item->code])}}" data-target="#modal-cart" data-action="detail-cart" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                    @endif

                    @if($item->hasAllPermission([$permission.'xoa'], $permission))
                        <button data-action="{{$mod}}s/{{$item->id}}" type="button" class="delete-item btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                    @endif
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="100" class="text-center">Không có dữ liệu</td>
            </tr>
        @endforelse
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
