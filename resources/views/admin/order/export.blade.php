<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Document</title>
</head>
<body>
    @php
        use App\Models\ProductsComponents;
        use App\Models\BranchesProducts;
        use App\Models\Attributes;
        use App\Models\Orders;

        $i=0;
    @endphp
    					
    <table>
        <tr>
            <th>ID</th>
            <th>Mã đặt hàng</th>
            <th>Thời gian</th>
            <th>Khách hàng</th>
            <th>Khách cần trả</th>
            <th>Khách đã trả</th>
            <th>Giảm giá</th>
            <th>Còn lại</th>
        </tr>

        @forelse ($records as $item)
            @php $i++; @endphp
            <tr>
                <td>{{$i}}</td>
                <td>{{$item->code}}</td>
                <td>{{date('d-m-Y H:i', strtotime($item->created_at))}}</td>
                <td>{{!empty($item->customer) ? $item->customer->name : ''}}</td>
                <td>
                    @if($item->price_final)
                        {{number_format($item->price_final)}}
                    @else
                        {{number_format($item->price)}}
                    @endif
                </td>
                <td>{{number_format($item->deposit)}}</td>
                <td>
                    @if($item->discount_unit == 'vnd')
                        {{number_format($item->discount)}}
                    @else
                        {{number_format($item->discount)}}%
                    @endif
                </td> 
                <td>
                    @if($item->discount_unit == 'vnd')
                        {{number_format($item->price - $item->deposit - $item->discount)}}
                    @else
                        {{number_format($item->price - $item->deposit - ($item->price * ($item->discount / 100)))}}
                    @endif
                </td> 
            </tr>
        @empty
            <tr>
                <td colspan="100" class="text-center"></td>
            </tr>
        @endforelse
    </table>
</body>
</html>
