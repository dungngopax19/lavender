<form action="{{url('admin/promotions/update')}}" method="post" id="form-edit-promotions">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <input type="hidden" name="id" value="{{$record->id}}">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">{{$title}}</h4>
        <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.text', ['label'=>'Tên chương trình', 'name'=>'name', 'value'=>$record->name, 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.select', ['label'=>'Nhóm sản phẩm', 'name'=>'cat_ids[]', 'value'=>$cat_ids, 'data' => $productCategories, 'multiple'=>true, 'select'=>'id', 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.text', ['type'=>'number', 'label'=>'Giá tiền', 'name'=>'price', 'value'=>$record->price, 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.datepicker', ['label'=>'Ngày bắt đầu', 'name'=>'from_date', 'value'=>$record->from_date->format('d-m-Y'), 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.datepicker', ['label'=>'Ngày kết thúc', 'name'=>'to_date', 'value'=>$record->to_date->format('d-m-Y'), 'required'=>true])
            </div>
        </div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
        <button data-form-target="#form-edit-promotions" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
    </div>
</form>

<script>
    $(document).ready(function(){

        $('input[name=from_date], input[name=to_date]').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            },
            autoUpdateInput: false,
        });

        $('input[name=from_date], input[name=to_date]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
    });
</script>
