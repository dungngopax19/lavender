<div class="row">
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Địa chỉ lấy hàng', 'readonly' => 'readonly', 'name'=>'', 'value' => $branchActive->address ])
    </div>
    <div class="col-md-6">
        @php
            $delivery_time = $detailOrders->shipmentDetail['delivery_time'];
            $delivery_time = ( $delivery_time == '1970-01-01') ? '' : $delivery_time;
        @endphp
        @include('components.inputs.datepicker', ['label'=>'Thời gian giao hàng', 'name'=>'delivery_time', 'value'=>$delivery_time])
        
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Tên người nhận hàng', 'required'=>true, 'name'=>'name', 'value'=>$detailOrders->shipmentDetail['name']])
    </div>
    <div class="col-md-6">
        @include('components.inputs.select', ['label'=>'Loại dịch vụ', 'name'=>'type_services_id', 'value'=>$detailOrders->shipmentDetail['type_services_id'], 'data'=> $type_services])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Số điện thoại', 'name'=>'phone', 'value'=>$detailOrders->shipmentDetail['phone']])
    </div>
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Mã vận đơn', 'name'=>'lading_code', 'value'=>$detailOrders->shipmentDetail['lading_code']])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.select', ['label'=>'Khu vực', 'name'=>'provinces_id', 'required' => true, 'value'=>$detailOrders->shipmentDetail['provinces_id'], 'data'=> $provinces])
    </div>
    <div class="col-md-6">
        @include('components.inputs.select', ['label'=>'Đối tác giao hàng', 'name'=>'partner_delivery_id', 'value'=>$detailOrders->shipmentDetail['partner_delivery_id'], 'data'=> $partners])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.text', ['label'=>'Địa chỉ nhận hàng', 'name'=>'address', 'value'=>$detailOrders->shipmentDetail['address']])
    </div>

    @if ($type == 'invoices')
        <div class="col-md-6">
            @include('components.inputs.label', ['label'=>'Trạng thái giao hàng', 'value'=>$detailOrders->shipmentStatus['name']])
        </div>
    @endif
</div>

<div class="row">
    <div class="col-md-12">
        @include('components.inputs.textarea', ['label'=>'Ghi chú giao hàng', 'name'=>'comment', 'value'=>$detailOrders->comment, 'colLeft'=>2, 'colRight'=>10])
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @include('components.inputs.textarea', ['label'=>'Ghi chú điều chuyển sp', 'name'=>'comment_transfer', 'value'=>$detailOrders->comment_transfer, 'colLeft'=>2, 'colRight'=>10])
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.singleDatePicker').daterangepicker({
            "singleDatePicker": true,
            "timePicker": true,
            "autoApply": true,
            "autoUpdateInput": false,
            "drops": "up",
            locale: {
                format: 'DD-MM-YYYY HH:II:SS'
            }
        }, function(start, end, label) {
            this.element.val(start.format('DD-MM-YYYY hh:mm'));
        });
    });
</script>