<form action="{{url('admin/create-orders')}}" method="post" id="form-edit-cart">
    {{ csrf_field() }}
    {{ method_field('POST') }}

    @if ($detailOrders->id)
        <input type="hidden" name="orders_id" value="{{$detailOrders->id}}">
    @endif

    <input type="hidden" name="is_invoices" value="{{($type == 'orders') ? 0 : 2}}">
    <input type="hidden" name="branches_id" value="{{$branchActive->id}}">
    <input type="hidden" name="discount_unit" value="vnd">
    <input type="hidden" name="create_end_invoices" value="0">
    <input type="hidden" name="isCopyCart" value="{{intval($isCopyCart)}}">
    <input type="hidden" name="cartNumber" value="{{$cartNumber}}">

    @if ($isCopyCart)
        <input type="hidden" name="orders_code_copy" value="{{$detailOrders->code}}">
    @endif

    <!-- Modal body -->
    <div class="modal-body">
        <ul class="nav nav-tabs">
            <li><a data-toggle="tab" class="active show" href="#basic-info">Thông tin {{$typeTxt}}</a></li>
            <li class="tab-order-histories hide"><a data-toggle="tab" href="#order-histories">Lịch sử mua hàng(khách hàng)</a></li>
        </ul>

        <div class="tab-content mt-3">
            <div id="basic-info" class="tab-pane fade in active show">
                @include('admin.cart.basic-info')
            </div>

            <div id="order-histories" class="tab-pane fade wrap-order-histories">
                {{--  render by JavaScript  --}}
            </div>
        </div>


    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>

    <button type="button" class="btn btn btn-success ajax-print {{ empty($detailOrders->id) ? 'hide' : '' }}">In</button>

        <button data-form-target="#form-edit-cart" type="button" class="btn btn-primary btn-submit-orders" auto-close-modal="true" data-form-method ="POST">
            @if (empty($detailOrders->id) || $isCopyCart)
                Tạo {{$typeTxt}}
            @else
                Cập nhật {{$typeTxt}}
            @endif
        </button>

        @if (!$isCopyCart && $detailOrders->id > 0 )
            <button data-params="{{json_encode(['type'=>$type, 'orders_code' => 'copy_' . $detailOrders->code])}}" data-target="#modal-cart" data-action="detail-cart" type="button" class="btn btn-warning ajax-modal">Sao chép {{$typeTxt}}</button>
        @endif

        @if ( !$isCopyCart && $type == 'orders' && $detailOrders->id > 0 && $detailOrders->verified != -1 )
            <button data-form-target="#form-edit-cart" type="button" class="btn btn-success btn-submit-orders btnCreateOrdersFromDrafts" auto-close-modal="true" data-form-method ="POST">Tạo đơn hàng từ phiếu đặt hàng</button>
        @endif

        @if( empty($detailOrders->verified) && $detailOrders->verified != -1 )
            @if ( $type == 'invoices' && empty($detailOrders->id) )
                <button data-form-target="#form-edit-cart" type="button" class="btn btn-success btn-submit-orders btnCreateEndOrders" auto-close-modal="true" data-form-method ="POST">Tạo và kết thúc đơn hàng</button>
            @endif
        @endif

    </div>
</form>
