<div class="row">
    <div class="col-md-6">
        @include('components.inputs.label', ['colLeft' => 5, 'labelAlign'=>'text-right', 'label'=>'Địa chỉ lấy hàng', 'value' => $branchActive->address ])
    </div>
    <div class="col-md-6">
        @include('components.inputs.label', ['colLeft' => 5, 'labelAlign'=>'text-right', 'label'=>'Đối tác giao hàng', 'value'=>$detailOrders->shipmentDetail['partner_delivery_id']])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.label', ['colLeft' => 5, 'labelAlign'=>'text-right', 'label'=>'Tên người nhận hàng', 'value'=>$detailOrders->shipmentDetail['name']])
    </div>
    <div class="col-md-6">
        @include('components.inputs.label', ['colLeft' => 5, 'labelAlign'=>'text-right', 'label'=>'Loại dịch vụ', 'value'=>$detailOrders->shipmentDetail['type_services_id']])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.label', ['colLeft' => 5, 'labelAlign'=>'text-right', 'label'=>'Số điện thoại', 'value'=>$detailOrders->shipmentDetail['phone']])
    </div>
    <div class="col-md-6">
        @include('components.inputs.label', ['colLeft' => 5, 'labelAlign'=>'text-right', 'label'=>'Mã vận đơn', 'value'=>$detailOrders->shipmentDetail['lading_code']])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.label', ['colLeft' => 5, 'labelAlign'=>'text-right', 'label'=>'Khu vực', 'value'=>$detailOrders->shipmentDetail['provinces_id']])
    </div>
    <div class="col-md-6">
        @include('components.inputs.label', ['colLeft' => 5, 'labelAlign'=>'text-right', 'label'=>'Thời gian giao hàng', 'value'=>$detailOrders->shipmentDetail['delivery_time']])
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        @include('components.inputs.label', ['colLeft' => 5, 'labelAlign'=>'text-right', 'label'=>'Địa chỉ nhận hàng', 'value'=>$detailOrders->shipmentDetail['address']])
    </div>

    @if ($type == 'invoices')
        <div class="col-md-6">
            @include('components.inputs.label', ['colLeft' => 5, 'labelAlign'=>'text-right', 'label'=>'Trạng thái giao hàng', 'value'=>$detailOrders->shipmentDetail['shipmentStatus']['name']])
        </div>
    @endif
</div>