@if ($customer->order_last)
    <div class="border-dashed">
    
        @if ($customer->membership)
            <div class="row mt-2">
                <div class="col-md-8">Cấp bậc khách hàng</div>
                <div class="col-md-4 text-right">
                    <input type="hidden" name="membership_id" value="{{str_replace('level-', '', $customer->membership)}}">
                    @php
                       $levels = config('constants.membership'); 
                       echo $levels[$customer->membership] ? $levels[$customer->membership] : '';
                    @endphp
                </div>
            </div>    
        @endif

        <div class="row mt-2 mb-2">
            <div class="col-md-8">Số đơn hàng đã thực hiện</div>
            <div class="col-md-4 text-right">{{ number_format($customer->totalOrders) }}</div>
        </div>

        <div class="row mt-2 mb-2">
            <div class="col-md-8">Số tiền giao dịch gần nhất</div>
            <div class="col-md-4 text-right">{{ number_format($customer->order_last->price) }} đ</div>
        </div>

        <div class="row mt-2">
            <div class="col-md-8">Giao dịch gần nhất</div>
            <div class="col-md-4 text-right">{{ date('d-m-Y', strtotime($customer->order_last->created_at)) }}</div>
        </div>

        <div class="row mt-2">
            <div class="col-md-8">
                Nhập mã coupon
                <div class="msg-coupon">
                    @if ($customer->coupon)
                        @if ($customer->discount_coupon)
                            <span style="color:#3390dc">Bạn được giảm {{number_format($customer->discount_coupon)}}đ</span>
                        @elseif(!empty($customer->coupon) && $customer->coupon != 'NULL')
                            <span style="color:red">Coupon không hợp lệ</span>
                        @endif    
                    @endif
                </div>
                <input type="hidden" name="discount_coupon" value="{{$customer->discount_coupon}}">
            </div>
            <div class="col-md-4 text-right">
                <input type="text" name="coupon" class="txt-border-bottom" value="{{($customer->coupon == 'NULL') ? '' : $customer->coupon}}" {{empty($ordersDetail->id) ? '': 'readonly'}}>
            </div>
        </div>

        @if ($customer->discountPrice && $customer->giftsProducts)
            @php
                $discountPrice = $customer->discountPrice;
            @endphp

            <div class="row mt-2 pb-2">
                <div class="col-md-5">Khuyến mãi</div>
                <div class="col-md-7 text-right">

                    @if (isset($ordersDetail->id) && $ordersDetail->id)
                        <select class="select-transparent" disabled id="option-gift-discount">
                            @if ($ordersDetail->discount_gift)
                                <option value="1">Giảm giá {{ number_format($ordersDetail->discount_gift) }} {{($ordersDetail->discount_gift_unit == 'vnd') ? 'đ' : '%'}}</option>    
                            @elseif($ordersDetail->discount_gift_product == 2)
                                <option value="2">Sản phẩm kèm theo</option>
                            @endif
                        </select>
                    @else
                        <select class="select-transparent" name="optionGiftDiscount" id="option-gift-discount"
                            data-discount-gift="{{$discountPrice->value}}" data-discount-gift-unit="{{$discountPrice->unit}}">
                            <option value="0">Chọn loại khuyến mãi</option>

                            @if ($discountPrice && $discountPrice->value > 0)
                                <option value="1">Giảm giá {{ number_format($discountPrice->value) }} {{($discountPrice->unit == 'vnd') ? 'đ' : '%'}}</option>    
                            @endif

                            @if (count($customer->giftsProducts) > 0)
                                <option value="2">Sản phẩm kèm theo</option>
                            @endif
                            
                        </select>
                    @endif
                </div>
            </div>

            @if (count($customer->giftsProducts) > 0)
                <ul class="pl-4 mb-2 gift-discount hide">
                    @foreach ($customer->giftsProducts as $item)
                        @php
                            $productGiftIds[] = $item->value;
                        @endphp
                        <li>{{$item->product['name']}} ({{$item->product['code']}})</li>            
                    @endforeach
                </ul>

                <input type="hidden" name="productGiftIds" value="{{implode(',', $productGiftIds)}}">
            @endif
        
        @else
            <div class="pb-2"></div>
        @endif

    </div>
    
@endif