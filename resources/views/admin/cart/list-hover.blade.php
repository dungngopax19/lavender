@php
    use App\Helpers\Product as ProductHelper;
    use App\Helpers\Common;
@endphp

@if (empty($records) || count($records) < 1)
    <div class="mb-2 mt-2 text-center">Chưa có sản phẩm nào trong {{$typeTxt}} của bạn</div>
@else
    <div class="item-cart">
        @foreach ($records as $k => $item)
            <div class="media mt-2 pb-2 ajax-modal" data-action="products/{{$item->id}}" data-target="#modal-edit-product">
                <div class="media-left">
                    <img src="{{Common::resizeImage($item->options['thumbnail'], 100)}}" alt="{{$item->options['thumbnail']}}">
                </div>
                <div class="media-body ml-2">
                    <div class="name">{{$item->name}}</div>
                    <div class="code">{{$item->options['code']}}</div>
                    <div class="price">{{number_format($item->price)}}đ</div>
                    <div>Số lượng: {{number_format($item->quantity)}}</div>
                </div>

                <a href="javascript:void(0);" data-id="{{$item->id}}" data-type="{{$type}}" data-uniqueId="{{$k}}" class="btn-icon btn-delete remove-item-cart" title="Xóa hàng hóa"><i></i></a>
            </div>
        @endforeach
    </div>

    <div class="row mt-2 mb-3">    
        <div class="col-md-11 text-right"><strong>Tổng tiền: </strong> <span class="total-price">{{number_format($totalPrice)}} đ<span></div>
    </div>

    <div class="row mt-2 mb-3">
        <div style="margin: auto">
            <button type="button" class="btn btn-success mr-2 btn-clear-cart" data-cartnumber="{{$cartNumber}}" data-type="{{$type}}">Xoá {{$typeTxt}}</button>
            <button type="button" class="btn btn-primary ajax-modal" data-params="{{json_encode(['type'=>$type, 'cartNumber' => $cartNumber ])}}" data-target="#modal-cart" data-action="detail-cart">Tạo {{$typeTxt}}</button>
        </div>
    </div>
@endif