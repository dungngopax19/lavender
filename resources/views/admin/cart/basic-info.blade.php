@php

    use App\Helpers\Product as ProductHelper;
    use App\Helpers\Common;
    use App\Helpers\Permissions;

    $verifyPermission = Permissions::hasAllPermission(['giao-dich-hoa-don-xac-nhan-don-hang']);
    $shipPermission = Permissions::hasAllPermission(['giao-dich-van-chuyen']);

    $is_disable_verified = ($detailOrders->verified != 0) ? 'readonly' : '';
    $txtType = $type == 'orders' ? 'Phiếu đặt hàng' : 'Đơn hàng';
@endphp

<div class="row cart-info modal-cart">
    <div class="col-md-12 wrap-print">
        @include('admin.components.prints.order', ['itemsCart'=> $itemsCart, 'detailOrder' => $detailOrders])
    </div>
    {{-- List item cart --}}
    <div class="col-md-9 col-left">
        <div class="html-cart-modal">
            @component('admin.components.cart.template-2', [
                'itemsCart' => $itemsCart,
                'type' => $type,
                'products' => $products,
                'detailOrders' => $detailOrders,
                'disabledEditCart' => $disabledEditCart
            ])@endcomponent
        </div>

        {{-- Thông tin giao hàng --}}
        <div class="border-dashed-top mt-5 pt-3" id="shipment-info">
            <h5>Thông tin giao hàng</h5>
            @include('admin.cart.shipment-info')
        </div>

        @if (!$isCopyCart)
            @if ($detailOrders->id)
                @include('admin.cart.receipt-histories')
            @endif

            {{-- Lịch sử thao tác --}}
            {{ Widget::run('Admin\ActivityLogInvoice', ['id' => $detailOrders->id]) }}
        @endif
    </div>

    {{-- Col right --}}
    <div class="col-md-3 mt-2 col-right">
        {{-- Ngày tạo / Mã Đơn hàng --}}
        @if ($detailOrders->id)
            <div class="row mb-2">
                <div class="col-md-4"><b>Code</b></div>
                <div class="col-md-8">{{ $isCopyCart ? 'Copy_' : '' }}{{$detailOrders->code}}</div>
            </div>

            @if (!$isCopyCart)
                <div class="row mb-2">
                    <div class="col-md-4"><b>Ngày tạo</b></div>
                    <div class="col-md-8">{{$detailOrders->created_at->format('d/m/Y H:i:s')}}</div>
                </div>
            @endif
        @endif

        {{-- Chi nhánh --}}
        <div class="row mt-2">
            <div class="col-md-4"><b>Chi nhánh</b></div>
            <div class="col-md-8">{{$branchActive->name}}</div>
        </div>

        {{-- Đơn hàng ưu tiên --}}
        <div class="row mt-3">
            <div class="col-md-4 mt-2"><b>Đơn hàng</b></div>
            <div class="col-md-8">
                <select class="select2 form-control" name="highlight">
                    <option value="0" @if($detailOrders->highlight == '0') selected @endif>Bình thường</option>
                    <option value="1" @if($detailOrders->highlight == '1') selected @endif>Ưu tiên</option>
                    <option value="2" @if($detailOrders->highlight == '2') selected @endif>Size riêng</option>
                </select>
            </div>
        </div>

        {{-- Kênh bán --}}
        <div class="row mt-3">
            <div class="col-md-4 mt-2"><b>Kênh bán</b></div>
            <div class="col-md-8">
                <select {{$disabledEditCart}} class="select2 form-control" name="channel">
                    <option value="1">Bán trực tiếp</option>
                    <option value="2">Facebook</option>
                    <option value="3">Khác</option>
                </select>
            </div>
        </div>

        {{-- users_id --}}
        <div class="row mt-3 form-group fg-users_id">
            <div class="col-md-4 mt-2"><b>Người bán</b></div>
            <div class="col-md-8">
                <select {{$disabledEditCart}} class="select2 form-control" name="users_id">
                    @foreach($users as $item)
                        <option value="{{ $item->id}}" @if($item->id == $currentUser->id) selected @endif>{{ $item->name}}</option>
                    @endforeach
                </select>
                <div class="error-msg"></div>
            </div>
        </div>

        {{-- Khách hàng --}}
        <div class="row mt-3 form-group fg-customers_id">
            <div class="col-md-4 mt-2"><b>Khách hàng</b></div>
            <div class="col-md-8">
                <div class="d-flex">
                    @if ($isCopyCart || empty($detailOrders->id))
                        <select class="select2 form-control search-customer-component" name="customers_id">
                            @if ($isCopyCart)
                                <option value="{{$detailOrders->customers_id}}">{{$detailOrders->customer['code'] . ' - ' . $detailOrders->customer['name']}}</option>    
                            @else
                                <option value="0">---</option>
                            @endif
                        </select>
                        <i class="fas fa-plus mt-2 ml-2 add-new-customer"></i>
                    @else
                        <select {{$disabledEditCart}} class="select2 form-control" name="customers_id">

                            @if (empty($detailOrders->customers_id))
                                <option>Khách lẻ</option>
                            @else
                                <option>{{$detailOrders->customer['code'] . ' - ' . $detailOrders->customer['name']}}</option>    
                            @endif
                            
                        </select>
                    @endif
                    
                </div>

                <div class="error-msg"></div>
            </div>
        </div>

        <div id="info-extra-customer">
            @if ($detailOrders->customers_id)
                <div class="border-dashed">
                    @if ($detailOrders->discount_gift > 0)
                        <div class="row mt-2 pb-2">
                            <div class="col-md-5">Khuyến mãi</div>
                            <div class="col-md-7 text-right">
                                Giảm giá {{ number_format($detailOrders->discount_gift) }} {{($detailOrders->discount_gift_unit == 'vnd') ? 'đ' : '%'}}
                            </div>
                        </div>
                    @elseif( count($giftProducts) > 0 )
                        <div class="row mt-2">
                            <div class="col-md-12">Quà tặng sản phẩm khuyến mãi:</div>
                        </div>
                        <ul class="pl-4 mb-2">
                            @foreach ($giftProducts as $item)
                                <li>{{$item->product['name']}} ({{$item->product['code']}})</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            @endif
        </div>

        <input type="hidden" name="discountGiftPrice" id="discountGiftPrice" value="{{$detailOrders->discount_gift ?? 0}}" />
        <input type="hidden" name="discountGiftUnit" id="discountGiftUnit" value="{{$detailOrders->discount_gift_unit ?? 'vnd'}}" />
        
        <div class="row mt-3">
            <div class="col-md-7"><b>Phí Ship</b></div>
            <div class="col-md-5 text-right">
                <input type="text" {{($disabledEditCart == 'disabled' ? 'readonly="true"' : '')}} name="ship" class="txt-border-bottom format-number" value="{{number_format($detailOrders->ship)}}">đ
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-8"><b>Tổng tiền hàng</b></div>
            <div class="col-md-4 text-right total-price-cart">
                @if ($detailOrders->id)
                    {{number_format($detailOrders->price)}}đ
                @else
                    {{number_format($totalPrice)}}đ
                @endif
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-4 mt-1"><b>Giảm giá</b></div>
            <div class="col-md-8 text-right discount-total-cart">
                <input type="text" {{($disabledEditCart == 'disabled' ? 'readonly="true"' : '')}} name="discount" value="{{number_format($detailOrders->discount)}}" class="txt-border-bottom format-number">
                <input type="button" {{$disabledEditCart}} data-unit="vnd" class="btn-discount {{(in_array($detailOrders->discount_unit, ['vnd', ''])) ? 'btn-info' : ''}}" value="vnd">
                <input type="button" {{$disabledEditCart}} data-unit="percent" class="btn-discount {{($detailOrders->discount_unit == 'percent') ? 'btn-info' : ''}}" value="%">
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-6 mt-1"><b>Khách cần trả</b></div>
            <div class="col-md-6 text-right">
                <input type="text" readonly="true" name="needpay" class="txt-border-bottom" value="{{$detailOrders->id ? number_format($detailOrders->price + $detailOrders->ship - $detailOrders->discount - $detailOrders->discount_gift) : number_format($totalPrice)}}">đ
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-7"><b>Khách đã thanh toán trước</b></div>
            <div class="col-md-5 text-right">
                <input type="text" {{($disabledEditCart == 'disabled' ? 'readonly="true"' : '')}} name="deposit" class="txt-border-bottom format-number" value="{{number_format($detailOrders->deposit)}}">đ
            </div>
        </div>

        <div class="row mt-3">
            <input type="hidden" name="order_customer_need_pay" value="0">
            <div class="col-md-7"><b>Số tiền còn lại phải thanh toán</b></div>
            <div class="col-md-5 text-right">
                <input type="text" readonly="true" name="againPay" class="txt-border-bottom" value="{{$detailOrders->id ? number_format($detailOrders->price_final) : number_format($totalPrice)}}">đ
            </div>
        </div>

        <div class="row hide">
            <div class="col-md-12">
                @include('components.inputs.textarea', ['label'=>'Ghi chú', 'name'=>'note', 'value'=>'', 'colLeft' => 12, 'colRight' => 12])
            </div>
        </div>

        @if (!$isCopyCart && $detailOrders->id)

            {{-- Xác nhận đơn hàng --}}
            @if ($detailOrders->verified == -1)
                <div class="row mb-2 border-dashed-top mt-2">
                    <div class="col-md-12">
                        <i class="fas fa-ban"></i><b>
                            Đã từ chối duyệt {{$txtType}}

                            @if ($detailOrders->date_verified_display != '01-01-1970')
                                ({{$detailOrders->date_verified_display}})    
                            @endif
                        </b>
                    </div>
                </div>

            @elseif( $detailOrders->verified == 0 )
                @if($verifyPermission)
                    <div class="row pt-3 mb-2 border-dashed-top mt-2">
                        <div class="col-md-7 mt-2"><b>Kiểm duyệt {{$txtType}}</b></div>
                        <div class="col-md-5">
                            <select class="select2 form-control" name="verified">
                                <option value="0">Đang chờ</option>
                                <option value="-1">Không duyệt</option>
                                <option value="1">Đã duyệt</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            @include('components.inputs.textarea', ['label'=>'Ghi chú của người xác nhận đơn/phiếu đặt hàng', 'name'=>'note_verified', 'value'=>$detailOrders->note_verified, 'colLeft' => 12, 'colRight' => 12])
                        </div>
                    </div>
                @endif
            @else
                <div class="row border-dashed-top mt-2">
                    <div class="col-md-12">
                        <i class="fas fa-check"></i><b>
                            {{$txtType}} đã được duyệt
                            @if ($detailOrders->date_verified_display != '01-01-1970')
                                ({{$detailOrders->date_verified_display}})    
                            @endif
                        </b>
                    </div>
                </div>

                {{-- Trạng thái đơn hàng --}}
                @if ($shipPermission)

                    <div class="row pt-2 border-dashed-top mt-2">
                        <div class="col-md-6 mt-2"><b>Trạng thái giao hàng</b></div>
                        <div class="col-md-6">
                            <select class="select2 form-control" name="shipment_status_id">
                                @foreach($shipment_status as $item)
                                    <option value="{{ $item->id}}" @if($item->id == $detailOrders->shipment_status_id) selected @endif>{{ $item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row pt-2 border-dashed-top mt-2">
                        <div class="col-md-6 mt-2"><b>Trạng thái đơn hàng</b></div>
                        <div class="col-md-6">
                            <select class="select2 form-control" name="orders_status_id">
                                @foreach($orderStatus as $item)
                                    <option value="{{ $item->id}}" @if($item->id == $detailOrders->orders_status_id) selected @endif>{{ $item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif

            @endif

        @endif

    </div>
</div>

<script>
    $(document).ready(function(){
        searchCustomer();
        window.searchAutoProduct();

        var customers_id = '{{$detailOrders->customers_id}}';
        if(customers_id){
            getInfoCustomer(customers_id);
        }

        //Add new customer in modal cart
        $("#modal-cart .add-new-customer").click(function(){
            var modal = '#modal-add-customer-in-cart';

            $(this).submitDataAjax({
                'url' : site_url + 'customers/0/?classBtnSubmit=btn-submit-customer-in-cart',
                'method': 'GET',
                'success': function(res){
                    $('#modal-cart').modal('hide');
                    $(modal).find('.modal-content').html(res.html);
                    $(modal).modal();
                    $('.select2').select2();

                    $('.select2-disabled').select2({
                        disabled: true
                    });

                    //Add new customer in modal cart
                    $(".btn-submit-customer-in-cart").click(function(){
                        $(this).submitFormAjax({
                            'form' : $( $(this).data('form-target') ),
                            'method' : $(this).data('form-method'),
                            'success': function(res){

                                getInfoCustomer(res.data.id);
                                backToModalCart();

                                setTimeout(function() {
                                    $(".search-customer-component").next().find('.select2-selection__rendered').html(res.data.name);    

                                    var $newOption = $("<option></option>").val(res.data.id).text(res.data.name);
                                    $(".search-customer-component").append($newOption).trigger('change');
                                    $("select[name=customers_id]").val(res.data.id).trigger("change");
                                    
                                }, 1000);
                            },
                        });
                    });
                }
            });
        });

        $('#modal-add-customer-in-cart').on('hidden.bs.modal', function (e) {
            backToModalCart();
        });

        //Print
        $(document).on('click', '.modal-footer .ajax-print', function (e) {
            let _order = {};
            _order.total = numeral(document.getNumber( $('.total-price-cart').html() )).format('0,0')
            _order.paid = $('input[name="deposit"]').val();
            _order.remain = $('input[name="againPay"]').val();

            $('.name-total').html( _order.total );
            $('.name-paid').html( _order.paid );
            $('.name-remain').html( _order.remain );
            $('.comment-orders-print').html( $('textarea[name=comment]').val() );

            var getpanel = document.getElementById("print");
            var MainWindow = window.open('');
            MainWindow.document.write('<html><head><title></title>');
            MainWindow.document.write("<link rel=\"stylesheet\" href=\"{{asset('css/theme-blue.css')}}\" type=\"text/css\"/>");
            MainWindow.document.write('</head><body onload="window.print();window.close()">');
            MainWindow.document.write(getpanel.innerHTML);
            MainWindow.document.write('</body></html>');
            MainWindow.document.close();
            setTimeout(function () {
                MainWindow.print();
            }, 500)
            return false;

        });
    });

    function backToModalCart(){
        $('#modal-add-customer-in-cart').modal('hide');
        
        setTimeout(function() {
            $('#modal-cart').modal('show');
        }, 500);

        //Recall search customer
        searchCustomer();
    }

    function searchCustomer(){
        $(".search-customer-component").select2({
            ajax: {
                url: 'search-customers',// 'search-products',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        q: params.term,
                    }
                    return query;
                },
                processResults: function (res) {
                    return {
                        results: res.records
                    };
                }
            },
            templateResult: function($item){
                var $item = $(
                    '<div>'+$item.text+'</div>'+'<div>'+$item.code+'</div>'
                );
                return $item;
            }
        }).on("select2:select", function (e) {
            let id = parseInt(e.params.data.id);
            $('.tab-shipment-info').show();
            getInfoCustomer(id);
        });
    }

    function addItemProductToCart(id){
        var type = '{{$type}}';

        $(this).submitDataAjax({
            'url' : site_url + 'add-to-cart',
            'method': 'POST',
            'data' : {
                _token : CSRF_TOKEN,
                id : id,
                qty : 1,
                uniqueId : null,
                type: type
            },
            'success': function(res){
                $.notify({
                    title: '<div><strong>'+ res.title +'</strong></div>',
                    message: res.msg
                },{
                    type: res.type,
                    delay: 1000,
                    placement: {
                        from: "bottom"
                    },
                });

                if(res.type == 'success'){
                    if(type == 'orders'){
                        $('.wrap-hover-orders').html(res.htmlCart);
                    }else{
                        $('.wrap-hover-invoices').html(res.htmlCart);
                    }
        
                    $('.html-cart-modal').html(res.htmlCartModal);
                    $('.html-cart-print').html(res.htmlCartPrint);
        
                    $('.total-price-cart').html(res.totalPrice);
                    calculatePaymentPrice();

                    window.searchAutoProduct();
                }
            }
        });
    }

    function getInfoCustomer(id){
        $(this).submitDataAjax({
            'url' : site_url + 'get-info-customer',
            'method': 'POST',
            'data' : {
                '_token' : CSRF_TOKEN,
                'id' : id,
                'orders_id' : '{{$detailOrders->id}}'
            },
            'success': function(res){
                if(res.status == 500){
                    alert(res.msg);
                    return false;
                }

                $('#info-extra-customer').html(res.info_extra_customer);

                if(res.info_extra_customer){
                    $('.wrap-order-histories').html(res.history_order_customer);
                    $('.tab-order-histories').show();
                }else{
                    $('.tab-order-histories').hide();
                }

                assignInfoShipment(res.customer);

                $('.name-customer-name').html(res.customer.name);
                $('.name-customer-address').html(res.customer.address);
                $('.name-customer-phone').html(res.customer.phone);

                window.calculatePaymentPrice();
            }
        });
    }

    function assignInfoShipment(customer){
        var nameObj = $('#shipment-info input[name=name]');
        var phoneObj = $('#shipment-info input[name=phone]');
        var addressObj = $('#shipment-info input[name=address]');
        var provincesObj = $('#shipment-info select[name=provinces_id]');

        if( nameObj.val() == '' && addressObj.val() == '' && phoneObj.val() == '' ){
            nameObj.val(customer.name);
            addressObj.val(customer.address);
            phoneObj.val(customer.phone);
            provincesObj.val(customer.province_id).trigger('change');
        }
    }


</script>
