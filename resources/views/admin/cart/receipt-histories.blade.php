@php
    use App\Models\Receipts;
@endphp

@if (count($receipts) > 0)
    <div class="border-dashed-top mt-5 pt-3">
        <h5>Lịch sử giao dịch</h5>
        <table class="table-striped mt-10 mb-0 table">
            <tr>
                <th>Mã phiếu thu</th>
                <th>Thời gian</th>
                <th>Người tạo</th>
                <th>Người thanh toán</th>
                <th>Phương thức</th>
                <th>Tiền thu</th>
            </th>

            @foreach ($receipts as $item)
                <tr>
                    <td>{{$item->code}}</td>
                    <td>{{$item->created_at_display}}</td>
                    <td>{{$item->user['name']}}</td>
                    <td>{{$item->customer['name']}}</td>
                    <td>{{Receipts::PAYMENT_METHOD[$item->payment_method]}}</td>
                    <td>{{number_format($item->price)}}đ</td>
                </tr>
            @endforeach
        </table>
    </div>
@endif