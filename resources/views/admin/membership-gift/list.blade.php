<div class="row">
    <div class="col-md-6 title-main"><h4>{{ucfirst($title)}}</h4></div>
</div>

<table class="list-item table-hover table mt-10 mb-0">
    <thead>
        <tr>
            <th>Thành viên</th>
            <th>Giảm giá</th>
            <th>Sản phẩm</th>
            <th>Thời gian</th>
            <th>Trạng thái</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @forelse ($records as $item)
            <tr>
                <td>{{$item->membership['name']}}</td>
                <td >
                    {{number_format($item->value)}} {{$item->unit == 'vnd' ? 'VND' : '%'}}
                </td>
                <td>
                    {{ count($item->products)}} sản phẩm
                </td>
                <td>
                    @if($item->is_expires)
                        Không hết hạn.
                    @else
                        {{$item->start_date->format('d-m-Y')}} đến {{$item->end_date->format('d-m-Y')}}
                    @endif
                </td>
                <td>{{$item->activity_status == 1 ? 'Đang' : 'Ngừng'}} hoạt động</td>
                <td>
                    @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                        <button data-action="{{$mod}}s/{{$item->membership_id}}" data-target="#modal-edit-{{$mod}}" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                    @endif
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="100" class="text-center">Không có dữ liệu</td>
            </tr>
        @endforelse
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
