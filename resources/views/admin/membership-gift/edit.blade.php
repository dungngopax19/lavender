@php
    $record->unit = ($record->unit) ? $record->unit : 'vnd';
    $record->activity_status = ($record->activity_status) ? $record->activity_status : 1;
    $is_disabled_membership_id = ($record->id) ? true : false;
    $disabled_start_date = ($record->is_expires) ? 'disabled' : '';
    $disabled_end_date = ($record->is_expires) ? 'disabled' : '';

@endphp
<form action="{{url('admin/'.$mod.'s/update')}}" method="post" id="form-edit-branch">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <input type="hidden" name="id" value="{{$record->id}}">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">{{($record->id)?'Cập nhật':'Thêm mới'}} {{$title}}</h4>
    <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.select', ['disabled' => 'disabled', 'label'=>'Cấp bậc', 'name'=>'membership_id', 'value'=>$record->membership_id, 'data'=>$memberships, 'required'=> true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row form-group fg-value">
                    <div class="col-md-4 text-left">
                        <label>
                            Giảm giá
                            {{-- <span class="text-danger"> ※</span> --}}
                        </label>
                    </div>
                    <div class="col-md-8">
                        <div class="input-group mb-2 mr-sm-2">
                            <input type="text" class="form-control format-number" name="value" value="{{ number_format($record->value) }}" min="0">
                            <input type="text" class="form-control hide" name="unit" value="{{ $record->unit}}">
                            <button type="button" name="vnd" class="btn btn-sm {{ ( !$record->unit||$record->unit == 'vnd') ? 'btn-primary' : 'btn-light'}} ">VND</button>
                            <button type="button" name="phantram" class="btn btn-sm {{ ($record->unit == '%') ? 'btn-primary' : 'btn-light'}}">%</button>
                        </div>
                        <div class="error-msg"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.select', ['label'=>'Sản phẩm', 'name'=>'product_ids[]', 'value'=>$product_ids, 'data'=>$products_gift, 'multiple'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.datepicker', ['label'=>'Thời gian bắt đầu', 'name'=>'start_date', 'value'=>$record->is_expires ? '' : $record->start_date, 'required'=>true, 'disabled'=> $disabled_start_date])
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.datepicker', ['label'=>'Thời gian kết thúc', 'name'=>'end_date', 'value'=>$record->is_expires ? '' : $record->end_date, 'required'=>true, 'disabled'=> $disabled_end_date])
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 offset-md-4">
                @include('components.inputs.checkbox', ['label'=>'Không bao giờ hết hạn', 'name'=>'is_expires', 'value'=>$record->is_expires])
            </div>
        </div>

    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
        <button data-form-target="#form-edit-branch" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        // select value type
        $('button[name=vnd]').on("click", function(e){
            $('button[name=vnd]').removeClass('btn-light');
            $('button[name=vnd]').toggleClass('btn-primary');
            $('button[name=phantram]').removeClass('btn-primary');
            $('button[name=phantram]').addClass('btn-light');
            $('input[name=unit]').val('vnd');
        });

        $('button[name=phantram]').on("click", function(e){
            $('button[name=phantram]').removeClass('btn-light');
            $('button[name=phantram]').addClass('btn-primary');
            $('button[name=vnd]').removeClass('btn-primary');
            $('button[name=vnd]').addClass('btn-light');
            $('input[name=unit]').val('%')
            $('input[name=value]').val(10);
        });

        //
        $('input[name=start_date]').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            },
            autoUpdateInput: false,
        });

        $('input[name="start_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('input[name=end_date]').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            },
            autoUpdateInput: false,
        });

        $('input[name="end_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('input[name=is_expires]').on("click", function(e){
            if($(this).is(':checked')){
                $('input[name=start_date]').val('').attr('disabled', 'disabled');
                $('input[name=end_date]').val('').attr('disabled', 'disabled');
            }else{
                $('input[name=start_date]').removeAttr('disabled');
                $('input[name=end_date]').removeAttr('disabled');
            }
        });

    });
</script>
