<div class="row">
    <div class="col-md-6 title-main"><h4>{{ucfirst($title)}}</h4></div>
    @if($is_permission_create)
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-success ajax-modal"
            data-target="#modal-edit-{{$mod}}" data-action="{{$mod}}s/0">
            <i class="fa fa-plus"></i> {{$title}}
        </button>
    </div>
    @endif
</div>

<table class="list-item table-hover table mt-10 mb-0">
    <thead>
        <tr>
            <th>#</th>
            <th>Mã đối tác</th>
            <th>Tên đối tác</th>
            <th>Điện thoại</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @forelse ($records as $item)
            <tr>
                <td>{{$item->id_display}}</td>
                <td>{{$item->code}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->phone}}</td>
                <td>
                    @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                        <button data-action="partner-deliveries/{{$item->id}}" data-target="#modal-edit-{{$mod}}" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                    @endif

                    @if($item->hasAllPermission([$permission.'xoa'], $permission))
                        <button data-action="partner-deliveries/{{$item->id}}" type="button" class="delete-item btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                    @endif
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="100" class="text-center">Không có dữ liệu</td>
            </tr>
        @endforelse
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
