@php
    $record->role = ($record->role) ? $record->role : 1;
    $record->activity_status = ($record->activity_status) ? $record->activity_status : 1;
@endphp
<!-- Modal Header -->
<div class="modal-header">
    <h4 class="modal-title">{{($record->id)?'Cập nhật':'Thêm mới'}} {{$title}}</h4>
    <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<div class="modal-body">
    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" class="active show" href="#info">Thông tin</a></li>
    </ul>

    <div class="tab-content mt-3">
        <div id="info" class="tab-pane fade in active show">
            <form action="{{url('admin/partner-deliveries/update')}}" method="post" id="form-edit-{{$mod}}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <input type="hidden" name="id" value="{{$record->id}}">

                <div class="row">
                    <div class="col-md-6">
                        @include('components.inputs.select', ['label'=>'Loại đối tác', 'name'=>'role', 'value'=>$record->role, 'data' => [['id'=>1, 'name'=>'Cá nhân'], ['id'=>2, 'name'=>'Công ty']], 'required'=>true, 'labelAlign' => 'text-right'])
                    </div>
                    <div class="col-md-6">
                        @include('components.inputs.text', ['label'=>'Địa chỉ', 'name'=>'address', 'value'=>$record->address, 'labelAlign' => 'text-right'])
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        @include('components.inputs.text', ['label'=>'Mã đối tác', 'name'=>'code', 'value'=>$record->code, 'required'=>true, 'labelAlign' => 'text-right'])
                    </div>
                    <div class="col-md-6">
                        @include('components.inputs.select', ['label'=>'Khu vực', 'name'=>'province_id', 'value'=>$record->province_id, 'data' => $provinces, 'labelAlign' => 'text-right'])
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        @include('components.inputs.text', ['label'=>'Tên đối tác', 'name'=>'name', 'value'=>$record->name, 'required'=>true, 'labelAlign' => 'text-right'])
                    </div>
                    <div class="col-md-6">
                        @include('components.inputs.select', ['label'=>'Trạng thái', 'name'=>'activity_status', 'value'=>$record->activity_status, 'data'=>$activity_status, 'required'=>true, 'labelAlign' => 'text-right'])
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        @include('components.inputs.text', ['label'=>'Điện thoại', 'name'=>'phone', 'value'=>$record->phone, 'labelAlign' => 'text-right'])
                        @include('components.inputs.text', ['label'=>'Email', 'name'=>'email', 'value'=>$record->email, 'labelAlign' => 'text-right'])
                    </div>
                    <div class="col-md-6">
                        @include('components.inputs.textarea', ['label'=>'Ghi chú', 'name'=>'note', 'value'=>$record->note, 'labelAlign' => 'text-right'])
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer pb-0">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                    <button data-form-target="#form-edit-{{$mod}}" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
