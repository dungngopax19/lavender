<div class="panel-group" id="accordion">
    @include('admin.components.searchs.keyword')
    @include('admin.components.searchs.role-personal-company', ['label'=> 'Loại đối tác'])
    @include('admin.components.searchs.activity-status')
    @include('admin.components.searchs.items-per-page')
</div>
