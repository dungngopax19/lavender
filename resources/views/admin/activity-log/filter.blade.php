<div class="panel-group" id="accordion">
    @include('admin.components.searchs.keyword')
    @include('admin.components.searchs.user')
    @include('admin.components.searchs.activity-log.func')
    @include('admin.components.searchs.activity-log.action')
    @include('admin.components.searchs.created')
    @include('admin.components.searchs.items-per-page')
</div>
