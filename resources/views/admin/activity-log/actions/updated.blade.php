@php
    use App\Helpers\ActivityLog as ActivityLogHelper;
@endphp

<tr>
    <th>Tên trường dữ liệu</th>
    <th>Dữ liệu trước khi thao tác</th>
    <th>Dữ liệu sau khi thao tác</th>
</tr>

@foreach ($dataOld as $key => $txt)
    <tr>
        <td>{{ isset($fields[$key]) ? $fields[$key] : $key }}</td>
        <td>{{ActivityLogHelper::showText($key, $record, $txt)}}</td>
        <td>{{ActivityLogHelper::showText($key, $record, $dataNew[$key] )}}</td>
    </tr>    
@endforeach