@php
    use App\Helpers\ActivityLog as ActivityLogHelper;
@endphp

<tr>
    <th>Tên trường dữ liệu</th>
    <th>Dữ liệu đã thêm mới</th>
</tr>

@foreach ($dataNew as $key => $txt)
    <tr>
        <td>{{ isset($fields[$key]) ? $fields[$key] : $key }}</td>
        <td>{{ActivityLogHelper::showText($key, $record, $txt)}}</td>
    </tr>    
@endforeach