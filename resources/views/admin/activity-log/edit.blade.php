<!-- Modal Header -->
<div class="modal-header">
    <h6 class="modal-title">
        {{$record->modelCauser->name}} Đã {{$actions[$record->description]}} {{$nameModels[$record->subject_type]}} Vào Lúc {{$record->created_at}}
    </h6>

    <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">

                @include('admin.activity-log.actions.' . $record->description)
                
            </table>
        </div>
    </div>
</div>