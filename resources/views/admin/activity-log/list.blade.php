<div class="row">
    <div class="col-md-6 title-main"><h4>{{ucfirst($title)}}</h4></div>
</div>

<table class="table mt-10 mb-0">
    <thead>
        <tr>
            <th>Nhân viên</th>
            <th>Thao tác</th>
            <th>Chức năng</th>
            <th>Thời gian</th>
            <th></th>
        </tr>
    </thead>
    <tbody>

        @if (count($records) > 0)
            @foreach ($records as $item)
                <tr>
                    <td>{{$item->modelCauser->name}}</td>
                    <td>Đã {{$actions[$item->description] ?? $item->description}}</td>
                    <td>{{$nameModels[$item->subject_type] ?? $item->subject_type}}</td>
                    <td>{{$item->created_at}}</td>
                    <td>
                        <button data-action="{{$mod}}/{{$item->id}}" data-target="#modal-edit-{{$mod}}" type="button" class="btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="100" class="text-center">Không có dữ liệu</td>
            </tr>
        @endif
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
