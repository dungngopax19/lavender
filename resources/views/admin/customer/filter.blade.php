<div class="panel-group" id="accordion">
    @include('admin.components.searchs.customer.id-code-name')
    @include('admin.components.searchs.role-personal-company', ['label'=> 'Loại khách hàng'])
    @include('admin.components.searchs.gender')
    @include('admin.components.searchs.created')
    @include('admin.components.searchs.activity-status')
    @include('admin.components.searchs.items-per-page')
</div>
