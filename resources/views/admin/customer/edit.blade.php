@php
    $record->thumbnail = $record->image;
    $record->role = ($record->role) ? $record->role : 1;
    $record->activity_status = ($record->activity_status) ? $record->activity_status : 1;
@endphp

<!-- Modal Header -->
<div class="modal-header">
    <h4 class="modal-title">{{($record->id)?'Cập nhật':'Thêm mới'}} {{$title}}</h4>
    <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<div class="modal-body">

    @if ($record->id > 0)
        <ul class="nav nav-tabs">
            <li><a data-toggle="tab" class="active show" href="#info">Thông tin</a></li>
            <li><a data-toggle="tab" href="#history">Lịch sử giao dịch</a></li>
        </ul>
    @endif

    <div class="tab-content mt-3">
        <div id="info" class="tab-pane fade in active show">
            <form action="{{url('admin/'.$mod.'s/update')}}" method="post" id="form-edit-{{$mod}}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <input type="hidden" name="id" value="{{$record->id}}">

                <div class="row">
                    <div class="col-md-2 text-center">

                        <div id="img-preview" class="w-100 mb-3 mt-2">
                            <input type="hidden" name="thumbnail" value="{{$record->thumbnail}}">
                            <span class="close-img-preview {{empty($record->thumbnail) ? 'hide' : ''}}">×</span>
                            <a data-fancybox="gallery" href="{{asset($record->thumbnailDisplay)}}">
                                <img src="{{asset($record->thumbnailDisplay)}}" alt="noimage">
                            </a>
                        </div>

                        <input class="btn btn-primary form-group dropzone-file" type="button" value="Upload file">
                    </div>

                    <div class="col-md-10">
                        @if ($record->code)
                            <div class="row">
                                <div class="col-md-6">
                                    @include('components.inputs.text', ['readonly'=>'readonly', 'label'=>'Code', 'required'=>true, 'name'=>'', 'value'=>$record->code, 'labelAlign' => 'text-right'])
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-6">
                                @include('components.inputs.select', ['label'=>'Loại khách hàng', 'name'=>'role', 'value'=>$record->role, 'data' => [['id'=>1, 'name'=>'Cá nhân'], ['id'=>2, 'name'=>'Công ty']], 'required'=>true, 'labelAlign' => 'text-right'])
                            </div>
                            <div class="col-md-6 js-show-hide-company @if($record->role != '2') hide @endif">
                                @include('components.inputs.text', ['label'=>'Tên công ty', 'required'=>true, 'name'=>'company', 'value'=>$record->company, 'labelAlign' => 'text-right'])
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                @include('components.inputs.text', ['label'=>'Tên khách hàng', 'name'=>'name', 'value'=>$record->name, 'required'=>true, 'labelAlign' => 'text-right'])
                            </div>
                            <div class="col-md-6">
                                @include('components.inputs.text', ['label'=>'Mã số thuế', 'name'=>'tax_code', 'value'=>$record->tax_code, 'labelAlign' => 'text-right'])
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                @include('components.inputs.select', ['classObj' => 'select2-disabled', 'label'=>'Cấp bậc', 'name'=>'membership_id', 'value'=>$record->membership_id, 'data' => $memberships, 'labelAlign' => 'text-right'])
                            </div>
                            <div class="col-md-6">
                                @include('components.inputs.select', ['label'=>'Trạng thái', 'required'=>true, 'name'=>'activity_status', 'value'=>$record->activity_status, 'data'=>$activity_status, 'labelAlign' => 'text-right'])
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                @include('components.inputs.text', ['label'=>'Điện thoại', 'maxlength' => 12, 'required'=>true, 'name'=>'phone', 'value'=>$record->phone, 'labelAlign' => 'text-right'])
                            </div>
                            <div class="col-md-6">
                                @include('components.inputs.text', ['label'=>'Email', 'name'=>'email', 'value'=>$record->email, 'labelAlign' => 'text-right'])
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                @include('components.inputs.datepicker', ['label'=>'Ngày sinh', 'name'=>'birthday', 'value'=>$record->birthday_display, 'labelAlign' => 'text-right'])
                            </div>
                            <div class="col-md-6">
                                @include('components.inputs.select', ['label'=>'Giới tính', 'name'=>'gender', 'value'=>$record->gender ?? 1, 'data' => [['id'=>1, 'name'=>'Nam'], ['id'=>2, 'name'=>'Nữ']], 'labelAlign' => 'text-right'])
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                @include('components.inputs.select', ['label'=>'Khu vực', 'required'=>true, 'name'=>'province_id', 'value'=>$record->province_id, 'data' => $provinces, 'labelAlign' => 'text-right'])
                                @include('components.inputs.text', ['label'=>'Địa chỉ', 'name'=>'address', 'value'=>$record->address, 'labelAlign' => 'text-right', 'required'=>true])
                            </div>
                            <div class="col-md-6">
                                @include('components.inputs.textarea', ['label'=>'Ghi chú', 'name'=>'text', 'value'=>$record->text, 'labelAlign' => 'text-right'])
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Modal footer -->
                <div class="modal-footer pb-0 pr-0">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                <button data-form-target="#form-edit-{{$mod}}" type="button" class="btn btn-primary {{$classBtnSubmit ?? 'btn-submit'}}" auto-close-modal="true" data-form-method ="PUT">OK</button>
                </div>
            </form>
        </div>

        <div id="history" class="tab-pane fade">
            {{ Widget::run('Admin.Customer.History', ['customer_id'=>$record->id]) }}
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $('input[name=birthday]').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            },
            autoUpdateInput: false,
        });

        $('input[name="birthday"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        if($(".dropzone-file").length > 0){
            $(".dropzone-file").dropzone({
                url: "/admin/upload-file",
                sending: function(file, xhr, formData) {
                    formData.append("_token", CSRF_TOKEN);
                },
                success: function(file, res){
                    if(res.status == 200){
                        $('#img-preview img').attr('src', res.url);
                        $('input[name=thumbnail]').val(res.pathTmpFile);
                        $(".close-img-preview").show();
                    }else{
                        $('#img-preview img').attr('src', urlNoImage);
                        $('input[name=thumbnail]').val('');
                        $(".close-img-preview").hide();
                    }
                }
            });
        }

        // select role
        $('select[name=role]').on("change", function(e){
            if($(this).val() === '2'){
                $('.js-show-hide-company').removeClass('hide');
            }else{
                $('.js-show-hide-company').addClass('hide');
            }
        });


    });
</script>
