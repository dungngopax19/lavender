
<!-- Modal Header -->
<div class="modal-header">
    <h4 class="modal-title">Nhập khách hàng từ file dữ liệu</h4>
    <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<div class="modal-body mt-2">
    <div class="warning-content p-2">
        <div class="mb-3"><strong><i class="fas fa-exclamation-triangle"></i> Lưu ý</strong></div>
        <div class="mb-2">Tải về File mẫu: <a href="{{asset('file/customer-import-example.xlsx')}}" target="_blank">Excel File</a></div>
        <div class="mb-2">Hệ thống cho phép nhập tối đa 5.000 khách hàng mỗi lần từ file .</div>
        <div class="mb-2">Dữ liệu chứa kí tự đặc biệt (@, #, $, *, /, -, _,...) và chữ có dấu sẽ gây khó khăn khi In và sử dụng mã vạch.</div>
    </div>
</div>

<!-- Modal footer -->
<div class="modal-footer">
    <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
    <button type="button" class="btn btn-primary dropzone-file-import">Chọn file dữ liệu và import</button>
</div>

<script>
    $(document).ready(function(){
        var loading = false;

        if($(".dropzone-file-import").length > 0){
            $(".dropzone-file-import").dropzone({
                maxFiles: 1,
                timeout: 300000, /*milliseconds*/
                url: "/admin/customer-submit-import",
                acceptedFiles: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                sending: function(file, xhr, formData) {
                    formData.append("_token", CSRF_TOKEN);
                },
                accept: function(file, done) {
                    if(!loading){
                        done();
                    }
                },
                processing: function(){
                    loading = true;
                    $(".dropzone-file-import").css('cursor', 'progress');
                    $('#preloader').show();
                },
                complete: function(){
                    $('#preloader').hide();
                },
                success: function(file, res){
                    console.log(res);
                    location.reload();
                }
            });
        }
    });
</script>
