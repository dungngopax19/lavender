<div class="row">
    <div class="col-md-6 title-main"><h4>{{ucfirst($title)}}</h4></div>
    <div class="col-md-6 text-right">
        @if($is_permission_create)
            <button type="button" class="btn btn-success ajax-modal mr-2"
                data-target="#modal-edit-{{$mod}}" data-action="{{$mod}}s/0">
                <i class="fa fa-plus"></i> {{$title}}
            </button>
        @endif

        @if($is_permission_import)
            <button type="button" class="btn btn-success ajax-modal mr-2"
                data-target="#modal-import-customer" data-action="customer-import">
                <i class="fa fa-plus"></i> Import
            </button>

            <a href="{{url( 'admin/customer-export/?' . http_build_query(request()->query()))}}" target="_blank">
                <button type="button" class="btn btn-success"<i class="fas fa-file-export"></i> Export</button>
            </a>
        @endif

    </div>
</div>

<table class="list-item table-hover table mt-10 mb-0">
    <thead>
        <tr>
            <th class="text-center">Mã KH</th>
            <th>Tên {{lcfirst($title)}}</th>
            <th class="text-center">Cấp bậc</th>
            <th class="text-center">Tổng tiền</th>
            <th class="text-center">Số lần mua hàng</th>
            <th>Điện thoại</th>
            <th>Email</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @forelse ($records as $item)
            <tr>
                <td class="text-center">{{$item->code}}</td>
                <td>{{$item->name}}</td>
                <td class="text-center">{{$item->membership_id}}</td>
                <td class="text-center">{{number_format($item->total_price)}}đ</td>
                <td class="text-center">{{$item->buys}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->email}}</td>
                <td>
                    @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                        <button data-action="{{$mod}}s/{{$item->id}}" data-target="#modal-edit-{{$mod}}" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                    @endif

                    @if($item->hasAllPermission([$permission.'xoa'], $permission))
                        <button data-action="{{$mod}}s/{{$item->id}}" type="button" class="delete-item btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                    @endif
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="100" class="text-center">Không có dữ liệu</td>
            </tr>
        @endforelse
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
