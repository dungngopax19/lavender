<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Document</title>
</head>
<body>
    <table>
        <tr>
            <th></th>
            <th>Mã khách hàng</th>
            <th>Tên khách hàng</th>
            <th class="text-center">Cấp bậc</th>
            <th class="text-center">Tổng tiền</th>
            <th class="text-center">Số lần mua hàng</th>
            <th>Điện thoại</th>
            <th>Địa chỉ</th>
            <th>Ngày sinh</th>
            <th>Email</th>
        </tr>

        @foreach ($records as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->code}}</td>
                <td>{{$item->name}}</td>
                <td class="text-center">{{$item->membership_id}}</td>
                <td class="text-center">{{number_format($item->total_price)}}đ</td>
                <td class="text-center">{{$item->buys}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->address}}</td>
                <td>{{$item->birthday}}</td>
                <td>{{$item->email}}</td>
            </tr>
        @endforeach
    </table>
</body>
</html>
