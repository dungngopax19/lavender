@php
    use App\Helpers\Common;
@endphp

@if ($records)
    @php
        $totalPriceSell = 0;
        $totalPrice = 0;
        $inventory = 9999999;
    @endphp

    <table class="table mt-10 mb-0">
        <thead style="background-color: #DCF5FC;">
            <tr>
                <th>#</th>
                <th>Hình ảnh</th>
                <th style="width:15%">Mã sản phẩm</th>
                <th>Tên sản phẩm</th>
                <th class="text-center">Tồn kho</th>
                <th class="text-center">Số lượng</th>
                <th class="text-center">Giá bán</th>
                <th class="text-center" style="width:15%">Thành tiền</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($records as $i => $record)
                @php
                    $item = $record['detail'];
                    $totalPriceSell += $item->price_sell * $record['qty'];
                    $totalPrice += $item->price * $record['qty'];

                    $inventory = min($inventory, intval(floor($item->inventory_number / $record['qty'])));
                @endphp

                <tr>
                    <td class="td-trash">
                        <input type="hidden" name="component_ids[]" value="{{$item->id}}">
                        <i data-id="{{$item->id}}" class="fas fa-trash-alt"></i>  
                    </td>

                    <td>
                        <a data-fancybox="gallery" href="{{asset($item->thumbnailDisplay)}}">
                            <img src="{{Common::resizeImage($item->thumbnail, 100)}}" alt="">
                        </a>
                    </td>

                    <td><strong>{{$item->code}}</strong></td>

                    <td>{{$item->name}}</td>

                    <td class="text-center">{{number_format($item->inventory_number)}}</td>

                    <td class="text-center">
                        <input type="hidden" name="component_amount[]" value="{{$record['qty']}}">
                        <input class="amount-of" data-id="{{$item->id}}" type="number" max="{{$item->inventory_number}}" value="{{$record['qty']}}" style="width:60px">
                    </td>

                    <td class="price-unit text-center" data-price="{{$item->price}}">{{number_format($item->price_sell)}}</td>

                    <td class="price-amount text-center">
                        {{number_format( $record['qty'] * $item->price_sell )}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row mb-2">
        <div class="col-md-10 text-right"><strong>Tổng giá vốn thành phần:</strong></div>
        <div class="col-md-2">{{number_format($totalPrice)}}</div>
    </div>

    <div class="row">
        <div class="col-md-10 text-right"><strong>Tổng giá bán thành phần:</strong></div>
        <div class="col-md-2">{{number_format($totalPriceSell)}}</div>
    </div>

    <input type="hidden" name="inventory_list_comps" value="{{number_format($inventory)}}">
@endif