<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Document</title>
</head>
<body>
    @php
        use App\Models\ProductsComponents;
        use App\Models\BranchesProducts;
        use App\Models\Attributes;

    @endphp

    <table>
        <tr>
            <th>ID</th>
            <th>Loại hàng</th>
            <th>Nhóm hàng(2 Cấp)</th>
            <th>Mã hàng</th>
            <th>Mã hàng thành phần</th>
            <th>Tên hàng hóa</th>
            <th>Giá bán</th>
            <th>Giá vốn</th>

            @foreach ($branches as $branch)
                <th>{{$branch->name}}</th>
            @endforeach

            <th>Khổ vải</th>
            <th>Số lượng mét vải</th>
            <th>SL ước tính(bộ)</th>
            <th>Tồn kho</th>

            @foreach ($attributes as $attribute)
                <th>{{$attribute->name}}</th>
            @endforeach

            <th>Trạng thái</th>
            <th>Được bán trực tiếp</th>
            <th>Hình ảnh</th>
            <th>Mô tả</th>
            <th>Mẫu ghi chú</th>
        </tr>

        @foreach ($records as $item)

            <tr>
                <td>{{$item->id_display}}</td>
                <td>{{$item->productTypes->name}}</td>
                <td>
                    @php
                        $cat = $item->product_categories;
                        if(!empty($cat)){
                            $parentCat = $cat->parent;
                            if($parentCat){
                                echo $parentCat->name . ' >> ';
                            }
                            echo $cat->name;
                        }
                    @endphp
                </td>
                <td>{{$item->code}}</td>

                <td>
                    @php
                        $arr = [];
                        $components = ProductsComponents::where('products_id_parent', $item->id)->where('qty', '>', 0)->get();
                        if($components){
                            foreach($components as $comp){
                                $nameComp = $comp->products->code;
                                $txt = ($comp->qty > 1) ? $nameComp . '('.$comp->qty . ')' : $nameComp;
                                $arr[] = $txt;
                            }
                        }
                        echo implode('|', $arr);
                    @endphp
                </td>

                <td>{{$item->name}}</td>
                <td>{{number_format($item->price_sell)}}</td>
                <td>{{number_format($item->price)}}</td>

                @foreach ($branches as $branch)
                    @php
                        $branchesProducts = $branch->branchesProducts()->where('products_id', $item->id)->first();
                    @endphp
                    <td>{{$branchesProducts ? $branchesProducts->inventory : 0}}</td>
                @endforeach

                <td>{{$item->cloth_template}}</td>
                <td>{{number_format($item->meter_number)}}</td>
                <td>{{number_format($item->number_of_sets)}}</td>
                <td>{{number_format($item->inventory_number)}}</td>

                @foreach ($attributes as $attribute)
                <td>
                    @php
                        $names = Attributes::where('parent_id', $attribute->id)
                            ->whereHas('attributes_products', function($q) use ($records, $item){
                                $q->where('products_id', $item->id);
                            })->pluck('name')->toArray();

                        if(count($names) > 0){
                            echo implode('<br/>', $names);
                        }
                    @endphp
                </td>
                @endforeach

                <td>{{($item->activity_status == 1) ? 'Đang hoạt động' : 'Ngừng hoạt động'}}</td>
                <td>{{($item->direct_selling == 1) ? 'Được bán trực tiếp' : 'Không bán trực tiếp'}}</td>
                <td>
                    @if ( !empty($item->thumbnail) )
                        <a href="{{url($item->thumbnail_display)}}" target="_blank">Link Thumbnail</a>
                    @endif
                </td>
                <td>{{$item->description}}</td>
                <td>{{$item->sample_notes}}</td>
            </tr>
        @endforeach
    </table>
</body>
</html>
