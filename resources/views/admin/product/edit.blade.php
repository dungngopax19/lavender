<form action="{{url('admin/'.$mod.'s/update')}}" method="post" id="form-edit-{{$mod}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <input type="hidden" name="id" value="{{$record->id}}">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">{{($record->id)?'Cập nhật':'Thêm mới'}} {{$title}}</h4>
        <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        
        <ul class="nav nav-tabs">
            <li><a data-toggle="tab" class="active show" href="#info">Thông tin</a></li>
            <li class="tab-inventory"><a data-toggle="tab" href="#inventory">Tồn kho 
                @if ($record->id > 0)
                    ( <span id="inventoryCombo" style="color:red">{{number_format($record->inventory_number)}}</span> ) sp
                @endif
            </a></li>
            <li><a data-toggle="tab" href="#attribute">Thuộc tính</a></li>
            <li class="tab-component {{($record->product_types_id == '1') ? '' : 'hide'}}"><a data-toggle="tab" href="#component">Thành phần (Combo Đóng gói)</a></li>
            <li><a data-toggle="tab" href="#detail">Mô tả chi tiết</a></li>
        </ul>
                    
        <div class="tab-content mt-3">
            {{-- Thông tin --}}
            <div id="info" class="tab-pane fade in active show"> 
                <div class="row">
                    <div class="col-md-8">
                        @include('components.inputs.text', ['label'=>'Mã sản phẩm', 'name'=>'code', 'value'=>$record->code, 'required'=>true, 'colLeft'=>3])
                        @include('components.inputs.text', ['label'=>'Tên sản phẩm', 'name'=>'name', 'value'=>$record->name, 'required'=>true, 'colLeft'=>3])
                        @include('components.inputs.select', ['label'=>'Loại sản phẩm', 'name'=>'product_types_id', 'value'=>$record->product_types_id, 'required'=>true, 'data' => $productTypes, 'colLeft'=>3])
                        @include('components.inputs.select', ['label'=>'Nhóm sản phẩm', 'name'=>'product_categories_id', 'value'=>$record->product_categories_id, 'required'=>true,'data' => $productCategories, 'colLeft'=>3])
                        @include('components.inputs.text', ['label'=>'Giá bán (đ)', 'classObj'=>'format-number', 'name'=>'price_sell', 'value'=>number_format($record->price_sell), 'colLeft'=>3])
                        @include('components.inputs.text', ['label'=>'Giá vốn (đ)', 'classObj'=>'format-number', 'name'=>'price', 'value'=>number_format($record->price), 'colLeft'=>3])
                        @include('components.inputs.select', ['label'=>'Sản phẩm quà tặng', 'name'=>'is_gift', 'value'=>$record->is_gift, 'data' => ['Không', 'Có'], 'colLeft'=>3])
                        @if ($record->id > 0)
                            @include('components.inputs.text', ['label'=>'Ngày tạo', 'name'=>'created_at', 'value'=>$record->created_at, 'readonly' => 'readonly' , 'colLeft'=>3])
                            @include('components.inputs.text', ['label'=>'Ngày cập nhật', 'name'=>'updated_at', 'value'=>$record->updated_at, 'readonly' => 'readonly' , 'colLeft'=>3])
                            @include('components.inputs.text', ['label'=>'User tạo', 'name'=>'create_user', 'value'=>$record->create_user, 'readonly' => 'readonly' , 'colLeft'=>3])
                        @endif
                    </div>
                    <div class="col-md-4">
                        <input class="btn btn-primary form-group dropzone-file" type="button" value="Upload file">
                        <input class="btn btn-success form-group btn_link_thumbnail float-right" type="button" value="Upload theo link">

                    <div class="form-group wrap_link_thumbnail {{$record->thumbnail ? '' : 'hide'}}">
                            <input type="text" value="{{$record->thumbnail}}" name="link_thumbnail" id="link_thumbnail" class="form-control w-100" placeholder="Nhập link thumbnail cần upload">
                        </div>

                        <div id="img-preview" class="w-100">
                            <input type="hidden" name="thumbnail" value="{{$record->thumbnail}}">
                            <span class="close-img-preview {{empty($record->thumbnail) ? 'hide' : ''}}">×</span>
                            <a data-fancybox="gallery" href="{{asset($record->thumbnailDisplay)}}">
                                <img src="{{$record->resizeImage($record->thumbnail, 100)}}" alt="">
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>

            {{-- Tồn kho --}}
            <div id="inventory" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-12">
                        <div id="accordion">
                            <div class="card mt-3 stock-product {{($record->product_types_id == '1') ? 'hide' : ''}}">
                                <div class="card-header"><a class="card-link" data-toggle="collapse" href="#inventory0">Nhà xưởng</a></div>
                                <div id="inventory0" class="collapse show" data-parent="#accordion">
                                    <div class="card-body pb-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                @include('components.inputs.select', ['label'=>'Khổ vải', 'name'=>'cloth_template', 'value'=>$record->cloth_template, 'data' => array('1.6' => '1.6 m (14m/bộ)', '2.5' => '2.5 m (11m/bộ)'), 'colLeft'=>6])
                                            </div>
                                            <div class="col-md-6">
                                                @include('components.inputs.text', ['type'=>'number', 'label'=>'Số lượng mét vải', 'name'=>'meter_number', 'value'=>$record->meter_number, 'colLeft'=>6])
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                @include('components.inputs.text', ['label'=>'SL ước tính(bộ)', 'name'=>'number_of_sets', 'value'=>$record->number_of_sets, 'colLeft'=>6, 'readonly'=>'readonly'])
                                            </div>

                                            @if ($record->id)
                                                <div class="col-md-6">
                                                    @include('components.inputs.text', ['type'=>'number', 'label'=>'Số lượng mét vải (Đã đặt hàng)', 'name'=>'', 'value'=>$record->meter_number_register, 'readonly'=>'readonly', 'colLeft'=>6])
                                                </div>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            </div>

                            {{-- Kho hàng(Chi nhánh) --}}
                            @foreach ($branches as $item)
                                @php
                                    $inventory = 0;
                                    $minInventory = $item->min_inventory;

                                    if( !$record->id && $branchActive->id == $item->id ){
                                        $inBranch = 1;
                                        $readonlyInventory = '';
                                    }else{
                                        $inBranch = '';
                                        $readonlyInventory = 'readonly';
                                    }

                                    if($record->id){
                                        $branchesProducts = $item->branchesProducts()->where('products_id', $record->id)->first();
                                        if($branchesProducts){
                                            $inBranch = $branchesProducts->in_branch;
                                            $inventory = $branchesProducts->inventory;
                                            $minInventory = $branchesProducts->min_inventory;
                                            $readonlyInventory = ($inBranch > 0) ? '' : 'readonly';
                                        }
                                    }
                                @endphp
                                <div class="card mt-3">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#inventory{{$item->id}}">
                                            {{$item->name}} 
                                            @if ($record->id && $inBranch && ( $inventory <= $minInventory ) )
                                                <span class="out-of-stock">( {{ ($inventory > 0) ? 'Sắp hết hàng' : 'Đã hết hàng' }} )</span>
                                            @endif
                                        </a>
                                    </div>

                                    <div id="inventory{{$item->id}}" data-parent="#accordion">
                                        <div class="card-body pb-1" style="background-color:{{$branchActive->id == $item->id ? 'antiquewhite' : 'white'}}">
                                            <div class="row">
                                                <input type="hidden" name="branches_id[]" value="{{$item->id}}">
                                                <div class="col-md-4">
                                                    @include('components.inputs.select', ['label'=>'Thuộc chi nhánh này', 'name'=>'in_branch[]', 'value'=>$inBranch, 'classObj' => 'in-branch-stock', 'data'=>['0'=>'Không', '1' => 'Có'], 'required'=>true, 'colLeft'=>7])
                                                </div>
                                                <div class="col-md-4">
                                                    @include('components.inputs.text', ['readonly' => $readonlyInventory, 'type'=>'number', 'label'=>'Tồn kho', 'name'=>'inventory[]', 'classObj' => 'inventory-stock', 'value'=>$inventory, 'required'=>true, 'colLeft'=>5])
                                                </div>
                                                <div class="col-md-4">
                                                    @include('components.inputs.text', ['readonly' => $readonlyInventory, 'label'=>'Tồn kho tối thiểu', 'name'=>'min_inventory[]', 'classObj' => 'min-inventory-stock', 'value'=>$minInventory, 'required'=>true, 'colLeft'=>7])
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            {{-- Thuộc tính --}}
            <div id="attribute" class="tab-pane fade">
                @if ($attributes)
                    @foreach ($attributes as $item)
                        <div class="row form-group">
                            <div class="col-md-2 text-right">
                                <label>{{$item->name}}</label>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <input type="hidden" name="attrs_id[]" value="{{$item->id}}">

                                    <div class="col-md-7">
                                        <select class="select2 form-control" name="attrs_child_id[]" multiple>
                                            <option value="0">---</option>
                                            @foreach ($item->childrens as $i => $itemChild)
                                                <option {{in_array($itemChild->id, $attributesSelected)?'selected':''}} value="{{$itemChild->id}}">{{$itemChild->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-5">
                                        <input class="tagsinput" type="text" name="attrs_more[]" placeholder="Thêm mới thuộc tính..." data-role="tagsinput"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            {{-- Thành phần --}}
            <div id="component" class="tab-pane fade">
                {{ Widget::run('Admin.Product.Components', ['products_id' => $record->id]) }}
            </div>

            {{-- Thông tin chi tiết --}}
            <div id="detail" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-12">
                        @include('components.inputs.textarea', ['label'=>'Mô tả', 'name'=>'description', 'value'=>$record->description, 'colLeft' => 12, 'colRight' => 12])
                        @include('components.inputs.textarea', ['label'=>'Mẫu ghi chú (Đơn hàng, đặt hàng)', 'name'=>'sample_notes', 'value'=>$record->sample_notes, 'colLeft' => 12, 'colRight' => 12])
                    </div>
                </div>
            </div>
        </div>
        
    </div>
        
    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
        <button data-form-target="#form-edit-{{$mod}}" type="button" class="btn btn-primary btn-submit-edit-product" auto-close-modal="true" data-form-method ="PUT">OK</button>
    </div>

</form>

<script>
    $(document).ready(function(){
        $('.tagsinput').tagsinput({
            tagClass: "badge badge-info"
        });

        if($(".dropzone-file").length > 0){
            $(".dropzone-file").dropzone({
                url: "/admin/upload-file",
                sending: function(file, xhr, formData) {
                    formData.append("_token", CSRF_TOKEN);
                },
                success: function(file, res){
                    $('#link_thumbnail').val('');
                    
                    if(res.status == 200){
                        $('#img-preview img').attr('src', res.url);
                        $('input[name=thumbnail]').val(res.pathTmpFile);
                        $(".close-img-preview").show();
                    }else{
                        $('#img-preview img').attr('src', urlNoImage);
                        $('input[name=thumbnail]').val('');
                        $(".close-img-preview").hide();
                    }
                }
            });
        }
        
    });
</script>

@if ($type_action == 'edit-store')
    <script>
        $(document).ready(function(){
            $('.nav-tabs li').hide();
            $('.tab-inventory').show();
            $('.tab-inventory a').tab('show');
        });
    </script>
@endif