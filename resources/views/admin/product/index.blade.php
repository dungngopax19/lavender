@extends('layouts.admin')

@section('title', ucfirst($title))

@section('content')
    <div class="row">
        <div class="col-md-2 main-left">
            <form action="" method="get" class="auto-submit">
                @include('admin.'.$mod.'.filter')
            </form>
        </div>

        <div class="col-md-10 main-right">
            @include('admin.'.$mod.'.list')
        </div>
    </div>

    
@endsection

@push('modals')
<div class="modal fade" id="modal-import-product">
    <div class="modal-dialog max-width-70 modal-dialog-centered">
        <div class="modal-content"></div>
    </div>
</div>
@endpush

@push('scripts')
<script>
    $(document).ready(function(){

        $(window).scroll(function(){

            if( $(this).scrollTop() > 500){
                if($('.box-dropdown-orders').find('.item-cart').length > 0){
                    $('.box-dropdown-orders').addClass('position-fixed-orders');
                }

                if($('.box-dropdown-invoices').find('.item-cart').length > 0){
                    $('.box-dropdown-invoices').addClass('position-fixed-invoices');
                }
            }else{
                $('.box-dropdown-orders').removeClass('position-fixed-orders');
                $('.box-dropdown-invoices').removeClass('position-fixed-invoices');
            }
        });
    });
</script>
@endpush