
<!-- Modal Header -->
<div class="modal-header">
    <h4 class="modal-title">Nhập sản phẩm từ file dữ liệu</h4>
    <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<div class="modal-body mt-2">
    <div class="row-item">Xử lý dữ liệu (Tải về File mẫu: <a href="{{asset('file/product-import-example.xlsx')}}" target="_blank">Excel File</a>):</div>
    
    <div class="row-item pb-3">
        <div><strong>Xử lý trùng mã sản phẩm, khác tên sản phẩm?</strong></div>
        <div class="ml-3">
            <div class="pretty p-default p-round d-flex mt-3">
                <input type="radio" value="skip" id="like_code_skip" checked name="like_code">
                <div class="state">
                    <label for="like_code_skip">Bỏ qua sản phẩm đó</label>
                </div>
            </div>

            <div class="pretty p-default p-round d-flex mt-3">
                <input type="radio" value="update" id="like_code_update" name="like_code">
                <div class="state">
                    <label for="like_code_update">Cập nhật lại sản phẩm đó</label>
                </div>
            </div>
        </div>
    </div>

    <div class="warning-content p-3"> 
        <div class="mb-2"><strong><i class="fas fa-exclamation-triangle"></i> Lưu ý</strong></div>
        <div>Hệ thống cho phép nhập tối đa 5.000 mặt hàng mỗi lần từ file .</div> 
        <div>Mã hàng chứa kí tự đặc biệt (@, #, $, *, /, -, _,...) và chữ có dấu sẽ gây khó khăn khi In và sử dụng mã vạch.</div>
    </div>
</div>
    
<!-- Modal footer -->
<div class="modal-footer">
    <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
    <button type="button" class="btn btn-primary dropzone-file-import">Chọn file dữ liệu và import</button>
</div>

<script>
    $(document).ready(function(){
        var loading = false;
        var urlImportProduct = 'admin/product-submit-import';

        if($(".dropzone-file-import").length > 0){
            $(".dropzone-file-import").dropzone({
                maxFiles: 1,
                timeout: 300000, /*milliseconds*/
                url: 'product-submit-import',
                acceptedFiles: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                sending: function(file, xhr, formData) {
                    formData.append("_token", CSRF_TOKEN);
                    formData.append("like_code", $('input[name=like_code]:checked').val());
                },
                accept: function(file, done) {
                    if(!loading){
                        done();
                    }
                },
                processing: function(){
                    loading = true;
                    $(".dropzone-file-import").css('cursor', 'progress');
                    $('#preloader').show();
                },
                success: function(file, res){
                    console.log(res);
                    continueSubmit(res);
                }
            });
        }
    });

    function continueSubmit(res){
        if(res.stop){
            $('#preloader').hide();
            location.reload();
        }else{
            $.ajax({
                url: 'product-submit-import',
                type: 'POST',
                data: {
                    _token : CSRF_TOKEN,
                    like_code : $('input[name=like_code]:checked').val(),
                    page : res.page,
                    path : res.path
                },
                dataType: 'JSON',
                success: function (res) {
                    continueSubmit(res);
                }
            });
        }
    }
</script>