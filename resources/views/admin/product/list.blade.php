@php
    use App\Helpers\Product as ProductHelper;
@endphp
<div class="row">
    <div class="col-md-6 title-main"><h4>{{ucfirst($title)}}</h4></div>
    <div class="col-md-6 text-right">

        {{-- Add New --}}
        @if($is_permission_create)
            <button type="button" class="btn btn-success ajax-modal mr-2"
                data-target="#modal-edit-{{$mod}}" data-action="{{$mod}}s/0">
                <i class="fa fa-plus"></i> {{ucfirst($title)}}
            </button>
        @endif

        @if($is_permission_import)
            <button type="button" class="btn btn-success ajax-modal mr-2"
                data-target="#modal-import-product" data-action="product-import">
                <i class="fa fa-plus"></i> Import
            </button>

            <a href="{{url( 'admin/product-export/?' . http_build_query(request()->query()))}}" target="_blank">
                <button type="button" class="btn btn-success"<i class="fas fa-file-export"></i> Export</button>
            </a>
        @endif
    </div>
</div>

<table class="table mt-10 mb-0">
    <thead>
        <tr>
            <th>Hình ảnh</th>
            <th>Mã sản phẩm</th>
            <th style="width:20%">Tên {{$title}}</th>
            <th>Giá bán</th>
            <th>Số bộ(sp) có thể đặt hàng</th>
            <th>Tồn kho</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @if ( count($records) > 0 )
            @foreach ($records as $item)
                @php
                    $inventory = $item->getInventoryByProduct();
                @endphp

                <tr>
                    <td>
                        <a data-fancybox="gallery" href="{{asset($item->thumbnailDisplay)}}">
                            <img src="{{$item->resizeImage($item->thumbnail, 100)}}" alt="">
                        </a>
                    </td>
                    <td>{{$item->code}}</td>
                    <td>{{$item->name}}</td>
                    <td class="text-center">{{number_format($item->price_sell)}}đ</td>
                    <td class="text-center">
                        @php
                            $sets = ProductHelper::getSetsInventory($item->id);
                        @endphp
                        <b style="color:{{ $sets > 0 ? '#3595DA' : 'red' }}">{{number_format($sets)}}</b>
                    </td>
                    <td class="text-center">
                        <b style="color:{{ $inventory > 0 ? '#3595DA' : 'red' }}">{{number_format($inventory)}}</b>
                    </td>
                    <td>
                        @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                            <button data-action="{{$mod}}s/{{$item->id}}" data-target="#modal-edit-{{$mod}}" type="button" class="btn btn-primary ajax-modal" data-toggle="tooltip" title="Cập nhật sản phẩm">
                                <i class="fas fa-edit"></i>
                            </button>
                        @endif

                        <button data-id="{{$item->id}}" type="button" class="btn btn-info add-to-cart" data-type="orders" data-toggle="tooltip" title="Đặt hàng">
                            <i class="fas fa-receipt"></i>
                        </button>

                        <button data-id="{{$item->id}}" type="button" class="btn btn-success add-to-cart" data-type="invoices" data-toggle="tooltip" title="Đơn hàng">
                            <i class="fas fa-file-invoice-dollar"></i>
                        </button>

                        @if($item->hasAllPermission([$permission.'xoa'], $permission))
                            <button data-action="{{$mod}}s/{{$item->id}}" type="button" class="delete-item btn btn-danger" data-toggle="tooltip" title="Xoá sản phẩm">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8" class="text-center">Không có dữ liệu</td>
            </tr>
        @endif

    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
