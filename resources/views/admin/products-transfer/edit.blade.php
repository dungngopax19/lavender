<form action="{{url('admin/products-transfer/update')}}" method="post" id="form-edit-products-transfer">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <input type="hidden" name="id" value="{{$record->id}}">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">{{$title}} lệnh điều chuyển</h4>
        <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.select', ['required'=>true, 'classObj'=>'search-product-component', 'label'=>'Sản phẩm', 'name'=>'products_id', 'value'=>$record->products_id, 'data' => $products, 'colLeft'=>4])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.text', ['required'=>true, 'type'=>'number', 'label'=>'Số lượng', 'name'=>'qty', 'value'=>$record->qty, 'colLeft'=>4])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.select', ['required'=>true, 'label'=>'Từ chi nhánh', 'name'=>'branch_from', 'value'=>$record->branch_from, 'data' => $branches, 'colLeft'=>4])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.select', ['required'=>true, 'label'=>'Đến chi nhánh', 'name'=>'branch_to', 'value'=>$record->branch_to, 'data' => $branches, 'colLeft'=>4])
            </div>
        </div>

        @if($record->hasAllPermission([$permission.'xac-thuc'], $permission))
            <div class="row">
                <div class="col-md-12">
                    @include('components.inputs.select', ['label'=>'Trạng thái', 'name'=>'is_confirmed', 'value'=>$record->is_confirmed, 'data' => config('constants.confirmed'), 'colLeft'=>4])
                </div>
            </div>
        @endif

        @if ($record->is_confirmed == 2)
            <div class="row">
                <div class="col-md-12">
                    @include('components.inputs.text', ['type'=>'text', 'label'=>'Ngày xác thực', 'name'=>'', 'value'=>$record->date_confirmed, 'colLeft'=>4])
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    @include('components.inputs.text', ['type'=>'text', 'label'=>'Người xác thực', 'name'=>'', 'value'=>$record->userConfirmed['name'], 'colLeft'=>4])
                </div>
            </div>
        @endif
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <span class="msg-form"></span>
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>

        @if ($record->is_confirmed == 0)
            <button data-form-target="#form-edit-products-transfer" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>    
        @endif
        
    </div>
</form>

<script>
    $(document).ready(function(){
        searchProduct();
    });

    function searchProduct(){
        $(".search-product-component").select2({
            ajax: {
                url: 'search-products-cart',// 'search-products',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        q: params.term,
                    }
                    return query;
                },
                processResults: function (res) {
                    return {
                        results: res.records
                    };
                }
            },
            templateResult: function($item){
                var $item = $(
                    '<div>'+$item.code+'</div>'
                );
                return $item;
            }
        }).on("select2:select", function (e) {
            setTimeout(function() {
                $('.search-product-component').next().find('.select2-selection__rendered').html(e.params.data.code);    
            }, 500);
        });
    }
    
    </script>