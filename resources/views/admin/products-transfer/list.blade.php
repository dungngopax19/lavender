<div class="row">
    <div class="col-md-6 title-main"><h4>Điều chuyển sản phẩm</h4></div>
    @if($is_permission_create)
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-success ajax-modal"
            data-target="#modal-edit-products-transfer" data-action="products-transfer/0">
            <i class="fa fa-plus"></i> Điều chuyển sản phẩm
        </button>
    </div>
    @endif
</div>

<table class="list-item table-hover table mt-10 mb-0">
    <thead>
        <tr>
            <th>Sản phẩm</th>
            <th class="text-center">Số lượng</th>
            <th>Từ chi nhánh</th>
            <th>Đến chi nhánh</th>
            <th class="text-center">Ngày xác thực</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $item)
            <tr>
                <td>{{$item->product['code']}}</td>
                <td class="text-center">{{ number_format($item->qty) }}</td>
                <td>{{$item->branchFrom['name']}}</td>
                <td>{{$item->branchTo['name']}}</td>
                <td class="text-center">{{$item->date_confirmed}}</td>
                <td>
                    @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                        <button data-action="products-transfer/{{$item->id}}" data-target="#modal-edit-products-transfer" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                    @endif
                    @if($item->hasAllPermission([$permission.'xoa'], $permission))
                        <button data-action="products-transfer/{{$item->id}}" type="button" class="delete-item btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
