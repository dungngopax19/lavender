@php
    $record->thumbnail = $record->image;
    $record->status = ($record->status) ? $record->status : 1;
    $is_disabled_role = (!$record->id || $record->id && $record->hasAllPermission([$permission.'phan-quyen'], $permission)) ? '' : 'disabled';
@endphp
<!-- Modal Header -->
<div class="modal-header">
    <h4 class="modal-title">{{($record->id)?'Cập nhật':'Thêm mới'}} {{$title}}</h4>
    <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<div class="modal-body">
    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" class="active show" href="#info">Thông tin</a></li>
        @if($record->id && $record->hasAllPermission([$permission.'phan-quyen'], $permission))
        <li><a data-toggle="tab" href="#permission">Phân quyền</a></li>
        @endif
    </ul>

    <div class="tab-content mt-3">
        <div id="info" class="tab-pane fade in active show">
            <form action="{{url('admin/'.$mod.'s/update')}}" method="post" id="form-edit-{{$mod}}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <input type="hidden" name="id" value="{{$record->id}}">
                <input type="hidden" name="permissions" id="permissions">

                <div class="row">
                    <div class="col-md-2 text-center">

                        <div id="img-preview" class="w-100 mb-3 mt-2">
                            <input type="hidden" name="thumbnail" value="{{$record->thumbnail}}">
                            <span class="close-img-preview {{empty($record->thumbnail) ? 'hide' : ''}}">×</span>
                            <a data-fancybox="gallery" href="{{asset($record->thumbnailDisplay)}}">
                                <img src="{{asset($record->thumbnailDisplay)}}" alt="noimage">
                            </a>
                        </div>

                        <input class="btn btn-primary form-group dropzone-file" type="button" value="Upload file">
                    </div>

                    <div class="col-md-10">
                        <div class="row">

                            <div class="col-md-6">
                                @include('components.inputs.text', ['label'=>'Tên người dùng', 'name'=>'name', 'value'=>$record->name, 'required'=>true])
                                @include('components.inputs.text', ['label'=>'Tên đăng nhập', 'name'=>'username', 'value'=>$record->username, 'required'=>true])
                                
                                @if(!$record->id)
                                    @include('components.inputs.password', ['label'=>'Mật khẩu', 'name'=>'password', 'value'=>'', 'required'=>true])
                                    @include('components.inputs.password', ['label'=>'Gõ lại mật khẩu', 'name'=>'re_password', 'value'=>'', 'required'=>true])
                                @else
                                    @include('components.inputs.password', ['label'=>'Mật khẩu', 'name'=>'password', 'value'=>$record->password])
                                    @include('components.inputs.password', ['label'=>'Gõ lại mật khẩu', 'name'=>'re_password', 'value'=>$record->password])
                                @endif

                                @include('components.inputs.text', ['label'=>'Email', 'name'=>'email', 'value'=>$record->email])
                                @include('components.inputs.text', ['label'=>'Điện thoại', 'name'=>'phone', 'value'=>$record->phone, 'required'=>true])
                            </div>

                            <div class="col-md-6">
                                @include('components.inputs.select', ['label'=>'Khu vực', 'name'=>'province_id', 'value'=>$record->province_id, 'data' => $provinces])
                                @include('components.inputs.text', ['label'=>'Địa chỉ', 'name'=>'address', 'value'=>$record->address])
                                @include('components.inputs.datepicker', ['label'=>'Ngày sinh', 'name'=>'birthday', 'value'=>$record->birthday  ])
                                @include('components.inputs.select', ['label'=>'Trạng thái', 'name'=>'status', 'value'=>$record->status, 'data'=>$activity_status, 'required'=> true])
                                @include('components.inputs.textarea', ['label'=>'Ghi chú', 'name'=>'text', 'value'=>$record->text])
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                @include('components.inputs.select', ['label'=>'Chi nhánh', 'name'=>'branches[]', 'value'=>$user_branches, 'data' => $branches, 'multiple'=>true, 'select'=>'id', 'required'=>true, 'colLeft' => 2])
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                @include('components.inputs.select', ['label'=>'Nhóm người dùng', 'name'=>'roles[]', 'value'=>$user_roles, 'data' => $roles, 'multiple'=>true, 'select'=>'name', 'required'=>true, 'disabled'=>$is_disabled_role, 'colLeft' => 2])
                            </div>
                        </div>
                    </div>


                </div>


                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                    <button data-form-target="#form-edit-{{$mod}}" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
                </div>
            </form>
        </div>

        <div id="permission" class="tab-pane fade">
            <div class="row">
                <div class="col-md-3">
                    <div id="treeviewSystem"></div>
                </div>
                <div class="col-md-3">
                    <div id="treeviewGoods"></div>
                </div>
                <div class="col-md-3">
                    <div id="treeviewPartners"></div>
                </div>
                <div class="col-md-3">
                    <div id="treeviewDeals"></div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                <button data-form-target="#form-edit-{{$mod}}" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        $('input[name=birthday]').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            },
            autoUpdateInput: false,
        });

        $('input[name="birthday"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        if($(".dropzone-file").length > 0){
            $(".dropzone-file").dropzone({
                url: "/admin/upload-file",
                sending: function(file, xhr, formData) {
                    formData.append("_token", CSRF_TOKEN);
                },
                success: function(file, res){
                    if(res.status == 200){
                        $('#img-preview img').attr('src', res.url);
                        $('input[name=thumbnail]').val(res.pathTmpFile);
                        $(".close-img-preview").show();
                    }else{
                        $('#img-preview img').attr('src', urlNoImage);
                        $('input[name=thumbnail]').val('');
                        $(".close-img-preview").hide();
                    }
                }
            });
        }

    });


    var a = {!! json_encode($dataSourceSystem) !!};

    console.log(a);

    $("#treeviewSystem").kendoTreeView({
        checkboxes: {checkChildren: true},
        check: onCheck,
        dataSource: {!! json_encode($dataSourceSystem) !!},
    });
    $("#treeviewGoods").kendoTreeView({
        checkboxes: {checkChildren: true},
        check: onCheck,
        dataSource: {!! json_encode($dataSourceGoods) !!}
    });
    $("#treeviewDeals").kendoTreeView({
        checkboxes: {checkChildren: true},
        check: onCheck,
        dataSource: {!! json_encode($dataSourceDeals) !!}
    });
    $("#treeviewPartners").kendoTreeView({
        checkboxes: {checkChildren: true},
        check: onCheck,
        dataSource: {!! json_encode($dataSourcePartners) !!}
    });

    $("#disableNode").click(function() {
        var selectedNode = treeview.select();

        treeview.enable(selectedNode, false);
    });

    // function that gathers IDs of checked nodes
    function checkedNodeIds(nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].id);
            }

            if (nodes[i].hasChildren) {
                checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    }

    // show checked node IDs on datasource change
    function onCheck() {
        var checkedNodes = [],
        treeviewSystem = $("#treeviewSystem").data("kendoTreeView"),
        treeviewGoods = $("#treeviewGoods").data("kendoTreeView"),
        treeviewDeals = $("#treeviewDeals").data("kendoTreeView"),
        treeviewPartners = $("#treeviewPartners").data("kendoTreeView"),
        ids = '';

        checkedNodeIds(treeviewSystem.dataSource.view(), checkedNodes);
        checkedNodeIds(treeviewGoods.dataSource.view(), checkedNodes);
        checkedNodeIds(treeviewDeals.dataSource.view(), checkedNodes);
        checkedNodeIds(treeviewPartners.dataSource.view(), checkedNodes);

        if (checkedNodes.length > 0) {
            ids = checkedNodes.join(",");
        }

        $('#permissions').val(ids);
    }
</script>
