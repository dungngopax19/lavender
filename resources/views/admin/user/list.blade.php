<div class="row">
    <div class="col-md-6 title-main"><h4>{{$title}}</h4></div>
    @if($is_permission_create)
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-success ajax-modal"
            data-target="#modal-edit-{{$mod}}" data-action="{{$mod}}s/0">
            <i class="fa fa-plus"></i> {{$title}}
        </button>
    </div>
    @endif
</div>

<table class="list-item table-hover table mt-10 mb-0">
    <thead>
        <tr>
            <th>#</th>
            <th>Hình ảnh</th>
            <th>Vai trò</th>
            <th>Tên đăng nhập</th>
            <th>Tên {{lcfirst($title)}}</th>
            <th>Điện thoại</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>
                    <a data-fancybox="gallery" href="{{asset('storage/'.$item->image)}}">
                        <img src="{{$item->resizeImage($item->image, 50)}}" alt="">
                    </a>
                </td>
                <td>
                    @php
                        $roles = $item->getRoleNames();
                    @endphp

                    @foreach ($roles as $role)
                        <div>
                            <a href="{{url('admin/roles?name=' . $role)}}" target="_blank">{{$role}}</a>
                        </div>
                    @endforeach
                </td>
                <td>{{$item->username}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->phone}}</td>
                <td>
                    @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                        <button data-action="{{$mod}}s/{{$item->id}}" data-target="#modal-edit-{{$mod}}" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                    @endif
                    
                    @if($item->hasAllPermission([$permission.'xoa'], $permission))
                        <button data-action="{{$mod}}s/{{$item->id}}" type="button" class="delete-item btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
