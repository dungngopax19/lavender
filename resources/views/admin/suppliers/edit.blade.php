<form action="{{url('admin/'.$mod.'/update')}}" method="post" id="form-edit-{{$mod}}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <input type="hidden" name="id" value="{{$record->id}}">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">{{$record?'Thêm mới':'Cập nhật'}} {{$title}}</h4>
        <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <ul class="nav nav-tabs">
            <li><a data-toggle="tab" class="active show" href="#info">Thông tin</a></li>
            <li><a data-toggle="tab" href="#history">Lịch sử nhập/trả hàng</a></li>
            <li><a data-toggle="tab" href="#payment">Nợ cần trả NCC</a></li>
        </ul>

        <div class="tab-content mt-3">
            {{-- Thông tin --}}
            <div id="info" class="tab-pane fade in active show">
                <div class="row">
                    <div class="col-md-6">
                        @include('components.inputs.text', ['label'=>'Mã nhà cung cấp', 'name'=>'code', 'value'=>$record->code, 'required'=>true])
                    </div>
                    <div class="col-md-6">
                        @include('components.inputs.text', ['label'=>'Email', 'name'=>'email', 'value'=>$record->email])
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        @include('components.inputs.text', ['label'=>'Tên nhà cung cấp', 'name'=>'name', 'value'=>$record->name, 'required'=>true])
                    </div>
                    <div class="col-md-6">
                        @include('components.inputs.text', ['label'=>'Công ty', 'name'=>'company', 'value'=>$record->company])
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        @include('components.inputs.text', ['label'=>'Điện thoại', 'name'=>'phone', 'value'=>$record->phone])
                    </div>
                    <div class="col-md-6">
                        @include('components.inputs.text', ['label'=>'Mã số thuế', 'name'=>'tax_code', 'value'=>$record->tax_code])
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        @include('components.inputs.select', ['label'=>'Tỉnh/thành phố', 'name'=>'province_id', 'value'=>$record->province_id, 'data' => $provinces])
                    </div>
                    <div class="col-md-6">
                        @include('components.inputs.text', ['label'=>'Địa chỉ', 'name'=>'address', 'value'=>$record->address])
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        @include('components.inputs.select', ['label'=>'Trạng thái', 'name'=>'activity_status', 'value'=>$record->activity_status, 'data'=>$activity_status])
                    </div>
                    <div class="col-md-6">
                        @include('components.inputs.textarea', ['label'=>'Ghi chú', 'name'=>'note', 'value'=>$record->note])
                    </div>
                </div>
            </div>

            {{-- Lịch sử nhập/trả hàng --}}
            <div id="history" class="tab-pane fade">
                Lịch sử nhập/trả hàng
            </div>

            {{-- Nợ cần trả NCC --}}
            <div id="payment" class="tab-pane fade">
                Nợ cần trả NCC
            </div>
        </div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
        <button data-form-target="#form-edit-{{$mod}}" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
    </div>
</form>