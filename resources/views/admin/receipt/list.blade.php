<div class="row">
    <div class="col-md-6 title-main"><h4>{{ucfirst($title)}}</h4></div>
    @if($is_permission_create)
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-success ajax-modal" data-target="#modal-edit-{{$mod}}" data-action="{{$mod}}s/0">
            <i class="fa fa-plus"></i> {{ucfirst($title)}}
        </button>
    </div>
    @endif
</div>

<table class="list-item table-hover table mt-10 mb-0">
    <thead>
        <tr>
            <th>Mã phiếu</th>
            <th>Thời gian</th>
            <th>Người tạo</th>
            <th>Người thanh toán</th>
            <th>Phương thức</th>
            <th>Tiền thu</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @php
            use App\Models\Receipts;
        @endphp
        @forelse ($records as $item)
            <tr>
                <td>{{$item->code}}</td>
                <td>{{date('d-m-Y H:i', strtotime($item->created_at))}}</td>
                <td>{{!empty($item->user) ? $item->user->name : ''}}</td>
                <td>{{!empty($item->customer) ? $item->customer->name : ''}}</td>
                <td>{{Receipts::PAYMENT_METHOD[$item->payment_method]}}</td>
                <td>{{number_format($item->price)}}</td>
                <td>
                    @if($item->hasAllPermission([$permission.'cap-nhat'], $permission))
                        <button data-action="{{$mod}}s/{{$item->id}}" data-target="#modal-edit-{{$mod}}" type="button" class="btn-edit btn btn-primary ajax-modal"><i class="fas fa-edit"></i></button>
                    @endif
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="100" class="text-center">Không có dữ liệu</td>
            </tr>
        @endforelse
    </tbody>
</table>

<div class="mt-15">
    {{$records->links()}}
</div>
