@php
    use App\Models\Receipts;
    use App\Models\Orders;
    use App\User; 

@endphp

<form action="{{url('admin/'.$mod.'s/update')}}" method="post" id="form-edit-{{$mod}}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <input type="hidden" name="id" value="{{$record->id}}"> 

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">{{$title}}</h4>
        <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.select', ['label'=>'Đơn hàng', 'name'=>'orders_id', 'value'=>$record->orders_id, 'data'=>$ordersData, 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.select', ['label'=>'Người thanh toán', 'name'=>'customers_id', 'value'=>$record->customers_id, 'data'=>$customersData, 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.select', ['label'=>'Phương thức', 'name'=>'payment_method', 'value'=>$record->payment_method, 'data'=>Receipts::PAYMENT_METHOD, 'required'=>true])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('components.inputs.text', ['label'=>'Tiền thu', 'name'=>'price', 'value'=>number_format($record->price), 'required'=>true])
            </div>
        </div>
                
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
        <button data-form-target="#form-edit-{{$mod}}" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
    </div>

</form>

<script>
    $(document).ready(function(){
        var temp = [];
        $("select[name=customers_id]").select2({
            ajax: {
                url: 'search-customers', 
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        q: params.term,
                    }
                    return query;
                },
                processResults: function (res) {
                    return {
                        results: res.records
                    };
                }
            },
            templateResult: function($item){
                var $item = $(
                    '<div>'+$item.text+'</div>'//+'<div>'+$item.code+'</div>'
                );
                return $item;
            }
        }).on("select2:select", function (e) { 
            // 
        });

        $("select[name=orders_id]").select2({
            ajax: {
                url: 'search-orders', 
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        q: params.term,
                    }
                    return query;
                },
                processResults: function (res) {
                    return {
                        results: res.records
                    };
                }
            },
            templateResult: function($item){
                var $item = $(
                    '<div>'+$item.text+'</div>'+'<div>'+'Ngày đặt: '+$item.order_date+'</div>'
                );
                return $item;
            }
        }).on("select2:select", function (e) { 
            // 
            $("select[name=customers_id]").select2('open');
            var params = e.params.data; 
            $.ajax({
                type: "GET",
                url: '/admin/get-customer',
                data: params,
                error: function(xhr, status, error) {},
                success: function( response ) {
                    if (response.status == 200) {
                        console.log(response);
                        var example = $("select[name=customers_id]").select2(); 
                        example.select2();
                        var data = {
                            id: response.customer.id,
                            text: response.customer.name,
                            code: response.customer.code
                        }; 
                        if (temp.indexOf(data.id) == -1) {
                            var newOption = new Option(data.text, data.id, false, false);
                            $("select[name=customers_id]").append(newOption).trigger('change');
                            example.val(response.customer.id).trigger('change.select2'); 
                            // example.select2({
                            //     templateResult: function($item){
                            //         var $item = $(
                            //             '<div>'+$item.text+'</div>'//+'<div>'+$item.id+'</div>'
                            //         );
                            //         return $item;
                            //     }
                            // });
                            temp.push(data.id);
                        } 
                    }  
                }
            }); 
        });

        $('input[name="price"]').on('keypress', function(e) {
            if(e.which == 13) {
                e.preventDefault(); 
                var value = numeral($(this).val()).format('0,0');
                $(this).val(value);
            }
        });
    });
</script>