@php
    use App\Helpers\Common;

@endphp

<div class="row">
    <div class="col-md-12">
        <table class="table mt-10 mb-0">
            <thead style="background-color: #DCF5FC;">
                <tr>
                    <td>#</td>
                    <td>Hình ảnh</td>
                    <td>Mã sản phẩm</td>
                    <td>Tên sản phẩm</td>
                    <td class="text-center">Số lượng</td>
                    <td class="text-center">Đơn giá</td>
                    <td class="text-center">Thành tiền</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($record->orderDetail as $i => $orderDetail)
                    @php
                        $product = $orderDetail['product'];
                    @endphp

                    @if ($product)
                        <tr>
                            <td>{{ $product->id_display}}</td>
                            <td>
                                <a data-fancybox="gallery" href="{{asset($product->thumbnailDisplay)}}">
                                    <img src="{{Common::resizeImage($product->thumbnail, 100)}}" alt="">
                                </a>
                            </td>
                            <td><strong>{{$product->code}}</strong></td>
                            <td>{{$product->name}}</td>
                            <td class="text-center">{{!empty($orderDetail->qty) ? number_format($orderDetail->qty) : 0}}</td>
                            <td class="text-center">{{!empty($orderDetail->price) ? number_format($orderDetail->price) : 0}}</td>
                            <td class="text-center">{{!empty($orderDetail->price) ? number_format( $orderDetail->qty * $orderDetail->price ) : 0}}</td>
                        </tr>
                    @endif
                @endforeach

                <tr>
                    <td colspan="6" class="text-right">
                        <p>Tổng số lượng:</p>
                        <p>Tổng tiền hàng:</p>
                        <p>Giảm giá:</p>
                        <p>Khách đã trả:</p>
                        <p>Khách cần trả:</p>
                    </td>
                    <td class="text-right font-weight-bold">
                        <p>{{!empty($record->orderDetail) ? number_format(count($record->orderDetail)) : 0}}</p>
                        <p>{{!empty($record->price) ? number_format($record->price) : 0}}đ</p>
                        <p>{{$record->discount}}{{($record->discount_unit == 'percent') ? '%' : 'đ'}}</p>
                        <p>{{!empty($record->deposit) ? number_format($record->deposit) : 0}}đ</p>
                        <p>{{!empty($record->price_final) ? number_format($record->price_final) : 0}}đ</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>