<table class="table table-hover mb-0">
    @if (count($records) > 0)
        @foreach ($records as $i => $item)
            <tr>
                <td>
                    {{($loop->iteration + ($records->currentPage() - 1)*$records->perPage())}}. {{$item->code}} 

                    @if ($item->shipmentDetail['delivery_time'])
                        ( {{ date('d-m-Y', strtotime($item->shipmentDetail['delivery_time'])) }} )    
                    @endif
                    
                </td>
            </tr>
        @endforeach
    @else
        <tr class="text-center ">
            <td>Không có dữ liệu</td>
        </tr>
    @endif
</table>
