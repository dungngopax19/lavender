@php
    use App\Helpers\Product as ProductHelper;
    use App\Helpers\Common;
@endphp

<div class="table-responsive">
    <table class="table-striped mt-10 mb-0 table mb-3">
        <tbody>
            <tr>
                <td class="text-center">ID</td>
                <td>Hình ảnh</td>
                <td>Mã sản phẩm</td>
                <td>Tên sản phẩm</td>
                <td class="text-center">Giảm giá</td>
                <td class="text-center">Đơn giá</td>
                <td class="text-center">Số lượng</td>
                <td class="text-center">Giá tiền</td>
                <td class="text-center">Tồn kho</td>
                <td class="text-center">Số bộ(sp) có thể đặt hàng</td>
            </tr>

            @foreach ($itemsCart as $uniqueId => $item)
                @php
                    $inventory = ProductHelper::getInventoryByBranch($item->id);

                    if($type == 'orders'){
                        $sets_register = ProductHelper::getSetsInventory($item->id);
                    }else{
                        $sets_register = 0;
                    }

                    $discountUnit = $item->options['discountUnit'];
                    
                    $codes = [
                        request()->code_product_delivered,
                        request()->code_product_order_highlight
                    ];
                    $activeCodeProduct = in_array($item->options['code'], $codes) ? 'active' : '';
                @endphp

                <tr class="shopping-cart-item" data-inventory="{{$inventory + $sets_register}}" 
                        data-id="{{$item->id}}" 
                        data-uniqueId="{{$uniqueId}}" data-type="{{$type}}">

                    <td>
                        @if (!$disabledEditCart)
                            <a href="javascript:void(0);" class="btn-icon btn-delete remove-item-cart-modal" title="Xóa hàng hóa"><i></i></a>    
                        @endif
                    </td>

                    <td>
                        <a data-fancybox="gallery" href="{{asset($item->options['thumbnail'])}}">
                            <img src="{{Common::resizeImage($item->options['thumbnail'], 100)}}" alt="">
                        </a>
                    </td>

                    <td>
                        <span class="ajax-modal link {{ $activeCodeProduct }}" data-action="products/{{$item->id}}" data-target="#modal-edit-product">{{$item->options['code']}}<span>
                    </td>

                    <td>
                        <span class="ajax-modal link" data-action="products/{{$item->id}}" data-target="#modal-edit-product">{{$item->name}}</span><br/>
                        @if ($disabledEditCart)
                            <input data-orders-id="{{$detailOrders->id}}" readonly data-products-id="{{$item->id}}" class="update-discount-note" type="text" name="discount_note" value="{{$item->options['discount_note'] ?? ''}}">
                        @else
                            <input class="update-discount-note" type="text" value="{{$item->options['discount_note'] ?? ''}}">
                        @endif
                    </td>

                    <td class="text-center discount-item-cart">
                        <input type="text" {{$disabledEditCart}} name="discount" value="{{number_format($item->options['discount'])}}" class="txt-border-bottom format-number text-center" style="width:70px; margin-bottom: 10px;"><br/>
                        <input type="button" {{$disabledEditCart}} data-unit="vnd" class="btn-discount {{($discountUnit == 'vnd') ? 'btn-info' : ''}}" value="vnd">
                        <input type="button" {{$disabledEditCart}} data-unit="percent" class="btn-discount {{($discountUnit == '%') ? 'btn-info' : ''}}" value="%">
                    </td>

                    <td class="text-center">
                        <input type="text" {{$disabledEditCart}} class="price-item-cart" value="{{number_format($item->price)}}" style="width:100px; text-align: center;">

                        @if ($item->price != $item->options['price_sell'])
                            <div class="small-price">{{number_format($item->options['price_sell'])}}đ</div>    
                        @endif
                    </td>

                    <td class="text-center">
                        <input type="text" {{$disabledEditCart}} class="qty-item-cart" value="{{number_format($item->quantity)}}" style="width:50px; text-align: center;">
                    </td>

                    <td class="text-right"><strong class="item-sum">{{number_format($item->price * $item->quantity)}}đ<strong></td>

                    <td class="text-center">{{ number_format($inventory) }}</td>

                    <td class="text-center">{{ number_format(ProductHelper::getSetsInventory($item->id)) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@if (!$disabledEditCart && count($itemsCart) > 0)
    <div class="mt-3 row">
        <div class="col-md-9 text-right pt-2">
            <h6>Chọn mua thêm sản phẩm</h6>
        </div>
        <div class="col-md-3">
            <select class="search-product-component form-control" name="users_id">
                @foreach($products as $item)
                    <option value="{{ $item->id}}">{{ $item->code}}</option>
                @endforeach
            </select>
        </div>
    </div>
@endif