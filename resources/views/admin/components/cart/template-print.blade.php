@php
    use App\Helpers\Product as ProductHelper;
    use App\Helpers\Common;
@endphp

<table class="table table-bordered">
    <tbody>
        <tr>
            <td>ID</td>
            <td>Mã sản phẩm</td>
            <td>Tên sản phẩm</td>
            <td class="text-right">Đơn giá</td>
            <td class="text-center">Số lượng</td>
            <td class="text-right">Giá tiền</td>
        </tr>

        @foreach ($itemsCart as $uniqueId => $item)
            @php
                $inventory = ProductHelper::getInventoryByBranch($item->id);

                if($type == 'orders'){
                    $sets_register = ProductHelper::getSetsInventory($item->id);
                }else{
                    $sets_register = 0;
                }

                $discountUnit = $item->options['discountUnit'];

            @endphp

            <tr class="shopping-cart-item">

                <td>{{$item->id}}</td>
                <td>
                    {{$item->options['code']}}
                </td>
                <td>
                    {{$item->name}}
                </td>

                <td class="text-right">
                    {{number_format($item->price)}}

                    @if ($item->price != $item->options['price_sell'])
                        <div class="small-price">{{number_format($item->options['price_sell'])}}</div>
                    @endif
                </td>

                <td class="text-center">
                    {{number_format($item->quantity)}}
                </td>

                <td class="text-right">{{number_format($item->price * $item->quantity)}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
