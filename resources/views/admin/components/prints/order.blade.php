<div class="hide" style="width:21cm !important; margin: 0 auto; font-size: 13px; padding-right: 50px">
    <div id="print" >
        <div class="row">
            <div class="col-md-12 text-center font-weight-bold" style="font-size: 25px">ĐƠN HÀNG</div>
        </div>

        <div class="row mt-10">
            <div class="col-md-8">
                <div>
                    <img src="{{asset('images/logo.png')}}" alt="" width="140px">
                </div>
                <div class="mt-3">Drap cao cấp cho gia đình & khách sạn </div>
                <div class="mt-2">Website: <strong>www.lavendervn.com</strong></div>
                <div class="mt-2">Hotline: 0966 37 39 79</div>
                <div class="mt-2">CN 3/2:(028) 6272 3554</div>
                <div class="mt-2">CN Bình Tân: (028) 2210 2332</div>
                <div class="mt-2">CN CMT8: (028) 6273 3554</div>
                <div class="mt-2">Giao nhận: (028) 6682 7897</div>
                <div class="mt-2">Địa chỉ 1: <span class="name-address-1">89A Đường 3/2, Phường 11, Quận 10, TP.HCM <br/>(Gần Cao Thắng)</span></div>
                <div class="mt-2">Địa chỉ 2: <span class="name-address-2">191 Đường Số 1, Bình Trị Đông B, Quận Bình Tân</span></div>
                <div class="mt-2">Địa chỉ 3: <span class="name-address-2">1058 CMT8, Phường 4, Q.Tân Bình, HCM</span></div>
            </div>

            <div class="col-md-4">
                <div class="mt-3">Số: <span class="name-customer-name">{{$detailOrder->code}}</span></div>
                <div class="mt-2">Ngày {{ date('d')}} tháng {{ date('m')}} năm {{ date('Y')}}</div>
                <div class="mt-2">Mã khách hàng: <span class="name-customer-name">{{$detailOrder->customer['code']}}</span></div>
                <div class="mt-2">Tên khách hàng: <span class="name-customer-name">{{$detailOrder->customer['name']}}</span></div>
                <div class="mt-2">Địa chỉ: <span class="name-customer-address">{{$detailOrder->customer['address']}}</span></div>
                <div class="mt-2">Điện thoại: <span class="name-customer-phone">{{$detailOrder->customer['phone']}}</span></div>
            </div>
        </div>

        <div class="row mt-10">
            <div class="col-md-12 html-cart-print" style="font-size: 11px">
                <table class="table table-bordered-print">
                    <thead>
                        <tr>
                            <th class="text-center" style="border: 2px solid black;">STT</th>
                            <th class="text-center" style="border: 2px solid black;">Mã Hàng</th>
                            <th class="text-center" style="border: 2px solid black;">Sản Phẩm</th>
                            <th class="text-right" style="border: 2px solid black;">Đơn Giá</th>
                            <th class="text-center" style="border: 2px solid black;">Số Lượng</th>
                            <th class="text-right" style="border: 2px solid black;">Thành tiền</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $total = 0;
                        @endphp
                        @foreach($itemsCart as $item)
                        @php
                            $item->quatity = 10;
                            $total += $item->price*$item->quatity;
                        @endphp
                        <tr>
                            <td class="text-center" style="width: 5%;border: 2px solid black; font-size: 11px">{{ $loop->iteration}}</td>
                            <td class="text-center" style="width: 15%;border: 2px solid black; font-size: 11px">{{$item->options['code']}}</td>
                            <td class="text-center" style="width: 30%;border: 2px solid black;font-size: 11px">
                                <b>{{ $item->name}}</b>

                                @if ( isset($item->options['discount_note']) )
                                    <p style="margin-top: 10px;">{{ $item->options['discount_note'] }} </p>
                                @endif

                            </td>
                            <td class="text-right" style="width: 10%;border: 2px solid black;font-size: 11px">{{ number_format($item->price)}} đ</td>
                            <td class="text-center" style="width: 10%;border: 2px solid black;font-size: 11px"><span class="name_{{$item->id}}_quantity">{{ number_format($item->quantity)}}</span></td>
                            <td class="text-right" style="width: 10%;border: 2px solid black;font-size: 11px"><span class="name_{{$item->id}}_thanhtien">{{ number_format($item->price * $item->quantity)}} đ</span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="col-md-10">
                <div class="text-right" style="padding: 2px 12px 2px 0px;">Phí Ship:</div>
                <div class="text-right" style="padding: 2px 12px 2px 0px;">Tổng tiền hàng:</div>
                <div class="text-right" style="padding: 2px 12px 2px 0px;">Giảm Giá:</div>
                <div class="text-right" style="padding: 2px 12px 2px 0px;">Khách đã thanh toán trước:</div>
                <div class="text-right" style="padding: 2px 12px 2px 0px;">Số tiền còn phải thanh toán:</div>
            </div>

            <div class="col-md-2">
                <div class="text-right" style="padding: 2px 12px 2px 0px;"><span class="print-ship">{{number_format($detailOrder->ship)}}</span> đ</div>
                <div class="text-right" style="padding: 2px 12px 2px 0px;"><span class="print-price">{{number_format($detailOrder->price)}}đ</span></div>
                <div class="text-right" style="padding: 2px 12px 2px 0px;"><span class="print-discount">{{number_format($detailOrder->discount)}}</span> đ</div>
                <div class="text-right font-weight-bold" style="padding: 2px 12px 2px 0px;"><span class="print-deposit">{{number_format($detailOrder->deposit)}}</span> đ</div>
                <div class="text-right" style="padding: 2px 12px 2px 0px;"><span class="print-againPay">{{number_format($detailOrder->price_final)}}</span> đ</div>
            </div>
        </div>

        <div class="row mt-10">
            <div class="col-md-12">
                <div class="font-weight-bold">
                    Phiếu mua hàng này có giá trị thay phiếu bảo hành, khách hàng lưu ý giữ lại.
                </div>
                <div class="mt-2">
                    (Ghi chú: <span class="comment-orders-print">{{$detailOrder->comment}}</span>)
                </div>
            </div>

        </div>

        <div class="row mt-2">
            <div class="col-md-4 text-center font-weight-bold">
                Khách hàng
            </div>
            <div class="col-md-4 text-center font-weight-bold">
                Kế toán
            </div>
            <div class="col-md-4 text-center font-weight-bold">
                Bán hàng Lavender
            </div>
        </div>

    </div>
</div>