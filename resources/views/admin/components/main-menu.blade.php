@php
    use App\Helpers\Permissions;
    $seg2 = Request::segment(2);
    $cartActive = Request::has('gh') ? Request::get('gh') : 1;
@endphp

<div class="main-menu">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 pl-0">
                <ul class="nav">
                    <li class="nav-item {{ ($seg2  == 'dashboard' ) ? 'active' : '' }}">
                        <a class="nav-link" href="{{url('admin/dashboard')}}"><i class="fas fa-eye"></i> Tổng quan</a>
                    </li>

                    <li class="nav-item dropdown {{in_array($seg2, ['product-categories', 'products'])?'active':''}}">
                        <a class="nav-link" href="{{url('admin/products')}}"><i class="fas fa-cube"></i> Hàng hoá</a>
                        <div class="dropdown-content">
                            @if(Permissions::hasAllPermission(['hang-hoa-danh-muc-san-pham-xem-ds']))
                            <a href="{{url('admin/product-categories')}}" class="{{ ($seg2  == 'product-categories' ) ? 'active' : '' }}"><i class="fas fa-th"></i> Nhóm hàng</a>
                            @endif
                            @if(Permissions::hasAllPermission(['hang-hoa-san-pham-xem-ds']))
                            <a href="{{url('admin/products')}}" class="{{ ($seg2  == 'products' ) ? 'active' : '' }}"><i class="fas fa-tshirt"></i> Sản phẩm</a>
                            @endif
                        </div>
                    </li>

                    <li class="nav-item dropdown {{in_array($seg2, ['orders', 'invoices', 'receipts', 'confirm-order', 'transport-order'])?'active':''}}">
                        <a class="nav-link" href=""><i class="fas fa-exchange-alt"></i> Giao dịch</a>
                        <div class="dropdown-content">
                            @if(Permissions::hasAllPermission(['giao-dich-hoa-don-xac-nhan-don-hang']))
                                <a href="{{url('admin/confirm-order')}}" class="{{ ($seg2  == 'confirm-order' ) ? 'active' : '' }}"><i class="fas fa-receipt"></i> Kiểm duyệt đơn hàng</a>
                            @endif

                            @if(Permissions::hasAllPermission(['giao-dich-van-chuyen-xem-ds']))
                                <a href="{{url('admin/transport-order')}}" class="{{ ($seg2  == 'transport-order' ) ? 'active' : '' }}"><i class="fas fa-receipt"></i> Vận chuyển</a>
                            @endif

                            @if(Permissions::hasAllPermission(['giao-dich-hoa-don-xem-ds']))
                                <a href="{{url('admin/invoices')}}" class="{{ ($seg2  == 'invoices' ) ? 'active' : '' }}"><i class="fas fa-file"></i> Hoá đơn</a>
                            @endif

                            @if(Permissions::hasAllPermission(['giao-dich-hoa-don-xem-ds']))
                                <a href="{{url('admin/orders')}}" class="{{ ($seg2  == 'orders' ) ? 'active' : '' }}"><i class="fas fa-inbox"></i> Phiếu đặt hàng</a>
                            @endif

                            @if(Permissions::hasAllPermission(['giao-dich-phieu-thu-xem-ds']))
                                <a href="{{url('admin/receipts')}}" class="{{ ($seg2  == 'receipts' ) ? 'active' : '' }}"><i class="fas fa-receipt"></i> Phiếu thu</a>
                            @endif

                        </div>
                    </li>

                    <li class="nav-item dropdown {{in_array($seg2, ['customers', 'suppliers', 'partner-deliveries'])?'active':''}}">
                        <a class="nav-link" href="{{url('admin/suppliers')}}"><i class="fas fa-male"></i> Đối tác</a>
                        <div class="dropdown-content">
                            @if(Permissions::hasAllPermission(['doi-tac-khach-hang-xem-ds']))
                                <a href="{{url('admin/customers')}}" class="{{ ($seg2  == 'customers' ) ? 'active' : '' }}"><i class="fas fa-user"></i> Khách hàng</a>
                            @endif

                            @if(Permissions::hasAllPermission(['doi-tac-nha-cung-cap-xem-ds']))
                                <a href="{{url('admin/suppliers')}}" class="{{ ($seg2  == 'suppliers' ) ? 'active' : '' }}"><i class="fas fa-undo"></i> Nhà cung cấp</a>
                            @endif

                            @if(Permissions::hasAllPermission(['doi-tac-doi-tac-giao-hang-xem-ds']))
                                <a href="{{url('admin/partner-deliveries')}}" class="{{ ($seg2  == 'partner-deliveries' ) ? 'active' : '' }}"><i class="fas fa-user-plus"></i> Đối tác giao hàng</a>
                            @endif
                        </div>
                    </li>

                    @if(Permissions::hasAllPermission(['hang-hoa-kho-hang-xem-ds']))
                        <li class="nav-item dropdown {{in_array($seg2, ['stores', 'products-transfer'])?'active':''}}">
                            <a class="nav-link" href="{{url('admin/stores')}}"><i class="fas fa-store"></i> Kho hàng</a>
                            <div class="dropdown-content">
                                <a href="{{url('admin/products-transfer')}}" class="{{ ($seg2 == 'products-transfer' ) ? 'active' : '' }}">
                                    <i class="fas fa-puzzle-piece"></i> Điều chuyển sản phẩm
                                </a>
                            </div>
                        </li>
                    @endif

                    <li class="nav-item dropdown {{in_array($seg2, ['coupons', 'membership-gifts', 'promotions'])?'active':''}}">
                        <a class="nav-link" href="javascript:void(0)"><i class="fab fa-hotjar"></i> Khuyến mãi</a>
                        <div class="dropdown-content">

                            @if(Permissions::hasAllPermission(['khuyen-mai-coupons-xem-ds']))
                                <a href="{{url('admin/coupons')}}" class="{{ ($seg2 == 'coupons' ) ? 'active' : '' }}">
                                    <i class="fas fa-puzzle-piece"></i> Coupon
                                </a>
                            @endif

                            @if(Permissions::hasAllPermission(['khuyen-mai-membership-xem-ds']))
                                <a href="{{url('admin/membership-gifts')}}" class="{{ ( $seg2 == 'membership-gifts' ) ? 'active' : '' }}">
                                    <i class="fa fa-cart-plus"></i> Membership
                                </a>
                            @endif

                            @if(Permissions::hasAllPermission(['khuyen-mai-nhom-san-pham-xem-ds']))
                                <a href="{{url('admin/promotions')}}" class="{{ ($seg2 == 'promotions' ) ? 'active' : '' }}">
                                    <i class="fas fa-object-ungroup"></i> Nhóm sản phẩm
                                </a>
                            @endif

                        </div>
                    </li>



                    <li class="nav-item dropdown {{in_array($seg2, ['roles', 'users', 'branches', 'surcharges', 'membership-gifts', 'activity-log'])?'active':''}}">
                        <a class="nav-link" href="javascript:void(0)"><i class="fas fa-cog"></i> Thiết lập</a>
                        <div class="dropdown-content">

                            @if(Permissions::hasAllPermission(['he-thong-nhom-nguoi-dung-xem-ds']))
                                <a href="{{url('admin/roles')}}" class="{{ ($seg2 == 'roles' ) ? 'active' : '' }}">
                                    <i class="fas fa-puzzle-piece"></i> Quản lý nhóm người dùng
                                </a>
                            @endif

                            @if(Permissions::hasAllPermission(['he-thong-nguoi-dung-xem-ds']))
                                <a href="{{url('admin/users')}}" class="{{ ( $seg2 == 'users' ) ? 'active' : '' }}">
                                    <i class="fas fa-male"></i> Quản lý người dùng
                                </a>
                            @endif

                            @if(Permissions::hasAllPermission(['he-thong-chi-nhanh-xem-ds']))
                                <a href="{{url('admin/branches')}}" class="{{ ( $seg2 == 'branches' ) ? 'active' : '' }}">
                                    <i class="fas fa-puzzle-piece"></i> Quản lý chi nhánh
                                </a>
                            @endif

                            @if(Permissions::hasAllPermission(['he-thong-thu-khac-xem-ds']))
                                <a href="{{url('admin/surcharges')}}" class="{{ ( $seg2 == 'surcharges' ) ? 'active' : '' }}">
                                    <i class="fa fa-cart-plus"></i> Quản lý thu khác
                                </a>
                            @endif

                            @if(Permissions::hasAllPermission(['hang-hoa-unit-inventory-xem-ds']))
                                <a href="{{url('admin/unit-inventory')}}" class="{{ ( $seg2 == 'unit-inventory' ) ? 'active' : '' }}">
                                    <i class="fa fa-cart-plus"></i> Chuyển đổi vải thành phẩm
                                </a>
                            @endif

                            @if(Permissions::hasAllPermission(['he-thong-lich-su-thao-tac']))
                                <a href="{{url('admin/activity-log')}}" class="{{ ( $seg2 == 'activity-log' ) ? 'active' : '' }}"><i class="fas fa-history"></i> Lịch sử thao tác</a>
                            @endif
                        </div>
                    </li>
                </ul>
            </div>

            <div class="col-md-3">
                <ul class="nav float-right">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="javascript:void(0)"><i class="fas fa-receipt"></i> Phiếu đặt hàng</a>
                        <div class="dropdown-content box-dropdown-orders dropdown-content-cart wrap-hover-orders menu-right pl-3 pr-3 pt-1">
                            {{ Widget::run('Admin.Carts', ['type' => 'orders', 'cartNumber' => $cartActive]) }}
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link" href="javascript:void(0)"><i class="fas fa-file-invoice-dollar"></i> Đơn hàng</a>
                        <div class="dropdown-content box-dropdown-invoices dropdown-content-cart wrap-hover-invoices menu-right pl-3 pr-3 pt-1">
                            {{ Widget::run('Admin.Carts', ['type' => 'invoices', 'cartNumber' => $cartActive]) }}
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
