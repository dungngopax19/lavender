@php
    use App\Models\Customers;
    $data = Customers::orderBy('name', 'asc')->get();
    $customers_id = Request::get('customers_id')??[];
@endphp

<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-customer">
            <a href="javascript:;">Khách hàng</a>
            <a href="javascript:;" class="float-right">
                @if($customers_id)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-customer" class="panel-collapse collapse @if($customers_id) show @endif">
        <div class="panel-body">
            <div class=" mt-3">
                <select class="select2 form-control" multiple="multiple" name="customers_id[]">
                    <option value="">---</option>
                    @foreach ($data as $i => $item)
                    <option {{ (in_array($item['id'], $customers_id))? 'selected' : ''}} value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
