@php
    use App\Models\ProductTypes;
    $productTypes = ProductTypes::orderBy('name', 'asc')->get();
    $product_types = Request::get('product_types')??[];

@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-product-type">
            <a href="javascript:;">Loại hàng</a>
            <a href="javascript:;" class="float-right">
                @if(count($product_types))
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-product-type" class="panel-collapse collapse @if(count($product_types)) show @endif">
        <div class="panel-body">
            @foreach ($productTypes as $i => $item)
                <div class="mt-3">
                    <div class="pretty p-icon p-smooth">
                    <input type="checkbox"
                        {{in_array($item->id, $product_types)?'checked':''}}
                        value="{{$item->id}}" name="product_types[]"
                        id="product_types_{{$item->id}}" class="auto-submit-item">

                    <div class="state p-success">
                        <i class="icon fa fa-check"></i>
                        <label for="product_types_{{$item->id}}">{{$item->name}}</label>
                    </div>
                </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
