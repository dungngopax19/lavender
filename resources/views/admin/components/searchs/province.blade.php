@php
    use App\Models\Provinces;

    $province_id = intval(Request::get('province_id'));
    $data = Provinces::with('childrens')->where('parent_id', 0)->orderBy('name')->select('id', 'name')->get();
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-province">
            <a href="javascript:;">Khu vực</a>
            <a href="javascript:;" class="float-right">
                @if($province_id)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-province" class="panel-collapse collapse @if($province_id) show @endif">
        <div class="panel-body">
            <div class=" mt-3">
                <select class="select2 form-control" name="province_id">
                    <option value="0">---</option>
                    @foreach ($data as $i => $item)
                        <optgroup label="{{$item['name']}}">
                            @foreach ($item['childrens'] as $itemChild)
                                <option {{ ($itemChild['id'] == $province_id)? 'selected' : ''}} value="{{$itemChild['id']}}">{{$itemChild['name']}}</option>
                            @endforeach
                        </optgroup>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
