<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-keyword">Tìm kiếm</a>
        </h6>
    </div>

    <div id="collapse-keyword" class="panel-collapse collapse show">
        <div class="panel-body mt-3">
            <input type="text" class="form-control auto-submit-item" name="keyword" value="{{Request::get('keyword')}}" placeholder="Tìm theo từ khoá">
        </div>
    </div>
</div>
