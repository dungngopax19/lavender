@php
    $inventory_status = intval(Request::get('inventory_status'));
    $data = config('constants.inventory_status');
    $isCreated = Request::get('created_range');
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-created">
            <a href="javascript:;">Ngày tạo</a>
            <a href="javascript:;" class="float-right">
                @if($isCreated)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-created" class="panel-collapse collapse @if($isCreated) show @endif">
        <div class="panel-body">
            <div class=" mt-3">
            <input type="text" class="form-control daterange-picker auto-submit-item" name="created_range" value="{{request()->created_range}}">
            </div>
        </div>
    </div>
</div>
