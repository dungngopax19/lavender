@php
    $data = [10, 15, 20, 30, 50];
    $items_per_page = Request::get('items_per_page');
@endphp
<div class="panel panel-default">

    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-items-page">
            <a href="javascript:;">Lựa chọn hiển thị</a>
            <a href="javascript:;" class="float-right">
                @if($items_per_page)
                    <i class="fa fa-chevron-circle-up"></i>
                @else
                    <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-items-page" class="panel-collapse collapse @if($items_per_page) show @endif">
        <div class="panel-body mt-3">
            <select class="select2 form-control auto-submit-item" name="items_per_page">
                <option value="">Chọn số bản ghi</option>
                @foreach ($data as $item)
                    <option {{ ($item == $items_per_page)? 'selected' : ''}} value="{{$item}}">Số bản ghi: {{$item}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
