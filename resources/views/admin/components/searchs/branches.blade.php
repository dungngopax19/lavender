@php
    use App\Models\Branches;
    $branches = Branches::where('activity_status', 1)->orderBy('name', 'asc')->get();
    $branches_id = Request::get('branches_id')??[];
@endphp

<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-branch">
            <a href="javascript:;">Chi nhánh</a>
            <a href="javascript:;" class="float-right">
                @if(count($branches_id))
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-branch" class="panel-collapse collapse @if(count($branches_id)) show @endif">
        <div class="panel-body">
            @foreach ($branches as $i => $item)
                <div class="pretty p-icon p-smooth mt-3">
                    <input type="checkbox"
                        {{in_array($item->id, $branches_id)?'checked':''}}
                        value="{{$item->id}}" name="branches_id[]"
                        id="branches_id_{{$item->id}}" class="auto-submit-item">

                    <div class="state p-success">
                        <i class="icon fa fa-check"></i>
                        <label for="branches_id_{{$item->id}}">{{$item->name}}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
