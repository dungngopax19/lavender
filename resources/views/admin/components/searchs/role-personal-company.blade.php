@php
    $role = intval(Request::get('role'));
    $data = config('constants.roles-personal-company');
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-role">
            <a href="javascript:;">{{ $label}}</a>
            <a href="javascript:;" class="float-right">
                @if($role)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-role" class="panel-collapse collapse @if($role) show @endif">
        <div class="panel-body">
            @foreach ($data as $i => $item)
                <div class="pretty p-default p-round mt-3 d-block">
                    <input type="radio" value="{{$i}}" id="role-{{$i}}" {{$role == $i ? 'checked' : ''}} name="role" class="auto-submit-item">
                    <div class="state">
                        <label for="role-{{$i}}">{{$item}}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
