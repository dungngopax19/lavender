<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#search-name">Tìm kiếm</a>
        </h6>
    </div>

    <div id="search-name" class="panel-collapse collapse show">
        <div class="panel-body">
            <input type="text" class="form-control mt-3 auto-submit-item" name="product_name_code" value="{{Request::get('product_name_code')}}" placeholder="Theo ID, mã, tên hàng">
            <input type="text" class="form-control mt-2 auto-submit-item" name="product_note" value="{{Request::get('product_note')}}" placeholder="Theo ghi chú đặt hàng">
        </div>
    </div>
</div>
