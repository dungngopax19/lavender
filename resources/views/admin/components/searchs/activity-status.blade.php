@php
    $activity_status = intval(Request::get('activity_status'));
    $data = config('constants.activity_status');
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-activity-status">
            <a href="javascript:;">Trạng thái</a>
            <a href="javascript:;" class="float-right">
                @if($activity_status)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-activity-status" class="panel-collapse collapse @if($activity_status) show @endif">
        <div class="panel-body">
            @foreach ($data as $i => $item)
                <div class="pretty p-default p-round mt-3">
                    <input type="radio" value="{{$i}}" id="activity_status-{{$i}}" {{$activity_status == $i ? 'checked' : ''}} name="activity_status" class="auto-submit-item">
                    <div class="state">
                        <label for="activity_status-{{$i}}">{{$item}}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
