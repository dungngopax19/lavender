@php
    $delivery_time = Request::get('delivery_time');
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-delivery-time">
            <a href="javascript:;">Thời gian giao hàng</a>
            <a href="javascript:;" class="float-right">
                @if($delivery_time)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-delivery-time" class="panel-collapse collapse @if($delivery_time) show @endif">
        <div class="panel-body">
            <div class=" mt-3">
            <input type="text" class="form-control daterange-picker auto-submit-item" name="delivery_time" value="{{request()->delivery_time}}">
            </div>
        </div>
    </div>
</div>
