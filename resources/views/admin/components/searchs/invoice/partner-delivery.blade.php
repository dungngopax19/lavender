@php
    use App\Models\PartnerDeliveries;

    $parner_delivery = intval(Request::get('parner_delivery'));
    $data = PartnerDeliveries::where('activity_status', 1)->orderBy('name', 'asc')->get();;
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-parner_delivery">
            <a href="javascript:;">Đối tác giao hàng</a>
            <a href="javascript:;" class="float-right">
                @if($parner_delivery)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-parner_delivery" class="panel-collapse collapse @if($parner_delivery) show @endif">
        <div class="panel-body">
            <div class=" mt-3">
                <select class="select2 form-control" name="parner_delivery">
                    <option value="0">---</option>
                    @foreach ($data as $i => $item)
                    <option {{ ($item->id == $parner_delivery)? 'selected' : ''}} value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
