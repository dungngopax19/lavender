@php
    use App\Helpers\Invoice as InvoiceHelper;
    $codeProducts = InvoiceHelper::productCodeYetDelivered();
    $totalQty = 0;
    $code_product_delivered = request()->code_product_delivered;
    $id_product_delivered = request()->id_product_delivered;
    $exclude_code_product_delivered = request()->exclude_code_product_delivered;

    $queryString = "shipment_status_id=1&created_range=" . request()->created_range;
@endphp

@if ($codeProducts)
    
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-status-order">
            <a href="javascript:;">Mã sản phẩm chưa giao</a>
            <a href="javascript:;" class="float-right">
                @if($code_product_delivered || $exclude_code_product_delivered)
                    <i class="fa fa-chevron-circle-up"></i>
                @else
                    <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-status-order" class="panel-collapse collapse @if($code_product_delivered || $exclude_code_product_delivered) show @endif">
        <input type="hidden" class="form-control mt-3 auto-submit-item" name="id_product_delivered" value="{{$id_product_delivered}}" placeholder="Tìm theo mã hàng" style="font-size: 13px;">

        <input type="text" class="form-control mt-3 auto-submit-item" name="code_product_delivered" value="{{$code_product_delivered}}" placeholder="Tìm theo mã hàng" style="font-size: 13px;">
        <input type="text" class="form-control mt-3 auto-submit-item" name="exclude_code_product_delivered" value="{{$exclude_code_product_delivered}}" placeholder="Hoăc loại trừ mã hàng" style="font-size: 13px;">
        

        <div class="panel-body" style="max-height:500px; overflow-y: scroll;">
            
            @foreach ($codeProducts as $i => $item)
                @php
                    $totalQty += $item->totalQty;
                @endphp
                <div class="mt-3 d-block">
                    <div class="state p-success">
                        <a href="{{url('admin/transport-order?'.$queryString.'&id_product_delivered=' . $item->product['id'] . '&code_product_delivered=' . $item->product['code'])}}">
                            <label style="font-size: 13px;">
                                {{$item->product['code']}} <br/>
                                <b>({{$item->totalQty}}SP) - ({{$item->totalOrders}}ĐH)</b>
                            </label>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="mt-3"><b>Tổng cộng: ({{number_format($totalQty)}}SP)</b></div>
    </div>
</div>

@endif