@php
    use App\Models\ShipmentStatus;
    $status = ShipmentStatus::where('activity_status', 1)->orderBy('id', 'asc')->get();
    $shipment_status_id = Request::get('shipment_status_id')??[];
@endphp

<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-delivery-status">
            <a href="javascript:;">Trạng thái giao hàng</a>
            <a href="javascript:;" class="float-right">
                @if(count($shipment_status_id))
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-delivery-status" class="panel-collapse collapse @if(count($shipment_status_id)) show @endif">
        <div class="panel-body">
            @foreach ($status as $i => $item)
                <div class="pretty p-icon p-smooth mt-3 d-block">
                    <input type="checkbox"
                        {{in_array($item->id, $shipment_status_id)?'checked':''}}
                        value="{{$item->id}}" name="shipment_status_id[]"
                        id="shipment_status_id_{{$item->id}}" class="auto-submit-item">

                    <div class="state p-success">
                        <i class="icon fa fa-check"></i>
                        <label for="shipment_status_id_{{$item->id}}">{{$item->name}}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
