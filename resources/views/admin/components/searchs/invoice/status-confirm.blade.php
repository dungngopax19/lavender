@php
    $status_confirm_orders = [
        ['key'=>'-1', 'name'=>'Không duyệt'],
        ['key'=>'0', 'name'=>'Đang chờ'],
        ['key'=>'1', 'name'=>'Đã duyệt']
    ];
    $status_confirm_order = Request::get('status_confirm_order')??[];
@endphp

<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-status-order">
            <a href="javascript:;">Trạng thái xác nhận</a>
            <a href="javascript:;" class="float-right">
                @if(count($status_confirm_order))
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-status-order" class="panel-collapse collapse show">
        <div class="panel-body">
            @foreach ($status_confirm_orders as $i => $item)
                <div class="pretty p-icon p-smooth mt-3 d-block">
                    <input type="checkbox"
                        {{in_array($item['key'], $status_confirm_order)?'checked':''}}
                        value="{{ $item['key']}}" name="status_confirm_order[]"
                        id="status_confirm_order_{{ $item['key']}}" class="auto-submit-item">

                    <div class="state p-success">
                        <i class="icon fa fa-check"></i>
                        <label for="status_confirm_order_{{ $item['key']}}">{{$item['name']}}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
