@php
    use App\Models\OrderStatus;
    $status = OrderStatus::where('type', 'invoices')->where('activity_status', 1)->orderBy('id', 'asc')->get();
    $invoices_status = Request::get('invoices_status')??[];
@endphp

<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-order-status">
            <a href="javascript:;">Trạng thái Đơn hàng</a>
            <a href="javascript:;" class="float-right">
                @if(count($invoices_status))
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-order-status" class="panel-collapse collapse @if(count($invoices_status)) show @endif">
        <div class="panel-body">
            @foreach ($status as $i => $item)
                <div class="pretty p-icon p-smooth mt-3 d-block">
                    <input type="checkbox"
                        {{in_array($item->id, $invoices_status)?'checked':''}}
                        value="{{$item->id}}" name="invoices_status[]"
                        id="invoices_status_{{$item->id}}" class="auto-submit-item">

                    <div class="state p-success">
                        <i class="icon fa fa-check"></i>
                        <label for="invoices_status_{{$item->id}}">{{$item->name}}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
