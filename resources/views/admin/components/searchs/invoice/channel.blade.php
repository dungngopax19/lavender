@php
    $channel = intval(Request::get('channel'));
    $data = [
        (object)['id'=> 1, 'name'=>'Bán trực tiếp'],
        (object)['id'=> 2, 'name'=>'Facebook'],
        (object)['id'=> 3, 'name'=>'Khác']
    ];
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-channel">
            <a href="javascript:;">Kênh bán</a>
            <a href="javascript:;" class="float-right">
                @if($channel)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-channel" class="panel-collapse collapse @if($channel) show @endif">
        <div class="panel-body">
            <div class=" mt-3">
                <select class="select2 form-control" name="channel">
                    <option value="0">---</option>
                    @foreach ($data as $i => $item)
                    <option {{ ($item->id == $channel)? 'selected' : ''}} value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
