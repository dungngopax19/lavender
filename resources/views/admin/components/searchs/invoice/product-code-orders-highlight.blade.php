@php
    use App\Helpers\Invoice as InvoiceHelper;
    $codeProducts = InvoiceHelper::productCodeOrderHighlight();
    $totalQty = 0;

    $activeMenu = request()->highlight || request()->code_product_order_highlight;

@endphp

@if ($codeProducts)
    
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-product-code-orders-highlight">
            <a href="javascript:;">Mã sản phẩm ưu tiên</a>
            <a href="javascript:;" class="float-right">
                @if($activeMenu)
                    <i class="fa fa-chevron-circle-up"></i>
                @else
                    <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-product-code-orders-highlight" class="panel-collapse collapse @if($activeMenu) show @endif">
        <input type="text" class="form-control mt-3 auto-submit-item" name="code_product_order_highlight" value="{{request()->code_product_order_highlight}}" placeholder="Tìm theo mã hàng" style="font-size: 13px;">

        @if (request()->highlight)
            <input type="hidden" name="highlight" value="1">
        @endif

        <div class="panel-body" style="max-height:500px; overflow-y: scroll;">
            
            @foreach ($codeProducts as $i => $item)
                @php
                    $totalQty += $item->totalQty;
                @endphp
                <div class="mt-3 d-block">
                    <div class="state p-success">
                        <a href="{{url('admin/transport-order?highlight=1&id_product_order_highlight='.$item->product['id'].'&code_product_order_highlight=' . $item->product['code'])}}">
                            <label style="font-size: 13px;">
                                {{$item->product['code']}} <br/>
                                <b>({{$item->totalQty}}SP) - ({{$item->totalOrders}}ĐH)</b>
                            </label>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="mt-3"><b>Tổng cộng: ({{number_format($totalQty)}}SP)</b></div>
    </div>
</div>

@endif