<div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-keyword">Tìm kiếm</a>
            </h6>
        </div>

        <div id="collapse-keyword" class="panel-collapse collapse show">
            <div class="panel-body">
                <input type="text" class="form-control mt-3 auto-submit-item" name="filter_invoice" value="{{Request::get('filter_invoice')}}" placeholder="Theo mã Đơn hàng">
                <input type="text" class="form-control mt-2 auto-submit-item" name="filter_customer" value="{{Request::get('filter_customer')}}" placeholder="Theo thông tin khách hàng">
                <input type="text" class="form-control mt-2 auto-submit-item" name="note" value="{{Request::get('note')}}" placeholder="Theo ghi chú">
            </div>
        </div>
    </div>
