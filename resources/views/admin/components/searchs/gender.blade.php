@php
    $gender = intval(Request::get('gender'));
    $data = config('constants.genders');
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-customer-gender">
            <a href="javascript:;">Giới tính</a>
            <a href="javascript:;" class="float-right">
                @if($gender)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-customer-gender" class="panel-collapse collapse @if($gender) show @endif">
        <div class="panel-body">
            @foreach ($data as $i => $item)
                <div class="pretty p-default p-round mt-3 d-block">
                    <input type="radio" value="{{$i}}" id="gender-{{$i}}" {{$gender == $i ? 'checked' : ''}} name="gender" class="auto-submit-item">
                    <div class="state">
                        <label for="gender-{{$i}}">{{$item}}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
