@php
    use Spatie\Permission\Models\Role;
    $roles = Role::where('activity_status', 1)->orderBy('id', 'desc')->get();
    $role_id = Request::get('role_id')??[];
@endphp

<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-role">
            <a href="javascript:;">Nhóm người dùng</a>
            <a href="javascript:;" class="float-right">
                @if(count($role_id))
                    <i class="fa fa-chevron-circle-up"></i>
                @else
                    <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-role" class="panel-collapse collapse @if(count($role_id)) show @endif">
        <div class="panel-body">
            @foreach ($roles as $i => $item)
                <div class="pretty p-icon p-smooth mt-3">
                    <input type="checkbox"
                        {{in_array($item->id, $role_id)?'checked':''}}
                        value="{{$item->id}}" name="role_id[]"
                        id="role_id_{{$item->id}}" class="auto-submit-item">

                    <div class="state p-success">
                        <i class="icon fa fa-check"></i>
                        <label for="role_id_{{$item->id}}">{{$item->name}}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
