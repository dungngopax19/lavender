@php
    $inventory_status = intval(Request::get('inventory_status'));
    $data = config('constants.inventory_status');
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-inventory-status">
            <a href="javascript:;">Tồn kho</a>
            <a href="javascript:;" class="float-right">
                @if($inventory_status)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-inventory-status" class="panel-collapse collapse @if($inventory_status) show @endif">
        <div class="panel-body">
            @foreach ($data as $i => $item)
                <div class="pretty p-default p-round mt-3">
                    <input type="radio" value="{{$i}}" id="inventory_status-{{$i}}" {{$inventory_status == $i ? 'checked' : ''}} name="inventory_status" class="auto-submit-item">
                    <div class="state">
                        <label for="inventory_status-{{$i}}">{{$item}}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
