<div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#search-name">Tìm kiếm</a>
            </h6>
        </div>

        <div id="search-name" class="panel-collapse collapse show">
            <div class="panel-body">
                <input type="text" class="form-control mt-3 auto-submit-item" name="keyword" value="{{Request::get('keyword')}}" placeholder="Theo mã, loại thu">
            </div>
        </div>
    </div>
