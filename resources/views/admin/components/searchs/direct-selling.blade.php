@php
    $direct_selling = intval(Request::get('direct_selling'));
    $data = config('constants.direct_selling');
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-direct-selling">
            <a href="javascript:;">Bán trực tiếp</a>
            <a href="javascript:;" class="float-right">
                @if($direct_selling)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-direct-selling" class="panel-collapse collapse @if($direct_selling) show @endif">
        <div class="panel-body">
            @foreach ($data as $i => $item)
                <div class="pretty p-default p-round mt-3">
                    <input type="radio" value="{{$i}}" id="direct_selling-{{$i}}" {{$direct_selling == $i ? 'checked' : ''}} name="direct_selling" class="auto-submit-item">
                    <div class="state">
                        <label for="direct_selling-{{$i}}">{{$item}}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
