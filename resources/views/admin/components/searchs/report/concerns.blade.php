@php
    use App\Models\OrderStatus;
    $status = OrderStatus::where('type', 'invoices')->where('activity_status', 1)->orderBy('id', 'asc')->get();
    $invoices_status = Request::get('invoices_status')??[];
    $concerns = [
        ['link'=>'sell', 'name'=>'Bán hàng'],
        ['link'=>'product', 'name'=>'Hàng hóa'],
        ['link'=>'all', 'name'=>'Tổng hợp'],
    ]
@endphp

<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-order-concerns">
            <a href="javascript:;">Mối quan tâm</a>
            <a href="javascript:;" class="float-right">
                @if(count($invoices_status))
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-order-concerns" class="panel-collapse collapse show">
        <div class="panel-body">
            @foreach ($concerns as $i => $item)
                <div class="pretty p-icon p-smooth mt-3 d-block">
                    <a href="{{url('admin/reports/end-day/sell?created_range=')}}{{ date('d-m-Y')}}+->+{{ date('d-m-Y')}}">
                        <p>{{$item['name']}}</p>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
