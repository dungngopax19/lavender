@php
    use App\User;
    $data = User::orderBy('name', 'asc')->get();
    $user_id = Request::get('user_id')??[];
@endphp

<div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-func">
                <a href="javascript:;">Chức năng</a>
                <a href="javascript:;" class="float-right">
                    @if($user_id)
                    <i class="fa fa-chevron-circle-up"></i>
                    @else
                    <i class="fa fa-chevron-circle-down"></i>
                    @endif
                </a>
            </h6>
        </div>

        <div id="search-func" class="panel-collapse collapse @if($user_id) show @endif">
            <div class="panel-body">
                <div class=" mt-3">
                    {{--  <select class="select2 form-control" multiple="multiple" name="user_id[]">
                        <option value="0">---</option>
                        @foreach ($data as $i => $item)
                        <option {{ (in_array($item['id'], $user_id))? 'selected' : ''}} value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>  --}}
                </div>
            </div>
        </div>
    </div>
