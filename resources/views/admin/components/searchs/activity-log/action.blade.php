@php
    $data = config('activitylog.actions');
    $action = Request::get('action')??[];
@endphp

<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-action">
            <a href="javascript:;">Thao tác</a>
            <a href="javascript:;" class="float-right">
                @if(count($action))
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-action" class="panel-collapse collapse @if(count($action)) show @endif">
        <div class="panel-body">
            @foreach ($data as $key => $item)
                <div class="pretty p-icon p-smooth mt-3 d-block">
                    <input type="checkbox"
                        {{in_array($key, $action)?'checked':''}}
                        value="{{$key}}" name="action[]"
                        id="action_{{$key}}" class="auto-submit-item">

                    <div class="state p-success">
                        <i class="icon fa fa-check"></i>
                        <label for="action_{{$key}}">{{$item}}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
