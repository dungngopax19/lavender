<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#search-name">Tìm kiếm</a>
        </h6>
    </div>

    <div id="search-name" class="panel-collapse collapse show">
        <div class="panel-body">
            <input type="text" class="form-control mt-3 auto-submit-item" name="id_code_name" value="{{Request::get('id_code_name')}}" placeholder="Theo ID, mã, tên khách hàng">
            <input type="text" class="form-control mt-2 auto-submit-item" name="phone" value="{{Request::get('phone')}}" placeholder="Theo số điện thoại">
            <input type="text" class="form-control mt-2 auto-submit-item" name="tax_code" value="{{Request::get('tax_code')}}" placeholder="Theo mã số thuế">
            <input type="text" class="form-control mt-2 auto-submit-item" name="email" value="{{Request::get('email')}}" placeholder="Theo email">
            <input type="text" class="form-control mt-2 auto-submit-item" name="text" value="{{Request::get('text')}}" placeholder="Theo ghi chú">
        </div>
    </div>
</div>
