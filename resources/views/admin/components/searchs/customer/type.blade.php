@php
    use App\Models\CustomerTypes;

    $type = intval(Request::get('customers_types_id'));
    $data = CustomerTypes::orderBy('name')->select('id', 'name')->get();
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#search-customer-type">
            <a href="javascript:;">Nhóm khách hàng</a>
            <a href="javascript:;" class="float-right">
                @if($type)
                <i class="fa fa-chevron-circle-up"></i>
                @else
                <i class="fa fa-chevron-circle-down"></i>
                @endif
            </a>
        </h6>
    </div>

    <div id="search-customer-type" class="panel-collapse collapse show">
        <div class="panel-body">
            <div class=" mt-3">
                <select class="select2 form-control" name="customers_types_id">
                    <option value="0">---</option>
                    @foreach ($data as $i => $item)
                    <option {{ ($item->id == $type)? 'selected' : ''}} value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
