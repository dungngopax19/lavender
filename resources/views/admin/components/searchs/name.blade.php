<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-name">Tìm kiếm</a>
        </h6>
    </div>

    <div id="collapse-name" class="panel-collapse collapse show">
        <div class="panel-body mt-3">
            <input type="text" class="form-control" name="name" value="{{Request::get('name')}}" placeholder="Tìm theo từ khoá">
        </div>
    </div>
</div>
