<table class="table table-hover mb-0">
    @if (count($records) > 0)
        @foreach ($records as $i => $item)
            <tr>
                <td>
                    {{$loop->iteration + ($records->currentPage() - 1)*$records->perPage()}}. 
                    {{$item->name}} - {{$item->code}} - 

                    @if ($type == 5)
                        {{ number_format($item->price)}}đ
                    @else
                        {{ $item->countOrders}} lần    
                    @endif
                </td>
            </tr>
        @endforeach
    @else
        <tr class="text-center ">
            <td>Không có dữ liệu</td>
        </tr>
    @endif
</table>
