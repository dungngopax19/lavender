@php
    use App\Models\Branches;
    use App\Helpers\Permissions;

    $seg2 = Request::segment(2);
    $branches = Branches::orderBy('name')->get();
    $branchActive = json_decode($_COOKIE['branch_active']);
    $cartActive = Request::has('gh') ? Request::get('gh') : 1;
@endphp

<header>
    <div class="top">
        <div class="container-fluid">
            <nav class="navbar">
                <a class="navbar-brand" href="{{url('admin/dashboard')}}">
                    <img src="{{asset('images/logo.png')}}" alt="">
                </a>

                <ul class="nav">

                    @asyncWidget('Admin\Inventory')

                    {{-- @asyncWidget('Admin\Message', ['user_receive_id'=>Auth::user()->id]) --}}

                    <li class="nav-item dropdown ml-3">
                        <a class="nav-link">
                            Giỏ hàng {{$cartActive}} <i class="fa fa-caret-down"></i>
                        </a>
                        <div class="dropdown-content">
                            @for ($i = 1; $i <= 10; $i++)
                                <a target="_blank" data-id="{{$i}}" href="?gh={{$i}}" class="cart-number {{($cartActive == $i) ? 'active' : ''}}">Giỏ hàng {{$i}}</a>
                            @endfor
                        </div>
                    </li>

                    <li class="nav-item dropdown ml-3">
                        <a class="nav-link">
                            {{$branchActive->name}} <i class="fa fa-caret-down"></i>
                        </a>
                        <div class="dropdown-content">
                            @foreach ($branches as $item)
                                <a data-name="{{$item->name}}" data-id="{{$item->id}}" href="javascript:void(0)" class="change-branch {{($branchActive->id == $item->id) ? 'active' : ''}}">{{$item->name}}</a>
                            @endforeach
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link">{{Auth::user()->name}} <i class="fa fa-caret-down"></i></a>
                        <div class="dropdown-content menu-right">
                            <a data-action="users/{{Auth::user()->id}}" data-target="#modal-edit-user" class="ajax-modal" href="javascript:void(0)"><i class="fas fa-unlock-alt"></i> Thông tin cá nhân</a>
                            <a href="{{route('admin.logout')}}"><i class="fas fa-external-link-alt"></i> Đăng xuất</a>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    @component('admin.components.main-menu')@endcomponent
</header>
