
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../libs');
require('../common');
require('./product');
require('./cart');

$(document).on('keyup',function(evt) {
    if (evt.keyCode == 27) {
        $('.modal').modal('hide');
    }
});