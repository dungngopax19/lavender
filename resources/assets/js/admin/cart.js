(function() {

    //Add item product to cart
    $(document).on("click", ".add-to-cart",function(e){
        event.stopPropagation();
        var id = parseInt($(this).data('id'));
        var type = $(this).data('type');
        updateItemCart(id, 1, null, type);
    });

    //Remove item product from cart
    $(document).on("click", ".remove-item-cart",function(e){
        var uniqueId = $(this).data('uniqueid');
        var id = parseInt($(this).data('id'));
        var type = $(this).data('type');
        var typeTxt = (type == 'orders') ? 'phiếu đặt hàng' : 'hoá đơn';

        if(confirm('Bạn có thật sự muốn xoá sản phẩm này khỏi ' + typeTxt +' ?')){
            updateItemCart(id, 0, uniqueId, type);
        }

        return false;
    });

    //Remove item product from cart in modal create PDH/HOA DON
    $(document).on("click", ".remove-item-cart-modal",function(e){
        var objTrParent = $(this).parent().parent();
        updateItemCartModal(objTrParent, true);
        return false;
    });

    //Clear all cart
    $(document).on("click", ".btn-clear-cart",function(e){
        var type = $(this).data('type');
        var cartNumber = $(this).data('cartnumber');
        var typeTxt = (type == 'orders') ? 'phiếu đặt hàng' : 'hoá đơn';

        if(confirm('Bạn có thật sự muốn xoá ' + typeTxt + ' không?')){
            $(this).submitDataAjax({
                'url' : site_url + 'clear-cart',
                'method': 'POST',
                'data' : { _token : CSRF_TOKEN, type : type, cartNumber : cartNumber },
                'success': function(res){

                    $.notify({
                        message: typeTxt + ' của bạn đã được xoá thành công.'
                    },{
                        type: 'success',
                        delay: 1000,
                        placement: {
                            from: "bottom"
                        },
                    });

                    if(type == 'orders'){
                        $('.wrap-hover-orders').html(res.htmlCart);
                    }else{
                        $('.wrap-hover-invoices').html(res.htmlCart);
                    }
                }
            });
        }
    });

    //Cart in modal detail cart
    $(document).on("keypress", ".modal-cart .qty-item-cart, .modal-cart .discount-item-cart input[name=discount], .price-item-cart", function(e){
        if(e.which == 13) {
            var objTrParent = $(this).parent().parent();
            updateItemCartModal(objTrParent);
            return false;
        }
    });

    //Change button discount unit
    $(document).on("click", ".btn-discount", function(){
        var objParent = $(this).parent();
        objParent.find('.btn-discount').removeClass('btn-info');
        $(this).addClass('btn-info');

        let discount_unit = $(this).data('unit');
        objParent.find('input[name=discount_unit]').val(discount_unit);

        if( objParent.hasClass('discount-total-cart') ) {
            calculatePayment();
        }else{
            var objTrParent = $(this).parent().parent();
            updateItemCartModal(objTrParent);
        }
    });

    //Nếu trạng thái giao hàng là "giao hàng công" "đang giao hàng" hoặc "hoàn thành" thì k chọn được "Ưu tiên"
    $(document).on("change", "select[name=shipment_status_id]", function(){
        var shipment_status_id = parseInt($(this).val());
        var ids = [2,4,7];

        if( ids.indexOf(shipment_status_id) > -1 ){
            $('select[name=highlight]').val(0);
            $('select[name=highlight]').select2();
            
        }
    });

    //Change discount price total cart / deposit price
    $(document).on("keypress", ".discount-total-cart input[name=discount], input[name=deposit], input[name=ship]", function(e){
        if(e.which == 13) {
            var value = numeral($(this).val()).format('0,0');
            $(this).val(value);
            calculatePayment();

            // set value for print order
            $('.name-discount').html( value );
        }
    });

    //Change discount coupon
    $(document).on("keypress", "input[name=coupon]", function(e){
        if(e.which == 13) {
            var coupon = $(this).val();

            $(this).submitDataAjax({
                'url' : site_url + 'validate-coupons',
                'method': 'POST',
                'data' : {
                    _token : CSRF_TOKEN,
                    coupon : coupon,
                    membership_id : $('input[name=membership_id]').val(),
                },
                'success': function(res){
                    $('.msg-coupon').html(res.msg);
                    $('input[name=discount_coupon]').val(res.discount_coupon);
                    calculatePayment();
                }
            });
        }
    });

    //Change option discount gift
    $(document).on("change", "select#option-gift-discount", function(e){

        let opt = $(this).val();
        if(opt == 2 && $('ul.gift-discount').length > 0){
            $('ul.gift-discount').show();
            $('#discountGiftPrice').val(0);
            $('#discountGiftUnit').val('vnd');
        }else{
            $('ul.gift-discount').hide();
            $('#discountGiftPrice').val( $(this).data('discount-gift') );
            $('#discountGiftUnit').val( $(this).data('discount-gift-unit') );
        }

        calculatePayment();
    });

    //Submit create orders / invoices
    $(document).on("click", ".btn-submit-orders", function(e){

        //Tao va ket thuc don hang
        if($(this).hasClass('btnCreateEndOrders')){
            //Nếu đã chọn thời gian giao hàng thì không được submit button "Tao va ket thuc don hang"

            var delivery_time = $('input[name=delivery_time]').val();
            var againPay = $('input[name=againPay]').val();

            if(delivery_time){
                alert('Bạn không thể tạo và kết thúc đơn hàng khi đã chọn thời gian giao hàng');
                return false;
            }else if(againPay != 0){
                alert('Bạn không thể tạo và kết thúc đơn hàng khi khách hàng chưa thanh toán hết tiền hàng');
                return false;
            }

            $('input[name=create_end_invoices]').val(1);
        }else{
            $('input[name=create_end_invoices]').val(0);
        }

        //Tao don hang tu phieu dat hang
        if($(this).hasClass('btnCreateOrdersFromDrafts')){
            $('input[name=is_invoices]').val(1);
        }

        $(this).submitFormAjax({
            'form' : $( $(this).data('form-target') ),
            'method' : $(this).data('form-method'),
            'error' : $(this).data('callback-error'),
            'success' : function(res){

                if(res.htmlPrint){
                    $('.wrap-print').html(res.htmlPrint);
                }
                
                if(res.status == 500){
                    $.notify({
                        title: '<div><strong>'+ res.title +'</strong></div>',
                        message: res.msg
                    },{
                        type: res.type,
                        delay: 5000,
                        placement: {
                            from: "bottom"
                        },
                    });
                }else{
                    alert(res.data);
                    $('.ajax-print').trigger('click');
                    location.reload();
                }
            }
        });
    });

    //Update discount note
    $(document).on("keypress", ".update-discount-note", function(e){
        
        if(e.which == 13) {
            $(this).submitDataAjax({
                'url' : site_url + 'update-discount-note',
                'method': 'POST',
                'data' : {
                    _token : CSRF_TOKEN,
                    orders_id : $(this).data('orders-id'),
                    products_id : $(this).data('products-id'),
                    discountNote : $(this).val(),
                    type : $(this).parent().parent().data('type'),
                    uniqueId : $(this).parent().parent().data('uniqueid'),
                    cartNumber : $('.cart-number.active').data('id')
                },
                'success': function(res){
                    $.notify({
                        message: res.msg
                    },{
                        type: res.type,
                        delay: 1000,
                        placement: {
                            from: "bottom"
                        },
                    });
                }
            });
        }
    });

})();

//Functions utility
var timeout = null;
function updateItemCart(id, qty = 1, uniqueId, type){
    clearTimeout(timeout);

    $(this).submitDataAjax({
        'url' : site_url + 'add-to-cart',
        'method': 'POST',
        'data' : {
            _token : CSRF_TOKEN,
            id : id,
            qty : qty,
            uniqueId : uniqueId,
            type: type,
            cartNumber : $('.cart-number.active').data('id')
        },
        'success': function(res){
            $.notify({
                title: '<div><strong>'+ res.title +'</strong></div>',
                message: res.msg
            },{
                type: res.type,
                delay: 1000,
                placement: {
                    from: "bottom"
                },
            });

            if(type == 'orders'){
                $('.wrap-hover-orders').html(res.htmlCart);

                if(res.type == 'success'){
                    $('.box-dropdown-orders').css('display', 'block');
                    timeout = setTimeout(function() {
                        $('.box-dropdown-orders').css('display', '');
                    }, 3000);
                }
            }else{
                $('.wrap-hover-invoices').html(res.htmlCart);

                if(res.type == 'success'){
                    $('.box-dropdown-invoices').css('display', 'block');
                    timeout = setTimeout(function() {
                        $('.box-dropdown-invoices').css('display', '');
                    }, 3000);
                }
            }

            if($(window).scrollTop() > 500){
                if($('.box-dropdown-orders').find('.item-cart').length > 0){
                    $('.box-dropdown-orders').addClass('position-fixed-orders');
                }

                if($('.box-dropdown-invoices').find('.item-cart').length > 0){
                    $('.box-dropdown-invoices').addClass('position-fixed-invoices');
                }
            }
        }
    });
}

function updateItemCartModal(objTrParent, remove = false){

    if(remove){
        var qty = 0;
    }else{
        var qty = numeral(objTrParent.find('.qty-item-cart').val()).format('0');
    }

    var customPrice = numeral(objTrParent.find('.price-item-cart').val()).format('0');
    var discount = numeral(objTrParent.find('input[name=discount]').val()).format('0');

    var discountUnit = objTrParent.find('.btn-discount.btn-info').val();

    var type = objTrParent.data('type');
    var id = objTrParent.data('id');
    var uniqueid = objTrParent.data('uniqueid');
    var inventory = objTrParent.data('inventory');

    var typeTxt = (type == 'orders') ? 'phiếu đặt hàng' : 'hoá đơn';

    if(qty == 0){
        if(confirm('Bạn có thật sự muốn xoá sản phẩm này trong ' + typeTxt +' ?')){
            updateModalCart(id, qty, uniqueid, customPrice, discount, discountUnit, type, typeTxt);
        }
    }else{

        //Tạm thời sẽ không giới hạn số lượng sản phẩm đặt hàng
        if(type == 'invoices'){
            if( inventory < qty ){
                if(inventory <= 0){
                    alert('Hiện tại sản phẩm này đã hết hàng');
                }else{
                    alert('Bạn chỉ có thể nhập tối đa ' + inventory + ' sản phẩm');
                }
                
                qty = inventory;
            }
        }

        $(this).val(qty);

        if(confirm('Bạn có thật sự muốn cập nhật sản phẩm này trong ' + typeTxt +' ?')){
            updateModalCart(id, qty, uniqueid, customPrice, discount, discountUnit, type, typeTxt);
        }
    }
}

function updateModalCart(id, qty, uniqueId, customPrice, discount, discountUnit, type, typeTxt){

    $(this).submitDataAjax({
        'url' : site_url + 'add-to-cart',
        'method': 'POST',
        'data' : {
            _token : CSRF_TOKEN,
            id : id,
            qty : qty,
            uniqueId : uniqueId,
            customPrice : customPrice,
            discount : discount,
            discountUnit : discountUnit,
            type: type,
            updateQty : true,
            cartNumber : $('.cart-number.active').data('id')
        },
        'success': function(res){

            $.notify({
                title: '<div><strong>'+ res.title +'</strong></div>',
                message: res.msg
            },{
                type: res.type,
                delay: 1000,
                placement: {
                    from: "bottom"
                },
            });

            if(res.type == 'success'){
                if(type == 'orders'){
                    $('.wrap-hover-orders').html(res.htmlCart);
                }else{
                    $('.wrap-hover-invoices').html(res.htmlCart);
                }
    
                $('.html-cart-modal').html(res.htmlCartModal);
                $('.html-cart-print').html(res.htmlCartPrint);
    
                $('.total-price-cart').html(res.totalPrice);
                calculatePayment();
                searchAutoProduct();
            }
        }
    });
}

function calculatePayment(){
    var discountTotal = 0;

    var totalPrice = document.getNumber( $('.total-price-cart').html() );

    var discount = document.getNumber( $('.discount-total-cart input[name=discount]').val() );
    var deposit = document.getNumber( $('input[name=deposit]').val() );
    var ship = document.getNumber( $('input[name=ship]').val() );
    var discountCoupon = document.getNumber( $('input[name=discount_coupon]').val() );
    var unitDiscount = $('.discount-total-cart .btn-discount.btn-info').val();

    if( unitDiscount == 'vnd' ){
        var needPay = totalPrice + ship - discount - discountCoupon;
    }else{
        var needPay = totalPrice + ship - (totalPrice * discount / 100) - discountCoupon;
    }

    var againPay = needPay - deposit;

    //Check option discount gift
    var optDiscountGift = $("select#option-gift-discount").val();
    if( optDiscountGift == 1 ){
        var discountGiftPrice = parseInt($('#discountGiftPrice').val());
        var discountGiftUnit = $('#discountGiftUnit').val();

        if( discountGiftUnit == 'vnd' ){
            againPay = againPay - discountGiftPrice;
            needPay = needPay - discountGiftPrice;
        }else{
            againPay = againPay - againPay * discountGiftPrice / 100;
            needPay = needPay - needPay * discountGiftPrice / 100;
        }
    }

    againPay = (againPay < 0) ? 0 : againPay;
    
    $('input[name=needpay]').val( numeral(needPay).format('0,0') );
    $('input[name=againPay]').val( numeral(againPay).format('0,0') );

    discountTotal = totalPrice - needPay;

    //Set value prices for printer
    $('.print-ship').html( $('input[name=ship]').val() );
    $('.print-price').html( $('.total-price-cart').html() );
    $('.print-discount').html( numeral(discountTotal).format('0,0') );
    $('.print-deposit').html( $('input[name=deposit]').val() );

    $('.print-againPay').html( $('input[name=againPay]').val() );

    if(totalPrice == 0){
        $('.btn-submit-orders').hide();
    }else{
        if(againPay < 0){
            alert('Bạn vui lòng kiểm tra lại các khoản chi phí');
            $('.btn-submit-orders').hide();
            $('input[name=needpay], input[name=againPay]').val(0);
        }else{
            $('.btn-submit-orders').show();
        }
    }

}

window.calculatePaymentPrice = calculatePayment;

function searchAutoProduct(){
    $(".search-product-component").select2({
        ajax: {
            url: 'search-products-cart',// 'search-products',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var query = {
                    q: params.term,
                }
                return query;
            },
            processResults: function (res) {
                return {
                    results: res.records
                };
            }
        },
        templateResult: function($item){
            var $item = $(
                '<div>'+$item.code+'</div>'
            );
            return $item;
        }
    }).on("select2:select", function (e) {
        let id = parseInt(e.params.data.id);
        addItemProductToCart(id);
    });
}
window.searchAutoProduct = searchAutoProduct;