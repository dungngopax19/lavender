(function() {

    // Update SL ước tính(bộ)
    $(document).on("change", "#form-edit-product select[name=cloth_template], #form-edit-product input[name=meter_number]", function (e) {
        var cloth_template = $('#form-edit-product select[name=cloth_template]').val();
        var meter_number = parseInt($('#form-edit-product input[name=meter_number]').val());
        var number_of_sets = 0;

        if( meter_number >= 0 && !isNaN(meter_number) ){
            if(cloth_template == '1.6'){
                number_of_sets = Math.floor(meter_number / 14);
            }else if(cloth_template == '2.5'){
                number_of_sets = Math.floor(meter_number / 11);
            }
            number_of_sets = isNaN(number_of_sets) ? 0 : number_of_sets;
        }

        $('#form-edit-product input[name=number_of_sets]').val(number_of_sets);
    });

    //Change product type
    $(document).on("change", "#form-edit-product select[name=product_types_id]", function (e) {
        let id = $(this).val();

        if(id == 1){//Combo - Đóng gói
            $('li.tab-component').show();
            $('.stock-product').hide();

            var inventoryCombo = 0;
            if($('input[name=inventory_list_comps]').length > 0){
                var inventoryCombo = $('input[name=inventory_list_comps]').val();
            }
            $('#inventoryCombo').html(inventoryCombo);
        }else{
            $('li.tab-component').hide();
            $('.stock-product').show();

            var inventoryStock = 0;
            $('#modal-edit-product .inventory-stock').each(function(e){
                inventoryStock += parseInt($(this).val());
            });
            $('#inventoryCombo').html(inventoryStock);
        }
    }); 
    
    //Submit form save product
    $(document).on("click", ".btn-submit-edit-product",function(e){

        var productType = $('#modal-edit-product select[name=product_types_id]').val();

        if(productType == 1){//Combo
            var inventoryStock = 0;
            $('#modal-edit-product .inventory-stock').each(function(e){
                inventoryStock += parseInt($(this).val());
            });

            var inventoryComps = parseInt($('input[name=inventory_list_comps]').val()) || 0;

            if(inventoryStock != inventoryComps){
                $('.tab-inventory a').tab('show');
                alert('Dữ liệu tồn kho không chính xác. Bạn vui lòng kiểm tra lại số liệu tồn kho của các chi nhánh');
                return false;
            }
        }

        //Kiểm tra 1 sản phẩm phải thuộc tối thiểu 1 chi nhánh
        /*var chkInBranch = 0;
        $('#modal-edit-product .in-branch-stock').each(function(e){
            var id = parseInt($(this).val());
            if( id > 0 ){
                chkInBranch = id;
            }
        });
        if(chkInBranch < 1){
            alert('Sản phẩm này phải thuộc tối thiểu 1 chi nhánh');
            return false;
        }*/

        $(this).submitFormAjax({
            'form' : $( $(this).data('form-target') ),
            'method' : $(this).data('form-method'),
            'error': $(this).data('callback-error'),
            'success': $(this).data('callback-success'),
            'autoCloseModal': $(this).data('auto-close-modal'),
        });
    });

    $(document).on("change", "#modal-edit-product .in-branch-stock", function (e) {
        var id = $(this).val();
        
        var cardBody = $(this).parents('.card-body');
        if(id == 1){
            cardBody.find('input.inventory-stock').removeAttr('readonly', 'readonly');
            cardBody.find('input.min-inventory-stock').removeAttr('readonly', 'readonly');
        }else{
            cardBody.find('input.inventory-stock').attr('readonly', 'readonly').val(0);
            cardBody.find('input.min-inventory-stock').attr('readonly', 'readonly');
        }
    });

    $(document).on("click", ".btn_link_thumbnail",function(e){
        $('.wrap_link_thumbnail').toggle();
    });

})();