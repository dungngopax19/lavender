$( ".box-status .checkbox label" ).on('change', function() {
    var values = $('.box-status [type="checkbox"]:checked').map(function () {
        return this.value;
    }).get();
    $('input[name=tt_giao_hang]').val( values.join('_') );

    $('#form-filter').submit();
});

$( ".box-order-close .checkbox label" ).on('change', function() {
    var values = $('.box-order-close [type="checkbox"]:checked').map(function () {
        return this.value;
    }).get();
    $('input[name=orderClose]').val( values.join('_') );

    $('#form-filter').submit();
});

$( ".box-order-over .checkbox label" ).on('change', function() {
    var values = $('.box-order-over [type="checkbox"]:checked').map(function () {
        return this.value;
    }).get();
    $('input[name=orderOver]').val( values.join('_') );

    $('#form-filter').submit();
});

$(document).ready(function() {
    $('.select2').select2();

    $('.delete-cloth').click(function(){
        if(confirm("Bạn có thật sự muốn xoá item này không?")){
            var id = $(this).data('id');

            $.ajax({
                url: '/kho-vai/' + id,
                type: "POST",
                dataType: "json",
                data: {
                    _method: 'DELETE',
                    _token: CSRF_TOKEN
                },
                success: function(data){
                    location.reload();
                }
            });
        }
    });

    $('.view-image').click(function(){
        $('#view-image img').attr('src', $(this).data('url'));
        $('#view-image').modal('show');
    });
});