(function() {

    //Init
    $('[data-toggle="tooltip"]').tooltip(); 
    $('.select2').select2();
    $('.select2-disabled').select2({disabled : true});

    $('.daterange-picker').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });

    $('.daterange-picker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' -> ' + picker.endDate.format('DD-MM-YYYY'));

        if($(this).hasClass('auto-submit-item')){
            $("form.auto-submit").submit();
        }
    });

    $('.daterange-picker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    //Click element tr table
    $(document).on("click", "table.list-item tr",function(e){
        $(this).find('.btn-edit').trigger('click');
    });

    $('.singleDatePicker').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        }
    });

    //Change branch
    $(document).on("click", ".change-branch",function(e){
        var id = $(this).data('id');
        var name = $(this).data('name');

        if( confirm('Bạn có muốn chuyển sang chi nhánh ' + name + '?') ){
            $(this).submitDataAjax({
                'url' : site_url + 'change-branch',
                'method': 'POST',
                'data' : {
                    '_token' : CSRF_TOKEN,
                    'id' : id
                },
                'success': function(res){
                    if(res.st == 200){
                        location.reload();
                    }else{
                        alert(res.msg);
                    }
                }
            });
        }
    });

    //Submit form ajax
    $(document).on("click", ".btn-submit",function(e){
        $(this).submitFormAjax({
            'form' : $( $(this).data('form-target') ),
            'method' : $(this).data('form-method'),
            'error': $(this).data('callback-error'),
            'success': $(this).data('callback-success'),
            'autoCloseModal': $(this).data('auto-close-modal'),
        });
    });

    //Auto submit form
    $(".auto-submit-item").change(function() {
        $("form.auto-submit").submit();
    });

    //Submit form ajax
    $(document).on("click", ".ajax-modal",function(e){
        e.stopPropagation();

        var modal = $(this).data('target');

        $(this).submitDataAjax({
            'url' : site_url + $(this).data('action'),
            'method': 'GET',
            'data' : $(this).data('params'),
            'success': function(res){
                $('.modal').modal('hide');
                $(modal).find('.modal-content').html(res.html);
                $('.select2').select2();

                $('.select2-disabled').select2({
                    disabled: true
                });

                setTimeout(function() {
                    $(modal).modal();
                }, 500);
            }
        });
    });

    //Confirm delete
    $('.delete-item').click(function(event){
        event.stopPropagation();
        
        if(confirm("Bạn có thật sự muốn xoá item này không?")){
            var id = $(this).data('id');

            $(this).submitDataAjax({
                'url' : site_url + $(this).data('action'),
                'method': 'POST',
                'data' : { _method: 'DELETE', _token: CSRF_TOKEN },
                'success': function(res){
                    location.reload();
                }
            });
        }
    });

    $('.table-tree-view .toogle').click(function(){
        var objI = $(this).find('i.fas');
        if(objI.hasClass('fa-plus')){
            $('.bg-white').hide();
            $('.bg-gray .fas').removeClass('fa-minus').addClass('fa-plus');

            objI.removeClass('fa-plus').addClass('fa-minus');
        }else{
            objI.removeClass('fa-minus').addClass('fa-plus');
        }
        var id = $(this).data('tr-id');
        $('.tr-child-' + id).toggle();
    });

    $(document).on("click", ".close-img-preview",function(e){
        $(this).hide();
        var parent = $(this).parent();
        parent.find('img').attr('src', urlNoImage);
        parent.find('input[type=hidden]').val('');
        $('div.wrap_link_thumbnail input[name=link_thumbnail]').val('');
    });

    $(document).on("click", "main .main-left .panel-group .panel .panel-title",function(e){
        if($( this ).find( ".fa-chevron-circle-down" ).length){
            $( this ).find( ".fa" ).removeClass('fa-chevron-circle-down');
            $( this ).find( ".fa" ).addClass('fa-chevron-circle-up');
        }else{
            $( this ).find( ".fa" ).removeClass('fa-chevron-circle-up');
            $( this ).find( ".fa" ).addClass('fa-chevron-circle-down');
        }
    });

    // submit the search form when changing the select 2.
    $(document).on("change", "main .main-left .panel-group .panel select", function (e) {
        $("form.auto-submit").submit();
    });

    //Validate max number
    $(document).on("change", "input[type=number]", function (e) {
        var value = parseInt($(this).val());
        value = Math.max(value, 0);

        var max = parseInt($(this).attr('max'));
        
        if(!isNaN(max)){
            $(this).val( Math.min(value, max) );
        }

        $(this).val(value);
    });

    var timeoutKeypress = null;
    $(document).on("keypress", "input.format-number", function(e){
        if(e.which == 13) {
            var value = numeral($(this).val()).format('0,0');
            $(this).val(value);
        }
    });
})();