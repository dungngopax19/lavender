<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\Orders;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Helpers\Common;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\ProductTypes;
use App\Helpers\DateHelper;
use Melihovv\ShoppingCart\Facades\ShoppingCart as Cart;
use App\Models\Products;
use App\Helpers\Invoice as InvoiceHelper;
use App\Helpers\Customer as CustomerHelper;

\App::setLocale('vi');

Route::redirect('/', '/admin/dashboard');

Route::prefix('admin')->name('admin.')->middleware(['auth'])->group(function () {
    Route::resource('dashboard', 'Admin\DashboardController');
    Route::resource('messages', 'Admin\MessageController');
    Route::get('messages-mark-read', 'Admin\MessageController@markRead');
    Route::resource('roles', 'Admin\RoleController');
    Route::resource('users', 'Admin\UserController');
    Route::get('customer-import', 'Admin\CustomerController@import');
    Route::post('customer-submit-import', 'Admin\CustomerController@submitImport');
    Route::get('customer-export', 'Admin\CustomerController@export');
    Route::resource('customers', 'Admin\CustomerController');
    Route::resource('promotions', 'Admin\PromotionsController');
    Route::resource('coupons', 'Admin\CouponsController');
    Route::post('validate-coupons', 'Admin\CouponsController@validateCoupon');
    Route::post('get-info-customer', 'Admin\CustomerController@getInfo');
    Route::resource('partner-deliveries', 'Admin\PartnerDeliveryController');
    Route::resource('branches', 'Admin\BranchController');
    Route::post('change-branch', 'Admin\BranchController@changeBranch');
    Route::resource('surcharges', 'Admin\SurchargeController');
    Route::resource('membership-gifts', 'Admin\MembershipGiftController');
    Route::resource('product-categories', 'Admin\ProductCategoryController');
    Route::resource('products', 'Admin\ProductController');

    Route::get('product-import', 'Admin\ProductController@import');
    Route::post('product-submit-import', 'Admin\ProductController@submitImport');
    Route::get('product-export', 'Admin\ProductController@export');

    Route::get('search-products', 'Admin\ProductController@search');
    Route::get('search-products-cart', 'Admin\ProductController@searchInCart');
    Route::post('list-product-component', 'Admin\ProductController@listComponents');

    Route::post('add-to-cart', 'Admin\CartController@addToCart');
    Route::get('detail-cart', 'Admin\CartController@detailCart');
    Route::post('update-cart', 'Admin\CartController@updateCart');
    Route::post('clear-cart', 'Admin\CartController@clearCart');
    Route::post('update-discount-note', 'Admin\CartController@updateDiscountNote');

    Route::resource('suppliers', 'Admin\SuppliersController');
    Route::resource('activity-log', 'Admin\ActivityLogController');
    Route::resource('stores', 'Admin\StoresController');
    Route::resource('products-transfer', 'Admin\ProductsTransferController');

    Route::post('upload-file', 'Admin\FileController@uploadTmp');
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

    Route::get('provinces/{province_id}/districts/{district_id}', 'Admin\ProvinceController@getDistrictByProvince');

    Route::post('create-orders', 'Admin\OrdersController@create');
    Route::resource('orders', 'Admin\OrdersController');
    Route::get('order-export', 'Admin\OrdersController@export');
    Route::get('order-export-detail', 'Admin\OrdersController@exportDetail');
    Route::get('get-orders', 'Admin\OrdersController@getOrders');
    Route::resource('report', 'Admin\ReportController');
    Route::get('search-customers', 'Admin\CustomerController@search');
    Route::get('get-customer', 'Admin\CustomerController@getCustomer');
    Route::get('update-level-customer', 'Admin\CustomerController@updateLevelCustomer');

    Route::resource('invoices', 'Admin\InvoiceController');
    Route::get('confirm-order', 'Admin\InvoiceController@getConfirmOrder');
    Route::get('transport-order', 'Admin\InvoiceController@getTransportOrder');
    Route::get('invoice-import', 'Admin\InvoiceController@import');
    Route::post('invoices-submit-import', 'Admin\InvoiceController@submitImport');
    Route::get('invoices-export', 'Admin\InvoiceController@export');

    Route::resource('activity-log', 'Admin\ActivityLogController');

    Route::get('dashboard-report01', 'Admin\DashboardController@report01');
    Route::get('dashboard-report02', 'Admin\DashboardController@report02');
    Route::get('dashboard-revenue', 'Admin\DashboardController@revenue');
    Route::get('dashboard-revenue-branches', 'Admin\DashboardController@revenueOfBranches');

    Route::resource('unit-inventory', 'Admin\UnitInventoryController');

    Route::resource('receipts', 'Admin\ReceiptsController');
    Route::get('search-orders', 'Admin\OrdersController@search');

    Route::get('reports/{type}/{concerns}', 'Admin\ReportController@report');
    Route::get('reports/{type}/{concerns}/xlsx', 'Admin\ReportController@export');

    Route::post('get-reports', 'Admin\ReportController@ajaxGetReport');


    Route::get('test-customer-info', 'Admin\CustomerController@getInfo');

});

Route::middleware(['auth'])->group(function () {
    Route::resource('sale', 'SaleController');
    Route::put('update-profile', '\App\Http\Controllers\Auth\ProfileController@index')->name('update-profile');
});

Auth::routes();


use App\Models\OrderDetail;
use Carbon\Carbon;
use App\Helpers\Product as ProductHelper;
use \App\Models\BranchesProducts;

//Functions Test
Route::get('/test', function(){
    $arr = [1,2,3];
    list($id, $name) = $arr;

    dd($name);
});