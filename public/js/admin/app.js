/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
__webpack_require__(6);
module.exports = __webpack_require__(7);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

__webpack_require__(2);
__webpack_require__(3);
__webpack_require__(4);
__webpack_require__(5);

$(document).on('keyup', function (evt) {
    if (evt.keyCode == 27) {
        $('.modal').modal('hide');
    }
});

/***/ }),
/* 2 */
/***/ (function(module, exports) {

var callbacks = $.Callbacks();
(function ($) {

    //Submit Form Ajax (Modal Popup)
    $.fn.submitFormAjax = function (params) {
        var self = this;
        self.attr('disabled', 'disabled');

        var formObj = params.form;
        formObj.find('.form-group').removeClass('has-error');
        formObj.find('.error-msg').html('');

        //Submit ajax
        $.ajax({
            url: formObj.attr('action'),
            type: params.method === undefined ? 'POST' : params.method,
            data: formObj.serialize(),
            dataType: 'JSON',
            error: function error(response) {
                if (params.error) {
                    if (typeof params.error == 'function') {
                        params.error.call(null, response);
                    } else {
                        callbacks.add(eval('document.' + params.error));
                    }
                } else {
                    var errors = response.responseJSON.errors;
                    for (var key in errors) {
                        // Replace "." degit by "_". Edit by DatHQ
                        formObj.find('.fg-' + key.replace('.', '_')).addClass('has-error').find('.error-msg').html(errors[key]);
                    }

                    //Style Form Tabs
                    if (formObj.parent().hasClass('form-tabs')) {
                        formObj.find('.nav-tabs li').each(function (e) {
                            var tabTarget = $(this).find('a').attr('href');
                            if ($(tabTarget).find('.has-error').length > 0) {
                                console.log(tabTarget);
                                console.log(e);
                                $('.nav-tabs li:eq(' + e + ') a').tab('show');
                                return false;
                            }
                        });
                    }
                }
            },
            success: function success(response) {

                if (response.st == 500) {
                    formObj.find('.msg-form').html(response.msg);
                    return false;
                }

                if (formObj.find(' .btn-close-modal').length > 0) {
                    formObj.find('.btn-close-modal').trigger('click');
                }

                if (params.success) {
                    if (typeof params.success == 'function') {
                        params.success.call(null, response);
                    } else {
                        callbacks.add(eval('document.' + params.success));
                    }

                    return false;
                }

                if (self.data('url-redirect')) {
                    location.href = self.data('url-redirect');
                } else {
                    location.reload();
                }
            },
            complete: function complete(response) {
                self.removeAttr('disabled');
                callbacks.fire(response);
            }
        });
    };

    //Submit Ajax
    $.fn.submitDataAjax = function (params) {

        var self = this;
        self.attr('disabled', 'disabled');

        //Submit ajax
        $.ajax({
            url: params.url,
            type: params.method,
            data: params.data,
            dataType: 'JSON',
            error: function error(response) {
                if (params.error) {
                    if (typeof params.error == 'function') {
                        params.error.call(null, response);
                    } else {
                        callbacks.add(eval('document.' + params.error));
                    }
                }
            },
            success: function success(response) {
                if (params.success) {
                    if (typeof params.success == 'function') {
                        params.success.call(null, response);
                    } else {
                        callbacks.add(eval('document.' + params.success));
                    }
                } else {
                    location.reload();
                }
            },
            complete: function complete(response) {
                self.removeAttr('disabled');
                callbacks.fire(response);
            }
        });
    };
})(jQuery);

document.getNumber = function (str) {
    if (!str) {
        return 0;
    }

    str = str.trim();
    number = parseInt(str.replace(/đ|,/g, ""));
    return isNaN(number) ? 0 : number;
};

/***/ }),
/* 3 */
/***/ (function(module, exports) {

(function () {

    //Init
    $('[data-toggle="tooltip"]').tooltip();
    $('.select2').select2();
    $('.select2-disabled').select2({ disabled: true });

    $('.daterange-picker').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });

    $('.daterange-picker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' -> ' + picker.endDate.format('DD-MM-YYYY'));

        if ($(this).hasClass('auto-submit-item')) {
            $("form.auto-submit").submit();
        }
    });

    $('.daterange-picker').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    //Click element tr table
    $(document).on("click", "table.list-item tr", function (e) {
        $(this).find('.btn-edit').trigger('click');
    });

    $('.singleDatePicker').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        }
    });

    //Change branch
    $(document).on("click", ".change-branch", function (e) {
        var id = $(this).data('id');
        var name = $(this).data('name');

        if (confirm('Bạn có muốn chuyển sang chi nhánh ' + name + '?')) {
            $(this).submitDataAjax({
                'url': site_url + 'change-branch',
                'method': 'POST',
                'data': {
                    '_token': CSRF_TOKEN,
                    'id': id
                },
                'success': function success(res) {
                    if (res.st == 200) {
                        location.reload();
                    } else {
                        alert(res.msg);
                    }
                }
            });
        }
    });

    //Submit form ajax
    $(document).on("click", ".btn-submit", function (e) {
        $(this).submitFormAjax({
            'form': $($(this).data('form-target')),
            'method': $(this).data('form-method'),
            'error': $(this).data('callback-error'),
            'success': $(this).data('callback-success'),
            'autoCloseModal': $(this).data('auto-close-modal')
        });
    });

    //Auto submit form
    $(".auto-submit-item").change(function () {
        $("form.auto-submit").submit();
    });

    //Submit form ajax
    $(document).on("click", ".ajax-modal", function (e) {
        e.stopPropagation();

        var modal = $(this).data('target');

        $(this).submitDataAjax({
            'url': site_url + $(this).data('action'),
            'method': 'GET',
            'data': $(this).data('params'),
            'success': function success(res) {
                $('.modal').modal('hide');
                $(modal).find('.modal-content').html(res.html);
                $('.select2').select2();

                $('.select2-disabled').select2({
                    disabled: true
                });

                setTimeout(function () {
                    $(modal).modal();
                }, 500);
            }
        });
    });

    //Confirm delete
    $('.delete-item').click(function (event) {
        event.stopPropagation();

        if (confirm("Bạn có thật sự muốn xoá item này không?")) {
            var id = $(this).data('id');

            $(this).submitDataAjax({
                'url': site_url + $(this).data('action'),
                'method': 'POST',
                'data': { _method: 'DELETE', _token: CSRF_TOKEN },
                'success': function success(res) {
                    location.reload();
                }
            });
        }
    });

    $('.table-tree-view .toogle').click(function () {
        var objI = $(this).find('i.fas');
        if (objI.hasClass('fa-plus')) {
            $('.bg-white').hide();
            $('.bg-gray .fas').removeClass('fa-minus').addClass('fa-plus');

            objI.removeClass('fa-plus').addClass('fa-minus');
        } else {
            objI.removeClass('fa-minus').addClass('fa-plus');
        }
        var id = $(this).data('tr-id');
        $('.tr-child-' + id).toggle();
    });

    $(document).on("click", ".close-img-preview", function (e) {
        $(this).hide();
        var parent = $(this).parent();
        parent.find('img').attr('src', urlNoImage);
        parent.find('input[type=hidden]').val('');
        $('div.wrap_link_thumbnail input[name=link_thumbnail]').val('');
    });

    $(document).on("click", "main .main-left .panel-group .panel .panel-title", function (e) {
        if ($(this).find(".fa-chevron-circle-down").length) {
            $(this).find(".fa").removeClass('fa-chevron-circle-down');
            $(this).find(".fa").addClass('fa-chevron-circle-up');
        } else {
            $(this).find(".fa").removeClass('fa-chevron-circle-up');
            $(this).find(".fa").addClass('fa-chevron-circle-down');
        }
    });

    // submit the search form when changing the select 2.
    $(document).on("change", "main .main-left .panel-group .panel select", function (e) {
        $("form.auto-submit").submit();
    });

    //Validate max number
    $(document).on("change", "input[type=number]", function (e) {
        var value = parseInt($(this).val());
        value = Math.max(value, 0);

        var max = parseInt($(this).attr('max'));

        if (!isNaN(max)) {
            $(this).val(Math.min(value, max));
        }

        $(this).val(value);
    });

    var timeoutKeypress = null;
    $(document).on("keypress", "input.format-number", function (e) {
        if (e.which == 13) {
            var value = numeral($(this).val()).format('0,0');
            $(this).val(value);
        }
    });
})();

/***/ }),
/* 4 */
/***/ (function(module, exports) {

(function () {

    // Update SL ước tính(bộ)
    $(document).on("change", "#form-edit-product select[name=cloth_template], #form-edit-product input[name=meter_number]", function (e) {
        var cloth_template = $('#form-edit-product select[name=cloth_template]').val();
        var meter_number = parseInt($('#form-edit-product input[name=meter_number]').val());
        var number_of_sets = 0;

        if (meter_number >= 0 && !isNaN(meter_number)) {
            if (cloth_template == '1.6') {
                number_of_sets = Math.floor(meter_number / 14);
            } else if (cloth_template == '2.5') {
                number_of_sets = Math.floor(meter_number / 11);
            }
            number_of_sets = isNaN(number_of_sets) ? 0 : number_of_sets;
        }

        $('#form-edit-product input[name=number_of_sets]').val(number_of_sets);
    });

    //Change product type
    $(document).on("change", "#form-edit-product select[name=product_types_id]", function (e) {
        var id = $(this).val();

        if (id == 1) {
            //Combo - Đóng gói
            $('li.tab-component').show();
            $('.stock-product').hide();

            var inventoryCombo = 0;
            if ($('input[name=inventory_list_comps]').length > 0) {
                var inventoryCombo = $('input[name=inventory_list_comps]').val();
            }
            $('#inventoryCombo').html(inventoryCombo);
        } else {
            $('li.tab-component').hide();
            $('.stock-product').show();

            var inventoryStock = 0;
            $('#modal-edit-product .inventory-stock').each(function (e) {
                inventoryStock += parseInt($(this).val());
            });
            $('#inventoryCombo').html(inventoryStock);
        }
    });

    //Submit form save product
    $(document).on("click", ".btn-submit-edit-product", function (e) {

        var productType = $('#modal-edit-product select[name=product_types_id]').val();

        if (productType == 1) {
            //Combo
            var inventoryStock = 0;
            $('#modal-edit-product .inventory-stock').each(function (e) {
                inventoryStock += parseInt($(this).val());
            });

            var inventoryComps = parseInt($('input[name=inventory_list_comps]').val()) || 0;

            if (inventoryStock != inventoryComps) {
                $('.tab-inventory a').tab('show');
                alert('Dữ liệu tồn kho không chính xác. Bạn vui lòng kiểm tra lại số liệu tồn kho của các chi nhánh');
                return false;
            }
        }

        //Kiểm tra 1 sản phẩm phải thuộc tối thiểu 1 chi nhánh
        /*var chkInBranch = 0;
        $('#modal-edit-product .in-branch-stock').each(function(e){
            var id = parseInt($(this).val());
            if( id > 0 ){
                chkInBranch = id;
            }
        });
        if(chkInBranch < 1){
            alert('Sản phẩm này phải thuộc tối thiểu 1 chi nhánh');
            return false;
        }*/

        $(this).submitFormAjax({
            'form': $($(this).data('form-target')),
            'method': $(this).data('form-method'),
            'error': $(this).data('callback-error'),
            'success': $(this).data('callback-success'),
            'autoCloseModal': $(this).data('auto-close-modal')
        });
    });

    $(document).on("change", "#modal-edit-product .in-branch-stock", function (e) {
        var id = $(this).val();

        var cardBody = $(this).parents('.card-body');
        if (id == 1) {
            cardBody.find('input.inventory-stock').removeAttr('readonly', 'readonly');
            cardBody.find('input.min-inventory-stock').removeAttr('readonly', 'readonly');
        } else {
            cardBody.find('input.inventory-stock').attr('readonly', 'readonly').val(0);
            cardBody.find('input.min-inventory-stock').attr('readonly', 'readonly');
        }
    });

    $(document).on("click", ".btn_link_thumbnail", function (e) {
        $('.wrap_link_thumbnail').toggle();
    });
})();

/***/ }),
/* 5 */
/***/ (function(module, exports) {

(function () {

    //Add item product to cart
    $(document).on("click", ".add-to-cart", function (e) {
        event.stopPropagation();
        var id = parseInt($(this).data('id'));
        var type = $(this).data('type');
        updateItemCart(id, 1, null, type);
    });

    //Remove item product from cart
    $(document).on("click", ".remove-item-cart", function (e) {
        var uniqueId = $(this).data('uniqueid');
        var id = parseInt($(this).data('id'));
        var type = $(this).data('type');
        var typeTxt = type == 'orders' ? 'phiếu đặt hàng' : 'hoá đơn';

        if (confirm('Bạn có thật sự muốn xoá sản phẩm này khỏi ' + typeTxt + ' ?')) {
            updateItemCart(id, 0, uniqueId, type);
        }

        return false;
    });

    //Remove item product from cart in modal create PDH/HOA DON
    $(document).on("click", ".remove-item-cart-modal", function (e) {
        var objTrParent = $(this).parent().parent();
        updateItemCartModal(objTrParent, true);
        return false;
    });

    //Clear all cart
    $(document).on("click", ".btn-clear-cart", function (e) {
        var type = $(this).data('type');
        var cartNumber = $(this).data('cartnumber');
        var typeTxt = type == 'orders' ? 'phiếu đặt hàng' : 'hoá đơn';

        if (confirm('Bạn có thật sự muốn xoá ' + typeTxt + ' không?')) {
            $(this).submitDataAjax({
                'url': site_url + 'clear-cart',
                'method': 'POST',
                'data': { _token: CSRF_TOKEN, type: type, cartNumber: cartNumber },
                'success': function success(res) {

                    $.notify({
                        message: typeTxt + ' của bạn đã được xoá thành công.'
                    }, {
                        type: 'success',
                        delay: 1000,
                        placement: {
                            from: "bottom"
                        }
                    });

                    if (type == 'orders') {
                        $('.wrap-hover-orders').html(res.htmlCart);
                    } else {
                        $('.wrap-hover-invoices').html(res.htmlCart);
                    }
                }
            });
        }
    });

    //Cart in modal detail cart
    $(document).on("keypress", ".modal-cart .qty-item-cart, .modal-cart .discount-item-cart input[name=discount], .price-item-cart", function (e) {
        if (e.which == 13) {
            var objTrParent = $(this).parent().parent();
            updateItemCartModal(objTrParent);
            return false;
        }
    });

    //Change button discount unit
    $(document).on("click", ".btn-discount", function () {
        var objParent = $(this).parent();
        objParent.find('.btn-discount').removeClass('btn-info');
        $(this).addClass('btn-info');

        var discount_unit = $(this).data('unit');
        objParent.find('input[name=discount_unit]').val(discount_unit);

        if (objParent.hasClass('discount-total-cart')) {
            calculatePayment();
        } else {
            var objTrParent = $(this).parent().parent();
            updateItemCartModal(objTrParent);
        }
    });

    //Nếu trạng thái giao hàng là "giao hàng công" "đang giao hàng" hoặc "hoàn thành" thì k chọn được "Ưu tiên"
    $(document).on("change", "select[name=shipment_status_id]", function () {
        var shipment_status_id = parseInt($(this).val());
        var ids = [2, 4, 7];

        if (ids.indexOf(shipment_status_id) > -1) {
            $('select[name=highlight]').val(0);
            $('select[name=highlight]').select2();
        }
    });

    //Change discount price total cart / deposit price
    $(document).on("keypress", ".discount-total-cart input[name=discount], input[name=deposit], input[name=ship]", function (e) {
        if (e.which == 13) {
            var value = numeral($(this).val()).format('0,0');
            $(this).val(value);
            calculatePayment();

            // set value for print order
            $('.name-discount').html(value);
        }
    });

    //Change discount coupon
    $(document).on("keypress", "input[name=coupon]", function (e) {
        if (e.which == 13) {
            var coupon = $(this).val();

            $(this).submitDataAjax({
                'url': site_url + 'validate-coupons',
                'method': 'POST',
                'data': {
                    _token: CSRF_TOKEN,
                    coupon: coupon,
                    membership_id: $('input[name=membership_id]').val()
                },
                'success': function success(res) {
                    $('.msg-coupon').html(res.msg);
                    $('input[name=discount_coupon]').val(res.discount_coupon);
                    calculatePayment();
                }
            });
        }
    });

    //Change option discount gift
    $(document).on("change", "select#option-gift-discount", function (e) {

        var opt = $(this).val();
        if (opt == 2 && $('ul.gift-discount').length > 0) {
            $('ul.gift-discount').show();
            $('#discountGiftPrice').val(0);
            $('#discountGiftUnit').val('vnd');
        } else {
            $('ul.gift-discount').hide();
            $('#discountGiftPrice').val($(this).data('discount-gift'));
            $('#discountGiftUnit').val($(this).data('discount-gift-unit'));
        }

        calculatePayment();
    });

    //Submit create orders / invoices
    $(document).on("click", ".btn-submit-orders", function (e) {

        //Tao va ket thuc don hang
        if ($(this).hasClass('btnCreateEndOrders')) {
            //Nếu đã chọn thời gian giao hàng thì không được submit button "Tao va ket thuc don hang"

            var delivery_time = $('input[name=delivery_time]').val();
            var againPay = $('input[name=againPay]').val();

            if (delivery_time) {
                alert('Bạn không thể tạo và kết thúc đơn hàng khi đã chọn thời gian giao hàng');
                return false;
            } else if (againPay != 0) {
                alert('Bạn không thể tạo và kết thúc đơn hàng khi khách hàng chưa thanh toán hết tiền hàng');
                return false;
            }

            $('input[name=create_end_invoices]').val(1);
        } else {
            $('input[name=create_end_invoices]').val(0);
        }

        //Tao don hang tu phieu dat hang
        if ($(this).hasClass('btnCreateOrdersFromDrafts')) {
            $('input[name=is_invoices]').val(1);
        }

        $(this).submitFormAjax({
            'form': $($(this).data('form-target')),
            'method': $(this).data('form-method'),
            'error': $(this).data('callback-error'),
            'success': function success(res) {

                if (res.htmlPrint) {
                    $('.wrap-print').html(res.htmlPrint);
                }

                if (res.status == 500) {
                    $.notify({
                        title: '<div><strong>' + res.title + '</strong></div>',
                        message: res.msg
                    }, {
                        type: res.type,
                        delay: 5000,
                        placement: {
                            from: "bottom"
                        }
                    });
                } else {
                    alert(res.data);
                    $('.ajax-print').trigger('click');
                    location.reload();
                }
            }
        });
    });

    //Update discount note
    $(document).on("keypress", ".update-discount-note", function (e) {

        if (e.which == 13) {
            $(this).submitDataAjax({
                'url': site_url + 'update-discount-note',
                'method': 'POST',
                'data': {
                    _token: CSRF_TOKEN,
                    orders_id: $(this).data('orders-id'),
                    products_id: $(this).data('products-id'),
                    discountNote: $(this).val(),
                    type: $(this).parent().parent().data('type'),
                    uniqueId: $(this).parent().parent().data('uniqueid'),
                    cartNumber: $('.cart-number.active').data('id')
                },
                'success': function success(res) {
                    $.notify({
                        message: res.msg
                    }, {
                        type: res.type,
                        delay: 1000,
                        placement: {
                            from: "bottom"
                        }
                    });
                }
            });
        }
    });
})();

//Functions utility
var timeout = null;
function updateItemCart(id) {
    var qty = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
    var uniqueId = arguments[2];
    var type = arguments[3];

    clearTimeout(timeout);

    $(this).submitDataAjax({
        'url': site_url + 'add-to-cart',
        'method': 'POST',
        'data': {
            _token: CSRF_TOKEN,
            id: id,
            qty: qty,
            uniqueId: uniqueId,
            type: type,
            cartNumber: $('.cart-number.active').data('id')
        },
        'success': function success(res) {
            $.notify({
                title: '<div><strong>' + res.title + '</strong></div>',
                message: res.msg
            }, {
                type: res.type,
                delay: 1000,
                placement: {
                    from: "bottom"
                }
            });

            if (type == 'orders') {
                $('.wrap-hover-orders').html(res.htmlCart);

                if (res.type == 'success') {
                    $('.box-dropdown-orders').css('display', 'block');
                    timeout = setTimeout(function () {
                        $('.box-dropdown-orders').css('display', '');
                    }, 3000);
                }
            } else {
                $('.wrap-hover-invoices').html(res.htmlCart);

                if (res.type == 'success') {
                    $('.box-dropdown-invoices').css('display', 'block');
                    timeout = setTimeout(function () {
                        $('.box-dropdown-invoices').css('display', '');
                    }, 3000);
                }
            }

            if ($(window).scrollTop() > 500) {
                if ($('.box-dropdown-orders').find('.item-cart').length > 0) {
                    $('.box-dropdown-orders').addClass('position-fixed-orders');
                }

                if ($('.box-dropdown-invoices').find('.item-cart').length > 0) {
                    $('.box-dropdown-invoices').addClass('position-fixed-invoices');
                }
            }
        }
    });
}

function updateItemCartModal(objTrParent) {
    var remove = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;


    if (remove) {
        var qty = 0;
    } else {
        var qty = numeral(objTrParent.find('.qty-item-cart').val()).format('0');
    }

    var customPrice = numeral(objTrParent.find('.price-item-cart').val()).format('0');
    var discount = numeral(objTrParent.find('input[name=discount]').val()).format('0');

    var discountUnit = objTrParent.find('.btn-discount.btn-info').val();

    var type = objTrParent.data('type');
    var id = objTrParent.data('id');
    var uniqueid = objTrParent.data('uniqueid');
    var inventory = objTrParent.data('inventory');

    var typeTxt = type == 'orders' ? 'phiếu đặt hàng' : 'hoá đơn';

    if (qty == 0) {
        if (confirm('Bạn có thật sự muốn xoá sản phẩm này trong ' + typeTxt + ' ?')) {
            updateModalCart(id, qty, uniqueid, customPrice, discount, discountUnit, type, typeTxt);
        }
    } else {

        //Tạm thời sẽ không giới hạn số lượng sản phẩm đặt hàng
        if (type == 'invoices') {
            if (inventory < qty) {
                if (inventory <= 0) {
                    alert('Hiện tại sản phẩm này đã hết hàng');
                } else {
                    alert('Bạn chỉ có thể nhập tối đa ' + inventory + ' sản phẩm');
                }

                qty = inventory;
            }
        }

        $(this).val(qty);

        if (confirm('Bạn có thật sự muốn cập nhật sản phẩm này trong ' + typeTxt + ' ?')) {
            updateModalCart(id, qty, uniqueid, customPrice, discount, discountUnit, type, typeTxt);
        }
    }
}

function updateModalCart(id, qty, uniqueId, customPrice, discount, discountUnit, type, typeTxt) {

    $(this).submitDataAjax({
        'url': site_url + 'add-to-cart',
        'method': 'POST',
        'data': {
            _token: CSRF_TOKEN,
            id: id,
            qty: qty,
            uniqueId: uniqueId,
            customPrice: customPrice,
            discount: discount,
            discountUnit: discountUnit,
            type: type,
            updateQty: true,
            cartNumber: $('.cart-number.active').data('id')
        },
        'success': function success(res) {

            $.notify({
                title: '<div><strong>' + res.title + '</strong></div>',
                message: res.msg
            }, {
                type: res.type,
                delay: 1000,
                placement: {
                    from: "bottom"
                }
            });

            if (res.type == 'success') {
                if (type == 'orders') {
                    $('.wrap-hover-orders').html(res.htmlCart);
                } else {
                    $('.wrap-hover-invoices').html(res.htmlCart);
                }

                $('.html-cart-modal').html(res.htmlCartModal);
                $('.html-cart-print').html(res.htmlCartPrint);

                $('.total-price-cart').html(res.totalPrice);
                calculatePayment();
                searchAutoProduct();
            }
        }
    });
}

function calculatePayment() {
    var discountTotal = 0;

    var totalPrice = document.getNumber($('.total-price-cart').html());

    var discount = document.getNumber($('.discount-total-cart input[name=discount]').val());
    var deposit = document.getNumber($('input[name=deposit]').val());
    var ship = document.getNumber($('input[name=ship]').val());
    var discountCoupon = document.getNumber($('input[name=discount_coupon]').val());
    var unitDiscount = $('.discount-total-cart .btn-discount.btn-info').val();

    if (unitDiscount == 'vnd') {
        var needPay = totalPrice + ship - discount - discountCoupon;
    } else {
        var needPay = totalPrice + ship - totalPrice * discount / 100 - discountCoupon;
    }

    var againPay = needPay - deposit;

    //Check option discount gift
    var optDiscountGift = $("select#option-gift-discount").val();
    if (optDiscountGift == 1) {
        var discountGiftPrice = parseInt($('#discountGiftPrice').val());
        var discountGiftUnit = $('#discountGiftUnit').val();

        if (discountGiftUnit == 'vnd') {
            againPay = againPay - discountGiftPrice;
            needPay = needPay - discountGiftPrice;
        } else {
            againPay = againPay - againPay * discountGiftPrice / 100;
            needPay = needPay - needPay * discountGiftPrice / 100;
        }
    }

    againPay = againPay < 0 ? 0 : againPay;

    $('input[name=needpay]').val(numeral(needPay).format('0,0'));
    $('input[name=againPay]').val(numeral(againPay).format('0,0'));

    discountTotal = totalPrice - needPay;

    //Set value prices for printer
    $('.print-ship').html($('input[name=ship]').val());
    $('.print-price').html($('.total-price-cart').html());
    $('.print-discount').html(numeral(discountTotal).format('0,0'));
    $('.print-deposit').html($('input[name=deposit]').val());

    $('.print-againPay').html($('input[name=againPay]').val());

    if (totalPrice == 0) {
        $('.btn-submit-orders').hide();
    } else {
        if (againPay < 0) {
            alert('Bạn vui lòng kiểm tra lại các khoản chi phí');
            $('.btn-submit-orders').hide();
            $('input[name=needpay], input[name=againPay]').val(0);
        } else {
            $('.btn-submit-orders').show();
        }
    }
}

window.calculatePaymentPrice = calculatePayment;

function searchAutoProduct() {
    $(".search-product-component").select2({
        ajax: {
            url: 'search-products-cart', // 'search-products',
            dataType: 'json',
            delay: 250,
            data: function data(params) {
                var query = {
                    q: params.term
                };
                return query;
            },
            processResults: function processResults(res) {
                return {
                    results: res.records
                };
            }
        },
        templateResult: function templateResult($item) {
            var $item = $('<div>' + $item.code + '</div>');
            return $item;
        }
    }).on("select2:select", function (e) {
        var id = parseInt(e.params.data.id);
        addItemProductToCart(id);
    });
}
window.searchAutoProduct = searchAutoProduct;

/***/ }),
/* 6 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 7 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);