<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
		$faker = Faker\Factory::create('ja_JP');
		for ($i=1; $i< 100; $i++) {
			/*
			DB::table('orders')->insert([
				'id' => $i+1,
				'code' => 'ORDER0000'.$i,
				'is_invoices' => 0,
				'users_id' => 1,
				'customers_id' => $faker->numberBetween(1, 4),
				'branches_id' => $faker->numberBetween(6, 9),
				'price' => $faker->numberBetween(1, 25)*100000,
				'channel' => $faker->numberBetween(1, 3),
				'discount' => 10000,
				'discount_unit' => 'vnd',
				'deposit' => $faker->numberBetween(1, 25),
				'orders_status_id' => 1,
				'interval' => 1,
				'create_user' => 1,
				'update_user' => 1,
				'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
				//'updated_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
				//'deleted_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
				//'delete_user' => 1,
			]);
			*/

			// /*
			DB::table('messages')->insert([
				'id' => $i,
				'user_send_id' => 2,
				'user_receive_id' => 1,
				'content' => $i.' Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
				'status' => 1,
				'create_user' => 'admin@gmail.com',
				'update_user' => 'admin@gmail.com',
                'created_at' => date('YmdHis'),
                'updated_at' => date('YmdHis'),
			]);
			// */

		}
    }
}
