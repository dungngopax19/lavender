<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branches_id');
            $table->unsignedInteger('users_id');
            $table->timestamps();

            $table->foreign('branches_id')
                ->references('id')
                ->on('branches')
                ->onDelete('cascade');

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches_users');
    }
}
