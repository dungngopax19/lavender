<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmpClothsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_cloths', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ma_sp')->nullable();
            $table->string('kho_vai')->nullable();
            $table->string('hinh_anh')->nullable();
            $table->string('sl_met_vai')->nullable();
            $table->string('vo_chan')->nullable();
            $table->string('mem_chan')->nullable();
            $table->string('vo_goi_nam')->nullable();
            $table->string('drap')->nullable();
            $table->string('custom')->nullable();
            $table->string('ngay_cap_nhat')->nullable();

            $table->string('create_user')->nullable();
            $table->string('update_user')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_cloths');
    }
}
