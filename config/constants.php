<?php
return [
    'action_activity_log' => [
        'xác thực', 'tạo', 'cập nhật', 'xoá'
    ],
    'confirmed' => [
        'Chưa xác thực', 'Không xác thực', 'Đã xác thực'
    ],
    'order_status' => [ 'Chưa giao hàng', 'Đang giao hàng', 'Giao hàng thành công', 'Đang chuyển hoàn', 'Đã chuyển hoàn', 'Đã huỷ'],
    'col_txt' => ['Chi nhánh','Mã hóa đơn','Phí giao hàng (trả ĐT)','Thời gian','Thời gian tạo','Mã đặt hàng','Mã khách hàng','Tên khách hàng','Số điện thoại','Địa chỉ (Khách hàng)','Khu vực (Khách hàng)','Phường/Xã (Khách hàng)','Người bán','Kênh bán','Người tạo','Đối tác giao hàng','Người nhận','Điện thoại','Địa chỉ (Người nhận)','Khu vực (Người nhận)','Phường/Xã (Người nhận)','Dịch vụ','Trọng lượng','Dài','Rộng','Cao','Ghi chú','Tổng tiền hàng','Giảm giá hóa đơn','Thu khác','Khách cần trả','Khách đã trả','Tiền mặt','Thẻ','Chuyển khoản','Điểm','Voucher','Mã voucher','Thời gian giao hàng','Trạng thái','Trạng thái giao hàng','Mã hàng','Tên hàng','ĐVT','Ghi chú hàng hóa','Số lượng','Đơn giá','Giảm giá %','Giảm giá','Giá bán','Thành tiền'],
    'roles' => [
        'nhan-vien-thu-ngan' => 'Nhân viên thu ngân',
        'nhan-vien-ban-hang' => 'Nhân viên bán hàng',
        'nhan-vien-kho' => 'Nhân viên kho',
        'quan-tri-chi-nhanh' => 'Quản trị chi nhánh'
    ],
    'activity_status' => ['Tất cả', 'Đang hoạt động', 'Ngừng hoạt động'],
    'inventory_status' => ['Tất cả', 'Dưới định mức tồn', 'Còn hàng trong kho', 'Hết hàng trong kho'],
    'direct_selling' => ['Tất cả', 'Được bán trực tiếp', 'Không được bán trực tiếp'],
    'roles-personal-company' => ['Tất cả', 'Cá nhân', 'Công ty'],
    'genders'=> ['Tất cả', 'Nam', 'Nữ'],
    'membership'=> [
        'level-1' => 'Cấp 1',
        'level-2' => 'Cấp 2',
        'level-3' => 'Cấp 3',
        'level-4' => 'Cấp 4',
        'level-5' => 'Cấp 5',
        'level-6' => 'Cấp 6',
        'titan' => 'Cấp titan',
        'diamond' => 'Cấp kim cương',
        'gold' => 'Cấp vàng',
        'silver' => 'Cấp bạc',
    ],
    'channels' =>[
        1 => 'Bán trực tiếp',
        2 => 'Facebook',
        3 => 'Khác'
    ],
    'payment_method' => [
        1 => 'Tiền mặt',
        2 => 'Thẻ',
        3 => 'Chuyển khoản'
    ]
];
