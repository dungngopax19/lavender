<?php
return [
    'systems' =>[
        'Hệ thống' => [
            'Nhóm người dùng' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá', 'Owner'],
            'Người dùng' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá', 'Owner', 'Phân quyền'],
            'Chi nhánh' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá', 'Owner'],
            'Thu khác' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá', 'Owner'],
            'Khuyến mại' => ['Xem DS', 'Cập nhật'],
            'Lịch sử thao tác',
            'Tổng quan'
        ]
    ],
    'goods' => [
        'Hàng hoá' => [
            'Danh mục sản phẩm' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá', 'Import dữ liệu', 'Export dữ liệu'],
            'Sản phẩm' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá', 'Xuất file'],
            'Kho hàng' => ['Xem DS', 'Cập nhật'],
            'Điều chuyển sản phẩm' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xác thực'],
        ]
    ],
    'partners' => [
        'Đối tác' => [
            'Khách hàng' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá', 'Xuất file'],
            'Nhà cung cấp' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá', 'Xuất file'],
            'Đối tác giao hàng' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá', 'Xuất file'],
        ]
    ],
    'deals' => [
        'Giao dịch' => [
            'Phiếu đặt hàng' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá', 'Tạo hoá đơn'],
            'Hoá đơn' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá', 'Xem tồn kho', 'Xác nhận đơn hàng', 'Xác nhận hoàn thành đơn hàng'],
            'Phiếu thu' => ['Xem DS', 'Thêm mới', 'Cập nhật', 'Xoá'],
            'Vận chuyển' => ['Xem DS', 'Cập nhật'],
            'Xem tất cả các giao dịch'
        ]
    ]
];
