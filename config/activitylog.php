<?php

return [

    /*
     * If set to false, no activities will be saved to the database.
     */
    'enabled' => env('ACTIVITY_LOGGER_ENABLED', true),

    /*
     * When the clean-command is executed, all recording activities older than
     * the number of days specified here will be deleted.
     */
    'delete_records_older_than_days' => 365,

    /*
     * If no log name is passed to the activity() helper
     * we use this default log name.
     */
    'default_log_name' => 'default',

    /*
     * You can specify an auth driver here that gets user models.
     * If this is null we'll use the default Laravel auth driver.
     */
    'default_auth_driver' => null,

    /*
     * If set to true, the subject returns soft deleted models.
     */
    'subject_returns_soft_deleted_models' => false,

    /*
     * This model will be used to log activity.
     * It should be implements the Spatie\Activitylog\Contracts\Activity interface
     * and extend Illuminate\Database\Eloquent\Model.
     */
    'activity_model' => \Spatie\Activitylog\Models\Activity::class,

    /*
     * This is the name of the table that will be created by the migration and
     * used by the Activity model shipped with this package.
     */
    'table_name' => 'activity_log',

    'actions' => [
        'created' => 'Thêm Mới',
        'updated' => 'Cập Nhật'
    ],
    'models' => [
        'App\Models\Branches' => 'Chi nhánh',
        'App\Models\Products' => 'Sản phẩm',
        'App\Models\Attributes' => 'Thuộc tính',
        'App\Models\AttributesProducts' => 'Thuộc tính sản phẩm',
        'App\Models\BranchesProducts' => 'Sản phẩm tại chi nhánh',
        'App\Models\BranchesSurcharges' => 'Phí thu khác theo chi nhánh',
        'App\Models\BranchesSurcharges' => 'Phí thu khác theo chi nhánh',
        'App\Models\BranchesUsers' => 'Nhân viên tại chi nhánh',
        'App\Models\Customers' => 'Khách hàng',
        'App\Models\CustomerTypes' => 'Loại khách hàng',
        'App\Models\MembershipGifts' => 'Quà tặng - Thành viên',
        'App\Models\Memberships' => 'Thành viên',
        'App\Models\OrderDetail' => 'Chi tiết đơn hàng',
        'App\Models\Orders' => 'Đơn hàng',
        'App\Models\OrderStatus' => 'Trạng thái',
        'App\Models\PartnerDeliveries' => 'Đối tác giao hàng',
        'App\Models\ProductCategories' => 'Danh mục sản phẩm',
        'App\Models\Products' => 'Sản phẩm',
        'App\Models\ProductsComponents' => 'Sản phẩm thành phần',
        'App\Models\ProductTypes' => 'Loại sản phẩm',
        'App\Models\Provinces' => 'Tỉnh thành',
        'App\Models\Roles' => 'Nhóm người dùng',
        'App\User' => 'Người dùng',
        'App\Models\ShipmentDetails' => 'Thông tin giao hàng',
        'App\Models\ShipmentStatus' => 'Trạng thái giao hàng',
        'App\Models\Suppliers' => 'Nhà cung cấp',
        'App\Models\Surcharges' => 'Phụ phí',
        'App\Models\TypeServices' => 'Loại dịch vụ',
        'App\Models\Receipts'=> 'Loại thu',
    ],
    'fields' => [
        'code' => 'Mã code',
        'name' => 'Tên',
        'address' => 'Địa chỉ',
        'phone' => 'Số điện thoại',
        'activity_status' => 'Trạng thái',
        'create_user' => 'User đã tạo',
        'update_user' => 'User đã cập nhật',
        'product_types_id' => 'Loại sản phẩm',
        'product_categories_id' => 'Danh mục sản phẩm',
        'thumbnail' => 'Hình ảnh',
        'direct_selling' => 'Bán trực tiếp',
        'price_sell' => 'Giá bán',
        'price' => 'Giá vốn',
        'cloth_template' => 'Mẫu vải',
        'number_of_sets' => 'Số bộ',
        'meter_number' => 'Số mét vải',
        'warning_out_of_stock' => 'Cảnh báo hết hàng',
        'inventory_number' => 'Số lượng tồn kho',
        'description' => 'Mô tả',
        'sample_notes' => 'Mẫu ghi chú',
        'is_gift' => 'Sản phẩm quà tặng',
        'weight' => 'Trọng lượng',
        'branches_id' => 'Chi nhánh',
        'products_id' => 'Sản phẩm',
        'inventory' => 'Tồn kho',
        'min_inventory' => 'Tồn kho tối thiểu',
        'attributes_id' => 'Thuộc tính',
        'verified' => 'Kiểm duyệt đơn hàng',
        'shipment_status_id' => 'Trạng thái giao hàng',
        'orders_status_id' => 'Trạng thái đơn hàng',
    ]
];
